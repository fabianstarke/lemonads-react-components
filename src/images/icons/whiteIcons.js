import React from 'react'

export const InformationWhite = (

        <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>Interface-Essential / Alerts / information-circle</title>
        <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
            <g id="Notification-Valid" transform="translate(-20.000000, -18.000000)" stroke="#FFFFFF">
                <g id="Warning-info-Copy-3">
                    <g id="Interface-Essential-/-Alerts-/-information-circle-White" transform="translate(20.000000, 18.000000)">
                        <g id="Group" transform="translate(0.583333, 0.583333)">
                            <g id="information-circle" strokeWidth="0.583333333">
                                <circle id="Oval" cx="6.41666667" cy="6.41725" r="6.41666667"></circle>
                                <path d="M7.875,9.33625 L7,9.33625 C6.6778339,9.33625 6.41666667,9.07508277 6.41666667,8.75291667 L6.41666667,4.96125 C6.41666667,4.80016695 6.28608305,4.66958333 6.125,4.66958333 L5.25,4.66958333" id="Shape"></path>
                                <path d="M6.26791667,3.20833333 C6.18737514,3.20833333 6.12208333,3.27362514 6.12208333,3.35416667 C6.12208333,3.43470819 6.18737514,3.5 6.26791667,3.5 C6.34845819,3.5 6.41375,3.43470819 6.41375,3.35416667 C6.41375,3.27362514 6.34845819,3.20833333 6.26791667,3.20833333" id="Shape"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const CloseWhite = (
    <svg width="14px" height="15px" viewBox="0 0 14 15" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <title>Interface-Essential / Form-Validation / close White</title>
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g id="Notification-Valid" transform="translate(-834.000000, -16.000000)" stroke="#FFFFFF">
            <g id="Warning-info-Copy-3">
                <g id="Interface-Essential-/-Form-Validation-/-close-White" transform="translate(834.000000, 17.000000)">
                    <g id="Group">
                        <g id="close" strokeWidth="0.583333333">
                            <line x1="0.291666667" y1="0.291083333" x2="13.7083333" y2="13.70775" id="Shape"></line>
                            <line x1="13.7083333" y1="0.291083333" x2="0.291666667" y2="13.70775" id="Shape"></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);

export const CheckWhite = (
    <svg width="11px" height="11px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g id="Billing-1" transform="translate(-654.000000, -472.000000)" stroke="#FFFFFF">
            <g id="Table" transform="translate(240.000000, 439.000000)">
                <g id="Icon" transform="translate(414.000000, 33.000000)">
                    <g id="Interface-Essential-/-Form-Validation-/-check-1-Green">
                        <g id="Group">
                            <g id="check-1" strokeWidth="1">
                                <polyline id="Shape" points="13.7083333 0.291083333 4.08333333 13.70775 0.291666667 9.91608333"></polyline>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);

export default { InformationWhite, CloseWhite }