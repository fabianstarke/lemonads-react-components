import React from "react";
export const Add = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/add"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <path
                d="M10,3 C10.2761424,3 10.5,3.22385763 10.5,3.5 L10.5,3.5 L10.499,9.5 L16.5,9.5 C16.7454599,9.5 16.9496084,9.67687516 16.9919443,9.91012437 L17,10 C17,10.2761424 16.7761424,10.5 16.5,10.5 L16.5,10.5 L10.499,10.5 L10.5,16.5 C10.5,16.7454599 10.3231248,16.9496084 10.0898756,16.9919443 L10,17 C9.72385763,17 9.5,16.7761424 9.5,16.5 L9.5,16.5 L9.499,10.5 L3.5,10.5 C3.25454011,10.5 3.05039163,10.3231248 3.00805567,10.0898756 L3,10 C3,9.72385763 3.22385763,9.5 3.5,9.5 L3.5,9.5 L9.499,9.5 L9.5,3.5 C9.5,3.25454011 9.67687516,3.05039163 9.91012437,3.00805567 Z"
                id="Combined-Shape"
                fill="#000000"
                fillRule="nonzero"
            ></path>
        </g>
    </svg>
);

export const ArrowBottom = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/arrow-bottom"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Interface-Essential-/-Keyboard-/-keyboard-arrow-right"
                transform="translate(10.000000, 10.500000) rotate(-270.000000) translate(-10.000000, -10.500000) translate(3.000000, 7.000000)"
                stroke="#000000"
            >
                <g id="Group">
                    <g id="keyboard-arrow-right">
                        <polyline
                            id="Shape"
                            points="10.7916667 0.37325 13.7083333 3.28991667 10.7916667 6.20658333"
                        ></polyline>
                        <line
                            x1="13.7083333"
                            y1="3.28991667"
                            x2="0.291666667"
                            y2="3.28991667"
                            id="Shape"
                        ></line>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Business = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/business"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Building-Construction-/-Buildings-/-building-2"
            >
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#878787" strokeWidth="0.8">
                        <g id="building-2">
                            <line
                                x1="0.287179487"
                                y1="12.9575385"
                                x2="13.4974359"
                                y2="12.9575385"
                                id="Shape"
                            ></line>
                            <rect
                                id="Rectangle-path"
                                x="1.43589744"
                                y="1.92984615"
                                width="10.9128205"
                                height="11.0276923"
                            ></rect>
                            <line
                                x1="0.287179487"
                                y1="1.92984615"
                                x2="13.4974359"
                                y2="1.92984615"
                                id="Shape"
                            ></line>
                            <path
                                d="M11.774359,1.92984615 L11.774359,0.827076923 C11.774359,0.522555609 11.5172097,0.275692308 11.2,0.275692308 L2.58461538,0.275692308 C2.26740568,0.275692308 2.01025641,0.522555609 2.01025641,0.827076923 L2.01025641,1.92984615"
                                id="Shape"
                            ></path>
                            <path
                                d="M6.03076923,12.9575385 L6.03076923,11.0276923 C6.03076923,10.5709103 6.41649314,10.2006154 6.89230769,10.2006154 C7.36812225,10.2006154 7.75384615,10.5709103 7.75384615,11.0276923 L7.75384615,12.9575385"
                                id="Shape"
                            ></path>
                            <line
                                x1="3.15897436"
                                y1="10.752"
                                x2="4.88205128"
                                y2="10.752"
                                id="Shape"
                            ></line>
                            <line
                                x1="8.9025641"
                                y1="10.752"
                                x2="10.625641"
                                y2="10.752"
                                id="Shape"
                            ></line>
                            <line
                                x1="3.15897436"
                                y1="8.54646154"
                                x2="5.45641026"
                                y2="8.54646154"
                                id="Shape"
                            ></line>
                            <line
                                x1="8.32820513"
                                y1="8.54646154"
                                x2="10.625641"
                                y2="8.54646154"
                                id="Shape"
                            ></line>
                            <line
                                x1="3.15897436"
                                y1="6.34092308"
                                x2="5.45641026"
                                y2="6.34092308"
                                id="Shape"
                            ></line>
                            <line
                                x1="8.32820513"
                                y1="6.34092308"
                                x2="10.625641"
                                y2="6.34092308"
                                id="Shape"
                            ></line>
                            <line
                                x1="3.15897436"
                                y1="4.13538462"
                                x2="5.45641026"
                                y2="4.13538462"
                                id="Shape"
                            ></line>
                            <line
                                x1="8.32820513"
                                y1="4.13538462"
                                x2="10.625641"
                                y2="4.13538462"
                                id="Shape"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

const ArrowLeft = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/arrow-left"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Interface-Essential-/-Keyboard-/-keyboard-arrow-right"
                transform="translate(10.000000, 10.500000) rotate(-180.000000) translate(-10.000000, -10.500000) translate(3.000000, 7.000000)"
                stroke="#000000"
            >
                <g id="Group">
                    <g id="keyboard-arrow-right">
                        <polyline
                            id="Shape"
                            points="10.7916667 0.37325 13.7083333 3.28991667 10.7916667 6.20658333"
                        ></polyline>
                        <line
                            x1="13.7083333"
                            y1="3.28991667"
                            x2="0.291666667"
                            y2="3.28991667"
                            id="Shape"
                        ></line>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

const ArrowRight = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/arrow-right"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Interface-Essential-/-Keyboard-/-keyboard-arrow-right"
                transform="translate(3.000000, 7.000000)"
                stroke="#000000"
            >
                <g id="Group">
                    <g id="keyboard-arrow-right">
                        <polyline
                            id="Shape"
                            points="10.7916667 0.37325 13.7083333 3.28991667 10.7916667 6.20658333"
                        ></polyline>
                        <line
                            x1="13.7083333"
                            y1="3.28991667"
                            x2="0.291666667"
                            y2="3.28991667"
                            id="Shape"
                        ></line>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

const ArrowTop = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/arrow-top"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Interface-Essential-/-Keyboard-/-keyboard-arrow-right"
                transform="translate(10.000000, 10.500000) rotate(-90.000000) translate(-10.000000, -10.500000) translate(3.000000, 7.000000)"
                stroke="#000000"
            >
                <g id="Group">
                    <g id="keyboard-arrow-right">
                        <polyline
                            id="Shape"
                            points="10.7916667 0.37325 13.7083333 3.28991667 10.7916667 6.20658333"
                        ></polyline>
                        <line
                            x1="13.7083333"
                            y1="3.28991667"
                            x2="0.291666667"
                            y2="3.28991667"
                            id="Shape"
                        ></line>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const BulletList = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/Bullet-list"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Lists-/-list-bullets-1"
            >
                <g transform="translate(3.000000, 3.500000)">
                    <g id="Group" stroke="#000000">
                        <g id="list-bullets-1">
                            <line
                                x1="4.95833333"
                                y1="1.62383333"
                                x2="13.7083333"
                                y2="1.62383333"
                                id="Shape"
                                strokeWidth="0.8"
                            ></line>
                            <line
                                x1="4.95833333"
                                y1="6.2905"
                                x2="13.7083333"
                                y2="6.2905"
                                id="Shape"
                                strokeWidth="0.8"
                            ></line>
                            <line
                                x1="4.95833333"
                                y1="10.9571667"
                                x2="13.7083333"
                                y2="10.9571667"
                                id="Shape"
                                strokeWidth="0.8"
                            ></line>
                            <rect
                                id="Rectangle-path"
                                strokeWidth="0.6"
                                x="0.291666667"
                                y="0.457166667"
                                width="2.33333333"
                                height="2.33333333"
                                rx="0.583333333"
                            ></rect>
                            <rect
                                id="Rectangle-path"
                                strokeWidth="0.6"
                                x="0.291666667"
                                y="5.12383333"
                                width="2.33333333"
                                height="2.33333333"
                                rx="0.583333333"
                            ></rect>
                            <rect
                                id="Rectangle-path"
                                strokeWidth="0.6"
                                x="0.291666667"
                                y="9.7905"
                                width="2.33333333"
                                height="2.33333333"
                                rx="0.583333333"
                            ></rect>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Calendar = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/calendar"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Date/Calendar-/-calendar-2"
            >
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group">
                        <g id="calendar">
                            <rect
                                id="Rectangle-path"
                                stroke="#000000"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                x="0.291666667"
                                y="1.45891667"
                                width="13.4166667"
                                height="12.25"
                                rx="0.583333333"
                            ></rect>
                            <line
                                x1="3.20833333"
                                y1="0.29225"
                                x2="3.20833333"
                                y2="3.20891667"
                                id="Shape"
                                stroke="#000000"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            ></line>
                            <line
                                x1="10.7916667"
                                y1="0.29225"
                                x2="10.7916667"
                                y2="3.20891667"
                                id="Shape"
                                stroke="#000000"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            ></line>
                            <line
                                x1="0.291666667"
                                y1="4.37558333"
                                x2="13.7083333"
                                y2="4.37558333"
                                id="Shape"
                                stroke="#000000"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            ></line>
                            <text
                                id="31"
                                fontFamily="OpenSans-SemiBold, Open Sans"
                                fontSize="7"
                                fontWeight="500"
                                fill="#000000"
                            >
                                <tspan x="3.5" y="11.5">
                                    31
                                </tspan>
                            </text>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const ChangePassword = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/change-password"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Synchronize-/-synchronize-arrows-lock"
            >
                <g transform="translate(1.000000, 3.000000)">
                    <g id="Group" stroke="#000000" strokeWidth="0.8">
                        <g id="synchronize-arrows-lock">
                            <polyline
                                id="Shape"
                                points="0.53475 5.77 2.62575 8.8825 5.02875 6.004"
                            ></polyline>
                            <polyline
                                id="Shape"
                                points="17.4645 8.995 15.37575 5.8825 12.9705 8.7625"
                            ></polyline>
                            <path
                                d="M15.34725,5.91175 C15.8732571,9.24462725 13.7476865,12.4215519 10.46625,13.207 C8.32839642,13.7136925 6.08032964,13.0830093 4.518,11.53825"
                                id="Shape"
                            ></path>
                            <path
                                d="M2.62875,8.86375 C2.19936948,7.14366308 2.47612079,5.32328069 3.39729308,3.80851726 C4.31846537,2.29375383 5.8074577,1.21057093 7.53225,0.8005 C9.81944193,0.257004556 12.2199958,1.01826129 13.776,2.7805"
                                id="Shape"
                            ></path>
                            <g
                                id="Group-2"
                                transform="translate(6.800000, 3.629500)"
                            >
                                <path
                                    d="M0.75,6 L3.75,6 C4.16421356,6 4.5,5.66421356 4.5,5.25 L4.5,3 C4.5,2.58578644 4.16421356,2.25 3.75,2.25 L0.75,2.25 C0.335786438,2.25 0,2.58578644 0,3 L0,5.25 C0,5.66421356 0.335786438,6 0.75,6 Z"
                                    id="Shape"
                                ></path>
                                <path
                                    d="M2.25,0 L2.25,0 C1.42157288,0 0.75,0.671572875 0.75,1.5 L0.75,2.25 L3.75,2.25 L3.75,1.5 C3.75,0.671572875 3.07842712,0 2.25,0 Z"
                                    id="Shape"
                                ></path>
                                <path
                                    d="M2.25,3.9525 C2.14644661,3.9525 2.0625,4.03644661 2.0625,4.14 C2.0625,4.24355339 2.14644661,4.3275 2.25,4.3275 C2.35355339,4.3275 2.4375,4.24355339 2.4375,4.14 C2.4375,4.03644661 2.35355339,3.9525 2.25,3.9525"
                                    id="Shape"
                                ></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const CheckValid = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/check-valid"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Interface-Essential-/-Form-Validation-/-check-1-Green">
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="check-1">
                            <polyline
                                id="Shape"
                                points="13.7083333 0.291083333 4.08333333 13.70775 0.291666667 9.91608333"
                            ></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const CheckGreen = (
    <svg
        width="14px"
        height="14px"
        viewBox="0 0 14 14"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Page-1"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Billing-1"
                transform="translate(-654.000000, -472.000000)"
                stroke="#23C73A"
            >
                <g id="Table" transform="translate(240.000000, 439.000000)">
                    <g id="Icon" transform="translate(414.000000, 33.000000)">
                        <g id="Interface-Essential-/-Form-Validation-/-check-1-Green">
                            <g id="Group">
                                <g id="check-1" strokeWidth="1">
                                    <polyline
                                        id="Shape"
                                        points="13.7083333 0.291083333 4.08333333 13.70775 0.291666667 9.91608333"
                                    ></polyline>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const ChevronDown = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/chevron-down"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Shape-Copy-3">
                <g transform="translate(10.000000, 10.000000) rotate(-270.000000) translate(-10.000000, -10.000000) translate(6.000000, 3.000000)">
                    <path
                        d="M8,0 L0.118536633,6.73456195 C0.0436929708,6.79859053 0,6.89643183 0,7 C0,7.10356817 0.0436929708,7.20140947 0.118536633,7.26543805 L7.99937902,14"
                        id="Shape-Copy-2"
                        stroke="#000000"
                        transform="translate(4.000000, 7.000000) scale(-1, 1) translate(-4.000000, -7.000000) "
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const ChevronLeft = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/chevron-left"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Shape-Copy-2">
                <g transform="translate(6.000000, 3.000000)">
                    <path
                        d="M8,0 L0.118536633,6.73456195 C0.0436929708,6.79859053 0,6.89643183 0,7 C0,7.10356817 0.0436929708,7.20140947 0.118536633,7.26543805 L7.99937902,14"
                        id="Shape-Copy"
                        stroke="#000000"
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const ChevronRight = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/chevron-right"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Shape-Copy-3">
                <g transform="translate(6.000000, 3.000000)">
                    <path
                        d="M8,0 L0.118536633,6.73456195 C0.0436929708,6.79859053 0,6.89643183 0,7 C0,7.10356817 0.0436929708,7.20140947 0.118536633,7.26543805 L7.99937902,14"
                        id="Shape-Copy-2"
                        stroke="#000000"
                        transform="translate(4.000000, 7.000000) scale(-1, 1) translate(-4.000000, -7.000000) "
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const ChevronUp = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/chevron-up"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Shape-Copy-3">
                <g transform="translate(10.000000, 10.000000) rotate(-90.000000) translate(-10.000000, -10.000000) translate(6.000000, 3.000000)">
                    <path
                        d="M8,0 L0.118536633,6.73456195 C0.0436929708,6.79859053 0,6.89643183 0,7 C0,7.10356817 0.0436929708,7.20140947 0.118536633,7.26543805 L7.99937902,14"
                        id="Shape-Copy-2"
                        stroke="#000000"
                        transform="translate(4.000000, 7.000000) scale(-1, 1) translate(-4.000000, -7.000000) "
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const Close = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/close"
            stroke="#000000"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="close">
                <g transform="translate(3.000000, 3.000000)">
                    <line
                        x1="0.285326087"
                        y1="0.284755435"
                        x2="13.4103261"
                        y2="13.4097554"
                        id="Shape"
                    ></line>
                    <line
                        x1="13.4103261"
                        y1="0.284755435"
                        x2="0.285326087"
                        y2="13.4097554"
                        id="Shape"
                    ></line>
                </g>
            </g>
        </g>
    </svg>
);

export const CloseRed = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <desc>Created with Sketch.</desc>
        <g
            id="Page-1"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Billing-1"
                transform="translate(-654.000000, -537.000000)"
                stroke="#FF4242"
            >
                <g id="Table" transform="translate(240.000000, 439.000000)">
                    <g id="Icon" transform="translate(414.000000, 33.000000)">
                        <g
                            id="Interface-Essential-/-Form-Validation-/-close-Red"
                            transform="translate(0.000000, 66.000000)"
                        >
                            <g id="Group">
                                <g id="close" strokeWidth="1">
                                    <line
                                        x1="0.291666667"
                                        y1="0.291083333"
                                        x2="13.7083333"
                                        y2="13.70775"
                                        id="Shape"
                                    ></line>
                                    <line
                                        x1="13.7083333"
                                        y1="0.291083333"
                                        x2="0.291666667"
                                        y2="13.70775"
                                        id="Shape"
                                    ></line>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Delete = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/delete"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Interface-Essential-/-Delete-/-bin-1">
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#000000" strokeWidth="0.8">
                        <g id="bin-1">
                            <path
                                d="M12.25,2.625 L11.193,12.6635833 C11.130624,13.2572157 10.6302337,13.70802 10.0333333,13.7083333 L3.96666667,13.7083333 C3.36954317,13.7083333 2.86881607,13.2574375 2.80641667,12.6635833 L1.75,2.625"
                                id="Shape"
                            ></path>
                            <line
                                x1="0.291666667"
                                y1="2.625"
                                x2="13.7083333"
                                y2="2.625"
                                id="Shape"
                            ></line>
                            <path
                                d="M4.375,2.625 L4.375,0.875 C4.375,0.552833896 4.63616723,0.291666667 4.95833333,0.291666667 L9.04166667,0.291666667 C9.36383277,0.291666667 9.625,0.552833896 9.625,0.875 L9.625,2.625"
                                id="Shape"
                            ></path>
                            <line
                                x1="7"
                                y1="5.25"
                                x2="7"
                                y2="11.375"
                                id="Shape"
                            ></line>
                            <line
                                x1="9.625"
                                y1="5.25"
                                x2="9.33333333"
                                y2="11.375"
                                id="Shape"
                            ></line>
                            <line
                                x1="4.375"
                                y1="5.25"
                                x2="4.66666667"
                                y2="11.375"
                                id="Shape"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Download = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/download"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="download-bottom">
                <g transform="translate(3.000000, 3.000000)" stroke="#000000">
                    <path
                        d="M13.4166667,10.6271667 L13.4166667,11.7430833 C13.4163447,12.6672445 12.6672445,13.4163447 11.7430833,13.4166667 L2.25691667,13.4166667 C1.33262211,13.4166667 0.583333333,12.6673779 0.583333333,11.7430833 L0.583333333,10.6271667"
                        id="Shape"
                        strokeWidth="0.8"
                    ></path>
                    <line
                        x1="7"
                        y1="10.563"
                        x2="7"
                        y2="0.583333333"
                        id="Shape"
                        strokeWidth="0.8"
                    ></line>
                    <polyline
                        id="Shape"
                        strokeWidth="0.8"
                        points="11.0833333 6.47966667 7 10.563 2.91666667 6.47966667"
                    ></polyline>
                </g>
            </g>
        </g>
    </svg>
);

export const ErrorSystem = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/error-system"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Computers-Devices-Electronics-/-Desktop-Actions-/-monitor-warning-Copy"
            >
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#FF4242" strokeWidth="0.8">
                        <g id="monitor-warning">
                            <line
                                x1="3.20833333"
                                y1="10.2083333"
                                x2="6.70833333"
                                y2="10.2083333"
                                id="Shape"
                            ></line>
                            <line
                                x1="0.291666667"
                                y1="6.70833333"
                                x2="8.45833333"
                                y2="6.70833333"
                                id="Shape"
                            ></line>
                            <path
                                d="M4.95833333,8.45833333 L4.95833333,9.33333333 C4.95833333,9.81658249 4.56658249,10.2083333 4.08333333,10.2083333"
                                id="Shape"
                            ></path>
                            <path
                                d="M11.9583333,6.70833333 L11.9583333,0.875 C11.9583333,0.552833896 11.6971661,0.291666667 11.375,0.291666667 L0.875,0.291666667 C0.552833896,0.291666667 0.291666667,0.552833896 0.291666667,0.875 L0.291666667,7.875 C0.291666667,8.1971661 0.552833896,8.45833333 0.875,8.45833333 L7.29166667,8.45833333"
                                id="Shape"
                            ></path>
                            <path
                                d="M10.2083333,11.9583333 C10.2888749,11.9583333 10.3541667,12.0236251 10.3541667,12.1041667 C10.3541667,12.1847082 10.2888749,12.25 10.2083333,12.25 C10.1277918,12.25 10.0625,12.1847082 10.0625,12.1041667 C10.0625,12.0236251 10.1277918,11.9583333 10.2083333,11.9583333"
                                id="Shape"
                            ></path>
                            <line
                                x1="10.2083333"
                                y1="10.7916667"
                                x2="10.2083333"
                                y2="9.04166667"
                                id="Shape"
                            ></line>
                            <path
                                d="M10.6971667,7.29166667 C10.6008102,7.11122696 10.4128891,6.9985415 10.2083333,6.9985415 C10.0037776,6.9985415 9.81585646,7.11122696 9.7195,7.29166667 L6.77016667,12.9120833 C6.6815372,13.0805103 6.68841231,13.2832152 6.78825,13.44525 C6.88930081,13.6080276 7.06740743,13.7069022 7.259,13.7065841 L13.1576667,13.7065841 C13.3492302,13.7067896 13.5272857,13.6079434 13.6284167,13.44525 C13.7282544,13.2832152 13.7351295,13.0805103 13.6465,12.9120833 L10.6971667,7.29166667 Z"
                                id="Shape"
                            ></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const HourGlass = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/hourglass"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Hourglass">
                <g transform="translate(5.000000, 3.000000)">
                    <path
                        d="M8.75,2.8 C8.75,4.81031649 7.05241301,6.44 4.95833333,6.44 C2.86425366,6.44 1.16666667,4.81031649 1.16666667,2.8 L1.16666667,-2.57127653e-13 L8.75,-2.57127653e-13 L8.75,2.8 Z"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <path
                        d="M8.75,10.08 C8.75,8.06968351 7.05241301,6.44 4.95833333,6.44 C2.86425366,6.44 1.16666667,8.06968351 1.16666667,10.08 L1.16666667,12.88 L8.75,12.88 L8.75,10.08 Z"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <line
                        x1="1.06137321e-12"
                        y1="0.00056"
                        x2="9.91666667"
                        y2="0.00056"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <line
                        x1="1.06137321e-12"
                        y1="12.88056"
                        x2="9.91666667"
                        y2="12.88056"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <polyline
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                        points="1.16666667 10.64056 3.20833333 10.64056 4.95833333 8.96056 6.70833333 10.64056 8.75 10.64056"
                    ></polyline>
                    <line
                        x1="1.981"
                        y1="5.04056"
                        x2="7.93566667"
                        y2="5.04056"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                </g>
            </g>
        </g>
    </svg>
);

export const Information = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/information"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Alerts-/-information-circle"
            >
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="information-circle">
                            <circle
                                id="Oval"
                                cx="7"
                                cy="7.00058333"
                                r="6.41666667"
                            ></circle>
                            <path
                                d="M8.45833333,9.91958333 L7.58333333,9.91958333 C7.26116723,9.91958333 7,9.6584161 7,9.33625 L7,5.54458333 C7,5.38350028 6.86941639,5.25291667 6.70833333,5.25291667 L5.83333333,5.25291667"
                                id="Shape"
                                strokeWidth="0.8"
                            ></path>
                            <path
                                d="M6.85125,3.79166667 C6.77070847,3.79166667 6.70541667,3.85695847 6.70541667,3.9375 C6.70541667,4.01804153 6.77070847,4.08333333 6.85125,4.08333333 C6.93179153,4.08333333 6.99708333,4.01804153 6.99708333,3.9375 C6.99708333,3.85695847 6.93179153,3.79166667 6.85125,3.79166667"
                                id="Shape"
                                strokeWidth="0.8"
                            ></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Lock = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/lock"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Lock/Unlock-/-lock-1"
            >
                <g transform="translate(5.000000, 3.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="lock-1">
                            <circle
                                id="Oval"
                                strokeWidth="0.6"
                                cx="5"
                                cy="8.75"
                                r="1"
                            ></circle>
                            <line
                                x1="5"
                                y1="9.33333333"
                                x2="5"
                                y2="11.0833333"
                                id="Shape"
                                strokeWidth="0.6"
                            ></line>
                            <rect
                                id="Rectangle-path"
                                x="0.0416666667"
                                y="5.54166667"
                                width="9.91666667"
                                height="8.16666667"
                                rx="0.583333333"
                            ></rect>
                            <path
                                d="M1.79166667,3.5 C1.79166667,1.72808643 3.22808643,0.291666667 5,0.291666667 C6.77191357,0.291666667 8.20833333,1.72808643 8.20833333,3.5 L8.20833333,5.54166667 L1.79166667,5.54166667 L1.79166667,3.5 Z"
                                id="Shape"
                            ></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const MessageBubble = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/message-bubble"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Message-bubble">
                <g transform="translate(3.000000, 3.000000)">
                    <path
                        d="M12.5258824,10.7364706 L7.6863369,10.7364706 L3.87846866,13.9275882 C3.79224581,13.9997526 3.67397868,14.0132083 3.57497848,13.9621175 C3.47597827,13.9110266 3.41408428,13.8045954 3.41614973,13.689 L3.41614973,10.7364706 L1.13871658,10.7364706 C0.509820777,10.7364706 1.53602621e-13,10.2023726 1.53602621e-13,9.54352941 L1.53602621e-13,1.19294118 C1.53602621e-13,0.534097957 0.509820777,2.19432315e-14 1.13871658,2.19432315e-14 L12.5258824,2.19432315e-14 C13.1547782,2.19432315e-14 13.6645989,0.534097957 13.6645989,1.19294118 L13.6645989,9.54352941 C13.6645989,10.2023726 13.1547782,10.7364706 12.5258824,10.7364706 Z"
                        id="Shape"
                        stroke="#000000"
                    ></path>
                    <line
                        x1="2.83668449"
                        y1="3.57882353"
                        x2="11.0719786"
                        y2="3.57882353"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <line
                        x1="2.83668449"
                        y1="7.15764706"
                        x2="8.60139037"
                        y2="7.15764706"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                </g>
            </g>
        </g>
    </svg>
);

export const NetworksInformation = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/Networks-information"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="network-information">
                <g transform="translate(3.000000, 3.000000)">
                    <line
                        x1="1.10716667"
                        y1="10.2083333"
                        x2="5.54225"
                        y2="10.2083333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <line
                        x1="1.46533333"
                        y1="3.20833333"
                        x2="12.5393333"
                        y2="3.20833333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <line
                        x1="6.125"
                        y1="6.70833333"
                        x2="0.298083333"
                        y2="6.70833333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                    <path
                        d="M6.63716667,13.699 C3.13710407,13.5052794 0.376295749,10.6494321 0.301100685,7.14481924 C0.225905621,3.64020634 2.8616793,0.668569862 6.35021167,0.3249018 C9.83874405,-0.0187662628 13.0037154,2.38141764 13.6138333,5.83333333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <path
                        d="M6.57358333,0.306833333 C3.07358333,4.0985 3.13716667,9.03233333 6.63716667,13.699"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <path
                        d="M7.42583333,0.306833333 C8.73304714,1.66747806 9.60090043,3.38977052 9.91666667,5.25"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="10.2083333"
                        cy="10.2083333"
                        r="3.5"
                    ></circle>
                    <path
                        d="M10.2083333,11.9583333 L10.2083333,9.91666667 C10.2083333,9.75558361 10.0777497,9.625 9.91666667,9.625 L9.33333333,9.625"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <path
                        d="M9.77083333,8.16666667 C9.85137486,8.16666667 9.91666667,8.23195847 9.91666667,8.3125 C9.91666667,8.39304153 9.85137486,8.45833333 9.77083333,8.45833333 C9.69029181,8.45833333 9.625,8.39304153 9.625,8.3125 C9.625,8.23195847 9.69029181,8.16666667 9.77083333,8.16666667"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <line
                        x1="9.33333333"
                        y1="11.9583333"
                        x2="11.0833333"
                        y2="11.9583333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></line>
                </g>
            </g>
        </g>
    </svg>
);

export const Placeholder = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/placeholder"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <rect
                id="Rectangle"
                fill="#000000"
                fillRule="nonzero"
                x="10"
                y="2"
                width="1"
                height="16"
                rx="0.5"
            ></rect>
        </g>
    </svg>
);

export const SearchTag = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/Search-tag"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                filter="url(#filter-1)"
                id="Interface-Essential-/-Tags/Bookmarks-/-tags-search"
            >
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="tags-search">
                            <path
                                d="M8.82116667,6.125 L10.6995,4.20816667 C10.9440973,3.93024322 11.0879428,3.57802735 11.1078333,3.20833333 L11.1078333,0.875 C11.1078333,0.552833896 10.8466661,0.291666667 10.5245,0.291666667 L8.04066667,0.294583333 C7.67146032,0.315089645 7.32025389,0.460756988 7.04491667,0.707583333 L0.728583333,7.02391667 C0.500860536,7.25170825 0.500860536,7.62095842 0.728583333,7.84875 L3.54958333,10.66975 C3.65858971,10.778756 3.80665382,10.839655 3.96080997,10.8388952 C4.11496613,10.8381211 4.26241696,10.7757519 4.37033333,10.6656667 L5.677,9.33333333"
                                id="Shape"
                                strokeWidth="0.8"
                            ></path>
                            <circle
                                id="Oval"
                                strokeWidth="0.6"
                                cx="9.06616667"
                                cy="2.33216667"
                                r="1"
                            ></circle>
                            <circle
                                id="Oval"
                                strokeWidth="0.8"
                                cx="9.66641667"
                                cy="9.93241667"
                                r="2.64191667"
                            ></circle>
                            <line
                                x1="13.4411667"
                                y1="13.7071667"
                                x2="11.5465"
                                y2="11.8125"
                                id="Shape"
                                strokeWidth="0.8"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Supp = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Icon/base/supp"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <path
                d="M17,10 C17,9.72385763 16.7761424,9.5 16.5,9.5 L3.5,9.5 C3.22385763,9.5 3,9.72385763 3,10 C3,10.2761424 3.22385763,10.5 3.5,10.5 L16.5,10.5 C16.7761424,10.5 17,10.2761424 17,10 Z"
                id="Combined-Shape-Copy"
                fill="#000000"
                fillRule="nonzero"
            ></path>
        </g>
    </svg>
);

export const ToLeft = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/to-left"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Arrow-stop-left">
                <g transform="translate(3.000000, 6.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="keyboard-arrow-previous">
                            <line
                                x1="13.7083333"
                                y1="4.28991667"
                                x2="2.625"
                                y2="4.28991667"
                                id="Shape"
                            ></line>
                            <polyline
                                id="Shape"
                                points="6.125 0.789916667 2.625 4.28991667 6.125 7.78991667"
                            ></polyline>
                            <line
                                x1="0.291666667"
                                y1="7.78991667"
                                x2="0.291666667"
                                y2="0.789916667"
                                id="Shape"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const ToRight = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/to-right"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Arrow-stop-right">
                <g transform="translate(10.000000, 10.000000) rotate(-180.000000) translate(-10.000000, -10.000000) translate(3.000000, 6.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="keyboard-arrow-previous">
                            <line
                                x1="13.7083333"
                                y1="4.28991667"
                                x2="2.625"
                                y2="4.28991667"
                                id="Shape"
                            ></line>
                            <polyline
                                id="Shape"
                                points="6.125 0.789916667 2.625 4.28991667 6.125 7.78991667"
                            ></polyline>
                            <line
                                x1="0.291666667"
                                y1="7.78991667"
                                x2="0.291666667"
                                y2="0.789916667"
                                id="Shape"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Upload = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/upload"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Internet-Networks-Servers-/-Upload/Download-/-upload-bottom">
                <g transform="translate(3.000000, 3.000000)">
                    <g id="Group" stroke="#000000">
                        <g id="upload-bottom">
                            <path
                                d="M13.4166667,10.6271667 L13.4166667,11.7430833 C13.4163447,12.6672445 12.6672445,13.4163447 11.7430833,13.4166667 L2.25691667,13.4166667 C1.33262211,13.4166667 0.583333333,12.6673779 0.583333333,11.7430833 L0.583333333,10.6271667"
                                id="Shape"
                            ></path>
                            <line
                                x1="7"
                                y1="0.583333333"
                                x2="7"
                                y2="10.563"
                                id="Shape"
                            ></line>
                            <polyline
                                id="Shape"
                                points="2.91666667 4.66666667 7 0.583333333 11.0833333 4.66666667"
                            ></polyline>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const UserPicture = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/user-picture"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Light">
                <g transform="translate(3.000000, 3.000000)">
                    <rect
                        id="Rectangle-path"
                        stroke="#000000"
                        strokeWidth="0.8"
                        x="6.70833333"
                        y="6.70833333"
                        width="7"
                        height="7"
                        rx="0.583333333"
                    ></rect>
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="9.11458333"
                        cy="9.04166667"
                        r="1"
                    ></circle>
                    <path
                        d="M13.7083333,12.3958333 L12.3135833,10.3034167 C12.2334326,10.1830413 12.0989744,10.1100514 11.9543656,10.1083894 C11.8097568,10.1067834 11.6736836,10.1767164 11.5908333,10.29525 L10.4241667,11.9583333 L9.70316667,11.3814167 C9.60730782,11.3049241 9.483775,11.2721144 9.3625942,11.2909623 C9.2414134,11.3098102 9.13368129,11.3785897 9.06558333,11.4805833 L7.58333333,13.7083333"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="4.375"
                        cy="2.77083333"
                        r="2.47916667"
                    ></circle>
                    <path
                        d="M4.95833333,6.84145861 C3.78583439,6.67320899 2.59802184,7.02202025 1.70266129,7.79751441 C0.807300739,8.57300857 0.292507757,9.69886652 0.291666667,10.8833753 L4.95833333,10.8833753"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const UserSingle = (
    <svg
        width="20px"
        height="20px"
        viewBox="0 0 20 20"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icon/base/user-single"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g filter="url(#filter-1)" id="Light">
                <g transform="translate(5.000000, 3.000000)">
                    <circle
                        id="Oval"
                        stroke="#000000"
                        cx="4.99074074"
                        cy="3.38657407"
                        r="3.03009259"
                    ></circle>
                    <path
                        d="M0,13.4768519 C0,10.7205419 2.23443077,8.48611119 4.99074074,8.48611119 C7.74705071,8.48611119 9.98148148,10.7205419 9.98148148,13.4768519 L0,13.4768519 Z"
                        id="Shape"
                        stroke="#000000"
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const SelectArrow = (
    <svg
        width="9px"
        height="8px"
        viewBox="0 0 9 8"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <g
            id="Component/select/select-arrow"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <path
                d="M4.69930915,2.17716369 L8.41024935,5.4757772 C8.53408419,5.58585261 8.5452384,5.77547432 8.435163,5.89930915 C8.37823277,5.96335566 8.29663155,6 8.2109402,6 L0.789059797,6 C0.623374372,6 0.489059797,5.86568542 0.489059797,5.7 C0.489059797,5.61430866 0.525704136,5.53270743 0.589750645,5.4757772 L4.30069085,2.17716369 C4.41435665,2.07612742 4.58564335,2.07612742 4.69930915,2.17716369 Z"
                id="Triangle"
                fill="#000000"
                transform="translate(4.500000, 4.000000) rotate(-180.000000) translate(-4.500000, -4.000000) "
            ></path>
        </g>
    </svg>
);

export const Loading = (
    <svg
        height="40"
        viewBox="0 0 55 70"
        xmlns="http://www.w3.org/2000/svg"
        fill="#0336ff"
    >
        <g transform="matrix(1 0 0 -1 0 70)">
            <rect width="10" height="20" rx="3">
                <animate
                    attributeName="height"
                    begin="0s"
                    dur="4.3s"
                    values="20;45;57;70;64;32;66;45;64;23;66;13;64;56;34;34;2;23;66;69;20"
                    calcMode="linear"
                    repeatCount="indefinite"
                />
            </rect>
            <rect x="15" width="10" height="70" rx="3">
                <animate
                    attributeName="height"
                    begin="0s"
                    dur="3s"
                    values="70;55;33;5;65;23;70;33;12;14;60;70"
                    calcMode="linear"
                    repeatCount="indefinite"
                />
            </rect>
            <rect x="30" width="10" height="50" rx="3">
                <animate
                    attributeName="height"
                    begin="0s"
                    dur="2.4s"
                    values="50;34;68;23;56;23;34;66;70;54;21;50"
                    calcMode="linear"
                    repeatCount="indefinite"
                />
            </rect>
            <rect x="45" width="10" height="30" rx="3">
                <animate
                    attributeName="height"
                    begin="0s"
                    dur="3s"
                    values="30;45;13;70;56;62;45;66;34;23;67;30"
                    calcMode="linear"
                    repeatCount="indefinite"
                />
            </rect>
        </g>
    </svg>
);

export const SearchGlass = (
    <svg
        width="16px"
        height="16px"
        viewBox="0 0 16 16"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Interface-Essential / Search / search-1</title>
        <g
            id="Icônes"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Icons---Base"
                transform="translate(-653.000000, -736.000000)"
                stroke="#000000"
            >
                <g
                    id="Interface-Essential-/-Search-/-search-1"
                    transform="translate(654.000000, 737.000000)"
                >
                    <g id="Group">
                        <g id="search-1">
                            <circle
                                id="Oval"
                                cx="6.125"
                                cy="6.125"
                                r="5.83333333"
                            ></circle>
                            <line
                                x1="13.7083333"
                                y1="13.7083333"
                                x2="10.24975"
                                y2="10.24975"
                                id="Shape"
                            ></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const OpenQuote = (
    <svg
        width="24px"
        height="18px"
        viewBox="0 0 24 18"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Interface-Essential / Text-Formatting / open-quote</title>
        <g
            id="Page-1"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Onboarding-1"
                transform="translate(-780.000000, -497.000000)"
                stroke="#000000"
            >
                <g id="Quote" transform="translate(660.000000, 494.000000)">
                    <g
                        id="open-quote"
                        transform="translate(120.000000, 0.000000)"
                    >
                        <path
                            d="M11,3.5 C5.20101013,3.5 0.5,8.20101013 0.5,14 L0.5,15.5"
                            id="Shape"
                        ></path>
                        <circle id="Oval" cx="5.5" cy="15.498" r="5"></circle>
                        <path
                            d="M23.5,3.5 C17.7010101,3.5 13,8.20101013 13,14 L13,15.5"
                            id="Shape"
                        ></path>
                        <circle id="Oval" cx="18" cy="15.498" r="5"></circle>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const CloseQuote = (
    <svg
        width="24px"
        height="19px"
        viewBox="0 0 24 19"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Interface-Essential / Text-Formatting / open-quote Copy</title>
        <g
            id="Page-1"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Onboarding-1"
                transform="translate(-1236.000000, -597.000000)"
                stroke="#000000"
            >
                <g id="Quote" transform="translate(660.000000, 494.000000)">
                    <g
                        id="close-quote"
                        transform="translate(576.000000, 100.000000)"
                    >
                        <path
                            d="M13,21 C18.7989899,21 23.5,16.2989899 23.5,10.5 L23.5,9"
                            id="Shape"
                        ></path>
                        <circle id="Oval" cx="18.5" cy="8.998" r="5"></circle>
                        <path
                            d="M0.5,21 C6.29898987,21 11,16.2989899 11,10.5 L11,9"
                            id="Shape"
                        ></path>
                        <circle id="Oval" cx="6" cy="8.998" r="5"></circle>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Rocket = (
    <svg
        width="14px"
        height="14px"
        viewBox="0 0 14 14"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Business-Products / Product-Launch / startup-launch-1</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Icônes"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Business-Products-/-Product-Launch-/-startup-launch-1"
                transform="translate(-927.000000, -169.000000)"
                filter="url(#filter-1)"
            >
                <g
                    transform="translate(927.000000, 169.000000)"
                    id="Group"
                    stroke="#000000"
                    strokeWidth="0.3"
                >
                    <g id="startup-launch-1">
                        <line
                            x1="5.25"
                            y1="1.16666667"
                            x2="5.25"
                            y2="2.33333333"
                            id="Shape"
                        ></line>
                        <line
                            x1="5.83333333"
                            y1="1.75"
                            x2="4.66666667"
                            y2="1.75"
                            id="Shape"
                        ></line>
                        <line
                            x1="9.33333333"
                            y1="12.25"
                            x2="9.33333333"
                            y2="13.4166667"
                            id="Shape"
                        ></line>
                        <line
                            x1="9.91666667"
                            y1="12.8333333"
                            x2="8.75"
                            y2="12.8333333"
                            id="Shape"
                        ></line>
                        <path
                            d="M13.4166667,8.16664394 L12.425,8.16664394 C12.0887054,8.16407574 11.7908757,8.38328387 11.6933861,8.70514805 C11.5958964,9.02701223 11.7220376,9.37463716 12.00325,9.55908333 L13.0765833,10.27425 C13.3572358,10.4583473 13.483481,10.8050372 13.3869229,11.1264936 C13.2903648,11.4479499 12.9939769,11.6676906 12.6583333,11.6666702 L11.6666667,11.6666702"
                            id="Shape"
                        ></path>
                        <line
                            x1="12.8333333"
                            y1="8.16666667"
                            x2="12.8333333"
                            y2="7.58333333"
                            id="Shape"
                        ></line>
                        <line
                            x1="12.25"
                            y1="12.25"
                            x2="12.25"
                            y2="11.6666667"
                            id="Shape"
                        ></line>
                        <path
                            d="M2.33333333,1.16665561 L1.34166667,1.16665561 C1.00569799,1.16486142 0.708591027,1.38433628 0.611567703,1.70599555 C0.514544379,2.02765482 0.640736258,2.37481093 0.921666667,2.55908333 L1.995,3.27425 C2.27593041,3.4585224 2.40212229,3.80567852 2.30509896,4.12733779 C2.20807564,4.44899706 1.91096868,4.66847191 1.575,4.66667772 L0.583333333,4.66667772"
                            id="Shape"
                        ></path>
                        <line
                            x1="1.75"
                            y1="1.16666667"
                            x2="1.75"
                            y2="0.583333333"
                            id="Shape"
                        ></line>
                        <line
                            x1="1.16666667"
                            y1="5.25"
                            x2="1.16666667"
                            y2="4.66666667"
                            id="Shape"
                        ></line>
                        <path
                            d="M4.11308333,11.9256667 C3.53383333,12.5049167 1.75,12.5416667 1.75,12.5416667 C1.75,12.5416667 1.88416667,10.8546667 2.46283333,10.276 C3.0415,9.69733333 3.64466667,10.2176667 3.90658333,10.4819167 C4.1685,10.7461667 4.69175,11.3464167 4.11308333,11.9256667 Z"
                            id="Shape"
                        ></path>
                        <line
                            x1="5.25058333"
                            y1="6.76258333"
                            x2="7.52908333"
                            y2="9.04108333"
                            id="Shape"
                        ></line>
                        <line
                            x1="11.3394167"
                            y1="2.95225"
                            x2="11.9583333"
                            y2="2.33333333"
                            id="Shape"
                        ></line>
                        <path
                            d="M11.5459167,5.22083333 L11.3394167,2.95225 L9.07083333,2.74575 C8.40844565,2.6825483 7.76803791,3.00275217 7.42116667,3.57058333 L3.91475,8.72666667 L4.74016667,9.5515 L5.565,10.3769167 L10.7210833,6.8705 C11.2889145,6.52362875 11.6091184,5.88322101 11.5459167,5.22083333 L11.5459167,5.22083333 Z"
                            id="Shape"
                        ></path>
                        <path
                            d="M5.25,6.76258333 L4.62116667,6.52225 C4.19120048,6.35777451 3.70471649,6.46151511 3.37925,6.78708333 L2.23825,7.92866667 C2.16214308,8.00468198 2.1340532,8.11641667 2.16516367,8.21938622 C2.19627414,8.32235578 2.28153436,8.39984369 2.387,8.421 L3.91475,8.72666667"
                            id="Shape"
                        ></path>
                        <path
                            d="M7.52908333,9.04166667 L7.76941667,9.6705 C7.93389216,10.1004662 7.83015156,10.5869502 7.50458333,10.9124167 L6.363,12.054 C6.28698469,12.1301069 6.17525,12.1581968 6.07228044,12.1270863 C5.96931089,12.0959759 5.89182297,12.0107156 5.87066667,11.90525 L5.565,10.3769167"
                            id="Shape"
                        ></path>
                        <circle
                            id="Oval"
                            cx="9.07083333"
                            cy="5.22083333"
                            r="1"
                        ></circle>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Like = (
    <svg
        width="16px"
        height="14px"
        viewBox="0 0 16 14"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Like</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Social-Medias-Rewards-Rating-/-Likes-/-like-1"
                transform="translate(-2.000000, -3.000000)"
                filter="url(#filter-1)"
            >
                <g
                    transform="translate(3.000000, 4.000000)"
                    id="like-1"
                    stroke="#000000"
                >
                    <path
                        d="M11.6666667,8.13441667 L11.6666667,8.13441667 C11.9792741,8.13441665 12.2681352,8.30119071 12.4244389,8.57191666 C12.5807426,8.8426426 12.5807426,9.17619073 12.4244389,9.44691668 C12.2681352,9.71764262 11.9792741,9.88441668 11.6666667,9.88441667 L11.0833333,9.88441667 C11.5665825,9.88441667 11.9583333,10.2761675 11.9583333,10.7594167 C11.9583333,11.243 11.5663333,11.34275 11.0833333,11.34275 L7.29166667,11.34275 C5.62858333,11.34275 5.25,11.0510833 3.20833333,10.7594167 L3.20833333,5.80108333 C4.6375,5.80108333 7,3.17608333 7,0.84275 C7,-0.0795 8.27691667,-0.423083333 8.75,1.26216667 C9.04166667,2.30108333 8.16666667,4.34275 8.16666667,4.34275 L12.8333333,4.34275 C13.3165825,4.34275 13.7083333,4.73450084 13.7083333,5.21775 C13.7083333,5.70133333 13.3163333,6.38441667 12.8333333,6.38441667 L12.25,6.38441667 C12.7332492,6.38441667 13.125,6.77616751 13.125,7.25941667 C13.125,7.74266582 12.7332492,8.13441667 12.25,8.13441667 L11.6666667,8.13441667"
                        id="Shape"
                        strokeWidth="0.8"
                    ></path>
                    <rect
                        id="Rectangle-path"
                        strokeWidth="0.8"
                        x="0.291666667"
                        y="4.92608333"
                        width="2.91666667"
                        height="7"
                    ></rect>
                    <path
                        d="M1.89583333,10.46775 C1.81529181,10.46775 1.75,10.5330418 1.75,10.6135833 C1.75,10.6941249 1.81529181,10.7594167 1.89583333,10.7594167 C1.97637486,10.7594167 2.04166667,10.6941249 2.04166667,10.6135833 C2.04166667,10.5330418 1.97637486,10.46775 1.89583333,10.46775"
                        id="Shape"
                        strokeWidth="0.6"
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const BrowserIdea = (
    <svg
        width="16px"
        height="15px"
        viewBox="0 0 16 15"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Work-Office-Companies / Ideas/Creativity / browser-idea</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="0.53"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Work-Office-Companies-/-Ideas/Creativity-/-browser-idea"
                transform="translate(-2.000000, -2.000000)"
                filter="url(#filter-1)"
            >
                <g
                    transform="translate(3.000000, 3.000000)"
                    id="Group"
                    stroke="#000000"
                    strokeWidth="0.833333333"
                >
                    <g id="browser-idea">
                        <line
                            x1="0.291666667"
                            y1="2.52"
                            x2="11.9583333"
                            y2="2.52"
                            id="Shape"
                        ></line>
                        <path
                            d="M1.89583333,1.26 C1.81529181,1.26 1.75,1.32268014 1.75,1.4 C1.75,1.47731986 1.81529181,1.54 1.89583333,1.54 C1.97637486,1.54 2.04166667,1.47731986 2.04166667,1.4 C2.04166667,1.32268014 1.97637486,1.26 1.89583333,1.26 L1.89583333,1.26"
                            id="Shape"
                        ></path>
                        <path
                            d="M3.35416667,1.26 C3.27362514,1.26 3.20833333,1.32268014 3.20833333,1.4 C3.20833333,1.47731986 3.27362514,1.54 3.35416667,1.54 C3.43470819,1.54 3.5,1.47731986 3.5,1.4 C3.5,1.32268014 3.43470819,1.26 3.35416667,1.26 L3.35416667,1.26"
                            id="Shape"
                        ></path>
                        <path
                            d="M4.8125,1.26 C4.73195847,1.26 4.66666667,1.32268014 4.66666667,1.4 C4.66666667,1.47731986 4.73195847,1.54 4.8125,1.54 C4.89304153,1.54 4.95833333,1.47731986 4.95833333,1.4 C4.95833333,1.32268014 4.89304153,1.26 4.8125,1.26 L4.8125,1.26"
                            id="Shape"
                        ></path>
                        <path
                            d="M3.5,8.68 L1.45833333,8.68 C0.814001125,8.68 0.291666667,8.17855892 0.291666667,7.56 L0.291666667,1.4 C0.291666667,0.78144108 0.814001125,0.28 1.45833333,0.28 L10.7916667,0.28 C11.4359989,0.28 11.9583333,0.78144108 11.9583333,1.4 L11.9583333,3.64"
                            id="Shape"
                        ></path>
                        <line
                            x1="10.15"
                            y1="13.16"
                            x2="9.1"
                            y2="13.16"
                            id="Shape"
                        ></line>
                        <line
                            x1="10.7916667"
                            y1="12.04"
                            x2="8.45833333"
                            y2="12.04"
                            id="Shape"
                        ></line>
                        <line
                            x1="9.625"
                            y1="4.76"
                            x2="9.625"
                            y2="5.6"
                            id="Shape"
                        ></line>
                        <line
                            x1="5.54166667"
                            y1="8.68"
                            x2="6.41666667"
                            y2="8.68"
                            id="Shape"
                        ></line>
                        <line
                            x1="13.7083333"
                            y1="8.68"
                            x2="12.8333333"
                            y2="8.68"
                            id="Shape"
                        ></line>
                        <line
                            x1="6.25741667"
                            y1="6.00712"
                            x2="7"
                            y2="6.72"
                            id="Shape"
                        ></line>
                        <line
                            x1="12.9925833"
                            y1="6.00712"
                            x2="12.25"
                            y2="6.72"
                            id="Shape"
                        ></line>
                        <path
                            d="M11.6666667,8.68 C11.6666666,7.59752191 10.7525813,6.72000004 9.625,6.72000004 C8.49741865,6.72000004 7.58333336,7.59752191 7.58333333,8.68 C7.51668649,9.35661709 7.85587641,10.0109009 8.45833333,10.36784 L8.45833333,10.64 C8.45833333,10.7946397 8.58891695,10.92 8.75,10.92 L10.5,10.92 C10.6610831,10.92 10.7916667,10.7946397 10.7916667,10.64 L10.7916667,10.36 C11.3934326,10.006221 11.7329104,9.35442367 11.6666667,8.68 Z"
                            id="Shape"
                        ></path>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Pencil = (
    <svg
        width="16px"
        height="16px"
        viewBox="0 0 16 16"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Interface-Essential / Edit / pencil</title>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g id="Icon/base/edit" transform="translate(-2.000000, -2.000000)">
                <g id="Group" transform="translate(3.000000, 3.000000)">
                    <g id="pencil" stroke="#000000">
                        <polygon
                            id="Rectangle-path"
                            transform="translate(7.156339, 6.843995) rotate(45.000000) translate(-7.156339, -6.843995) "
                            points="5.40633872 0.636161603 8.90633872 0.636161603 8.90633872 13.0518283 5.40633872 13.0518283"
                        ></polygon>
                        <polygon
                            id="Shape"
                            points="1.52891667 9.996 0.291666667 13.7083333 4.004 12.4710833"
                        ></polygon>
                        <path
                            d="M12.7831667,3.69133333 L10.3086667,1.21683333 L10.7210833,0.804416667 C11.4077464,0.1412153 12.4992317,0.150700012 13.1742658,0.82573416 C13.8493,1.50076831 13.8587847,2.59225361 13.1955833,3.27891667 L12.7831667,3.69133333 Z"
                            id="Shape"
                        ></path>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const CopyLink = (
    <svg
        width="14px"
        height="16px"
        viewBox="0 0 14 16"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <defs>
            <rect id="path-1" x="0" y="0" width="20" height="20"></rect>
        </defs>
        <g
            id="Page-1"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g id="Settings-4" transform="translate(-528.000000, -355.000000)">
                <g id="Group" transform="translate(106.000000, 320.000000)">
                    <g transform="translate(419.000000, 33.000000)">
                        <g id="Background"></g>
                        <g
                            id="Files-Folders-/-Common-Files-/-common-file-double"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <g
                                transform="translate(3.500000, 3.000000)"
                                id="common-file-double"
                                stroke="#000000"
                            >
                                <path
                                    d="M9.64847619,0.291666667 C9.81264441,0.291666667 9.97007593,0.353179531 10.0861429,0.462583333 L12.1995714,2.45408333 C12.3156734,2.56345409 12.3809524,2.71180303 12.3809524,2.8665 L12.3809524,11.375 C12.3809524,11.6971661 12.1037953,11.9583333 11.7619048,11.9583333 L3.0952381,11.9583333 C2.75334754,11.9583333 2.47619048,11.6971661 2.47619048,11.375 L2.47619048,0.875 C2.47619048,0.552833896 2.75334754,0.291666667 3.0952381,0.291666667 L9.64847619,0.291666667 Z"
                                    id="Shape"
                                ></path>
                                <path
                                    d="M10.5238095,11.9583333 L10.5238095,13.125 C10.5238095,13.4471661 10.2466525,13.7083333 9.9047619,13.7083333 L1.23809524,13.7083333 C0.896204679,13.7083333 0.619047619,13.4471661 0.619047619,13.125 L0.619047619,2.625 C0.619047619,2.3028339 0.896204679,2.04166667 1.23809524,2.04166667 L2.47619048,2.04166667"
                                    id="Shape"
                                ></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const Mainstream = (
    <svg
        width="16px"
        height="17px"
        viewBox="0 0 16 17"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Mainstream</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Mainstream"
                transform="translate(-2.000000, -2.000000)"
                filter="url(#filter-1)"
            >
                <g transform="translate(3.000000, 2.500000)">
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="7"
                        cy="5.40416667"
                        r="1.60416667"
                    ></circle>
                    <path
                        d="M9.04166667,10.0416667 C9.04166667,8.91408532 8.12758135,8.00000004 7,8.00000004 C5.87241865,8.00000004 4.95833333,8.91408532 4.95833333,10.0416667 L4.95833333,11.7916667 L5.88175,11.7916667 L6.125,14.7083333 L7.875,14.7083333 L8.11825,11.7916667 L9.04166667,11.7916667 L9.04166667,10.0416667 Z"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="2.041667"
                        cy="2.1125"
                        r="1.3125"
                    ></circle>
                    <path
                        d="M2.04166667,4.5 C1.07516835,4.5 0.291666667,5.28350169 0.291666667,6.25 L0.291666667,7.70833333 L1.16666667,7.70833333 L1.45833333,10.3333333 L2.625,10.3333333 L2.91666667,7.70833333 L3.79166667,7.70833333 L3.79166667,6.25 C3.79166667,5.28350169 3.00816498,4.5 2.04166667,4.5 Z"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                    <circle
                        id="Oval"
                        stroke="#000000"
                        strokeWidth="0.8"
                        cx="11.9583333"
                        cy="2.1125"
                        r="1.3125"
                    ></circle>
                    <path
                        d="M11.9583333,4.5 C10.991835,4.5 10.2083333,5.28350169 10.2083333,6.25 L10.2083333,7.70833333 L11.0833333,7.70833333 L11.375,10.3333333 L12.5416667,10.3333333 L12.8333333,7.70833333 L13.7083333,7.70833333 L13.7083333,6.25 C13.7083333,5.28350169 12.9248316,4.5 11.9583333,4.5 Z"
                        id="Shape"
                        stroke="#000000"
                        strokeWidth="0.8"
                    ></path>
                </g>
            </g>
        </g>
    </svg>
);

export const AdultContent = (
    <svg
        width="14px"
        height="14px"
        viewBox="0 0 14 14"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>AdultContent</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g
                id="Users-/-Geometric-Close-Up-Single-User-Actions-Man-/-single-man-actions-money"
                transform="translate(-3.000000, -3.000000)"
                filter="url(#filter-1)"
            >
                <g transform="translate(3.000000, 3.000000)" id="Light">
                    <circle
                        id="Oval"
                        stroke="#000000"
                        cx="7"
                        cy="7"
                        r="6.5"
                    ></circle>
                    <text
                        id="18"
                        fontFamily="OpenSans-SemiBold, Open Sans"
                        fontSize="9"
                        fontWeight="500"
                        fill="#000000"
                    >
                        <tspan x="1.5" y="10.5">
                            18
                        </tspan>
                    </text>
                    <polygon
                        id="Rectangle"
                        fill="#000000"
                        transform="translate(6.947235, 6.997235) rotate(-45.000000) translate(-6.947235, -6.997235) "
                        points="0.447234842 6.39723484 13.4472348 6.39723484 13.4472348 7.59723484 0.447234842 7.59723484"
                    ></polygon>
                </g>
            </g>
        </g>
    </svg>
);

export const Eye = (
    <svg
        width="16px"
        height="10px"
        viewBox="0 0 16 10"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Interface-Essential / View / view</title>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <g
                id="Interface-Essential-/-View-/-view"
                transform="translate(-2.000000, -5.000000)"
            >
                <g
                    transform="translate(3.000000, 6.000000)"
                    id="Group"
                    stroke="#000000"
                    strokeWidth="0.8"
                >
                    <g id="view">
                        <path
                            d="M13.7083333,4 C13.7083333,4 10.7053333,7.79166667 7,7.79166667 C3.29466667,7.79166667 0.291666667,4 0.291666667,4 C0.291666667,4 3.29466667,0.208333333 7,0.208333333 C10.7053333,0.208333333 13.7083333,4 13.7083333,4 Z"
                            id="Shape"
                        ></path>
                        <circle id="Oval" cx="7" cy="4" r="2.33333333"></circle>
                        <path
                            d="M7,2.83333333 C7.64433221,2.83333333 8.16666667,3.35566779 8.16666667,4 C8.16666667,4.64433221 7.64433221,5.16666667 7,5.16666667 C6.35566779,5.16666667 5.83333333,4.64433221 5.83333333,4"
                            id="Shape"
                        ></path>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

export const DragDots = (
    <svg
        width="10px"
        height="14px"
        viewBox="0 0 10 14"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Slide dots</title>
        <defs>
            <filter id="filter-dragdots">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g
                id="Slide-dots"
                transform="translate(-5.000000, -3.000000)"
                filter="url(#filter-dragdots)"
            >
                <g transform="translate(5.500000, 3.000000)">
                    <rect
                        id="Rectangle"
                        fill="#000000"
                        x="0"
                        y="0"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                    <rect
                        id="Rectangle-9"
                        fill="#000000"
                        x="0"
                        y="5.25"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                    <rect
                        id="Rectangle-8"
                        fill="#000000"
                        x="5.25"
                        y="5.25"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                    <rect
                        id="Rectangle-10"
                        fill="#000000"
                        x="5.25"
                        y="10.5"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                    <rect
                        id="Rectangle-14"
                        fill="#000000"
                        x="5.25"
                        y="0"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                    <rect
                        id="Rectangle-15"
                        fill="#000000"
                        x="0"
                        y="10.5"
                        width="3.5"
                        height="3.5"
                        rx="0.875"
                    ></rect>
                </g>
            </g>
        </g>
    </svg>
);

export const ButtonClose = (
    <svg
        width="28px"
        height="51px"
        viewBox="0 0 28 51"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Bouton</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g
                id="Layout/Centre-de-Notifications/Publisher-1920-Expand"
                transform="translate(1.000000, -11.000000)"
            >
                <g id="Bouton" transform="translate(-0.499998, 11.000000)">
                    <path
                        d="M-3.51541515,33.0675585 C-0.513612414,33.1176989 1.58874275,32.7966625 2.79165035,32.1044493 C4.59601174,31.0661295 7.77443557,20.5325906 18.3394195,20.4972585 C28.9044035,20.4621052 31.4725332,30.8659105 33.760763,32.1044493 C36.0489928,33.3429881 39.1245862,33.0675585 41.4465638,33.0675585 C41.4972585,34.1408802 41.5099322,34.6174469 41.4845849,34.4972585 L-3.47739406,34.4972585 L-3.51541515,33.0675585 Z"
                        id="Path-2"
                        fill="#FFFFFF"
                        transform="translate(18.990922, 27.506337) rotate(-270.000000) translate(-18.990922, -27.506337) "
                    ></path>
                    <path
                        d="M12.9999977,0.5 C12.9999977,8.5 12.4999977,11 15.7535403,12.5 C19.0241453,14.0078664 25.5244908,18.0755741 25.5244908,26.5 C25.5244908,32.1162839 22.2675073,36.7829506 15.7535403,40.5 C13.9178452,41.5 13.0095776,44.6666667 13.0287373,50"
                        id="Path"
                        stroke="#EBEBEB"
                    ></path>
                    <g transform="translate(0.499998, 15.500000)">
                        <circle
                            id="Oval"
                            stroke="#000000"
                            strokeWidth="0.857142857"
                            fill="#FFFFFF"
                            cx="11"
                            cy="11"
                            r="10.5714286"
                        ></circle>
                        <g
                            id="Group"
                            transform="translate(3.500000, 3.500000)"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        >
                            <g id="close" filter="url(#filter-1)">
                                <g transform="translate(2.250000, 2.250000)">
                                    <line
                                        x1="0.213994565"
                                        y1="0.213566576"
                                        x2="10.0577446"
                                        y2="10.0573166"
                                        id="Shape"
                                        stroke="#FFFFFF"
                                    ></line>
                                    <line
                                        x1="10.0577446"
                                        y1="0.213566576"
                                        x2="0.213994565"
                                        y2="10.0573166"
                                        id="Shape"
                                        stroke="#FFFFFF"
                                    ></line>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

const ButtonOpen = (
    <svg
        width="28px"
        height="50px"
        viewBox="0 0 28 50"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
    >
        <title>Bouton</title>
        <defs>
            <rect id="path-1" x="0" y="0" width="20" height="20"></rect>
            <filter id="filter-3">
                <feColorMatrix
                    in="SourceGraphic"
                    type="matrix"
                    values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"
                ></feColorMatrix>
            </filter>
        </defs>
        <g
            id="Symbols"
            stroke="none"
            strokeWidth="1"
            fill="none"
            fillRule="evenodd"
        >
            <g
                id="Layout/Centre-de-Notifications/Publisher-1920-Retract"
                transform="translate(1.000000, -11.000000)"
            >
                <g id="Bouton" transform="translate(-0.499998, 11.000000)">
                    <path
                        d="M-3.5,33.0703 C-0.498197269,33.1204403 1.6041579,32.7994039 2.80706549,32.1071908 C4.61142689,31.068871 7.78985071,20.535332 18.3548347,20.5 C28.9198186,20.4648466 31.4879483,30.868652 33.7761781,32.1071908 C36.0644079,33.3457296 39.1400014,33.0703 41.4619789,33.0703 C41.4873263,34.0234333 41.5,34.5 41.5,34.5 L-3.46197892,34.5 L-3.5,33.0703 Z"
                        id="Path-2"
                        fill="#FFFFFF"
                        transform="translate(19.000000, 27.500000) rotate(-270.000000) translate(-19.000000, -27.500000) "
                    ></path>
                    <path
                        d="M12.9999977,0.5 C12.9999977,8.5 12.4999977,11 15.7535403,12.5 C19.0241453,14.0078664 25.5244908,18.0755741 25.5244908,26.5 C25.5244908,32.1162839 22.2675073,36.7829506 15.7535403,40.5 C13.9178452,41.5 13.0095776,44.6666667 13.0287373,50"
                        id="Path"
                        stroke="#EBEBEB"
                    ></path>
                    <g transform="translate(0.499998, 15.500000)">
                        <circle
                            id="Oval"
                            stroke="#000000"
                            strokeWidth="0.857142857"
                            fill="#FFFFFF"
                            cx="11"
                            cy="11"
                            r="10.5714286"
                        ></circle>
                        <g id="Group" transform="translate(1.000000, 1.000000)">
                            <mask id="mask-2" fill="white">
                                <use href="#path-1"></use>
                            </mask>
                            <g id="Background"></g>
                            <g
                                filter="url(#filter-3)"
                                id="Interface-Essential-/-Lists-/-list-bullets"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                            >
                                <g mask="url(#mask-2)">
                                    <g
                                        transform="translate(5.000000, 5.000000)"
                                        id="Group"
                                    >
                                        <line
                                            x1="0"
                                            y1="0.5"
                                            x2="10"
                                            y2="0.5"
                                            id="Shape"
                                            stroke="#000000"
                                            strokeWidth="0.8"
                                        ></line>
                                        <line
                                            x1="0"
                                            y1="5"
                                            x2="10"
                                            y2="5"
                                            id="Shape"
                                            stroke="#000000"
                                            strokeWidth="0.8"
                                        ></line>
                                        <line
                                            x1="0"
                                            y1="9.5"
                                            x2="10"
                                            y2="9.5"
                                            id="Shape"
                                            stroke="#000000"
                                            strokeWidth="0.8"
                                        ></line>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
);

const MoreDetails = (
    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
        <title>Icon/base/search-glass</title>
        <defs>
            <filter id="filter-1">
                <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
            </filter>
        </defs>
        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
            <g id="Icon/base/search-glass" transform="translate(-2.000000, -2.000000)" filter="url(#filter-1)">
                <g transform="translate(3.000000, 3.000000)" id="search-remove-1" stroke="#000000">
                    <g id="Group-5" transform="translate(6.125000, 6.773181) rotate(-315.000000) translate(-6.125000, -6.773181) translate(3.625000, 4.273181)">
                        <line x1="0" y1="0" x2="4.08333333" y2="4.08333333" id="Shape"></line>
                        <line x1="4.08333333" y1="0" x2="0" y2="4.08333333" id="Shape"></line>
                    </g>
                    <circle id="Oval" cx="6.125" cy="6.125" r="5.83333333"></circle>
                    <line x1="13.7083333" y1="13.7083333" x2="10.24975" y2="10.24975" id="Shape"></line>
                </g>
            </g>
        </g>
    </svg>
)

const ImageFile = (
    <svg width="15px" height="18px" viewBox="0 0 15 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
    <title>Files-Folders / Common-Files / common-file-empty</title>
    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g id="Icon/base/img-file" transform="translate(-2.000000, -1.000000)" stroke="#000000">
            <g id="common-file-empty" transform="translate(3.500000, 2.500000)">
                <path d="M12.1624458,2.6006192 C12.2843819,2.72251849 12.3529412,2.88786096 12.3529412,3.06027864 L12.3529412,14.3034056 C12.3529412,14.6624762 12.061857,14.9535604 11.7027864,14.9535604 L0.650154799,14.9535604 C0.291084218,14.9535604 0,14.6624762 0,14.3034056 L0,0.650154799 C0,0.291084218 0.291084218,0 0.650154799,0 L9.29266254,0 C9.46508022,0 9.63042268,0.06855923 9.75232198,0.190495356 L12.1624458,2.6006192 Z" id="Shape"></path>
                <path d="M2.4380805,11.7027864 L4.03095975,9.65414861 C4.23744926,9.41347844 4.58744237,9.35688614 4.85925697,9.52021672 L6.01393189,10.4024768 L7.09513932,9.01114551 C7.22482831,8.8556926 7.41953,8.76952356 7.62179696,8.77806287 C7.82406391,8.78660218 8.01080995,8.88887519 8.12693498,9.05470588 L9.91486068,11.7027864 L2.4380805,11.7027864 Z" id="Shape" strokeWidth="0.8"></path>
                <circle id="Oval" strokeWidth="0.8" cx="3.41331269" cy="6.82662539" r="1"></circle>
            </g>
        </g>
    </g>
</svg>
)

const ZipFile = (
    <svg width="14px" height="16px" viewBox="0 0 14 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
        <title>zip</title>
        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
            <g id="Icon/base/zip-file" transform="translate(-3.000000, -2.000000)" stroke="#000000">
                <g id="zip-file-1" transform="translate(3.000000, 2.000000)">
                    <path d="M5.54166667,11.7916667 L4.08333333,11.7916667 C3.97583908,11.7916111 3.87708865,11.7324337 3.82635125,11.637667 C3.77561385,11.5429003 3.78111483,11.4279074 3.84066667,11.3384167 L5.49266667,8.74491667 C5.5522185,8.65542591 5.55771949,8.54043307 5.50698209,8.44566634 C5.45624469,8.35089961 5.35749425,8.2917222 5.25,8.29166667 L3.79166667,8.29166667" id="Shape" strokeWidth="0.7"></path>
                    <line x1="7.29166667" y1="8.29166667" x2="7.29166667" y2="11.7916667" id="Shape" strokeWidth="0.8"></line>
                    <line x1="6.70833333" y1="11.7916667" x2="7.875" y2="11.7916667" id="Shape" strokeWidth="0.8"></line>
                    <line x1="6.70833333" y1="8.29166667" x2="7.875" y2="8.29166667" id="Shape" strokeWidth="0.8"></line>
                    <path d="M10.7916667,9.16666667 C10.7916667,9.64991582 10.3999158,10.0416667 9.91666667,10.0416667 L9.04166667,10.0416667 L9.04166667,8.29166667 L9.91666667,8.29166667 C10.3999158,8.29166667 10.7916667,8.68341751 10.7916667,9.16666667 L10.7916667,9.16666667 Z" id="Shape" strokeWidth="0.8"></path>
                    <line x1="9.04166667" y1="11.7916667" x2="9.04166667" y2="10.0416667" id="Shape" strokeWidth="0.8"></line>
                    <path d="M12.9139878,3.1006192 C13.0349368,3.22251849 13.102941,3.38786096 13.102941,3.56027864 L13.102941,14.8034056 C13.102941,15.1624762 12.8142132,15.4535604 12.4580494,15.4535604 L1.49489163,15.4535604 C1.13872782,15.4535604 0.85,15.1624762 0.85,14.8034056 L0.85,1.1501548 C0.85,0.791084218 1.13872782,0.5 1.49489163,0.5 L10.0674361,0.5 C10.238458,0.5 10.402462,0.56855923 10.5233745,0.690495356 L12.9139878,3.1006192 Z" id="Shape"></path>
                </g>
            </g>
        </g>
    </svg>
)


export default {
    Add,
    AdultContent,
    ArrowBottom,
    Business,
    ArrowLeft,
    ArrowRight,
    ArrowTop,
    BulletList,
    BrowserIdea,
    Calendar,
    CloseQuote,
    CloseRed,
    ChangePassword,
    CheckValid,
    CheckGreen,
    ChevronDown,
    ChevronLeft,
    ChevronRight,
    ChevronUp,
    Close,
    Delete,
    Download,
    DragDots,
    ErrorSystem,
    HourGlass,
    Information,
    Like,
    Loading,
    Lock,
    Mainstream,
    MessageBubble,
    NetworksInformation,
    OpenQuote,
    Placeholder,
    Rocket,
    SelectArrow,
    SearchGlass,
    SearchTag,
    Supp,
    ToLeft,
    ToRight,
    Upload,
    UserPicture,
    UserSingle,
    Pencil,
    CopyLink,
    Eye,
    ButtonClose,
    ButtonOpen,
    MoreDetails,
    ImageFile,
    ZipFile,
};
