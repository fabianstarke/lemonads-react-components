import React from 'react';

export const Billing = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <title>Icon/menu/billing</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/billing" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g filter="url(#filter-1)" id="money-atm">
            <g transform="translate(2.000000, 5.000000)">
                <path d="M15.14,5 L18.75,5 C19.2102373,5 19.5833333,4.62690396 19.5833333,4.16666667 L19.5833333,0.833333333 C19.5833333,0.373096042 19.2102373,0 18.75,0 L1.25,0 C0.789762708,0 0.416666667,0.373096042 0.416666667,0.833333333 L0.416666667,4.16666667 C0.416666667,4.62690396 0.789762708,5 1.25,5 L4.86666667,5" id="Shape" stroke="#000000" strokeWidth="1.11111111"></path>
                <path d="M16.25003,11.245 C16.2513365,11.3563706 16.2080286,11.4636405 16.1297431,11.5428654 C16.0514576,11.6220903 15.944712,11.6666667 15.8333333,11.6666667 L4.1725,11.6666667 C4.06112134,11.6666667 3.95437571,11.6220903 3.87609022,11.5428654 C3.79780473,11.4636405 3.75449679,11.3563706 3.75580329,11.245 C3.80083333,7.61666667 4.43833333,6 5.83333333,2.5 L14.1666667,2.5 C15.5691667,6 16.2058333,7.6175 16.25003,11.245 Z" id="Shape" stroke="#000000" strokeWidth="1.11111111"></path>
                <line x1="4.58666667" y1="13.3333333" x2="15.42" y2="13.3333333" id="Shape" stroke="#000000" strokeWidth="1.11111111"></line>
                <line x1="3.33666667" y1="2.5" x2="17.5033333" y2="2.5" id="Shape" stroke="#000000" strokeWidth="1.11111111"></line>
                <line x1="10.0033333" y1="4.58333333" x2="10.0033333" y2="3.75" id="Shape" stroke="#000000" strokeWidth="1.11111111"></line>
                <path d="M11.25,4.58333333 L9.55916667,4.58333333 C9.02364844,4.58370736 8.56355565,4.96366088 8.46193447,5.4894489 C8.36031328,6.01523693 8.64568887,6.53927015 9.1425,6.73916667 L10.8625,7.4275 C11.3593111,7.62739652 11.6446867,8.15142974 11.5430655,8.67721776 C11.4414443,9.20300579 10.9813516,9.58295931 10.4458333,9.58333333 L8.75,9.58333333" id="Shape" stroke="#000000" strokeWidth="1.11111111"></path>
                <line x1="10.0033333" y1="10.4166667" x2="10.0033333" y2="9.58333333" id="Shape" stroke="#000000" strokeWidth="1.11111111"></line>
            </g>
        </g>
    </g>
</svg>
);

export const Conversion = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <title>Icon/menu/conversion</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/conversion" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g filter="url(#filter-1)" id="business-climb-top">
            <g transform="translate(2.000000, 2.000000)">
                <path d="M3.54166667,19.5833333 L10.625,8.33333333 C10.8235096,8.07381169 11.1315956,7.92158823 11.4583333,7.92158823 C11.7850711,7.92158823 12.093157,8.07381169 12.2916667,8.33333333 L19.375,19.5833333 L3.54166667,19.5833333 Z" id="Shape" stroke="#000000" strokeWidth="0.985"></path>
                <path d="M3.54166667,19.5833333 L0.625,19.5833333 L3.54166667,15 C3.74553923,14.7464163 4.04980949,14.5942812 4.375,14.5833333 C4.70019051,14.5942812 5.00446077,14.7464163 5.20833333,15 L5.75,16.0833333" id="Shape" stroke="#000000" strokeWidth="0.985"></path>
                <path d="M7.25416667,13.6866667 L9.20666667,15.3383333 C9.53767029,15.6181034 10.0278668,15.5974521 10.3341667,15.2908333 L10.8691667,14.7558333 C11.1945832,14.4305151 11.7220835,14.4305151 12.0475,14.7558333 L12.5825,15.2908333 C12.8887999,15.5974521 13.3789964,15.6181034 13.71,15.3383333 L15.6266667,13.7191667" id="Shape" stroke="#000000" strokeWidth="0.985"></path>
                <line x1="11.4583333" y1="7.91666667" x2="11.4583333" y2="0.416666667" id="Shape" stroke="#000000" strokeWidth="0.985"></line>
                <path d="M11.4583333,0.416666667 L17.2583333,0.416666667 C17.4183281,0.416894424 17.5640621,0.508715598 17.6333315,0.65293808 C17.7026009,0.797160563 17.6831703,0.968309532 17.5833333,1.09333333 L16.6666667,2.24 C16.5452357,2.39206583 16.5452357,2.60793417 16.6666667,2.76 L17.5833333,3.90666667 C17.6831703,4.03169047 17.7026009,4.20283944 17.6333315,4.34706192 C17.5640621,4.4912844 17.4183281,4.58310558 17.2583333,4.58333333 L11.4583333,4.58333333" id="Shape" stroke="#000000" strokeWidth="0.985"></path>
            </g>
        </g>
    </g>
</svg>
);

export const DashBoard = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <title>Icon/menu/dashboard</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/dashboard" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g filter="url(#filter-1)" id="Icon/dashboard">
            <g transform="translate(2.000000, 2.000000)">
                <path d="M18,12 C19.1045695,12 20,12.8954305 20,14 L20,18 C20,19.1045695 19.1045695,20 18,20 L14,20 C12.8954305,20 12,19.1045695 12,18 L12,14 C12,12.8954305 12.8954305,12 14,12 L18,12 Z M18,13 L14,13 C13.4477153,13 13,13.4477153 13,14 L13,14 L13,18 C13,18.5522847 13.4477153,19 14,19 L14,19 L18,19 C18.5522847,19 19,18.5522847 19,18 L19,18 L19,14 C19,13.4477153 18.5522847,13 18,13 L18,13 Z" id="Combined-Shape" fill="#0036FF" fillRule="nonzero"></path>
                <path d="M6,12 C7.1045695,12 8,12.8954305 8,14 L8,18 C8,19.1045695 7.1045695,20 6,20 L2,20 C0.8954305,20 9.09629973e-13,19.1045695 9.09494702e-13,18 L9.09494702e-13,14 C9.09359431e-13,12.8954305 0.8954305,12 2,12 L6,12 Z M6,13 L2,13 C1.44771525,13 1,13.4477153 1,14 L1,14 L1,18 C1,18.5522847 1.44771525,19 2,19 L2,19 L6,19 C6.55228475,19 7,18.5522847 7,18 L7,18 L7,14 C7,13.4477153 6.55228475,13 6,13 L6,13 Z" id="Combined-Shape" fill="#0036FF" fillRule="nonzero"></path>
                <path d="M18,-4.4408921e-16 C19.1045695,-6.46995335e-16 20,0.8954305 20,2 L20,6 C20,7.1045695 19.1045695,8 18,8 L14,8 C12.8954305,8 12,7.1045695 12,6 L12,2 C12,0.8954305 12.8954305,-2.41183085e-16 14,-4.4408921e-16 L18,-4.4408921e-16 Z M18,1 L14,1 C13.4477153,1 13,1.44771525 13,2 L13,2 L13,6 C13,6.55228475 13.4477153,7 14,7 L14,7 L18,7 C18.5522847,7 19,6.55228475 19,6 L19,6 L19,2 C19,1.44771525 18.5522847,1 18,1 L18,1 Z" id="Combined-Shape" fill="#0036FF" fillRule="nonzero"></path>
                <path d="M6,-4.4408921e-16 C7.1045695,-6.46995335e-16 8,0.8954305 8,2 L8,6 C8,7.1045695 7.1045695,8 6,8 L2,8 C0.8954305,8 1.3527075e-16,7.1045695 0,6 L0,2 C-1.3527075e-16,0.8954305 0.8954305,-2.41183085e-16 2,-4.4408921e-16 L6,-4.4408921e-16 Z M6,1 L2,1 C1.44771525,1 1,1.44771525 1,2 L1,2 L1,6 C1,6.55228475 1.44771525,7 2,7 L2,7 L6,7 C6.55228475,7 7,6.55228475 7,6 L7,6 L7,2 C7,1.44771525 6.55228475,1 6,1 L6,1 Z" id="Combined-Shape" fill="#0036FF" fillRule="nonzero"></path>
            </g>
        </g>
    </g>
</svg>
);

export const MarketPlace = (
<svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <title>Icon/menu/marketplace</title>
    <defs>
        <rect id="path-1" x="0" y="0" width="24" height="24"></rect>
        <filter id="filter-3">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/marketplace" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <mask id="mask-2" fill="white">
            <use xlinkHref="#path-1"></use>
        </mask>
        <g id="Background"></g>
        <g filter="url(#filter-3)" id="Icon/marketplace/Inactive" strokeLinecap="round" strokeLinejoin="round">
            <g transform="translate(1.000000, 2.000000)">
                <path d="M2.689,0 C2.27423996,0.000201573379 1.90265454,0.256412503 1.755,0.644 L0.174,4.792 C0.00451938799,5.23726302 -0.00044519365,5.72840199 0.16,6.177 C0.611903705,7.4349181 1.90259355,8.1859843 3.21952588,7.95737076 C4.53645821,7.72875723 5.49849772,6.58662734 5.5,5.25 C5.5,6.76878306 6.73121694,8 8.25,8 C9.76878306,8 11,6.76878306 11,5.25 C11,6.76878306 12.2312169,8 13.75,8 C15.2687831,8 16.5,6.76878306 16.5,5.25 C16.5015023,6.58662734 17.4635418,7.72875723 18.7804741,7.95737076 C20.0974064,8.1859843 21.3880963,7.4349181 21.84,6.177 C22.0006541,5.72837209 21.9953329,5.23704322 21.825,4.792 L20.245,0.644 C20.0973455,0.256412503 19.72576,0.000201573379 19.311,0 L2.689,0 Z" id="Shape" stroke="#000000"></path>
                <rect id="Rectangle-path" stroke="#000000" x="3.957" y="11" width="7" height="6" rx="0.5"></rect>
                <path d="M12.957,20 L12.957,11.5 C12.957,11.2238576 13.1808576,11 13.457,11 L17.457,11 C17.7331424,11 17.957,11.2238576 17.957,11.5 L17.957,20" id="Shape" stroke="#000000"></path>
                <path d="M19.957,7.908 L19.957,19 C19.957,19.5522847 19.5092847,20 18.957,20 L2.957,20 C2.40471525,20 1.957,19.5522847 1.957,19 L1.957,7.884" id="Shape" stroke="#000000"></path>
            </g>
        </g>
    </g>
</svg>
);

export const MyLinks = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <title>Icon/menu/my links</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/my-links" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g filter="url(#filter-1)" id="hyperlink-2">
            <g transform="translate(2.000000, 2.000000)">
                <path d="M12.3333333,10.4666667 L12.4166667,10.55 C13.1466139,11.27991 14.3300527,11.27991 15.06,10.55 L18.765,6.84166667 C19.8541487,5.74438429 19.8541487,3.97394905 18.765,2.87666667 L17.1216667,1.23416667 C16.0254207,0.143293051 14.253746,0.143293051 13.1575,1.23416667 L9.45166667,4.93916667 C8.72175669,5.66911393 8.72175669,6.85255273 9.45166667,7.5825 L9.535,7.66583333" id="Shape" stroke="#000000"></path>
                <path d="M7.66666667,9.53166667 L7.58333333,9.44833333 C6.85338607,8.71842335 5.66994727,8.71842335 4.94,9.44833333 L1.23416667,13.1566667 C0.144108186,14.253574 0.144108186,16.0247593 1.23416667,17.1216667 L2.87666667,18.7641667 C3.9728208,19.8560502 5.74551254,19.8560502 6.84166667,18.7641667 L10.5466667,15.0591667 C10.8975917,14.7088498 11.0947905,14.2333531 11.0947905,13.7375 C11.0947905,13.2416469 10.8975917,12.7661502 10.5466667,12.4158333 L10.4633333,12.3325" id="Shape" stroke="#000000"></path>
                <line x1="6.365" y1="13.6341667" x2="13.6333333" y2="6.36416667" id="Shape" stroke="#000000"></line>
            </g>
        </g>
    </g>
</svg>
);

export const Profile = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <title>Icon/menu/profile</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
        <circle id="path-2" cx="12.4615385" cy="12.6153846" r="12"></circle>
    </defs>
    <g id="Icon/menu/profile" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g filter="url(#filter-1)" id="Avatar-single">
            <g transform="translate(-0.461538, -0.615385)">
                <mask id="mask-3" fill="white">
                    <use xlinkHref="#path-2"></use>
                </mask>
                <circle stroke="#000000" strokeWidth="0.923076923" cx="12.4615385" cy="12.6153846" r="11.5384615"></circle>
                <circle id="Oval" stroke="#000000" strokeWidth="0.923076923" strokeLinecap="round" strokeLinejoin="round" mask="url(#mask-3)" cx="12.4615385" cy="12.3846154" r="3.92307692"></circle>
                <path d="M6,24.1538462 C6,20.585237 8.89292935,17.6923078 12.4615385,17.6923078 C16.0301476,17.6923078 18.9230769,20.585237 18.9230769,24.1538462 L6,24.1538462 Z" id="Shape" stroke="#000000" strokeWidth="0.923076923" strokeLinecap="round" strokeLinejoin="round" mask="url(#mask-3)"></path>
            </g>
        </g>
    </g>
</svg>
);

export const Report = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <title>Icon/menu/report</title>
    <defs>
        <rect id="path-1" x="0" y="0" width="24" height="24"></rect>
    </defs>
    <g id="Icon/menu/report" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <mask id="mask-2" fill="white">
            <use xlinkHref="#path-1"></use>
        </mask>
        <g id="Background"></g>
        <g id="office-file-text-graph" strokeLinecap="round" strokeLinejoin="round">
            <g transform="translate(3.000000, 2.000000)">
                <ellipse id="Oval" stroke="#000000" cx="6.890625" cy="6.66583333" rx="3.375" ry="3.33333333"></ellipse>
                <polyline id="Shape" stroke="#000000" points="4.08178125 8.515 6.890625 6.66583333 6.890625 3.3325"></polyline>
                <path d="M16.7684063,3.75 C16.926651,3.90624394 17.015625,4.11817099 17.015625,4.33916667 L17.015625,18.75 C17.015625,19.2102373 16.6378653,19.5833333 16.171875,19.5833333 L1.828125,19.5833333 C1.36213474,19.5833333 0.984375,19.2102373 0.984375,18.75 L0.984375,1.25 C0.984375,0.789762708 1.36213474,0.41666533 1.828125,0.41666533 L13.0440938,0.41666533 C13.2676099,0.416271531 13.4821521,0.503486542 13.640625,0.659166667 L16.7684063,3.75 Z" id="Shape" stroke="#000000"></path>
                <line x1="12.375" y1="4.5825" x2="14.484375" y2="4.5825" id="Shape" stroke="#000000"></line>
                <line x1="3.515625" y1="12.0825" x2="14.484375" y2="12.0825" id="Shape" stroke="#000000"></line>
                <line x1="3.515625" y1="14.5825" x2="14.484375" y2="14.5825" id="Shape" stroke="#000000"></line>
                <line x1="3.515625" y1="17.0825" x2="14.484375" y2="17.0825" id="Shape" stroke="#000000"></line>
                <line x1="12.375" y1="7.0825" x2="14.484375" y2="7.0825" id="Shape" stroke="#000000"></line>
                <line x1="12.375" y1="9.5825" x2="14.484375" y2="9.5825" id="Shape" stroke="#000000"></line>
            </g>
        </g>
    </g>
</svg>
);

export const Settings = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <title>Icon/menu/settings</title>
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/settings" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g filter="url(#filter-1)" id="setting-cursor">
            <g transform="translate(2.000000, 2.000000)">
                <line x1="10" y1="11.3333333" x2="10" y2="0" id="Shape" stroke="#000000"></line>
                <line x1="10" y1="20" x2="10" y2="16.6666667" id="Shape" stroke="#000000"></line>
                <circle id="Oval" stroke="#000000" cx="10" cy="14" r="2.66666667"></circle>
                <line x1="2.66666667" y1="10" x2="2.66666667" y2="20" id="Shape" stroke="#000000"></line>
                <line x1="2.66666667" y1="0" x2="2.66666667" y2="4.66666667" id="Shape" stroke="#000000"></line>
                <circle id="Oval" stroke="#000000" cx="2.66666667" cy="7.33333333" r="2.66666667"></circle>
                <line x1="17.3333333" y1="10" x2="17.3333333" y2="20" id="Shape" stroke="#000000"></line>
                <line x1="17.3333333" y1="0" x2="17.3333333" y2="4.66666667" id="Shape" stroke="#000000"></line>
                <circle id="Oval" stroke="#000000" cx="17.3333333" cy="7.33333333" r="2.66666667"></circle>
            </g>
        </g>
    </g>
</svg>
);

export const Smartlinks = (
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" >
    <title>Icon/menu/smartlinks</title>
    <defs>
        <rect id="path-1" x="0" y="0" width="24" height="24"></rect>
        <filter id="filter-3">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Icon/menu/smartlinks" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <mask id="mask-2" fill="white">
            <use xlinkHref="#path-1"></use>
        </mask>
        <g id="Background"></g>
        <g filter="url(#filter-3)" id="app-window-link" strokeLinecap="round" strokeLinejoin="round">
            <g transform="translate(2.000000, 2.000000)">
                <path d="M16.6842967,15.5495652 L18.6371355,13.7052174 C19.1024215,13.2659361 19.284224,12.6255867 19.11406,12.0253827 C18.943896,11.4251787 18.4476173,10.9563053 17.8121674,10.7953827 C17.1767175,10.63446 16.4986363,10.805936 16.0333504,11.2452174 L14.0805115,13.0904348" id="Shape" stroke="#000000"></path>
                <path d="M12.7776982,14.32 L10.8257801,16.1643478 C10.1062562,16.843418 10.105844,17.9447974 10.8248594,18.6243478 C11.5438748,19.3038981 12.7100413,19.3042875 13.4295652,18.6252174 L15.3824041,16.7826087" id="Shape" stroke="#000000"></path>
                <line x1="16.3592839" y1="13.3982609" x2="13.1036317" y2="16.4721739" id="Shape" stroke="#000000"></line>
                <line x1="0" y1="3.63130435" x2="18.4143223" y2="3.63130435" id="Shape" stroke="#000000"></line>
                <path d="M2.76214834,1.67478261 C2.63502397,1.67478261 2.53196931,1.77211201 2.53196931,1.89217391 C2.53196931,2.01223582 2.63502397,2.10956522 2.76214834,2.10956522 C2.8892727,2.10956522 2.99232737,2.01223582 2.99232737,1.89217391 C2.99232737,1.77211201 2.8892727,1.67478261 2.76214834,1.67478261" id="Shape" stroke="#000000"></path>
                <path d="M4.60358056,1.67478261 C4.4764562,1.67478261 4.37340153,1.77211201 4.37340153,1.89217391 C4.37340153,2.01223582 4.4764562,2.10956522 4.60358056,2.10956522 C4.73070493,2.10956522 4.83375959,2.01223582 4.83375959,1.89217391 C4.83375959,1.77211201 4.73070493,1.67478261 4.60358056,1.67478261" id="Shape" stroke="#000000"></path>
                <path d="M6.44501279,1.67478261 C6.31788842,1.67478261 6.21483376,1.77211201 6.21483376,1.89217391 C6.21483376,2.01223582 6.31788842,2.10956522 6.44501279,2.10956522 C6.57213715,2.10956522 6.67519182,2.01223582 6.67519182,1.89217391 C6.67519182,1.77211201 6.57213715,1.67478261 6.44501279,1.67478261" id="Shape" stroke="#000000"></path>
                <path d="M7.3657289,14.066087 L1.84143223,14.066087 C0.824437289,14.066087 0,13.2874517 0,12.3269565 L0,1.89217391 C0,0.931678696 0.824437289,0.153043478 1.84143223,0.153043478 L16.57289,0.153043478 C17.589885,0.153043478 18.4143223,0.931678696 18.4143223,1.89217391 L18.4143223,7.10956522" id="Shape" stroke="#000000"></path>
            </g>
        </g>
    </g>
</svg>
);

export const ContentWrite = (
<svg width="36px" height="36px" viewBox="0 0 36 36" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <filter id="filter-1">
            <feColorMatrix in="SourceGraphic" type="matrix" values="0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 0 0.000000 0 0 0 1.000000 0"></feColorMatrix>
        </filter>
    </defs>
    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g id="Onboarding-1.2" transform="translate(-942.000000, -366.000000)">
            <g id="Icon" transform="translate(935.000000, 359.000000)">
                <g id="Content-/-Content-Creation-/-content-write" transform="translate(7.000000, 7.000000)" filter="url(#filter-1)">
                    <g transform="translate(0.500000, 0.500000)" id="Group" stroke="#000000" strokeWidth="1.16666667">
                        <g id="content-write">
                            <path d="M18.2291667,29.8958333 L2.1875,29.8958333 C1.38208474,29.8958333 0.729166667,29.2429153 0.729166667,28.4375 L0.729166667,2.1875 C0.729166667,1.38208474 1.38208474,0.729166667 2.1875,0.729166667 L22.6041667,0.729166667 C23.4095819,0.729166667 24.0625,1.38208474 24.0625,2.1875 L24.0625,9.47916667" id="Shape"></path>
                            <line x1="9.47916667" y1="5.10416667" x2="18.2291667" y2="5.10416667" id="Shape"></line>
                            <line x1="9.47916667" y1="9.47916667" x2="18.2291667" y2="9.47916667" id="Shape"></line>
                            <line x1="9.47916667" y1="13.8541667" x2="10.9375" y2="13.8541667" id="Shape"></line>
                            <path d="M5.10416667,4.73958333 C5.30552048,4.73958333 5.46875,4.90281285 5.46875,5.10416667 C5.46875,5.30552048 5.30552048,5.46875 5.10416667,5.46875 C4.90281285,5.46875 4.73958333,5.30552048 4.73958333,5.10416667 C4.73958333,4.90281285 4.90281285,4.73958333 5.10416667,4.73958333" id="Shape"></path>
                            <path d="M5.10416667,9.11458333 C5.30552048,9.11458333 5.46875,9.27781285 5.46875,9.47916667 C5.46875,9.68052048 5.30552048,9.84375 5.10416667,9.84375 C4.90281285,9.84375 4.73958333,9.68052048 4.73958333,9.47916667 C4.73958333,9.27781285 4.90281285,9.11458333 5.10416667,9.11458333" id="Shape"></path>
                            <path d="M5.10416667,13.4895833 C5.30552048,13.4895833 5.46875,13.6528129 5.46875,13.8541667 C5.46875,14.0555205 5.30552048,14.21875 5.10416667,14.21875 C4.90281285,14.21875 4.73958333,14.0555205 4.73958333,13.8541667 C4.73958333,13.6528129 4.90281285,13.4895833 5.10416667,13.4895833" id="Shape"></path>
                            <path d="M21.3047917,15.098125 C21.0850765,14.9413101 20.8534722,14.80186 20.6120833,14.6810417 L19.0079167,13.8804167 C18.0782989,13.4153634 16.967753,13.502344 16.121875,14.1064583 L13.6252083,15.8958333 C12.840059,16.4569725 12.3184583,17.3147159 12.1814583,18.27 L11.80375,20.9227083" id="Shape"></path>
                            <line x1="20.4166667" y1="25.5208333" x2="24.7916667" y2="34.2708333" id="Shape"></line>
                            <polyline id="Shape" points="9.87875 22.0995833 7.29166667 26.25 12.1654167 25.83"></polyline>
                            <path d="M17.4285417,22.6041667 L25.843125,17.4489583 C26.8579698,16.8114099 27.1708413,15.4759504 26.5447901,14.4539728 C25.918739,13.4319953 24.5868967,13.1040677 23.5579167,13.7185417 L9.87875,22.0995833 L12.1654167,25.83 L14.608125,24.3322917" id="Shape"></path>
                            <path d="M21.1458333,22.6041667 L16.0416667,22.6041667 C15.2362514,22.6041667 14.5833333,23.2570847 14.5833333,24.0625 C14.5833333,24.8679153 15.2362514,25.5208333 16.0416667,25.5208333 L22.6041667,25.5208333" id="Shape"></path>
                            <line x1="34.2708333" y1="34.2708333" x2="24.0070833" y2="18.5733333" id="Shape"></line>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);

export const Tools = (
    <svg width="22px" height="17px" viewBox="0 0 22 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
        <title>Icon/menu/my links</title>
        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
            <g id="Icon/menu/Tools" transform="translate(-1.000000, -4.000000)" stroke="#000000">
                <g id="tool-box" transform="translate(2.416667, 4.750000)">
                    <line x1="0" y1="7.50083333" x2="8.33333333" y2="7.50083333" id="Shape" strokeWidth="0.8"></line>
                    <line x1="11.0833333" y1="7.50083333" x2="19.1666667" y2="7.50083333" id="Shape"></line>
                    <rect id="Rectangle-path" x="0" y="3.33416667" width="19.1666667" height="11.6666667" rx="0.833333333"></rect>
                    <path d="M10.8333333,8.75 C10.8333333,9.44035594 10.2736893,10 9.58333333,10 C8.8929774,10 8.33333333,9.44035594 8.33333333,8.75 L8.33333333,6.66666667 L10.8333333,6.66666667 L10.8333333,8.75 Z" id="Shape" stroke-width="0.8"></path>
                    <path d="M6.25,3.33333333 C6.25,1.49238417 7.74238417,0 9.58333333,0 C11.4242825,0 12.9166667,1.49238417 12.9166667,3.33333333" id="Shape"></path>
                </g>
            </g>
        </g>
    </svg>
    )

export default { Billing, Conversion, DashBoard, MarketPlace, MyLinks, Profile, Report, Settings, Smartlinks, ContentWrite, Tools }
