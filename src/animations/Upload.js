import React, { useState } from "react";
import { Lottie } from "@crello/react-lottie";

import upload from "./json/upload.json";

const Upload = ({ children, ...props }) => {
    const [playing, setPlaying] = useState("stopped");

    const defaultOptions = {
        loop: true,
        autoplay: false,
        animationData: upload,
    };

    return (
        <div
            onMouseLeave={() => setPlaying("stopped")}
            onMouseEnter={() => setPlaying("playing")}
            {...props}
        >
            <Lottie
                config={defaultOptions}
                height={20}
                width={20}
                playingState={playing}
            />
            {children && children}
        </div>
    );
};

export default Upload;
