import React from "react";
import { Lottie } from "@crello/react-lottie";
import "../../style/index.css";

import error from "./json/error.json";
import valid from "./json/valid.json";
import info from "./json/info.json";
import warning from "./json/warning.json";

const ErrorAnim = ({ timeout }) => {
    const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: error,
        rendererSettings: { preserveAspectRatio: "none" },
    };

    return (
        <Lottie
            height={4}
            className="notification-animation"
            config={defaultOptions}
            speed={5 / timeout}
        />
    );
};

const ValidAnim = ({ timeout }) => {
    const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: valid,
        rendererSettings: { preserveAspectRatio: "none" },
    };

    return (
        <Lottie
            height={4}
            className="notification-animation"
            config={defaultOptions}
            speed={5 / timeout}
        />
    );
};

const InfoAnim = ({ timeout }) => {
    const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: info,
        rendererSettings: { preserveAspectRatio: "none" },
    };

    return (
        <Lottie
            height={4}
            className="notification-animation"
            config={defaultOptions}
            speed={5 / timeout}
        />
    );
};

const WarningAnim = ({ timeout }) => {
    const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: warning,
        rendererSettings: { preserveAspectRatio: "none" },
    };

    return (
        <Lottie
            height={4}
            className="notification-animation"
            config={defaultOptions}
            speed={5 / timeout}
        />
    );
};

export { ErrorAnim, ValidAnim, WarningAnim, InfoAnim };
