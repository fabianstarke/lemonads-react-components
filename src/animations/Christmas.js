import React, { useState } from "react";
import { Lottie } from "@crello/react-lottie";

import christmas from "./json/christmas.json";

const Christmas = ({ animationClassName, ...props }) => {
    const defaultOptions = {
        loop: false,
        autoplay: true,
        animationData: christmas,
    };

    return (
        <div
            {...props}
        >
            <Lottie
                speed={1}
                config={defaultOptions}

            />
        </div>
    );
};

export default Christmas;
