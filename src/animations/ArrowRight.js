import React, { useState } from "react";
import { Lottie } from "@crello/react-lottie";

import arrow from "./json/arrow_right.json";

const Close = ({ animationClassName, ...props }) => {
    const [direction, setDirection] = useState(-1);
    const [playingState, setPlayingState] = useState("stopped");

    const defaultOptions = {
        loop: false,
        autoplay: false,
        animationData: arrow,
    };

    return (
        <div
            onMouseLeave={() => {
                setDirection(-1);
                setPlayingState("playing");
            }}
            onMouseEnter={() => {
                setDirection(1);
                setPlayingState("playing");
            }}
            {...props}
        >
            <Lottie
                speed={2}
                config={defaultOptions}
                height={20}
                width={20}
                direction={direction}
                playingState={playingState}
            />
        </div>
    );
};

export default Close;
