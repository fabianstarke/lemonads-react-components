import React, { useState } from "react";
import { Lottie } from "@crello/react-lottie";
import PropTypes from "prop-types";

import download from "./json/download.json";

const Download = ({ children, height, width, ...props }) => {
    const [playing, setPlaying] = useState("stopped");

    const defaultOptions = {
        loop: true,
        autoplay: false,
        animationData: download,
    };

    return (
        <div
            onMouseLeave={() => setPlaying("stopped")}
            onMouseEnter={() => setPlaying("playing")}
            {...props}
        >
            {children && children}
            <Lottie
                config={defaultOptions}
                height={height}
                width={width}
                playingState={playing}
            />
        </div>
    );
};

Download.propTypes = {
    children: PropTypes.node,
    height: PropTypes.number,
    width: PropTypes.number
};

Download.defaultProps = {
    height: 20,
    width: 20
};

export default Download;
