import React, { useState } from "react";
import { Lottie } from "@crello/react-lottie";

import close from "./json/close.json";
import closeReverse from "./json/close_reverse.json";

const Close = ({ animationClassName, ...props }) => {
    const [direction, setDirection] = useState(closeReverse);
    const [playingState, setPlayingState] = useState("stopped");

    const defaultOptions = {
        loop: false,
        autoplay: false,
        animationData: direction,
    };

    return (
        <div
            onMouseLeave={() => {
                setDirection(closeReverse);
                setPlayingState("playing");
            }}
            onMouseEnter={() => {
                setDirection(close);
                setPlayingState("playing");
            }}
            {...props}
        >
            <Lottie
                config={defaultOptions}
                height={20}
                width={20}
                className={animationClassName}
                playingState={playingState}
            />
        </div>
    );
};

export default Close;
