import React, { useState, useLayoutEffect, useRef } from "react";

function useDimensions() {
    const ref = useRef();
    const [dimensions, setDimensions] = useState({});
    useLayoutEffect(() => {
        if(ref.current.getBoundingClientRect().toJSON) {
            setDimensions(ref.current.getBoundingClientRect().toJSON());
        }
    }, [ref.current]);
    return [ref, dimensions];
}

export default useDimensions