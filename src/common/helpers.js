
import moment from "moment-timezone";
import { DATEFORMAT } from "./variables";
import React from "react";

/**
 * Time helpers
 */

export const getDateUs = (date) => {
    return moment(date).format(DATEFORMAT);
};

export const getDateFrFromUs = (date) => {
    return getDateUs(date)
};

export const monthStart = (date) => {
    return moment(date).startOf("month").format(DATEFORMAT);
};

export const monthEnd = (date) => {
    return moment(date).endOf("month").format(DATEFORMAT);
};

export const getDateDayOff = (date, off) => {
    return moment(date).subtract(off, 'days').format(DATEFORMAT);
};

export const getDateMonthOff = (date, off) => {
    return {
        'start': moment(date).subtract(off, 'months').startOf("month").format(DATEFORMAT),
        'end': moment(date).subtract(off, 'months').endOf("month").format(DATEFORMAT)
    }
};

export const getCurrentWeekDate = (date) => {
    let currentDayOfWeekNumber = moment(date).format('E');
    return {
        'start': moment(date).subtract(currentDayOfWeekNumber - 1, 'days').format(DATEFORMAT),
        'end': moment(date).format(DATEFORMAT)
    }
};

export const getPreviousWeekDate = (date) => {
    let currentDayOfWeekNumber = moment(date).format('E');
    return {
        'start': moment(date).subtract(currentDayOfWeekNumber - 1 + 7, 'days').format(DATEFORMAT),
        'end': moment(date).subtract(currentDayOfWeekNumber, 'days').format(DATEFORMAT),
    }
};

export const getPreviousMonth = (date) => {
    return moment(date).subtract(1, 'months').format(DATEFORMAT);
};

export const getNextMonth = (date) => {
    return moment(date).add(1, 'months').format(DATEFORMAT);
};

export const getPreviousDay = (date) => {
    return moment(date).subtract(1, 'days').format(DATEFORMAT);
};

export const getNextDay = (date) => {
    return moment(date).add(1, 'days').format(DATEFORMAT);
};

export const formatLabel = (label, query) => {
    if (!query) {
        return label;
    }
    var composedLabel = [];
    var queryLength = query.length
    // Get indices
    var regex = new RegExp(query.toLowerCase(), "gi"), result, indices = [];
    while ((result = regex.exec(label.toLowerCase()))) {
        indices.push(result.index);
    }
    if (indices.length === 0) {
        return label;
    }
    // Rebuild the sentence
    if (indices[0] > 0) {
        // Starting non-occurence
        composedLabel.push(label.substring(0, indices[0]))
    }
    let start, end;
    for (let i = 0; i < indices.length; i++) {
        start = indices[i];
        end = start + queryLength;
        // Occurrence
        composedLabel.push(<b key={query + i}>{label.substring(start, end)}</b>)
        // Intermediary non-occurrence
        if (label.length > end + 1) {
            start = end;
            if(indices.length > i + 1){
                end = indices[i + 1]
            } else {
                end = label.length;
            }
            if(end > start) {
                composedLabel.push(label.substring(start, end))
            }
        }
    }
    return composedLabel;
};
