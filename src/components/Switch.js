import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Lottie } from "@crello/react-lottie";

import switchOffOnAnimation from "../animations/json/switch_off_on.json";
import switchOnOffAnimation from "../animations/json/switch_on_off.json";

import * as variables from "../common/variables";

const { BLACK } = variables;

export class Switch extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            enabled: this.props.On===true || this.props.Off===false
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.On !== this.props.On || prevProps.Off !== this.props.Off){
            this.setState({enabled: this.props.On===true || this.props.Off===false})
        }
    }

    onChange(){
        let enabled = !this.state.enabled;
        if(this.props.onChange){
            let alteredResult = this.props.onChange(enabled);
            if(alteredResult===true || alteredResult===false){
                enabled = alteredResult;
            }
        }
        this.setState({ enabled: enabled })
    }

    render() {
        const defaultLottieOptions = {
            loop: false,
            autoplay: false,
            animationData: this.state.enabled
                ? switchOffOnAnimation
                : switchOnOffAnimation,
        };

        return (
            <StyledSwitchWrapper className={this.props.className} role={this.props.role}>
                <StyledSwitch
                    type="button"
                    enabled={this.state.enabled}
                    onClick={this.onChange.bind(this)}
                >
                    <Lottie
                        config={defaultLottieOptions}
                        height={20}
                        width={38}
                    />
                </StyledSwitch>
                <label>{this.props.label}</label>
            </StyledSwitchWrapper>
        );    
    }
}

Switch.propTypes = {
    On: PropTypes.bool,
    Off: PropTypes.bool,
    label: PropTypes.string,
    className: PropTypes.string,
    role: PropTypes.string
};

Switch.defaultProps = {
    On: false,
    Off: true,
    onChange: null,
};

const StyledSwitchWrapper = styled.div`
    display: flex;
    label {
        display: flex;
        align-items:center;
        font-size: 14px;
        color: ${BLACK};
        line-height: 19px;
        margin-left: 10px;
    }
`;

const StyledSwitch = styled.div`
    cursor: pointer;
`;

export default Switch;
