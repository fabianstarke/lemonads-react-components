import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import baseIcons from "../images/icons/baseIcons";
import ReactSelect from "./ReactSelect";

const { BLACK, GREY_2, WHITE } = variables;


const PaginationBase = ({ currentPage = 1, pagesCount = 1, paginationLength = 7, handleChangePage = () => { } }) => {
    
    let canPrevious = (currentPage > 1);
    let canNext = (currentPage < pagesCount);

    const transformCountInArray = (count) => {
        let countArray = [];
        for (let i = 0; i < count; i++) {
            countArray.push(i + 1);
        }
        return countArray;
    }
    const slicedPages = () => {
        let sliceReferPage = (currentPage <= (Math.ceil(paginationLength / 2) - 1)) ? 1 : (currentPage > pagesCount - (Math.ceil(paginationLength / 2) - 1)) ? (pagesCount - paginationLength) + 1 : currentPage - (Math.ceil(paginationLength / 2) - 1);
        return (pagesCount > paginationLength) ? transformCountInArray(pagesCount).slice(sliceReferPage - 1, sliceReferPage - 1 + paginationLength) : transformCountInArray(pagesCount);
    };
    const handleClickPrevious = () => {
        canPrevious && handleChangePage(currentPage - 1);
    }
    const handleClickNext = () => {
        canNext && handleChangePage(currentPage + 1);
    }
    // const handleClickFirst = () => {
    //     handleChangePage(1);
    // }
    // const handleClickLast = () => {
    //     handleChangePage(pagesCount);
    // }

    return (
        <ul className="PaginationUL">
            <li className={!canPrevious ? 'disable' : ''} onClick={() => handleClickPrevious()}>
                {baseIcons.ArrowLeft}
            </li>
            {/* <li className={!canPrevious ? 'disable' : ''} onClick={() => handleClickFirst()}>First</li> */}
            {slicedPages().map((item, index) => (
                <li key={item} className={(item == currentPage) ? 'active' : ''} onClick={() => handleChangePage(item)}>
                    {item}
                </li>
            ))}
            {/* <li className={!canNext ? 'disable' : ''} onClick={() => handleClickLast()}>
                 Last
                </li> */}
            <li className={!canNext ? 'disable' : ''} onClick={() => handleClickNext()}>
                {baseIcons.ArrowRight}
            </li>
        </ul>
    )
}

export class Pagination extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentPage: 1,
            page: 1
        };

        this.changePage = this.changePage.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.getSafePage = this.getSafePage.bind(this);
    }

    handleChangePage(currentPage) {
        this.setState({ currentPage });
        this.setState({ page: currentPage });
        this.changePage(currentPage);
    }

    changePage(page) {
        const activePage = this.props.page + 1;

        if (page === activePage) {
            return;
        }
        this.props.onPageChange(page - 1);
    }

    getSafePage(page) {
        if (isNaN(page)) {
            page = this.props.page + 1;
        }
        return Math.min(Math.max(page, 1), this.props.pages);
    }

    render() {

        const activePage = this.state.currentPage;

        let pageOptions = this.props.pageSizeOptions.map((v,i)=>{return {label: v + " " + this.props.rowsText, value: v}});

        return (
            <StyledPaginationWrapper className={this.props.className + " -pagination"} style={this.props.style} role={this.props.role}>
                {this.props.showPageSizeOptions && (
                    <span className="select-wrap -pageSizeOptions">
                        <ReactSelect 
                            label={null} 
                            options={pageOptions} 
                            value={this.props.pageSize} 
                            width="128px" 
                            onChange={(e) => {this.props.onPageSizeChange(Number(e.value))}}
                            isSearchable={false}
                            classNamePrefix={this.props.selectClassNamePrefix}
                        />
                    </span>
                )}
                {this.props.pages > 1 ?
                ( <span className="paginationBar">
                        <PaginationBase
                            pagesCount={this.props.pages}
                            handleChangePage={index => this.handleChangePage(index)}
                            currentPage={activePage}
                            paginationLength={5}
                        />
                 </span>) : ""}
            </StyledPaginationWrapper>
        );
    }
}

Pagination.propTypes = {
    showPageSizeOptions: PropTypes.bool,
    className: PropTypes.string,
    pageSize: PropTypes.number,
    rowsText: PropTypes.string,
    onPageSizeChange: PropTypes.func,
    onPageChange: PropTypes.func,
    role: PropTypes.string,
    pageSizeOptions: PropTypes.array,
    selectClassNamePrefix: PropTypes.string
};

Pagination.defaultProps = {
    showPageSizeOptions: true,
    rowsText: ' rows',
    pageSize: 50,
    className: '',
    onPageSizeChange: (n) => {},
    onPageChange: (n) => {},
    pageSizeOptions: [25, 50, 100, 200, 500]
};

const StyledPaginationWrapper = styled.div`
display: flex;
flex-wrap: nowrap;
width: 100%;
justify-content: space-between;
align-items: center;
.select-wrap {
    margin: 20px 0 !important;
}
.PaginationUL{
    list-style: none;
    display: inline-flex;
    margin: 20px 0 !important;
    padding: 0px !important;
    li{
        height: 19px;
        width: 9px;
        color: ${BLACK};
        font-family: "Open Sans";
        font-size: 14px;
        letter-spacing: 0;
        line-height: 20px;
        height: 20px;
        margin: 0 10px 0 0;
        user-select: none;
        display: flex;
        align-items: center;
        justify-content: center;
        &:last-child{
            margin-right:0px;
            margin-left:3px;
            width: 18px;
        }
        &:first-child{
            margin-right:13px;
            width: 18px;
        }
        &.disable{
            color: ${GREY_2};
            &:hover{
              color: ${GREY_2};
            }
            svg {
                g {
                    stroke: ${GREY_2};
                }
            }
        }
        &.active{
            width: 20px;
            background-color: ${BLACK};
            border-radius:10px;
            color:${WHITE};
        }
        &:hover{
            cursor:pointer;
        }
    }
}
`;

export default Pagination;
