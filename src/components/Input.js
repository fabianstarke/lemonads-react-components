import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import { CheckValid, Close, Placeholder, Information } from "../images/icons/baseIcons";
import Tooltip from "./Tooltip";

const { SUCCESS, ERROR, PRIMARY_BASE, BLACK, GREY_5 } = variables;

export const Wrapper = styled.div`
    width: ${(props) => props.full ? "100%" : "202px"};
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const Label = styled.label`
    height: 19px;
    width: max-content;
    color: ${GREY_5};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
    margin-bottom: 10px;
    ${({ label }) => label}
    &:after {
        content: ${({ required }) => (required === true ? `"*"` : "")};
        color: red;
        margin-left: 0px;
    }
`;
export const Input = styled.input`
    width: auto;
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    border: 1px solid;
    height: 28px;
    padding-left: 10px;
    padding-right: 5px;
    box-sizing: border-box;
    border-color: ${({ mode, disabled, borderColor }) => borderColor !== null ? borderColor :
        mode === "error" ? ERROR : mode === "success" ? SUCCESS : disabled ? "#EBEBEB" : "#CBCBCB"};
    background-color: ${({ disabled }) => disabled ? "#F2F2F2" : "#FFFFFF"};
    background-repeat: no-repeat;
    background-position: center right 10px;
    background-size: 10px;
    &:focus {
        border: ${({ mode }) => (mode === "" ? "1px solid "+ PRIMARY_BASE : "1px solid inherit")};
        outline: none;
        caret-color: ${({ mode }) => (mode === "" ? PRIMARY_BASE : "")};
    }
    border: ${({ focus }) => (focus === true ? "1px solid " + PRIMARY_BASE : "")};
    border-radius: 5px;
    ::placeholder {
        color: #CBCBCB;
        opacity: 1;
    }
`;
const IconWrapper = styled.div`
    display: flex;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 9px;
    & svg {
        height: 16px;
        width: 16px;
        opacity: 0.3;
        g {
            stroke: ${({ mode }) => mode === "error" ? ERROR : mode === "success" ? SUCCESS : mode ==="focus" ? PRIMARY_BASE : BLACK };
        }
    }
`;

const InputWrapper = styled.div`
    position: relative;
    display: grid;
`;

const LabelWrapper = styled.div`
    display: flex;
    flex-direction: row;
    position: relative;
`;

const StyledInput = ({
    borderColor,
    label,
    mode,
    required,
    focus,
    className,
    full,
    ariaLabel,
    id,
    htmlFor,
    tooltip,
    icon,
    iconTooltip,
    onIconClick,
    ...props
}) => {

    if((htmlFor === undefined || htmlFor === null) && id !== undefined) {
        htmlFor = id;
    }
    
    return (
        <Wrapper aria-labelledby={ariaLabel} className={className} full={full}>
            { label && <LabelWrapper>
                <Label htmlFor={htmlFor} required={required} data-testid="input-label">{label}</Label>
                { tooltip && <Tooltip placement="right" overlay={tooltip}>
                    <span style={{ marginLeft: "8px" }}>{Information}</span>
                </Tooltip>}
            </LabelWrapper>}
            <InputWrapper>
                <Input
                    id={id}
                    mode={mode}
                    focus={focus}
                    borderColor={borderColor}
                    {...props}
                />
                {<Tooltip placement="top" overlay={iconTooltip}><IconWrapper role="icon-wrapper" style={{ cursor: 'pointer'}} onClick={onIconClick && onIconClick}>{icon}</IconWrapper></Tooltip>}
                {(mode || props.type) && (
                    <IconWrapper mode={mode}>
                        {mode === "success"
                            ? CheckValid
                            : mode === "error"
                            ? Close
                            : mode === "focus"
                            ? Placeholder
                            : null}
                    </IconWrapper>
                )}
            </InputWrapper>

        </Wrapper>
    );
};

StyledInput.propTypes = {
    label: PropTypes.string,
    borderColor: PropTypes.string,
    mode: PropTypes.string,
    required: PropTypes.bool,
    focus: PropTypes.bool,
    className: PropTypes.string,
    full: PropTypes.bool,
    htmlFor: PropTypes.string,
    id: PropTypes.string,
    ariaLabel: PropTypes.string,
    tooltip: PropTypes.string,
    icon: PropTypes.object,
    iconTooltip: PropTypes.string,
    onIconClick: PropTypes.func
};

StyledInput.defaultProps = {
    name: "input",
    type: "text",
    borderColor: null,
    required: false,
    disabled: false,
    focus: false,
    full: false,
};

export default StyledInput;
