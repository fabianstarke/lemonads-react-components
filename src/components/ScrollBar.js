import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import Skeleton from '../components/Skeleton'

const { GREY_1 } = variables;

const StyledScrollBar = styled.div`
    overflow: auto;
    &::-webkit-scrollbar {
        width: 20px;
        height: 4px;
    }

    &::-webkit-scrollbar-track {
        background-color: ${GREY_1};
        border-radius: 20px;
        border: solid 8px transparent;
        background-clip: content-box;   
        margin-top: 2px;
        margin-bottom: 2px;   
    }

    &::-webkit-scrollbar-thumb {
        opacity: 0.7;
        border-radius: 20px;
        background-color: #cbcbcb;
        border: solid 8px transparent;
        background-clip: content-box;
    }
`;

const ScrollBar = ({ onScroll, className, id, children, loading, skeletonHeight, skeletonWidth, animationDuration, ...props }) => {
    return (
        loading ? <Skeleton width={skeletonWidth} height={skeletonHeight} animationDuration={animationDuration} /> :
        <StyledScrollBar onScroll={onScroll} className={className} id={id} {...props} >
            {children}
        </StyledScrollBar>
    );
};

ScrollBar.propTypes = {
    onScroll: PropTypes.func,
    className: PropTypes.string,
    id: PropTypes.string,
    children: PropTypes.node,
    loading: PropTypes.bool,
    skeletonHeight: PropTypes.string,
    skeletonWidth: PropTypes.string,
    animationDuration: PropTypes.string
};

ScrollBar.defaultProps = {};

export default ScrollBar;
