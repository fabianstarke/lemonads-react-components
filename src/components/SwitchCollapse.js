import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import baseIcons from "../images/icons/baseIcons"

export class SwitchCollapse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: this.props.Open === true || this.props.Closed === false
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.Open !== this.props.Open || prevProps.Closed !== this.props.Closed) {
            this.setState({ open: this.props.Open === true || this.props.Closed === false })
        }
    }

    onChange() {
        let open = !this.state.open;
        if (this.props.onChange) {
            let alteredResult = this.props.onChange(open);
            if (alteredResult === true || alteredResult === false) {
                open = alteredResult;
            }
        }
        this.setState({ open: open })
    }

    render() {
        let icon = this.props.iconClosed && !this.state.open ? this.props.iconClosed : this.props.icon;
        let text = this.props.textClosed && !this.state.open ? this.props.textClosed : this.props.text;
        return (
            <span onClick={this.onChange.bind(this)} role="switchcollapse">
                <StyledSwitchCollapse className={this.props.className}>
                    {icon}
                    {text && <span className="text">{text}</span>}
                    {this.state.open ? baseIcons.ChevronUp : ""}
                </StyledSwitchCollapse>
            </span>
        );
    }
}

SwitchCollapse.propTypes = {
    Open: PropTypes.bool,
    Closed: PropTypes.bool,
    text: PropTypes.string,
    textClosed: PropTypes.string,
    icon: PropTypes.object,
    iconClosed: PropTypes.object,
    onChange: PropTypes.func
};

SwitchCollapse.defaultProps = {
    Open: false,
    Closed: true,
    text: null,
    onChange: () => { }
};

const StyledSwitchCollapse = styled.div`
    cursor:pointer;
    font-family: Open Sans;
    font-size: 14px;
    letter:normal;
    line-height:19px;
    font-weight:bold;
    display:flex;
    &>span{
        margin: 0 0 0 10px;
    }
    svg{
        width: 20px;
        height: 20px;
        margin-left: 10px;
    }
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -o-user-select: none;
    user-select: none;
`;

export default SwitchCollapse;
