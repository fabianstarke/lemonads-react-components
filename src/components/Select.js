import React from "react";
import PropTypes from "prop-types";
import { Label } from "./Input";
import styled from "styled-components";
import * as variables from "../common/variables";
import { SelectArrow } from "../images/icons/baseIcons";


export const Wrapper = styled.div`
    width: ${(props) => props.full ? "100%" : (props.width ? props.width : "202px")};
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const StyledSelect = styled.select`
    width: 100%;
    border-radius: 5px;
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    border: 1px solid;
    padding: 5px 10px;
    padding-left: ${({icon}) => icon ? '28px' : '10px'};
    outline: none;
    color: #000000;
    letter-spacing: 0;
    line-height: 19px;
    background-color: ${({ secondary, disabled }) =>
        secondary === true || disabled ? "#F2F2F2" : "#FFFFFF"};
    border-color: ${({ secondary }) =>
        secondary === true ? "#EBEBEB" : "#CBCBCB"};
    -moz-appearance: none; /* Firefox */
    -webkit-appearance: none; /* Safari and Chrome */
    appearance: none;
`;

const IconWrapper = styled.div`
    position: absolute;
    right: 12px;
    top: 0px;
    height: 100%;
    align-items: center;
    display: flex;`;

const StyledSelectInnerWrapper = styled.div`
    position:relative;
`;

const StyledSelectIcon = styled.div`
    position:absolute;
    top:8px;
    left:8px;
    svg{
        width:16px;
        height:16px;
    }
`;

const Select = ({
    label,
    secondary,
    options,
    name,
    id,
    onChange,
    onBlur,
    value,
    className,
    required,
    wrapperClassName,
    defaultOption,
    full,
    icon,
    disabled,
    width,
}) => {
    return (
        <Wrapper className={wrapperClassName} full={full} label={label} width={width}>
            { label && <Label required={required}>{label}</Label>}
            <StyledSelectInnerWrapper>
                {icon && <StyledSelectIcon>{icon}</StyledSelectIcon>}
                <StyledSelect
                    name={name}
                    id={id}
                    onChange={onChange}
                    secondary={secondary}
                    value={value}
                    onBlur={onBlur}
                    className={className}
                    icon={icon}
                    disabled={disabled}
                > 
                    {defaultOption && <option value={defaultOption.value}>
                        {defaultOption.name}
                    </option>}
                    {options &&
                        options.map((option, i) => (
                            <option key={i} value={option.value}>
                                {option.name}
                            </option>
                        ))}
                </StyledSelect>
                <IconWrapper label={label}>{SelectArrow}</IconWrapper>
            </StyledSelectInnerWrapper>
        </Wrapper>
    );
};

Select.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    secondary: PropTypes.bool,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        })
    ),
    defaultOption: 
        PropTypes.shape({
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    className: PropTypes.string,
    required: PropTypes.bool,
    className: PropTypes.string,
    wrapperClassName: PropTypes.string,
    full: PropTypes.bool,
    disabled: PropTypes.bool,
    width: PropTypes.string,
};

Select.defaultProps = {
    id: null,
    name: "select",
    secondary: false,
    full: false,
    disabled:false,
    width: null,
};

export default Select;
