import React from "react";
import PropTypes from "prop-types";
import baseIcons from "../images/icons/baseIcons"
import SwitchCollapse from "./SwitchCollapse";


const PageHelpCollapse = ({
    Open,
    Closed,
    onChange
}) => <SwitchCollapse textClosed="Need help ?" iconClosed={baseIcons.BrowserIdea} Open={Open} Closed={Closed} onChange={onChange} />;

PageHelpCollapse.propTypes = {
    Open: PropTypes.bool,
    Closed: PropTypes.bool,
    onChange: PropTypes.func,
};

PageHelpCollapse.defaultProps = {
    Open: false,
    Closed: true,
    onChange: () => {}
};


export default PageHelpCollapse;
