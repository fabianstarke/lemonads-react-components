import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import selectArrow from "../images/icons/baseIcons/selectArrow.svg";
import SearchBar from "./SearchBar";
import CheckBox from "./CheckBox";
import ScrollBar from "./ScrollBar";
import { Label } from "./Input";
import { formatLabel } from "../common/helpers"

const { WHITE, GREY_6 } = variables;

const StyledWrapper = styled.div`
    position: relative;
    input {
        border: 1px solid ${GREY_6};
        box-sizing: border-box;
    }
`;

const Controller = styled.div`
    border: 1px solid ${GREY_6};
    width: 191px;
    border-radius: 5px;
    background-color: ${WHITE};
    display: flex;
    align-items: center;
    font-size: 14px;
    line-height: 17px;
    height: 28px;
    cursor: pointer;
    padding-left: 10px;
    margin-top: 10px;
    .arrow {
        display: flex;
        margin-right: 10px;
        margin-left: auto;
    }
`;

const PanelWrapper = styled.div`
    border: 1px solid ${GREY_6};
    top: ${(props) => (props.hasLabel ? "62px" : "14px")};
    position: absolute;
    width: fit-content;
    border-radius: 5px;
    background-color: ${WHITE};
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.15);
    padding: 10px;
    padding-right: 10px;
    z-index: 1;
    .list {
        margin-top: 10px;
        height: 230px;
        &:first-child{
            height: 280px;
        }
        vertical-align: top;
        &::-webkit-scrollbar {
            width: 4px;
        }

        &::-webkit-scrollbar-track {
            border: solid 0px transparent;
        }

        &::-webkit-scrollbar-thumb {
            border: solid 0px transparent;
        }
    }
    .searchBar {
        width: 169px;
    }
`;

const Element = styled.div`
    display: flex;
    align-items: center;
    margin: 10px 0;
    :last-child{
        margin-bottom: 0;
    }
    cursor: pointer;
    padding-right: 10px;
    &:first-child{
        margin-top: 0;
    }
    .arrow {
        margin-right: 0px;
        margin-left: auto;
        transform: rotate(-90deg);
        cursor: pointer;
    }
    > div {
        overflow: hidden;
        width: 100%;
    }
    input {
        cursor: pointer;
    }
    label {
        cursor: pointer;
        width: calc(100% - 31px);
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        line-height: 19px;
    }
    &.selected {
        font-weight: bold;
    }
`;

const PanelLabel = styled.div`
    font-size: 14px;
    line-height: 19px;
    font-weight: 600;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin-top: 10px;
    margin-left: 3px;
}
`;

const Panel = styled.div`
    width: 169px;
    padding-left: 17px;
    &:first-child {
        padding-left: 0;
    }
`;

const ListWrapper = styled.div`
    display: flex;
`;

const SearchResults = styled.div`
    width: 169px;
    b {
        font-weight: normal;
        background: rgba(3, 54, 255, 0.2);
    }
`;

const NoResults = styled.div`
    padding-top: 15px;
    font-size: 15px;
`;

const MultiLevelSelect = ({
    options,
    handleCheckboxChange,
    onOpen,
    onClose,
    menuIsOpen,
    label,
    values,
    controllerLabel,
    role
}) => {
    const [listItems, setListItems] = useState([options]);
    const [openSelect, setOpenSelect] = useState(false);
    const [itemIndexes, setItemIndexes] = useState([]);
    const [search, setSearch] = useState('');

    useEffect(() => {
        if (menuIsOpen !== openSelect) {
            toggleController();
        }
    }, [menuIsOpen]);

    useEffect(() => {
        setListItems([options]);
    }, [options]);

    const getAllItems = (listItems, indexes) => {
        if (indexes === undefined) {
            indexes = [];
        }
        let results = [];
        listItems.forEach((item, index) => {
            let itemFlat = { ...item };
            let itemIndexes = [...indexes, index];
            itemFlat.indexes = itemIndexes;
            if (item.children) {
                results = results.concat(getAllItems(item.children, itemIndexes))
            }
            results.push(itemFlat)
        })
        return results
    }

    const changeSearch = e => {
        setSearch(e.target.value)
    }

    const renderChild = (elem, level, subkey) => {
        if (!elem.children || elem.children.length === 0) {
            removeChild(level);
        } else {
            let newListItems = [...listItems];
            newListItems[level + 1] = elem.children;
            setListItems(newListItems);
        }

        let newItemIndexes = [...itemIndexes];
        newItemIndexes[level] = subkey;
        setItemIndexes(newItemIndexes);
    };

    const removeChild = (level) => {
        if (listItems.length === 0) {
            return;
        }

        if (level < listItems.length - 1) {
            let newListItems = [...listItems];
            newListItems.splice(-1, level + 1);
            setListItems(newListItems);

            let newItemIndexes = [...itemIndexes];
            newItemIndexes.splice = (-1, level + 1);
            setItemIndexes(newItemIndexes);
        }
    };

    const removeAllChild = () => {
        if (listItems.length === 0) {
            return;
        }

        let newListItems = [listItems[0]];
        setListItems(newListItems);
        setItemIndexes([]);
    };

    const toggleController = () => {
        setSearch('');

        if (openSelect === true) {
            onClose();
        } else {
            onOpen();
        }
        setOpenSelect(!openSelect);
    };

    let content;

    if (search === "") {
        content = <ListWrapper role="list-wrapper" onMouseLeave={() => removeAllChild()}>
            {listItems &&
                listItems.map((item, level) => {
                    return (
                        <Panel key={"panel-" + level}>
                            {level > 0 ? (
                                <PanelLabel>
                                    {
                                        listItems[level - 1][
                                            itemIndexes[level - 1]
                                        ].label
                                    }
                                </PanelLabel>
                            ) : (
                                ""
                            )}
                            <ScrollBar
                                className="list"
                                key={level}
                                onMouseEnter={() =>
                                    removeChild(level)
                                }
                            >
                                {item &&
                                    item.map((subitem, subkey) => {
                                        return (
                                            <Element
                                                key={level + subkey}
                                                onMouseEnter={() =>
                                                    renderChild(
                                                        subitem,
                                                        level,
                                                        subkey
                                                    )
                                                }
                                                className={
                                                    itemIndexes[
                                                        level
                                                    ] == subkey
                                                        ? "selected"
                                                        : ""
                                                }
                                            >
                                                <CheckBox
                                                    id={"checkbox-" + level + subkey}
                                                    className="checkbox"
                                                    onChange={() => {
                                                        handleCheckboxChange(
                                                            listItems[0][
                                                                itemIndexes[0]
                                                            ].value,
                                                            subitem,
                                                            itemIndexes,
                                                            level
                                                        );
                                                    }}
                                                    labelRight={
                                                        subitem.label
                                                    }
                                                    checked={values.map(x => x === null ? null : String(x)).includes(
                                                        subitem.value.toString()
                                                    )}
                                                />
                                                {subitem.children &&
                                                    subitem.children
                                                        .length > 0 ? (
                                                    <img
                                                        className="arrow"
                                                        src={
                                                            selectArrow
                                                        }
                                                    />
                                                ) : (
                                                    ""
                                                )}
                                            </Element>
                                        );
                                    })}
                            </ScrollBar>
                        </Panel>
                    );
                })}
        </ListWrapper>;
    } else {
        // Search
        let items = getAllItems(listItems[0]);
        items = items.filter(item => item.label.toLowerCase().includes(search.toLowerCase())).map(item => {
            item.formattedLabel = formatLabel(item.label, search)
            return item;
        });
        if (items.length > 0) {
            content = <SearchResults>
                <ScrollBar
                    className="list"
                    key="sq"
                >
                    {items && items.map((item, key) => {
                        return <Element
                            key={key}
                        >
                            <CheckBox
                                id={"checkbox-" + key}
                                className="checkbox"
                                onChange={() => {
                                    handleCheckboxChange(
                                        listItems[0][
                                            item.indexes[0]
                                        ].value,
                                        item,
                                        item.indexes,
                                        item.indexes.length - 1
                                    );
                                }}
                                labelRight={
                                    item.formattedLabel
                                }
                                checked={values.map(x => x === null ? null : String(x)).includes(
                                    item.value.toString()
                                )}
                            />
                        </Element>
                    })}
                </ScrollBar>
            </SearchResults>;
        } else {
            content = <NoResults>No results</NoResults>;
        }

    }


    return (
        <StyledWrapper role={role}>
            {label && label.length > 0 && <Label>{label}</Label>}
            <Controller onClick={() => toggleController()} role="controller">
                {controllerLabel}
                <img className="arrow" src={selectArrow} />
            </Controller>
            {openSelect && (<PanelWrapper
                hasLabel={label && label.length > 0 ? true : false}
            >
                <SearchBar onChange={e => changeSearch(e)} placeHolder="Search category" wrapperClassName="searchBar"></SearchBar>
                {content}
            </PanelWrapper>)}
        </StyledWrapper>
    );
};

MultiLevelSelect.propTypes = {
    options: PropTypes.array,
    handleCheckboxChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    menuIsOpen: PropTypes.bool,
    label: PropTypes.string,
    values: PropTypes.array,
    controllerLabel: PropTypes.string,
    role: PropTypes.string
};

MultiLevelSelect.defaultProps = {
    options: [],
    handleCheckboxChange: () => { },
    onOpen: () => { },
    onClose: () => { },
    menuIsOpen: false,
    values: [],
    controllerLabel: "",
    role: ""
};

export default MultiLevelSelect;
