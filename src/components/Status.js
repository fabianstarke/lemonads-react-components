import React, { useState } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import * as variables from "../common/variables";

const { SUCCESS, ERROR, INFO, WARNING } = variables;

const StatusWrapper = styled.div`
    display: flex;
    position: relative;
    align-items: center;
`;

const Label = styled.div`
    color: #000000;
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    margin: 0;
`;

const Dot = styled.div`
    display: inline-block;
    height: 8px;
    width: 8px;
    border-radius: 50px;
    background: ${({ type }) =>
        type === "success" ? SUCCESS : type === "error" ? ERROR : type === "warning" ? WARNING : type === "info" ? INFO : INFO};
    margin-right: 8px;
`;

const Status = ({ label, type, className, role }) => {
    return (
        <StatusWrapper role={role}>
            <Dot type={type} className={className} role={"status-dot"}/>
            <Label> {label}</Label>
        </StatusWrapper>
    );
};

Status.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    className: PropTypes.string,
    role: PropTypes.string
};

Status.defaultProps = {
    type: "info",
};

export default Status;
