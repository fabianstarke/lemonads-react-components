import React, { useState } from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { GREY_1, BLACK, WHITE } from "../common/variables";
import { Close } from "../images/icons/baseIcons";

const TagContainer = styled.div`
    height: 24px;
    width: fit-content;
    border-radius: 5px;
    background-color: ${({ toggle, selected }) =>
        (toggle || selected === true)
            ? WHITE
            : GREY_1};
    border: ${({ selected }) => selected === true ? "solid black 1px" : "solid "+GREY_1+" 1px" };
    cursor: pointer;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-around;
    ${({ close }) =>
        close &&
        css`
            padding: 0px 5px 0px 10px;
        `}
`;
const TagLabel = styled.label`
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
    cursor: pointer;
    padding-right: ${({ close }) => (close ? "8px" : 0)};
    color: ${BLACK};

    ${({ close }) =>
        !close &&
        css`
            padding: 0px 10px;
        `}
`;

const IconWrapper = styled.div`
    display: flex;
    & svg {
        height: 14px;
        width: 14px;
    }
`;

const Tag = ({ label, close, selected, onClick, ...props }) => {
    const [active, setActive] = useState(false);

    const handleChange = () => {
        setActive(!active);
    };

    const handleClick = onClick || handleChange

    return (
        <TagContainer
            {...props}
            close={close}
            selected={selected}
            toggle={active}
            onClick={handleClick}
        >
            <TagLabel
                close={close}
                active={active}
                selected={selected}
                
            >
                {label}
            </TagLabel>
            {close && (
                <IconWrapper>
                    {Close}
                </IconWrapper>
            )}
        </TagContainer>
    );
};

Tag.propTypes = {
    label: PropTypes.string,
    close: PropTypes.bool,
    selected: PropTypes.bool,
    onClick: PropTypes.func,
};

Tag.defaultProps = {
    selected: false,
    close: false,
};

export default Tag;
