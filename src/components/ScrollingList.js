import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import chevronLeft from "../images/icons/baseIcons/chevron_left_background.svg";
import chevronRight from "../images/icons/baseIcons/chevron_right_background.svg";

import smoothscroll from 'smoothscroll-polyfill';

import * as variables from "../common/variables";
import Skeleton from '../components/Skeleton'

const {
    GREY_1,
    WHITE
} = variables;

export class ScrollingList extends React.Component {

    /* Notes:
    * - ChevronLeft/Right with blue color missing.
    * - If the sizes are taken from invisionapp, the final image size is different.
    * ***/

    constructor(props) {
        super(props);
        this.state = {
            canLeft: false,
            canRight: false,
            startingPos: 0,
            accumulatedDelta: 0,
            endingPos: null,
        }
        this.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        // update can left / can right
        this.onScroll = this.onScroll.bind(this)
        this.updateArrows = this.updateArrows.bind(this)
        // Arrow clicking
        this.scrollLeftOnArrow = this.scrollLeftOnArrow.bind(this);
        this.scrollRightOnArrow = this.scrollRightOnArrow.bind(this);
        this.scrollLeftClear = this.scrollLeftClear.bind(this);
        this.scrollRightClear = this.scrollRightClear.bind(this);
        window.__forceSmoothScrollPolyfill__ = true;
    }

    componentDidMount() {
        this.updateArrows()
        smoothscroll.polyfill();

        /**
         * Vertical wheel -> Horizontal scroll listener.
         * Manually added mousewheel because e.preventDefault() (to avoid vertical page scroll)
         * doesn't work with react onWheel + Chrome > 53 (issue with passive events).
         * Alternative would be rotating hacks, see:
         * https://stackoverflow.com/questions/18481308/set-mouse-wheel-to-horizontal-scroll-using-css
         */
        if (this._track) {
            let mousewheelevt=this.isFirefox? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
            this._track.parentElement.addEventListener(mousewheelevt,this.onWheel.bind(this), false);
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.children.length != this.props.children.length) {
            this.updateArrows();
        }
    }

    updateArrows() {
        if (this._track === undefined || this._track === null) return;
        let scroller = this._track.parentElement;
        let canRight = scroller.scrollWidth - scroller.scrollLeft - scroller.clientWidth - 1 > 0; // -1 instead of rounding the substraction
        let canLeft = scroller.scrollLeft > 0;
        this.setState({ canLeft: canLeft, canRight: canRight });
    }

    onScroll() {
        this.updateArrows()
    }

    scrollLeftOnArrow() {
        if (this._track === undefined || this._track === null) return;

        const timeout = this.props.pixelsMove / this.props.arrowSpeed;
        this.scrollLeftTimeout = setTimeout(this.scrollLeftOnArrow, timeout*1000);
        let position = this._track.parentElement.scrollLeft - this.props.pixelsMove;
        if(!this.state.canRight) {
            position = this.props.children.length > 4 ? this._track.parentElement.scrollLeft - this.props.pixelsMove : 0;
        }

        this.setState({startingPos: position, endingPos: null, accumulatedDelta: 0});

        this._track.parentElement.scrollTo({
            left: position,
            behavior: 'smooth'
        })
    }

    scrollRightOnArrow() {
        if (this._track === undefined || this._track === null) return;

        const timeout = this.props.pixelsMove / this.props.arrowSpeed;
        this.scrollRightTimeout = setTimeout(this.scrollRightOnArrow, timeout*1000);
        let position = this._track.parentElement.scrollLeft + this.props.pixelsMove;

        this.setState({startingPos: position, endingPos: null, accumulatedDelta: 0});

        this._track.parentElement.scrollTo({
            left: position,
            behavior: 'smooth'
        })
    }

    scrollLeftClear() {
        clearTimeout(this.scrollLeftTimeout);
    }

    scrollRightClear() {
        clearTimeout(this.scrollRightTimeout);
    }
    
    onWheel(e){
        e.preventDefault();

        let instantDelta = null;

        if(this.isFirefox){
            instantDelta = e.detail * 30;
        } else {
            instantDelta = 0.5 * (e.deltaY + e.deltaX + e.deltaZ);
        }

        // Determine continuous ending position.
        let accumulatedDelta = this.state.accumulatedDelta + instantDelta;
        let endingPosContinuous = this.state.startingPos + accumulatedDelta;
        let direction = accumulatedDelta >= 0;

        // Determine the discrete ending position.
        let previousNum = Math.round(this.state.startingPos / this.props.pixelsMove)
        let newNum = Math.round(endingPosContinuous / this.props.pixelsMove)
        let num = previousNum + (direction?1:-1) * Math.max(1, Math.abs(newNum - previousNum));

        let endingPos = Math.round(num * this.props.pixelsMove);

        // Determine current position
        let currentPos = this._track.parentElement.scrollLeft;

        // Determine if we finished scrolling
        let finished = (direction && !this.state.canRight) || 
                        (!direction && !this.state.canLeft) ||
                        (instantDelta == 0 && (endingPos == currentPos));

        if(finished) {
            this.setState({accumulatedDelta: 0, startingPos: currentPos, endingPos: null})
        } else {
            if(endingPos != this.state.endingPos){
                this._track.parentElement.scrollTo({
                    left: endingPos,
                    behavior: 'smooth'
                })
            }
            this.setState({accumulatedDelta, endingPos})
        }

    }

    render() {
        return (
            <ScrollingListWrapper role={this.props.role}>
                <ScrollingListVerticalContainer width={this.props.width}>
                    <ScrollingListHorizontalContainer width={this.props.width} onScroll={this.onScroll}>
                        <ScrollingListTrack ref={s => (this._track = s)}>
                            {this.props.loading ? <Skeleton animated={false} className={this.props.skeletonClassName} backgroundColor={WHITE} style={this.props.style}height={this.props.skeletonHeight} width={this.props.skeletonWidth}><Child /><Child /><Child /><Child /><Child /><Child /></Skeleton> : this.props.children}
                        </ScrollingListTrack>
                    </ScrollingListHorizontalContainer>
                    {this.state.canRight ? <RightShadow/> : ``}
                    {this.state.canLeft ? <LeftShadow/> : ``}
                </ScrollingListVerticalContainer>
                {this.state.canLeft || this.state.canRight ? 
                    <div>
                        {this.state.canLeft && <img 
                            role="scroll-left"
                            className="chevron left" src={chevronLeft}
                            onMouseDown={this.scrollLeftOnArrow}
                            onMouseUp={this.scrollLeftClear}
                            onMouseLeave={this.scrollLeftClear}
                        />}
                        {this.state.canRight && <img 
                            role="scroll-right"
                            className="chevron right" 
                            src={chevronRight}
                            onMouseDown={this.scrollRightOnArrow}
                            onMouseUp={this.scrollRightClear}
                            onMouseLeave={this.scrollRightClear}
                        />}
                    </div>
                    : ""
                }
                
            </ScrollingListWrapper>
        );
    }
}

ScrollingList.propTypes = {
    arrowSpeed: PropTypes.number,
    width: PropTypes.string,
    pixelsMove: PropTypes.number,
    loading: PropTypes.bool,
    role: PropTypes.string
};

ScrollingList.defaultProps = {
    width: "100%",
    arrowSpeed: 200,
    pixelsMove: 238.55,
    loading: false,
    role: null
};

const ScrollingListWrapper = styled.div`
    display: flex; 
    flex-direction: column;
    max-width: min-content; 
    position: relative;
    .chevron {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
        display: none;
        &.right {
            right: -38px;
            @media(max-width: 1440px) {
                right: -18px;
            }
        }
        &.left {
            left: -18px;
        }
        &.stop {
            cursor: unset;
        }
    }
    &:hover {
        .chevron {
            display: block;
        }
    }
`

const Child = styled.div`
        width: 253px;
        background-color: ${GREY_1};
        margin-right: 15px;
        @keyframes placeHolderShimmer {
            0%{
                background-position: -300px 0
            }
            100%{
                background-position: 300px 0
            }
        }
            animation-duration: 3s;
            animation-fill-mode: forwards;
            animation-iteration-count: infinite;
            animation-name: placeHolderShimmer;
            animation-timing-function: linear;
            background: linear-gradient(to right, #eeeeee 8%, #f5f5f5 18%, #eeeeee 33%);
` 
const LeftShadow = styled.div`
    position:absolute;
    width: 20px;
    top:0;
    bottom:0;
    left:0;
    background: rgba(238,238,238,1);
    background: -moz-linear-gradient(left, rgba(238,238,238,1) 0%, rgba(255,255,255,0) 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,0,0,0.10)), color-stop(100%, rgba(255,255,255,0)));
    background: -webkit-linear-gradient(left, rgba(0,0,0,0.10) 0%, rgba(255,255,255,0) 100%);
    background: -o-linear-gradient(left, rgba(0,0,0,0.10) 0%, rgba(255,255,255,0) 100%);
    background: -ms-linear-gradient(left, rgba(0,0,0,0.10) 0%, rgba(255,255,255,0) 100%);
    background: linear-gradient(to right, rgba(0,0,0,0.10) 0%, rgba(255,255,255,0) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebebeb', endColorstr='#ffffff', GradientType=1 );
`;

const RightShadow = styled.div`
    position:absolute;
    width: 20px;
    top:0;
    bottom:0;
    right:0;
    background: rgba(238,238,238,0);
     background: -moz-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(0,0,0,0.10) 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(255,255,255,0)), color-stop(100%, rgba(0,0,0,0.10)));
    background: -webkit-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(0,0,0,0.10) 100%);
    background: -o-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(0,0,0,0.10) 100%);
    background: -ms-linear-gradient(left, rgba(255,255,255,0) 0%, rgba(0,0,0,0.10) 100%); 
    background: linear-gradient(to right, rgba(238,238,238,0) 0%, rgba(0,0,0,0.10) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ebebeb', GradientType=1 );
`;

const ScrollingListVerticalContainer = styled.div`
    overflow:hidden;
    width:${props => props.width};
    position:relative;
`;


const ScrollingListHorizontalContainer = styled.div`
    overflow-x:scroll;
    -ms-overflow-style: none;
    scrollbar-width: none;
    ::-webkit-scrollbar {
        display: none;
    }
`;

const ScrollingListTrack = styled.div`
    display:flex;
    flex-direction:row;
`;


export default ScrollingList;
