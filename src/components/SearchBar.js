import React, {forwardRef} from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import { SearchGlass  } from "../images/icons/baseIcons";

const { PRIMARY_BASE } = variables;


export const Input = styled.input`
    width: 100%;
    font-family: "Open Sans";
    font-size: 12px;
    letter-spacing: 0;
    border: 1px solid #EBEBEB;
    padding: 6px 6px 5px 30px;
    background-color: #FFFFFF;
    &::placeholder {
        color: #CBCBCB;
    } 
    &:focus {
        border: 1px solid ${PRIMARY_BASE};
        outline: none;
        caret-color: ${PRIMARY_BASE};
    }
    border: ${({ focus }) => (focus === true ? `1px solid ${PRIMARY_BASE}` : "")};
    border-radius: 5px;
`;

const InputWrapper = styled.div`
    height: 28px;
    width: 191px;
    display: flex;
    align-items: center;
    position: relative;
    &  svg {
        position: absolute;
        left: 8px;
        height: 16px;
        width: 16px;
        g {
            stroke: #CBCBCB;
        }
    }

`

const SearchInput = forwardRef(({
    wrapperClassName,
    ...props

}, ref) => {
    return (

            <InputWrapper className={wrapperClassName}>
            <Input ref={ref}
                {...props}

            />
            {SearchGlass}
            </InputWrapper>
    );
});

SearchInput.propTypes = {
    wrapperClassName: PropTypes.string
};

export default SearchInput;
