import React, { useRef, useCallback, useState } from "react";
import PropTypes from "prop-types";

import styled, { css } from "styled-components";
import * as variables from "../common/variables";
import "../../style/index.css";
import { InformationWhite, CloseWhite } from "../images/icons/whiteIcons";
import { Information, Close } from "../images/icons/baseIcons";

import {
    ErrorAnim,
    ValidAnim,
    InfoAnim,
    WarningAnim,
} from "../animations/Notifications";

const { WHITE, ERROR, WARNING, SUCCESS, INFO } = variables;

const Wrapper = styled.div`
    position: relative;
    overflow: hidden;
`;

const AlertContainer = styled.div`
    position: relative;
    display: flex;
    height: 59px;
    margin-bottom: 10px;
    border-radius: 5px;
    align-items: center;
    background-color: rgba(0, 0, 0, 0.8);
    max-width: 1482px;

    &:nth-last-child() {
        margin: 0;
    }
    &.hide {
        display: none;
    }

    ${({ timeout }) =>
        timeout &&
        timeout > 0 &&
        css`
            animation: slidein 0.7s, slideout 0.7s ${timeout}s forwards;

            @keyframes slidein {
                0% {
                    transform:translateX(100vw);
                }
                100% {
                    transform:translateX(0);
                }
            }
            @keyframes slideout {
                0% {
                    transform:translateX(0);
                }
                100% {
                    transform:translateX(100vw);
                }
            }
        `}
`;

const AlertText = styled.div`
    margin-left: 13px;
    color: ${WHITE};
    font-family: "Open Sans";
    font-size: 14px;
    line-height: 19px;
    letter-spacing: normal;
    a {
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-left: 20px;
        width: 143px;
        border-radius: 5px;
        background-color: #ffffff;
        font-size: 14px;
        line-height: 18px;
        text-decoration: none;
        position: absolute;
        right: 20px;
        top: 9px;
    }
`;

const FixedAlertContainer = styled.div`
    position: relative;
    display: flex;
    height: 48px;
    margin-bottom: 20px;
    padding: 0 20px;
    border: 1px solid transparent;
    border-radius: 5px;
    align-items: center;

    &:nth-last-child() {
        margin: 0;
    }
    &.hide {
        display: none;
    }
    background: ${({ type }) =>
        type === "info"
            ? "#5494F9"
            : type === "error"
            ? ERROR
            : type === "success"
            ? SUCCESS
            : type === "warning"
            ? WARNING
            : type === "info"
            ? INFO
            : INFO};
`;

const FixedAlertText = styled.p`
    margin-left: 20px;
    flex: 1;
    color: ${({ type }) => (type === "warning" ? "#000000" : "white")};
    font-family: Geomanist Regular;
    font-size: 14px;
    line-height: 18px;
    letter-spacing: normal;
    a {
        height: 32px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-left: 20px;
        width: 143px;
        border-radius: 5px;
        background-color: #ffffff;
        font-size: 14px;
        line-height: 18px;
        text-decoration: none;
        position: absolute;
        right: 20px;
        top: 9px;
    }
`;

const Alert = ({ id, type, message, remove, timeout, holder, showClose, wrapperClassName, containerClassName }) => {
    return timeout > 0 ? (
        <Wrapper className={wrapperClassName}>
            <AlertContainer
                id={id}
                holder={holder}
                type={type}
                timeout={timeout}
                className={containerClassName}
                role="alert-animated"
            >
                <div style={{ display: "flex", flex: 1, padding: "0 20px" }}>
                    <div>{InformationWhite}</div>
                    <AlertText type={type}>{message}</AlertText>
                    {showClose && (
                        <div
                            style={{ flex: 1, textAlign: "right" }}
                            onClick={remove}
                        >
                            {CloseWhite}
                        </div>
                    )}
                </div>
                {type === "error" ? (
                    <ErrorAnim timeout={timeout} />
                ) : type === "success" ? (
                    <ValidAnim timeout={timeout} />
                ) : type === "warning" ? (
                    <WarningAnim timeout={timeout} />
                ) : (
                    <InfoAnim timeout={timeout} />
                )}
            </AlertContainer>
        </Wrapper>
    ) : (
        <FixedAlertContainer id={id} holder={holder} type={type} role="alert-fixed">
            {type === "warning" ? Information : InformationWhite}
            <FixedAlertText type={type}>{message}</FixedAlertText>
            {showClose && (
                <div onClick={remove} role="alert-close">
                    {type === "warning" ? Close : CloseWhite}
                </div>
            )}
        </FixedAlertContainer>
    );
};

Alert.propTypes = {
    id: PropTypes.string,
    type: PropTypes.string,
    holder: PropTypes.string,
    message: PropTypes.string,
    remove: PropTypes.func,
    showClose: PropTypes.bool,
    timeout: PropTypes.number,
    containerClassName: PropTypes.string,
    wrapperClassName: PropTypes.string
};

Alert.defaultProps = {
    id: null,
    type: "info",
    message: "",
    holder: null,
    timeout: 0,
    showClose: true,
    remove: () => {},
};

export default Alert;
