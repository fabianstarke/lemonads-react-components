import React from "react";
import styled from "styled-components";
import { useTable, useSortBy, usePagination, useExpanded } from "react-table";
import PropTypes from "prop-types";
import { ChevronDown, ChevronUp } from "../images/icons/baseIcons";
import { Pagination } from "./Pagination";
import * as variables from "../common/variables";

const { GREY_1, GREY_2,GREY_5, BLACK } = variables;

const StyledSecondaryTable = styled.div`
    font-size: 14px;
    table {
        width: 100%;
        border-collapse: collapse;
        thead {
            background-color: ${GREY_1};
            tr {
                height: 30px;
                border-bottom: 1px solid ${GREY_2};
                th {
                    color: ${GREY_5};
                }
            }

            .chevron {
                display: inline-flex;
                flex-direction: column;
                vertical-align: middle;
                inline-size: fit-content;
                float: right;
                height: 20px;
                svg {
                    width: 10px;
                    g {
                        stroke-width: 2px;
                    }
                    path {
                        stroke: #cbcbcb;
                    }
                }
                &.desc {
                    svg {
                        &:nth-child(2) {
                            path {
                                stroke: ${BLACK};
                            }
                        }
                    }
                }
                &.asc {
                    svg {
                        &:nth-child(1) {
                            path {
                                stroke: ${BLACK};
                            }
                        }
                    }
                }
            }
        }
        tbody {
            tr {
                height: 40px;
                border-bottom: 1px solid ${GREY_2};
                td {
                    .table-button {
                        display: inline-flex;
                        justify-content: center;
                        width: 100%;
                        height: 35px;
                        > * {
                            display: flex;
                        }

                        .table-action-icon {
                            margin-right: 10px;
                            cursor: pointer;
                            background-size: 20px;
                            background-repeat: no-repeat;
                            height: 100%;
                            width: 20px;
                            background-position: center;
                        }
                    }
                }
            }
        }
        tr {
            &.striped td {
                background-color: #fafafa;
            }
            th,
            td {
                font-weight: normal;
                text-align: left;
                padding: 10px;
                line-height: 19px;
                vertical-align: middle;
                .subrow-title {
                    color: #6b6b6b;
                }
                .subrow1 {
                    display: none;
                }
                .subrow2 {
                    display: none;
                }
            }
            td:first-child .subrow1 {
                display: block;
                padding-top: 10px;
            }
            td:nth-child(2) .subrow2 {
                display: block;
                padding-top: 10px;
            }
        }
    }
`;

const SecondaryTable = ({
    columns,
    data,
    defaultSort,
    renderRowSubComponent,
}) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        pageCount,
        gotoPage,
        setPageSize,

        state: { pageSize },
    } = useTable(
        {
            columns,
            data,
            initialState: { pageSize: 50, sortBy: defaultSort },
            disableSortRemove: true,
        },
        useSortBy,
        useExpanded,
        usePagination
    );

    return (
        <StyledSecondaryTable>
            <table {...getTableProps()}>
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => (
                                <th
                                    {...column.getHeaderProps([
                                        {
                                            style: column.style,
                                        },

                                        column.getSortByToggleProps(),
                                    ])}
                                >
                                    {column.render("Header")}
                                    {column.canSort ? (
                                        <div
                                            className={`chevron ${
                                                column.isSorted
                                                    ? column.isSortedDesc
                                                        ? "desc"
                                                        : "asc"
                                                    : ""
                                            }`}
                                        >
                                            {ChevronUp}
                                            {ChevronDown}
                                        </div>
                                    ) : (
                                        ""
                                    )}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <React.Fragment key={i}>
                                <tr
                                    className={i % 2 ? "striped" : ""}
                                    {...row.getRowProps()}
                                >
                                    {row.cells.map((cell) => {
                                        return (
                                            <td {...cell.getCellProps()}>
                                                {cell.render("Cell")}
                                            </td>
                                        );
                                    })}
                                </tr>

                                {row.isExpanded && (
                                    <tr
                                        className={`${
                                            i % 2 ? "striped" : ""
                                        } additional-infos`}
                                    >
                                        {renderRowSubComponent(row)}
                                    </tr>
                                )}
                            </React.Fragment>
                        );
                    })}
                </tbody>
            </table>
            <Pagination
                pageSizeOptions={[25, 50, 100, 200, 500]}
                pages={pageCount}
                onPageChange={(n) => gotoPage(n)}
                onPageSizeChange={(n) => setPageSize(n)}
                pageSize={pageSize}
            />
        </StyledSecondaryTable>
    );
};

SecondaryTable.propTypes = {
    columns: PropTypes.array,
    data: PropTypes.array,
    defaultSort: PropTypes.array,
    renderRowSubComponent: PropTypes.any,
};

SecondaryTable.defaultProps = {
    defaultSort: [],
};

export default SecondaryTable;
