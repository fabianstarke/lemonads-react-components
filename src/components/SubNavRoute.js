import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { ResizeObserver as ResizeObserverPolyfill } from '@juggle/resize-observer';

const ResizeObserver = window.ResizeObserver || ResizeObserverPolyfill;

import { SubNav } from "./SubNav";

const StyledSubNavRoute = ({ labels, location, link, animated, role }) => {
    const [width, setWidth] = useState();
    const [leftPos, setLeftPos] = useState();
    const [borderVisible, setBorderVisible] = useState(true);
    const [useAnimation, setUseAnimation] = useState(false);

    const itemsRef = useRef([]);

    const sizeObserver = useRef(
        new ResizeObserver(() => {
            let index = Object.keys(labels).find(
                (key) => labels[key].pathname === location
            );
            let activeElement = itemsRef.current[index];
            if(activeElement) {
                setWidth(activeElement.offsetWidth);
                setLeftPos(activeElement.offsetLeft);
            }
        })
    );

    useEffect(() => {
        if(animated === true) {
            let index = Object.keys(labels).find(
                (key) => labels[key].pathname === location
            );
            if (index !== undefined) {
                let activeElement = itemsRef.current[index];
                if(activeElement) {
                    setWidth(activeElement.offsetWidth);
                    setLeftPos(activeElement.offsetLeft);
                    if(useAnimation === false) {
                        sizeObserver.current.observe(activeElement);
                    }
                }
            }
        }
    }, [labels, location]);

    useEffect(() => {
        setBorderVisible(!useAnimation);
    }, [useAnimation]);

    let handleNavClick = (index) => {
        if (borderVisible === true || useAnimation === false) {
            sizeObserver.current.disconnect();
            setUseAnimation(true);
        }

        let activeElement = itemsRef.current[index];
        if(activeElement) {
            setWidth(activeElement.offsetWidth);
            setLeftPos(activeElement.offsetLeft);
        }
    };

    let LinkElement = link;
    return (
        <SubNav
            width={width}
            left={leftPos}
            borderVisible={borderVisible}
            useAnimation={useAnimation}
            animated={animated}
            role={role}
        >
            {labels.map((item, index) => (
                <LinkElement key={index} to={item.pathname}>
                    <li
                        key={index}
                        className={`${
                            location === item.pathname ? "active" : ""
                        } `}
                        title={item.title}
                        ref={(el) => (itemsRef.current[index] = el)}
                        onClick={() => animated ? handleNavClick(index) : {}}
                    >
                        {item.title}
                    </li>
                </LinkElement>
            ))}
            {animated === true ? <div className="border"></div> : ""}
        </SubNav>
    );
};

StyledSubNavRoute.propTypes = {
    labels: PropTypes.array,
    location: PropTypes.string,
    link: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.func
    ]),
    animated: PropTypes.bool,
    role: PropTypes.string
};

StyledSubNavRoute.defaultProps = {
    labels: [
        { title: "Profile", pathname: "/profile" },
        { title: "Business", pathname: "/profile/business" },
        { title: "User", pathname: "/profile/users" },
    ],
    location: "/profile",
    link: Link,
    animated: false
};
export default StyledSubNavRoute;
