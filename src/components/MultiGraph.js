import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Graph } from './Graph';
import ReactSelect from './ReactSelect';
import Skeleton from '../components/Skeleton'

const emptySelect = [{name:'No data', label:0}]

export class MultiGraph extends React.Component {
    constructor(props) {
        super(props);
        let options = this.props.data ? Object.keys(this.props.data) : [];
        this.state = {
            options: options,
            selected: this.props.defaultOption
        }
    }

    componentDidUpdate(prevProps){
        if(prevProps.data !== this.props.data || prevProps.uniqueid !== this.props.uniqueid){
            let options = this.props.data ? Object.keys(this.props.data) : [];
            this.setState({
                options: options,
            })    
        }
    }

    render() {
        let selectedOption = this.state.options.length>0 ? this.state.options[this.state.selected] : 'EMPTY';
        
        let isEmpty = this.props.data==null || this.props.data==undefined || this.props.data[selectedOption]==undefined || Object.keys(this.props.data).length==0;
        let dataX = isEmpty ? [] : this.props.data[selectedOption].dataX;
        let dataY = isEmpty ? [] : this.props.data[selectedOption].dataY;

        let options = isEmpty ? emptySelect : this.state.options.map((option, index) => { return { label: this.props.data[option].title, value: index } });
        let style = {};
        style.filter = (isEmpty ? "blur(3px)" : "");
        
        return (

            this.props.loading ?
            <Skeleton width={this.props.skeletonWidth} height={this.props.skeletonHeight} backgroundColor={this.props.backgroundColor}/> 
            :
            <GraphsContainer border={this.props.border} isEmpty={isEmpty} style={{height:this.props.height,width:this.props.width, overflow:this.props.overflow}}>
                    <div style={style}>
                        <SelectContainer>
                            <ReactSelect options={options} onChange={ev=> this.setState({selected: ev.value})} value={this.state.selected} isSearchable={false}/>
                        </SelectContainer>
                        <Graph
                            width={this.props.graphWidth} 
                            height={this.props.graphHeight}
                            options={this.props.options}
                            dataX={dataX}
                            dataY={dataY}
                            uniqueid={this.props.uniqueid+"#"+this.state.selected}/>
                    </div>
                    {isEmpty && <EmptyCover></EmptyCover>}
                    {isEmpty &&     <ButtonWrapper>No data yet</ButtonWrapper>}
            </GraphsContainer>
        );
    }
}

MultiGraph.propTypes = {
    border: PropTypes.bool,
    graphHeight: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    graphWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    height: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    data: PropTypes.object,
    size: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    overflow: PropTypes.string,
    defaultOption: PropTypes.number,
    skeletonHeight: PropTypes.string,
    skeletonWidth: PropTypes.string
}

MultiGraph.defaultProps = {
    border: true,
    height: null,
    width: null,
    graphHeight: null,
    graphWidth: null,
    data: {},
    uniqueid: '',
    defaultOption: 0,
}

const SelectContainer = styled.div`
&>div{
    position:relative;
    margin: 0 0 29px auto;    
}
`;

const GraphsContainer = styled.div`
border: ${(props) => props.border ? props.isEmpty ? '1px solid rgb(255,255,255,0.9)' : '1px solid #CBCBCB' : '0'};
border-radius: 5px;
box-sizing:border-box;
background: #FFF;
padding: 20px;
position: relative;
overflow: auto;
`;

const ButtonWrapper = styled.div`
z-index:98;
position: absolute;
top:50%;
left:50%;
transform: translate(-50%,-50% );
color: black;
text-align: center;
margin-top: auto;
line-height: 28px;
font-size: 16px;
font-weight: 600;
`;

const EmptyCover = styled.div`
background: #ffffffcc;
position:absolute;
z-index:97;
left:0;
top:0;
width: 100%;
height: 100%
/**/
`;

export default MultiGraph;
