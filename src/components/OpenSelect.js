import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";


export class OpenSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.selected
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected })
        }
    }

    handleChange(selected) {
        if (this.state.selected == selected) return;
        if (this.props.onChange) {
            this.props.onChange(selected);
        }
        this.setState({ selected })
    }

    render() {
        return (
            <StyledWrapper role="select" className={this.props.className}>
                {this.props.options.map((option,i) => {
                    return <StyledOption
                        key={i}
                        onClick={() => { this.handleChange.bind(this)(option.value) }}
                        className={this.state.selected == option.value && "selected"}
                        key={i}
                    >
                        {option.label}
                    </StyledOption>
                })}
            </StyledWrapper>
        );
    }
}

OpenSelect.propTypes = {
    className: PropTypes.string,
    selected: PropTypes.number,
};

OpenSelect.defaultProps = {
    selected: null
};

const StyledWrapper = styled.div`
    display:inline-block;
    border: 1px solid #CBCBCB;
    border-radius: 5px;
    padding: 10px 5px;
    cursor:default;
    user-select: none;
    `;

const StyledOption = styled.div`
    a{
        text-decoration: unset;
        color:unset;
    }
    white-space:nowrap;
    text-align:left;
    padding: 7px 10px;
    border-radius:5px;
    &.selected, &:hover {
        background:#000;
        color:#fff;
    }
    &:hover{
        &:not(.selected){
            cursor:pointer;
        }
    }
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
`;

export default OpenSelect;
