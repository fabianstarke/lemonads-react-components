import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { CheckValid } from "../images/icons/baseIcons";
import * as variables from "../common/variables";

const {GREY_2, WHITE, BLACK, SUCCESS, GREY_1} = variables;

const Wrapper = styled.div`
    width: fit-content;
    display: flex;
    flex-direction: column;
`;

const IconWrapper = styled.div`
         & svg {
        height: 16px;
        width: 16px;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        filter: invert(1);
    }

`;

const Cercle = styled.div`
    position: relative;
    height: 24px;
    width: 24px;
    border-radius: 50%;
    border: solid 1px ${GREY_2};
    background: ${({readOnly}) => readOnly === true ? GREY_1 : WHITE};
    cursor: pointer;
    &.active {
        background: ${SUCCESS};
        border: none;
    }
`;

const Label = styled.label`
    height: ${({ label }) => label ? "19px" : ""};
    color: ${BLACK};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
    margin-bottom: 7px;
    ${({ label }) => label}
`;

const SelectedIndicator = ({ selected, label, onClick, className, readOnly }) => {
    return (
        <Wrapper onClick={onClick} className={className} >
            {label && <Label>{label}</Label>}
            <IconWrapper>
                <Cercle className={`${selected ? 'active' : '""'}`} readOnly={readOnly}>
                    { selected && CheckValid }
                </Cercle>
            </IconWrapper>
        </Wrapper>
    );
};

SelectedIndicator.propTypes = {
    label: PropTypes.string,
    selected: PropTypes.bool,
    className: PropTypes.string,
    readOnly: PropTypes.bool
};

SelectedIndicator.defaultProps = {
    selected: false,
    readOnly: false
};

export default SelectedIndicator;
