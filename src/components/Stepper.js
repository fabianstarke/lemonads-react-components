import React, { Component } from "react";
import PropTypes from 'prop-types';

import { CheckValid } from "../images/icons/baseIcons";
import styled from "styled-components";
import * as variables from "../common/variables";
const { SUCCESS, BLACK, GREY_2, GREY_6, WHITE } = variables;

const Wrapper = styled.div`
    position: absolute;
    left: 0px;
    width: max-content;
    height: auto;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
`;

const NavBar = styled.div`
    width: 100%;
    margin: 0px auto 10px auto;
    text-align: center;
    display: flex;
`;

const Step = styled.div``;
const Circle = styled.div`
    display: inline-block;
    width: 24px;
    height: 24px;
    border-radius: 24px;
    &.done {
        border: 1px solid ${SUCCESS};
        background-color: ${SUCCESS};
    }
    &.previous {
        background-color: red !important;
    }

    &.span {
        color: red;
    }

    &.current {
        border: 1px solid ${BLACK};
        &.hide {
            display: inline-block !important;
        }
    }

    background: white;
    border: 1px solid ${GREY_6};
    position: relative;
    cursor: pointer;
`;

const Bar = styled.span`
    display: inline-block;
    width: 23px;
    height: 23px;
    border-radius: 24px;
    &.done {
        border-top: 1px solid ${SUCCESS};
    }

    &.hide {
        display: none;
    }

    &.current {
        &.hide {
            display: inline-block !important;
        }
    }
    &.small {
        width: 110px;
    }

    position: relative;
    border-top: 1px solid ${GREY_2};
    width: 178px;
    height: 0;
    top: -11px;
    margin-right: -5px;
    border-left: none;
    border-right: none;
    border-radius: 0;
`;

const Title = styled.span`
    color: ${BLACK};
    font-size: 14px;
    line-height: 19px;
    width: 160px;
    left: 50%;
    margin-left: -80px;
    position: absolute;
    top: 30px;
`;

const Index = styled.span`
    display: inline-block;
    width: 100%;
    height: 100%;
    line-height: 22px;
    border-radius: 50%;
    color: ${GREY_6};
    font-family: "Open Sans";
    font-size: 14px;
    padding: inherit;
    position: absolute;
    left: 0;
    svg {
        height: 11px;
        width: 11px;
        g {
            stroke: #ffffff;
        }
    }
    &.current {
        color: ${BLACK};
    }
    &.previous {
        background: white;
    }
    &.done {
        color: ${WHITE};
    }
`;

class Stepper extends Component {
    constructor(props) {
        super(props);
        this.handleNavClick = this.handleNavClick.bind(this);
        this.container = React.createRef();
        this.steps = [];

        this.state = {
            currentIndex: this.props.currentIndex || 0,
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.currentIndex !== this.props.currentIndex) {
            this.setState({
                currentIndex: this.props.currentIndex,
            });
        }
    }

    setNavitems(navItems) {
        return navItems.map((title, index) => {
            return {
                title,
                active: index === this.state.currentIndex ? true : false,
                done: index < this.state.currentIndex,
            };
        });
    }

    handleNavClick(e, title) {
        let titleIndex = this.props.navItems.indexOf(title);
        this.props.onChangePeriodIndex(e, titleIndex);
        this.setNavitems(this.props.navItems, titleIndex);
    }

    render() {
        const { navItems, small } = this.props;
        const { currentIndex, wrap } = this.state;

        let items = this.setNavitems(navItems, currentIndex);

        return (
            <Wrapper ref={this.container} role="stepper">
                <NavBar>
                    {items.map((item, index) =>
                        !this.props.small ||
                        (currentIndex < 3 && index <= 2) || 
                        (currentIndex > items.length - 3 && index > items.length - 5) ||
                        (currentIndex === index) ||
                        (index === currentIndex - 1) ||
                        (index === items.length - 1) || 
                        (index === currentIndex + 1 && currentIndex < 2) ? (
                            <Step
                                id={`step-${index}`}
                                ref={(ref) => (this.steps[index] = ref)}
                                key={index}
                            >
                                <Circle
                                    className={` ${
                                        item.active
                                            ? "current"
                                            : item.done
                                            ? "done"
                                            : ""
                                    }`}
                                    onClick={(e) =>
                                        this.handleNavClick(e, item.title)
                                    }
                                >
                                    <Index
                                        className={`${
                                            item.active
                                                ? "current"
                                                : item.done
                                                ? "done"
                                                : ""
                                        } ${
                                            wrap &&
                                            currentIndex > index &&
                                            index != 0
                                                ? "previous"
                                                : wrap && currentIndex < index
                                                ? "next"
                                                : ""
                                        }`}
                                    >
                                        {item.done &&
                                        index !== currentIndex &&
                                        !wrap
                                            ? CheckValid
                                            : wrap &&
                                              index != 0 &&
                                              index != items.length - 1 &&
                                              index != currentIndex
                                            ? "..."
                                            : navItems.indexOf(item.title) + 1}
                                    </Index>
                                    <Title>{item.title}</Title>
                                </Circle>
                                {index !== items.length - 1 ? (
                                    <Bar
                                        id={`bar-${index}`}
                                        className={`bar ${
                                            item.active
                                                ? "current"
                                                : item.done
                                                ? "done"
                                                : ""
                                        }  ${small ? "small" : ""}`}
                                    />
                                ) : (
                                    ""
                                )}
                            </Step>
                        ) : index === currentIndex - 2 ||
                          (index === currentIndex + 2 && currentIndex < 2) ||
                          (index === currentIndex + 1 && currentIndex > 1) ||
                          (currentIndex === items.length - 2 &&
                              index === currentIndex - 3) ||
                          (currentIndex === items.length - 1 &&
                              index === currentIndex - 4) ||
                          (currentIndex === 0 && index === currentIndex + 3) ? (
                            <Step
                                id={`step-${index}`}
                                ref={(ref) => (this.steps[index] = ref)}
                                key={index}
                            >
                                <Circle
                                    onClick={(e) =>
                                        this.handleNavClick(e, item.title)
                                    }
                                    className={` ${
                                        item.active
                                            ? "current"
                                            : item.done
                                            ? "done"
                                            : ""
                                    }`}
                                >
                                    <Index
                                        className={` ${
                                            item.active
                                                ? "current"
                                                : item.done
                                                ? "done"
                                                : ""
                                        }`}
                                    >
                                        ...
                                    </Index>
                                    <Title>
                                        {index < currentIndex
                                            ? "Previous Steps"
                                            : "Other Steps"}
                                    </Title>
                                </Circle>
                                <Bar
                                    id={`bar-${index}`}
                                    className={` bar${
                                        item.active
                                            ? "current"
                                            : item.done
                                            ? "done"
                                            : ""
                                    } ${small ? "small" : ""}`}
                                />
                            </Step>
                        ) : (
                            ""
                        )
                    )}
                </NavBar>
            </Wrapper>
        );
    }
}

Stepper.propTypes = {
    navItems: PropTypes.array,
    currentIndex: PropTypes.number,
    small: PropTypes.bool,
    onChangePeriodIndex: PropTypes.func,
};

Stepper.defaultProps = {
    navItems: ["Step1", "Step2", "Step3"],
    currentIndex: 0,
    onChangePeriodIndex: () => console.log("clicked"),
    small: false
};


export default Stepper;
