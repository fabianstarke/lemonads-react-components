import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";

const {
    WHITE,
    BLACK,
    GREY_2,
    GREY_3,
    GREY_5,
    PRIMARY_BASE,
    SUCCESS,
    WARNING,
    ERROR,
} = variables;

const StyledButton = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: ${(props) => props.type === "primary-disabled" || props.type === "secondary-disabled" ? "" : "pointer"};
    user-select: none;
    height: 28px;
    border-radius: 5px;
    font-size: 14px;
    line-height: 8px;
    border: 1px solid;
    padding: 0 30px;
    width: ${(props) => props.full ? "100%" : "fit-content"};
    color: ${(props) =>
        props.type === "primary" || props.type === "error" || props.type === "success"
            ? WHITE
            : props.type === "secondary-blue"
            ? PRIMARY_BASE
            : props.type === "primary-disabled"
            ? GREY_5
            : props.type === "secondary-disabled" 
            ? GREY_3 :
            BLACK};
    &:hover {
        color: ${(props) =>
            props.type === "secondary"
                ? WHITE
                : ""};
    }
    border-color: ${(props) =>
        props.type === "secondary-disabled"
            ? GREY_3
            : props.type === "primary-disabled"
                ? GREY_2
                : props.type === "secondary"
                    ? BLACK
                    : props.type === "error"
                        ? ERROR
                        : props.type === "warning"
                            ? WARNING
                            : props.type === "success"
                                ? SUCCESS
                            : PRIMARY_BASE};
    background-color: ${(props) =>
        props.type === "secondary" || props.type === "secondary-blue" || props.type === 'secondary-disabled'
            ? WHITE
            : props.type === "primary-disabled"
                ? GREY_2
                : props.type === "error"
                    ? ERROR
                    : props.type === "warning"
                        ? WARNING
                        : props.type === "success"
                            ? SUCCESS
                            : PRIMARY_BASE};
    &:hover {
        background-color: ${(props) =>
            props.type === "secondary"
                ? BLACK
                : ""};
    }

    a {
        text-decoration: none;
        color: inherit;
    }
`;

const Button = ({ type, label, onClick, className, full, id, ...props }) => {
    return (
        <StyledButton type={type} onClick={onClick} className={className} full={full} id={id} {...props}>
            {label}
        </StyledButton>
    );
};

Button.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    full: PropTypes.bool,
    id: PropTypes.string,
};

Button.defaultProps = {
    type: "primary",
    full: false
};

export default Button;
