import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import {
    sortableContainer,
    sortableElement,
    arrayMove,
} from "react-sortable-hoc";

class OrderableList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            order: this.getDefaultOrder(),
        };
        this.onSortEnd = this.onSortEnd.bind(this);
        this.onSortStart = this.onSortStart.bind(this);
    }

    getDefaultOrder(){
        if(this.props.order==null){
            var order = [];
            for(var i=0; i<this.props.children.length; i++){
                order.push(i)
            }
            return order;
        } else {
            return this.props.order;
        }
    }

    componentDidUpdate(prevProps) {
        if ((JSON.stringify(prevProps.order) != JSON.stringify(this.props.order))
            || (this.props.children.length != prevProps.children.length)) {
            this.setState({ order: this.getDefaultOrder() })
        }
    }

    onSortEnd({ oldIndex, newIndex }) {
        let order = this.state.order;
        let newOrder = arrayMove(order, oldIndex, newIndex);
        this.setState({ order: newOrder });

        let draggedItem = this.state.order[oldIndex];
        document.getElementById(draggedItem).classList.remove("hold");
        setTimeout(
            () => document.getElementById(draggedItem).classList.remove("hide"),
            1
        );
        if (this.props.onChange) {
            this.props.onChange(newOrder);
        }
    }

    onSortStart({ node, index, collection, isKeySorting }, event) {
        let draggedItem = this.state.order[index];
        setTimeout(
            () => document.getElementById(draggedItem).classList.add("hide"),
            1
        );
    }

    render() {
        const { order } = this.state;
        const { children } = this.props;

        const SortableItem = sortableElement(({ value }) => (
            <li key={value} className="sortable-item">
                <div id={value} className="handle">
                    {React.cloneElement(children[value], { key: value })}
                </div>
            </li>
        ));

        const SortableList = sortableContainer(({ items }) => {
            return (
                <ul className="sortable-list">
                    {items.map((value, index) => (
                        <SortableItem
                            key={`item-${value}`}
                            index={index}
                            value={value}
                        />
                    ))}
                </ul>
            );
        });

        return (
            <StyledWrapper role={this.props.role}>
                <SortableList
                    items={order}
                    lockAxis="y"
                    onSortEnd={this.onSortEnd}
                    onSortStart={this.onSortStart}
                    helperClass={this.props.helperClass}
                />
            </StyledWrapper>
        );
    }
}

OrderableList.propTypes = {
    order: PropTypes.array,
    onChange: PropTypes.func,
    helperClass: PropTypes.string,
    role: PropTypes.string
};

OrderableList.defaultProps = {
    order: null,
    onChange: () => { }
};

const StyledWrapper = styled.div`
    ul {
        margin: 0;
        padding: 0;
        list-style-type: none;

        li {
            position: relative;
        }
    }
`;


export default OrderableList;
