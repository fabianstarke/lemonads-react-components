import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Lottie } from "@crello/react-lottie";

import * as variables from "../common/variables";

import checkboxAnimation from "../animations/json/checkbox.json";
import checkValid from "../images/icons/baseIcons/checkValid_white.svg";

const { BLACK, WHITE } = variables;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
`;

const Label = styled.label`
    color: ${BLACK};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
    margin-bottom: 7px;
    ${({ label }) => label}
`;

const LabelRight = styled.label`
    color: ${BLACK};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    margin: 0;
    margin-left: 10px;
    align-self: center;
    ${({ labelRight }) => labelRight}
`;

const Input = styled.input`
    height: 13px;
    width: 13px;
    &.hidden {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
        padding: 0;
        margin: 0;
    }
    &.visible {
        border: 1px solid ${BLACK};
        background: ${({ checked }) => (checked ? BLACK : WHITE)};
        margin: 3px;
        border-radius: 4.5px;
        /* Ne pas afficher le style par défaut navigateur*/
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        &:focus {
            outline: none;
        }
        &.checked {
            background-image: ${`url(${checkValid})`};
            background-position: center;
            background-size: 11px;
            background-repeat: no-repeat;
        }
    }
`;

const InputWrapper = styled.div`
    display: flex;
    position: relative;
    svg {
        pointer-events: none;
    }
`;
const CheckBox = ({
    label,
    name,
    checked,
    value,
    id,
    className,
    labelRight,
    onChange,
}) => {
    const defaultLottieOptions = {
        loop: false,
        autoplay: false,
        animationData: checkboxAnimation,
    };

    const [animDirection, setAnimDirection] = useState(checked ? -1 : 1);
    const [playing, setPlaying] = useState("stopped");
    const [useAnimation, setUseAnimation] = useState(false);

    let handleChange = () => {
        let direction = animDirection * -1;
        setAnimDirection(direction);
        setPlaying("playing");
    };

    useEffect(() => {
        handleChange(checked);
    }, [checked]);

    return (
        <Wrapper>
            {label && <Label htmlFor={id}>{label}</Label>}
            <InputWrapper>
                <Input
                    type="checkbox"
                    className={`${className} ${
                        useAnimation ? "hidden" : "visible"
                    } ${
                        checked ? "checked" : ""
                    }`}
                    name={name}
                    onChange={onChange}
                    onClick={() => setUseAnimation(true)}
                    checked={checked}
                    value={value}
                    id={id}
                />
                {useAnimation ? (
                    <Lottie
                        config={defaultLottieOptions}
                        height={19}
                        width={19}
                        speed={1.5}
                        direction={animDirection}
                        playingState={playing}
                    />
                ) : (
                    ""
                )}

                {labelRight && (
                    <LabelRight htmlFor={id}> {labelRight}</LabelRight>
                )}
            </InputWrapper>
        </Wrapper>
    );
};

CheckBox.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool,
    ]),
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    className: PropTypes.string,
    labelRight: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
    onChange: PropTypes.func,
};

CheckBox.defaultProps = {
    checked: false,
    onChange: () => {}
};

export default CheckBox;
