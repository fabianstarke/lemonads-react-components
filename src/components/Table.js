import React, { useState, useEffect } from "react";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";

const { GREY_2, GREY_5, PRIMARY_BASE } = variables;

const Table = styled.div`
    table {
        width: 100%;
        thead {
            width: 100%;
            th.headTitle,
            th.headTitle-left {
                font-weight: 400;
                padding: 6px 5px;
                line-height: 19px;
                white-space: nowrap;
                font-size: 14px;
                color: ${GREY_5};
                user-select: none;
                border-bottom: 1px solid ${GREY_2};
                text-align: right;
                cursor: default;

                &:first-child {
                    padding-left: 10px;
                }
                &:last-child {
                    padding-right: 10px;
                }
                &.special-margin {
                    padding: 8px;
                    height: 100%;
                    vertical-align: middle;
                }
                .label-container {
                    display: flex;
                    text-align: right;
                    justify-content: flex-end;
                    color: ${GREY_5};
                    flex-direction: row;
                }
            }

            th.headTitle-left {
                text-align: left;
                .label-container {
                    text-align: left;
                    justify-content: space-between;
                }
            }
        }
        tr {
            td {
                border-bottom: 1px solid ${GREY_2};
                padding: 7px 5px;
                line-height: 19px;
                font-size: 14px;
                vertical-align: middle;
                text-align: right;
                &.align-left {
                    text-align: left;
                }
                &:first-child {
                    padding-left: 10px;
                }
                &:last-child {
                    padding-right: 10px;
                }
                a {
                    color: ${PRIMARY_BASE};
                }
                span {
                    display: inline-flex;
                    img {
                        padding-left: 10px;
                    }
                }
            }
        }
    }
    .load-more {
        color: ${PRIMARY_BASE};
        text-align: center;
        margin-top: 26px;
        line-height: 19px;
        font-size: 14px;
        cursor: pointer;
    }
`;

const sortItems = (listItems, headTitles) => {
    let items = [...listItems];

    const sortIndex = headTitles.findIndex(
        (item) => item.hasOwnProperty("sort") && item.sort !== 0
    );

    if (sortIndex === -1 || headTitles[sortIndex].sort === 0) {
        return items;
    }

    const sortType = headTitles[sortIndex].hasOwnProperty("type")
        ? headTitles[sortIndex].type
        : "default";

    const sortedItems = items.sort((a, b) => {
        let firstElem = Array.isArray(a) ? a : a['datas'];
        let secondElem = Array.isArray(b) ? b : b['datas'];

        const toSortA = getItemByType(firstElem[sortIndex], sortType);
        const toSortB = getItemByType(secondElem[sortIndex], sortType);
        
        if (toSortA > toSortB) {
            return -1;
        } else if (toSortA < toSortB) {
            return 1;
        } else {
            return 0;
        }
    });

    if (headTitles[sortIndex].sort === 1) {
        return sortedItems;
    } else {
        return sortedItems.reverse();
    }
};

const getItemByType = (item, type) => {
    switch (type) {
        case "dateInString":
            return setDateType(item);
        default:
            return setDefaultType(item);
    }
};

const setDateType = (item) => {
    const result = new moment(item, "YYYY-MM-DD");
    if (result.isValid) {
        return result;
    }
    return undefined;
};

const setDefaultType = (item) => {
    const regexIsNumber = new RegExp("^[0-9.]| %", "g");
    regexIsNumber.lastIndex = 0;

    if (item === "-") {
        item = 0;
    }
    if (regexIsNumber.test(item)) {
        return toInt(item);
    }

    if (typeof item === "string") {
        return item.toLowerCase();
    }
    return item;
};

const toInt = (str) => {
    return Number(`${str}`.replace(/\s/g, "").replace(/%/g, ""));
};

const StyledTable = ({
    id,
    headTitles,
    listItems,
    align,
    specialMargin,
    elementPerPage,
    clickToLoad,
    onLineClick,
    sortable,
    className,
    role,
}) => {
    const [loadedElements, setLoadedElements] = useState(elementPerPage);
    const [titles, setTitles] = useState(headTitles);

    useEffect(() => {
        setLoadedElements(elementPerPage);
        setTitles(headTitles);
    }, [elementPerPage, headTitles]);

    let tableHeadTitles = [];

    titles.forEach((title, index) => {
        tableHeadTitles[index] = "headTitle";
        if (align && align[index]) {
            tableHeadTitles[index] =
                align[index] === "left" ? "headTitle-left" : "headTitle";
        }
        if (specialMargin && specialMargin === true) {
            tableHeadTitles[index] += tableHeadTitles[index] !== "" ? " " : "";
            tableHeadTitles[index] +=
                specialMargin === true ? "special-margin" : "";
        }
    });

    let handleSortClick = (index) => {
        let newTitles = [...titles].map((item, itemIndex) => {
            if (item.hasOwnProperty("sort") && itemIndex !== index)
                item.sort = 0;
            return item;
        });
        newTitles[index].sort =
            newTitles[index].sort <= 0
                ? newTitles[index].sort === -1
                    ? 0
                    : 1
                : -1;

        setTitles(newTitles);
    };

    let items = sortItems(listItems, titles);
    items = clickToLoad ? items.slice(0, loadedElements) : items;

    return (
        <Table id={id} className={className} role={role}>
            <table>
                <thead>
                    <tr>
                        {titles.map((item, index) =>
                            typeof item === "object" ? (
                                <th
                                    className={tableHeadTitles[index]}
                                    key={index}
                                    onClick={() =>
                                        sortable ? handleSortClick(index) : ""
                                    }
                                >
                                    <div
                                        className="label-container"
                                        key={index}
                                    >
                                        {item.title}
                                    </div>
                                </th>
                            ) : (
                                <th
                                    className={tableHeadTitles[index]}
                                    key={index}
                                >
                                    {item}
                                </th>
                            )
                        )}
                    </tr>
                </thead>
                <tbody>
                    {items.map((items, index) => (
                        React.isValidElement(items) ? 
                        items :
                        <tr
                            id={"table" + index}
                            key={index}
                            className={
                                !Array.isArray(items) && items.className
                                    ? items.className
                                    : ""
                            }
                            onClick={() => onLineClick && onLineClick(items, index)}
                        >
                            {Array.isArray(items)
                                ? items.map((item, index) => (
                                      <td
                                          key={index}
                                          className={
                                              align &&
                                              align[index] &&
                                              align[index] === "left"
                                                  ? "align-left"
                                                  : ""
                                          }
                                      >
                                          {item}
                                      </td>
                                  ))
                                : items.datas.map((item, index) => (
                                      <td
                                          key={index}
                                          className={
                                              align &&
                                              align[index] &&
                                              align[index] === "left"
                                                  ? "align-left"
                                                  : ""
                                          }
                                      >
                                          {item}
                                      </td>
                                  ))}
                        </tr>
                    ))}
                </tbody>
            </table>
            {clickToLoad && loadedElements < listItems.length && (
                <div
                    className="load-more"
                    onClick={() => setLoadedElements(loadedElements + 5)}
                >
                    Load more
                </div>
            )}
        </Table>
    );
};

StyledTable.propTypes = {
    id: PropTypes.string,
    headTitles: PropTypes.array,
    listItems: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
    align: PropTypes.array,
    specialMargin: PropTypes.bool,
    elementPerPage: PropTypes.number,
    clickToLoad: PropTypes.bool,
    onLineClick: PropTypes.func,
    sortable: PropTypes.bool,
    className: PropTypes.string,
    role: PropTypes.string,
};

StyledTable.defaultProps = {
    specialMargin: false,
    elementPerPage: 20,
    clickToLoad: false,
    sortable: false,
};
export default StyledTable;
