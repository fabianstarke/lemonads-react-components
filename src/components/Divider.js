import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";

const { GREY_2 } = variables;

const StyledDivider = styled.hr`
    margin-top: ${(props) => props.marginTop};
    margin-bottom: ${(props) => props.marginBottom};
    border: none;
    border-top: 1px solid ${GREY_2};
`;

const Divider = ({ marginTop, marginBottom }) => {
    return (
        <StyledDivider
            role="divider"
            className="divider"
            marginTop={marginTop}
            marginBottom={marginBottom}
        />
    );
};

Divider.propTypes = {
    marginTop: PropTypes.string,
    marginBottom: PropTypes.string,
};

Divider.defaultProps = {
    marginTop: "30px",
    marginBottom: "30px",
};

export default Divider;
