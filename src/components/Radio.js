import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Lottie } from "@crello/react-lottie";

import { Label } from "./Input";

import radioAnimation from "../animations/json/radio.json";

export class RadioGroup extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            selected: this.props.selected
        }
        this.radiogroup = React.createRef();
    }

    componentDidMount() {
        if (!this.props.allowUnselected && !this.state.selected) {
            this.setState({
                selected: this.props.children[0].props.value
            })
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selected !== this.props.selected) {
            this.setState({ selected: this.props.selected })
        }
    }

    getChildContext() {
        const { name, disabled, animated } = this.props;
        return {
            radioGroup: {
                name,
                disabled,
                onChange: this.onChange.bind(this),
                animated
            }
        };
    }

    onChange(value) {
        if (!this.props.disabled) {
            if (this.state.selected == value && this.props.allowUnselected) {
                value = undefined;
                // Notify also when unselected (this is a hack)
                this.radiogroup.onchange = (e) => this.props.onChange(e);
                this.radiogroup.dispatchEvent(new Event("change"));
                this.radiogroup.onchange = null;
            }
            this.setState({ selected: value })
        }
    }

    render() {
        const { name, children, ...other } = this.props;
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }} role={this.props.parentRole}>
                {this.props.label && <Label required={this.props.required}>{this.props.label}</Label>}
                <RadioGroupWrapper {...other} ref={(radiogroup) => this.radiogroup = radiogroup}>
                    {children.map((v, i) => {
                        return React.cloneElement(v, { key: name + i, selected: this.state.selected })
                    })}
                </RadioGroupWrapper>
            </div>);
    }
}


const RadioGroupWrapper = styled.div`
    display:inline-flex;
    justify-content:space-between;
`;

RadioGroup.childContextTypes = {
    radioGroup: PropTypes.object,
};

RadioGroup.propTypes = {
    name: PropTypes.string.isRequired,
    selected: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool
    ]),
    children: PropTypes.node.isRequired,
    allowUnselected: PropTypes.bool,
    disabled: PropTypes.bool,
    parentRole: PropTypes.string
};

RadioGroup.defaultProps = {
    allowUnselected: false,
    disabled: false,
}

// eslint-disable-next-line react/no-multi-comp
export class Radio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            direction: 1,
            playing: "stopped",
        };
    }

    componentDidMount() {
        this.handleChange(-1, this.props.selected)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selected != this.props.selected) {
            this.handleChange(prevProps.selected, this.props.selected);
        }
    }

    handleChange(prevSelected, selected) {
        if (prevSelected == this.props.value) {
            this.setState({ direction: -1, playing: "playing" });
        } else if (selected == this.props.value) {
            this.setState({ direction: 1, playing: "playing" });
        }
    }
    render() {
        const { name, onChange, disabled, animated } = this.context.radioGroup;
        const { value, selected, label, ...props } = this.props;
        let checked = selected !== undefined && value == selected;
        const defaultLottieOptions = {
            loop: false,
            autoplay: false,
            animationData: radioAnimation,
        };
        return (
            <RadioWrapper role={this.props.wrapperRole}>
                {this.props.disabled || disabled || animated === false ? (
                    <RadioInputWrapper
                        disabled={disabled || this.props.disabled}
                        checked={checked}
                        animated={animated}
                    >
                        <RadioInput
                            type="radio"
                            name={name}
                            value={value}
                            checked={checked}
                            aria-checked={checked}
                            onClick={this.props.disabled || disabled ? () => { } : (e) => {
                                onChange(e.target.value);
                            }}
                            onChange={() => { }}
                            {...props}
                        />
                        <RadioFill disabled={disabled || this.props.disabled} />
                    </RadioInputWrapper>
                ) : (
                        <AnimationWrapper>
                            <RadioInput
                                type="radio"
                                name={name}
                                value={value}
                                checked={checked}
                                aria-checked={checked}
                                onClick={this.props.disabled || disabled ? () => { } : (e) => {
                                    onChange(e.target.value);
                                }}
                                onChange={() => { }}
                                {...props}
                            />
                            <Lottie
                                config={defaultLottieOptions}
                                height={19}
                                width={19}
                                direction={this.state.direction}
                                playingState={this.state.playing}
                            />
                        </AnimationWrapper>
                    )}
                <RadioLabel disabled={this.props.disabled}>{label}</RadioLabel>
            </RadioWrapper>
        );
    }
}

Radio.contextTypes = {
    radioGroup: PropTypes.object
};

RadioGroup.propTypes = {
    animated: PropTypes.bool
};

RadioGroup.defaultProps = {
    animated: false
};

Radio.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool
    ]),
    label: PropTypes.node,
    disabled: PropTypes.bool,
    wrapperRole: PropTypes.string
};

Radio.defaultProps = {
    value: null,
    label: null,
    disabled: false,
};

const RadioWrapper = styled.label`
    display:inline-flex;
    align-items:center;
`

const RadioInputWrapper = styled.div`
  width: 14px;
  height: 14px;
  position: relative;
  display:inline-flex;
  margin-right:10px;
  margin-left: ${(props) => props.disabled || props.animated ? "2.5px" : "0"};

  &::before {
    content: "";
    border-radius: 100%;
    border: 1.4px solid ${({ disabled, checked }) => (disabled ? '#EBEBEB' : (checked ? '#000000' : '#CBCBCB'))};
    background: #ffffff;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    box-sizing: border-box;
    pointer-events: none;
    z-index: 0;
  }
`;

const AnimationWrapper = styled.div`
    margin-right: 8px;
    width: 19px;
    height: 19px;
    position: relative;
`;

const RadioFill = styled.div`
  background: ${({ disabled }) => (disabled ? '#EBEBEB' : '#000000')};
  width: 0;
  height: 0;
  border-radius: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  pointer-events: none;
  z-index: 1;

  &::before {
    position: absolute;
    content: "";
    opacity: 0;
    width: 8px;
    height: 8px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

const RadioInput = styled.input`
  opacity: 0;
  z-index: 2;
  position: absolute;
  width: 100%;
  height: 100%;
  margin: 0;
  cursor: pointer;

  &:focus {
    outline: none;
  }

  &:checked {
    & ~ ${RadioFill} {
      width: 8px;
      height: 8px;

      &::before {
        opacity: 1;
      }
    }
  }
`;

const RadioLabel = styled.span`
    cursor:pointer;
    height: 19px;
    line-height: 19px;
    margin-right:20px;
    color: ${({ disabled }) => (disabled ? '#cbcbcb' : '#000000')};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
`;

