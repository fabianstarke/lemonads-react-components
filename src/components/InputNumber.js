import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import { ChevronDown, ChevronUp  } from "../images/icons/baseIcons";

const { PRIMARY_BASE, GREY_5 } = variables;

export const Wrapper = styled.div`
    width: ${(props) => props.full ? "100%" : "202px"};
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const Label = styled.label`
    height: 19px;
    width: max-content;
    color: ${GREY_5};
    font-family: "Open Sans";
    font-size: 14px;
    letter-spacing: 0;
    line-height: 19px;
    margin-bottom: 10px;
    ${({ label }) => label}
    &:after {
        content: ${({ required }) => (required === true ? `"*"` : "")};
        color: red;
        margin-left: 0px;
    }
`;

export const Input = styled.input`
    color: #000000;
    width: 100%;
    font-family: "Open Sans";
    font-size: 14px;
    height: 28px;
    line-height: 19px;
    letter-spacing: 0;
    border: 1px solid #CBCBCB;
    padding: 0px 26px 0px 6px;
    background-color: #FFFFFF;
    text-align: right;
    &::placeholder {
        color: #CBCBCB;
        text-align: right;
    } 
    &:focus {
        border: 1px solid ${PRIMARY_BASE};
        outline: none;
    }
    border-radius: 5px;
    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
    }
    -moz-appearance: textfield;
    appearance: textfield;
`;

const InputWrapper = styled.div`
    height: 28px;
    display: flex;
    align-items: center;
    position: relative;
    .spinners {
        position: absolute;
        right: 8px;
        width: 10px;
        div {
            height: 10px;
            position: relative;
            user-select: none;
            & svg {
                position: absolute;
                height: 10px;
                width:10px;
                g {
                    stroke: #000000;
                    stroke-width: 2px;
                }
            }
            &:hover {
                cursor: pointer;
                font-weight: bold;
            }
            &:active {
                & svg {
                    position: absolute;
                    height: 10px;
                    width: 10px;
                    g {
                        stroke-width: 2px;
                    }
                }
            }
        }
    }

`

const InputNumber = ({
    label,
    required,
    value,
    className,
    full,
    ...props
}) => {
    const [inputValue, setInputValue] = useState(0);

    useEffect(() => {
        if(Number.isInteger(parseInt(value))) {
            setInputValue(value);
        }
    }, [value]);

    let step = 1;
    let stepType = 'int';
    if(Number.isInteger(parseFloat(props.step)) === true) {
        step = parseInt(props.step);
    } else if(Number.isNaN(props.step) === false) {
        step = parseFloat(props.step);
        stepType = 'float';
    }

    const increment = (e) => {
        // idéalement il serait plus simple d'appeler la méthode interne du input
        //this.parentNode.querySelector('input[type=number]').stepUp()

        let newValue;
        if(stepType === 'int') {
            newValue = parseInt(inputValue);

            if(props.max === undefined || newValue + step <= props.max) {
                newValue += step;
            } else {
                newValue = props.max;
            }
        } else {
            newValue = parseFloat(inputValue);

            if(props.max === undefined || newValue + step <= props.max) {
                newValue = parseFloat(newValue + step).toFixed(2);
            } else {
                newValue = props.max;
            }
        }

        setInputValue(newValue);

        // on définie la target value dans l'évènement
        e.target.value = newValue;
        // appel de la méthode onChange définie sur le <InputNumber />
        props.onChange(e);
    };

    const decrement = (e) => {
        // idéalement il serait plus simple d'appeler la méthode interne du input
        //this.parentNode.querySelector('input[type=number]').stepDown()

        let newValue;
        if(stepType === 'int') {
            newValue = parseInt(inputValue);

            if(props.min === undefined || newValue - step >= props.min) {
                newValue -= step;
            } else {
                newValue = props.min;
            }
        } else {
            newValue = parseFloat(inputValue);

            if(props.min === undefined || newValue - step >= props.min) {
                newValue = parseFloat(newValue - step).toFixed(2);
            } else {
                newValue = props.min;
            }
        }
        setInputValue(newValue);

        // on définie la target value dans l'évènement
        e.target.value = newValue;
        // appel de la méthode onChange définie sur le <InputNumber />
        props.onChange(e);
    };

    const changeValue = (e) => {
        let newValue = parseFloat(e.target.value);
        
        if(props.max !== undefined && newValue > props.max) {
            setInputValue(props.max);
        } else if(props.min !== undefined && newValue < props.min) {
            setInputValue(props.min);
        } else {
            setInputValue(newValue);
        }

        // appel de la méthode onChange définie sur le <InputNumber />
        props.onChange(e);
    }

    return (
        <Wrapper className={className} full={full}>
            { label && <Label required={required}>{label}</Label>}
            <InputWrapper>
                <Input
                    type="number"
                    {...props}
                    value={inputValue}
                    onChange={(e) => changeValue(e)}
                />
                <div className="spinners">
                    <div role="button" onClick={(e) => increment(e)}>{ChevronUp}</div>
                    <div role="button" onClick={(e) => decrement(e)}>{ChevronDown}</div>
                </div>
            </InputWrapper>
        </Wrapper>
    );
};

InputNumber.propTypes = {
    label: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    full: PropTypes.bool,
};

InputNumber.defaultProps = {
    required: false,
    disabled: false,
    full: false
};


export default InputNumber;
