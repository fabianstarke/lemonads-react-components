import React, { useState, useEffect, forwardRef, useImperativeHandle } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import { Close } from "../images/icons/baseIcons";
import Divider from "./Divider";

const { WHITE, GREY_2 } = variables;

const StyledModal = styled.div`
    .Popup {
        font-size: initial;
        position: fixed;
        background: rgba(0, 0, 0, 0.8);
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        z-index: 100;
        display: flex;
        animation: 0.25s popupinfade;
        justify-content: center;
        align-items: center;
        .popup-scroll {
            overflow-y: auto;
            max-height: 100vh;
            padding: 40px;
        }
        ::-moz-scrollbars-none {
            overflow: hidden;
        }
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent; /* make scrollbar transparent */
        }
        .popup-container {
            animation: 0.4s popupin;
            min-width: 430px;
            .wrapper {
                padding: 20px 20px 30px 30px;
                background: ${WHITE};
                width: ${(props) =>
                    props.size == "medium" ? "726px" : "494px"};
                box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.35);
                border-radius: 10px;
                .head {
                    font-size: 16px;
                    font-weight: 600;
                    line-height: 20px;
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    .popup-close {
                        display: flex;
                        margin-left:auto;
                        padding: 5px;
                        cursor: pointer;
                        svg {
                            pointer-events: none;
                        }
                        &:hover{
                            background-color: ${GREY_2};
                            border-radius: 5px;
                        }
                    }
                }
            }
        }

        @keyframes popupin {
            from {
                transform: scale(0.5);
                opacity: 0.1;
            }
            30% {
                transform: scale(1.01);
            }
            60% {
                transform: scale(0.98);
            }
            to {
                opacity: 1;
                transform: scale(1);
            }
        }
        @keyframes popupinfade {
            from {
                opacity: 0.1;
            }
            to {
                opacity: 1;
            }
        }
    }
`;

const Modal = forwardRef(({
    size,
    activableComponent,
    className,
    customCloseClassName,
    children,
    propagateToggle,
    header,
    defaultToggle,
    hiddenCross,
    role,
}, ref) => {
    const [toggle, setToggle] = useState(defaultToggle);
    const [clickTarget, setClickTarget] = useState("");

    useEffect(() => {
        setToggle(defaultToggle);
    }, [defaultToggle]);

    const open = () => {
        propagateToggle(true);
        setToggle(true);
    };

    const close = () => {
        if(clickTarget === "Popup") { 
            propagateToggle(false);
            setToggle(false);
        }
    };

    useImperativeHandle(ref, () => ({
        closePopup() {
            propagateToggle(false);
            setToggle(false);
        },
        openPopup() {
            propagateToggle(true);
            setToggle(true);
        }
    }));

    const togglePopup = (e) => {
        propagateToggle(toggle);
        setToggle(!toggle);
    };

    const clickPropagation = (e) => {
        e.target !== null &&
        e.target.className !== undefined &&
        (e.target.className.includes(customCloseClassName) || e.target.className.includes("popup-close"))
            ? togglePopup()
            : e.stopPropagation();
    };
    return (
        <StyledModal
            size={size}
            className={className}
            header={header}
            hiddenCross={hiddenCross}
            role={role}
        >
            {activableComponent && <div onClick={open}>{activableComponent}</div>}
            {toggle && (
                <div className="Popup" onClick={close}  onMouseDown={(e) => setClickTarget(e.target.className)}>
                    <div className="popup-scroll" >
                        <div
                            className="popup-container"
                            onClick={(e) => clickPropagation(e)}
                        >
                            <div className="wrapper">
                                <div className="head">
                                    {header}
                                    {!hiddenCross && (
                                        <div className="popup-close">{Close}</div>
                                    )}
                                </div>
                                {header && <Divider marginTop={hiddenCross ? "18px" : "20px"} marginBottom={"20px"}/>}
                                <div className="modal-body" >{children}</div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </StyledModal>
    );
});

Modal.propTypes = {
    size: PropTypes.string,
    activableComponent: PropTypes.element,
    className: PropTypes.string,
    customCloseClassName: PropTypes.string,
    children: PropTypes.node,
    propagateToggle: PropTypes.func,
    header: PropTypes.string,
    defaultToggle: PropTypes.bool,
    hiddenCross: PropTypes.bool,
    role: PropTypes.string,
};

Modal.defaultProps = {
    header: null,
    size: "small",
    defaultToggle: false,
    propagateToggle: () => {},
    hiddenCross: false,
};

export default Modal;
