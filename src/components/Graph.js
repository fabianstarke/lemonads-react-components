import React from "react";
import PropTypes from "prop-types";
import { Line } from 'react-chartjs-2';

const emptyDataY = [
    { label: 'Data', labelColor: '', points: [200, 300, 400, 200, 100, 200] },
];
const emptyDataX = [1, 2, 3, 4, 5, 6];

const datasetTemplate = {
    fill: false,
    borderWidth: 1,
    pointHoverRadius: 4.5,
    pointHoverBackgroundColor: '#000',
    pointHoverBorderWidth: 1,
    pointRadius: 3.6,
    pointHitRadius: 10,
}

const chartjsOptions = {
    responsive: true,
    maintainAspectRatio: true,
    layout: {
        padding: {
            top: 5,
        }
    },
    elements: {
        line: {
            tension: 0
        }
    },
    legend: {
        display: false
    },
    title: {
        display: false,
        text: 'Chart.js Line Chart'
    },
    tooltips: {
        // Disable the on-canvas tooltip
        enabled: false,
        mode: 'index',
        intersect: false,

        custom: function (tooltip) {
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');

            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = "<table></table>"
                document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltip.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltip.yAlign) {
                tooltipEl.classList.add('top');
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltip.body) {
                var bodyLines = tooltip.body.map(getBody);

                var innerHtml = '<thead>';

                innerHtml += '</thead><tbody>';

                bodyLines.forEach(function (body, i) {
                    var colors = tooltip.labelColors[i];
                    var style = 'background:' + colors.backgroundColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 2px';
                    var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                    innerHtml += '<tr><td>' + span + body + '</td></tr>';
                });
                innerHtml += '</tbody>';

                var tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;
            }

            var position = this._chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.left = position.left + window.pageXOffset + tooltip.caretX + 'px';
            tooltipEl.style.top = position.top + window.pageYOffset + tooltip.caretY + 'px';
            tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
            tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
            tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
            tooltipEl.style.pointerEvents = 'none';
            tooltipEl.style.border = "1px solid #EBEBEB";
            tooltipEl.style.borderRadius = '5px';
        }
    },
    hover: {
        intersect: false
    },
    scales: {
        xAxes: [{
            display: true,
            gridLines: {
                display: true,
                drawBorder: true,
                drawOnChartArea: false,
                drawTicks: false
            },
            ticks: {
                padding: 10
            }
        }],
        yAxes: [{
            id: 0,
            display: false,
            ticks: {
                beginAtZero: false,
                autoSkip: false
            }

        },
        {
            id: 1,
            display: false,
            ticks: {
                beginAtZero: false,
                autoSkip: false
            }
        }]
    },
    animation: {
        duration: 2000
    }
};

export class Graph extends React.Component {
    constructor(props) {
        super(props);
        this.linesColors = [
            {
                border: '#0336FF',
                gradient: { start: 'rgba(255,255,255,0.5)', stop: 'rgba(255,255,255,0)' }
            },
            {
                border: '#25e47a',
                gradient: { start: 'rgba(200, 200, 200, 0.5)', stop: 'rgba(200, 200, 200, 0)' }
            },
            {
                border: '#7EE3AE',
                gradient: { start: 'rgba(73, 215, 141, 0.5)', stop: 'rgba(110,92,231,0)' }
            }
        ];
        this.state = {
            data: {},
            datasets: [],
        }
        this.chartReference = React.createRef();
        this.updateDatasets = this.updateDatasets.bind(this)
        this.isEmpty = this.isEmpty.bind(this)
    }

    componentDidMount() {
        this.updateDatasets()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.dataX !== this.props.dataX || prevProps.dataY !== this.props.dataY || prevProps.uniqueid !== this.props.uniqueid) {
            this.updateDatasets();
        }
    }

    componentDidCatch(error, info) {
        console.log(error, info);
    }

    updateDatasets() {
        // Project dataY into Graph.js datasets
        var canvas = this.chartReference.current.getElementsByTagName('canvas')[0];
        let dataY = this.isEmpty() ? emptyDataY : this.props.dataY;
        this.setState({
            datasets: dataY.map((curve, i) => {
                let datasetColor = i < this.linesColors.length ? this.linesColors[i].border : '#000';
                let gradientStart = i < this.linesColors.length ? this.linesColors[i].gradient.start : '#fff';
                let gradientEnd = i < this.linesColors.length ? this.linesColors[i].gradient.stop : '#fff';
                var ctx = canvas.getContext("2d")
                let height = this.props.height ? this.props.height : 0;
                if(typeof height === "string"){
                    height = parseInt(height.slice(0,-2));
                }
                var gradient = ctx.createLinearGradient(0, height, 0, 0)
                gradient.addColorStop(0, gradientStart)
                gradient.addColorStop(1, gradientEnd)
                return {
                    ...datasetTemplate,
                    label: curve.label,
                    data: curve.points,
                    fill: false,
                    borderColor: datasetColor,
                    pointBackgroundColor: '#FFFFFF',
                    pointHoverBackgroundColor: datasetColor,
                    backgroundColor: gradient,
                    yAxisID: i
                };
            })
        })
    }

    isEmpty() {
        return this.props.dataX == null || this.props.dataX == undefined || this.props.dataY == null || this.props.dataY == undefined || this.props.dataX.length == 0 || this.props.dataY.length == 0;
    }

    render() {
        let maintainAspectRatio = this.props.height ? false : true;
        let options = { ...chartjsOptions, maintainAspectRatio, ...this.props.options }
        let dataX = this.isEmpty() ? emptyDataX : this.props.dataX;
        let style = { position: 'relative', width: this.props.width, height: this.props.height };

        return (
            <div style={{ position: "relative" }} role="graph">
                <div ref={this.chartReference} style={style}>
                    <Line
                        options={options}
                        data={{ labels: dataX, datasets: this.state.datasets ? this.state.datasets : [] }}
                    />
                </div>
            </div>
        );
    }
}

Graph.propTypes = {
    height: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    option: PropTypes.object,
    dataY: PropTypes.array,
    dataX: PropTypes.array,
    uniqueid: PropTypes.string
}

Graph.defaultProps = {
    height: null,
    width: null,
    options: {},
    dataY: [],
    dataX: [],
    uniqueid: null
}

export default Graph;

/* To use a gradient white cover when empty data:
// Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,ffffff+44,ffffff+100&0.74+0,1+44,0.79+100
background: -moz-linear-gradient(top, rgba(255,255,255,0.74) 0%, rgba(255,255,255,1) 44%, rgba(255,255,255,0.79) 100%); //FF3.6-15
background: -webkit-linear-gradient(top, rgba(255,255,255,0.74) 0%,rgba(255,255,255,1) 44%,rgba(255,255,255,0.79) 100%); // Chrome10-25,Safari5.1-6
background: linear-gradient(to bottom, rgba(255,255,255,0.74) 0%,rgba(255,255,255,1) 44%,rgba(255,255,255,0.79) 100%); // W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bdffffff', endColorstr='#c9ffffff',GradientType=0 ); // IE6-9
*/
