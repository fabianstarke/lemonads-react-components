import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Label } from "./Input";
import { Link } from 'react-router-dom';

import styled from "styled-components";
import Select, { components } from "react-select";
import * as variables from "../common/variables";
import Divider from './Divider';
import selectArrow from "../images/icons/baseIcons/selectArrow.svg";
import checkValid from "../images/icons/baseIcons/checkValid.svg";
import checkValidWhite from "../images/icons/baseIcons/checkValid_white.svg";
import close from "../images/icons/baseIcons/close.svg";

const { WHITE, BLACK, GREY_1, GREY_2, GREY_5, ERROR } = variables;

export const Wrapper = styled.div`
    width: ${(props) =>
        props.full ? "100%" : props.width ? props.width : "202px"};
    display: flex;
    flex-direction: column;
    position: relative;
`;

export const LinkWrapper = styled.div`
    width: max-content;
    padding: 0 10px 6px 10px;
    a {
        color: #0336ff;
        font-family: "Open Sans";
        font-size: 14px;
        letter-spacing: 0;
        line-height: 19px;
        text-decoration: none;
    }
`;
const customStyle = {
    option: (provided, state) => ({
        lineHeight: "19px",
        color: state.isFocused ? WHITE : BLACK,
        backgroundColor: state.isFocused ? BLACK : null,
        padding: "2px 24px",
        minWidth:
            state.selectProps.responsiveMenu === true ? "max-content" : "",
        width: "auto",
        borderRadius: "5px",
        backgroundImage: !state.isSelected
            ? ""
            : state.isFocused
            ? `url(${checkValidWhite})`
            : `url(${checkValid})`,
        display: "block",
        backgroundSize: "14px",
        backgroundPosition: "5px center",
        backgroundRepeat: "no-repeat",
        cursor: "default",
        margin: "0px 5px"
    }),
    container: (provided, state, icon) => ({
        ...provided,
        color: "black",
        fontSize: "14px",
        outline: "none",
        lineHeight: "19px",
        fontWeight: "400",
        borderRadius: "5px"
    }),
    control: (provided, state) => ({
        ...provided,
        border: state.isDisabled
            ? `1px solid ${GREY_2} !important` : state.selectProps.mode === "error" ? `1px solid ${ERROR} !important`
            : "1px solid #CBCBCB !important",
        boxShadow: "none",
        minHeight: "28px",
        height: "28px",
        backgroundColor: state.isDisabled ? GREY_1 : WHITE,
    }),
    indicatorSeparator: () => ({
        display: "none",
    }),
    input: (provided, state) => ({
        ...provided,
        margin: 0,
        padding: 0,
        height: "19px",
    }),
    placeholder: (provided, state) => ({
        ...provided,
        color: state.selectProps.isSearchable ? "#CBCBCB" : BLACK,
        lineHeight: "19px",
        marginLeft: state.selectProps.icon ? `13px` : "2px",
    }),
    dropdownIndicator: (provided, state) => ({
        ...provided,
        transform: state.selectProps.menuIsOpen && "rotate(180deg)",
    }),
    clearIndicator: (provided, state) => ({
        ...provided,
        padding: 0,
        margin: "0 5px",
    }),
    menu: (provided, state) => ({
        ...provided,
        zIndex: 5,
        boxShadow: "none",
        width: state.selectProps.responsiveMenu === true ? "fit-content" : "",
        minWidth: "100%",
        borderRadius: "5px"
    }),
    menuList: (provided, state) => ({
        ...provided,
        marginRight: "5px",
        border: "1px solid #CBCBCB",
        backgroundColor: WHITE,
        position: "absolute",
        width: "auto",
        borderRadius: "5px",
        //maxHeight: "100px",
        "&::-webkit-scrollbar": {
            width: "4px",
            height: "4px",
        },

        "&::-webkit-scrollbar-track": {
            backgroundColor: GREY_1,
            borderRadius: "2px",
            margin: "10px 8px 10px 0",
        },

        "&::-webkit-scrollbar-thumb": {
            opacity: "0.7",
            borderRadius: "2px",
            backgroundColor: "#cbcbcb",
        },
    }),
    valueContainer: (provided, state) => ({
        ...provided,
        paddingLeft: state.selectProps.icon ? `28px` : "10px",
        backgroundImage: state.selectProps.icon
            ? `url(${state.selectProps.icon})`
            : "",
        backgroundPosition: "10px center",
        backgroundRepeat: "no-repeat",
    }),
    singleValue: (provided, state) => ({
        ...provided,
        marginLeft: state.selectProps.icon ? `13px` : "2px",
        color: BLACK,
    }),
    groupHeading: (provided, state) => ({
        ...provided,
        fontSize: "14px",
        color: GREY_5,
    }),
};

const ReactSelect = ({
    label,
    required,
    wrapperClassName,
    full,
    width,
    onChange,
    value,
    icon,
    mode,
    responsiveMenu,
    wrapperRole,
    customMenuListLabel,
    customMenuListLink,
    link,
    ...otherProps
}) => {
    const [selectedOptions, setSelectedOptions] = useState([]);
    const [inputValue, setInputValue] = useState("");

    const DropdownIndicator = (props) => {
        return (
            <components.DropdownIndicator {...props}>
                <img src={selectArrow} />
            </components.DropdownIndicator>
        );
    };

    const ClearIndicator = (props) => {
        return (
            <components.ClearIndicator {...props}>
                <img src={close} height={14} width={14} />
            </components.ClearIndicator>
        );
    };

    let LinkElement = link;

     const MenuList = (props) => {
         return (
             <components.MenuList {...props}>
                 {props.children}
                 {props.selectProps.options.length > 0 &&
                     props.selectProps.customMenuListLabel && (
                         <LinkWrapper>
                             <Divider marginTop="10px" marginBottom="10px" />
                             <LinkElement to={props.selectProps.customMenuListLink}>
                                 {props.selectProps.customMenuListLabel}
                             </LinkElement>
                         </LinkWrapper>
                     )}
             </components.MenuList>
         );
     };
 
    useEffect(() => {
        if (otherProps.defaultValue) {
            setSelectedOptions(otherProps.defaultValue);
        }
    }, []);

    useEffect(() => {
        if (value !== undefined) {
            let option = Array.isArray(value)
                ? otherProps.options.filter((option) =>
                      value.includes(option.value)
                  )
                : typeof value === "object"
                ? value
                : otherProps.options.filter((option) => option.value === value);
            if (option !== undefined) {
                setSelectedOptions(option);
            } else {
                setSelectedOptions([]);
            }
        }
    }, [JSON.stringify(value), otherProps.options]);

    const handleChange = (option, { action }) => {
        if (value === undefined) {
            setSelectedOptions(option);
        }
        onChange && onChange(option, { action });
    };

    return (
        <Wrapper
            className={wrapperClassName}
            full={full}
            label={label}
            width={width}
            role={wrapperRole}
        >
            {label && <Label required={required}>{label}</Label>}
            <Select
                styles={customStyle}
                components={{
                    DropdownIndicator,
                    MultiValue: components.SingleValue,
                    ClearIndicator,
                    MenuList
                }}
                hideSelectedOptions={false}
                onChange={(option, { action }) => {
                    handleChange(option, { action });
                }}
                onInputChange={(value) => setInputValue(value)}
                controlShouldRenderValue={
                    (inputValue && inputValue.length > 0) ||
                    (selectedOptions && selectedOptions.length > 1)
                        ? false
                        : true
                }
                value={selectedOptions}
                mode={mode ? mode :""}
                {...otherProps}
                icon={icon ? icon : ""}
                menuPlacement="auto"
                responsiveMenu={responsiveMenu}
                customMenuListLabel={customMenuListLabel}
                customMenuListLink={customMenuListLink}
            />
        </Wrapper>
    );
};

ReactSelect.propTypes = {
    label: PropTypes.string,
    required: PropTypes.bool,
    wrapperClassName: PropTypes.string,
    full: PropTypes.bool,
    width: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.object,
        PropTypes.array,
    ]),
    icon: PropTypes.string,
    responsiveMenu: PropTypes.bool,
    wrapperRole: PropTypes.string,
    mode: PropTypes.string,
    customMenuListLabel: PropTypes.string,
    customMenuListLink: PropTypes.string,
    link: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.func
    ]),
};

ReactSelect.defaultProps = {
    full: false,
    width: null,
    responsiveMenu: true,
    link: Link
};

export default ReactSelect;
