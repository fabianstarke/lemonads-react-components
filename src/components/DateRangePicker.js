import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { parseDate } from "react-day-picker/moment";
import {
    BLACK,
    GREY_1,
    GREY_2,
    GREY_5,
} from "../common/variables";
import { DatePickerStyle } from "./DatePicker";
import moment from "moment-timezone";
import Button from "./Button";
import {
    getDateFrFromUs,
    getDateUs,
    monthStart,
    monthEnd,
    getDateDayOff,
    getDateMonthOff,
    getCurrentWeekDate,
    getPreviousWeekDate,
    getPreviousMonth,
    getNextMonth,
} from "../common/helpers";
import { TODAY, YESTERDAY, DATEFORMAT } from "../common/variables";
import baseIcons from "../images/icons/baseIcons";

export class DateRangePicker extends React.Component {
    constructor(props) {
        super(props);
        this.CustomOverlay = this.CustomOverlay.bind(this);
        this.showFromMonth = this.showFromMonth.bind(this);
        this.from = React.createRef();
        this.to = React.createRef();

        this.state = {
            startDate: this.props.from,
            endDate: this.props.to,
            previousStartDate: this.props.from,
            previousEndDate: this.props.to,
            currentMonth: new Date(getPreviousMonth(this.props.from)),
            focusFrom: false,
            focusTo: false,
        };
        this.periodLabels = [
            "Today",
            "Yesterday",
            "Last 7 days",
            "This week",
            "Last week",
            "Last 30 days",
            "This month",
            "Last month",
        ];
    }

    CustomOverlay({ classNames, selectedDay, children, ...props }) {
        return (
            <div className={classNames.overlayWrapper} {...props}>
                <div className={classNames.overlay}>
                    <div className="row">
                        {this.getCustoms()}
                        <div>
                            {children}
                            <div className="button-group">
                                <Button
                                    label="Cancel"
                                    type="secondary"
                                    className="button-cancel"
                                    onClick={() => {
                                        this.to.current.hideDayPicker();
                                        this.from.current.hideDayPicker();
                                        this.onCancel();
                                    }}
                                />

                                <Button
                                    label="Apply"
                                    onClick={() => {
                                        this.props.onChange(
                                            this.state.startDate,
                                            this.state.endDate
                                        );
                                        this.to.current.hideDayPicker();
                                        this.from.current.hideDayPicker();
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    showFromMonth(dayPickerInput) {
        let dayPicker= this[dayPickerInput].current;
        if (dayPicker !== undefined && dayPicker !== null && dayPicker.getDayPicker() != null) {
            if (
                dayPickerInput === "from" || moment(monthStart(this.state.endDate)).diff(
                    moment(monthEnd(this.state.startDate)),
                    "months"
                ) < 0
            ) {
                dayPicker.getDayPicker().showMonth(this.getFromMonth(this.state.startDate));
            } else {
                dayPicker.getDayPicker().showMonth(this.getFromMonth(this.state.endDate));
            }
        }
    }

    handleSelectPeriod(item, index) {
        const getPeriodFromLabel = (periodIndex, option = { date: {} }) => {
            let periods = [
                { from: TODAY, to: TODAY }, //Today 0
                { from: YESTERDAY, to: YESTERDAY }, //Yesterday 1
                { from: getDateDayOff(YESTERDAY, 6), to: YESTERDAY }, //Last 7 days 2
                {
                    from: getCurrentWeekDate(TODAY).start,
                    to: getCurrentWeekDate(TODAY).end,
                }, //This week 3
                {
                    from: getPreviousWeekDate(TODAY).start,
                    to: getPreviousWeekDate(TODAY).end,
                }, //Last week 4
                { from: getDateDayOff(TODAY, 29), to: TODAY }, //Last 30 days 5
                { from: monthStart(TODAY), to: TODAY }, //This month 6
                {
                    from: getDateMonthOff(TODAY, 1).start,
                    to: getDateMonthOff(TODAY, 1).end,
                }, //Last month 7
                {
                    from: getDateFrFromUs(option.date.from),
                    to: getDateFrFromUs(option.date.to),
                }, //Custom 8
            ];
            return periods[periodIndex] || periods[0];
        };
        const dates = getPeriodFromLabel(index);
        const dateFrom = new Date(moment(dates.from, DATEFORMAT));
        const dateTo = new Date(moment(dates.to, DATEFORMAT));

        this.setState({ startDate: dateFrom, endDate: dateTo }, () =>
            this.props.onChange(dateFrom, dateTo)
        );

        if (
            this.from !== undefined && this.from !== null &&
            this.from.current.getDayPicker() != null
        ) {
            this.from.current
            .getDayPicker()
            .showMonth(this.getFromMonth(dateFrom));
        }

        if (
            this.to !== undefined && this.to !== null &&
            this.to.current.getDayPicker() != null
        ) {
            this.to.current
            .getDayPicker()
            .showMonth(this.getFromMonth(dateFrom));
        }
        this.props.handleCustomPeriodClick(item, index);
    }

    onCancel() {
        this.props.onCancel(
            this.state.previousStartDate,
            this.state.previousEndDate
        );
        this.setState({
            startDate: this.state.previousStartDate,
            endDate: this.state.previousEndDate,
        });
    }

    onStartDayChange(startDate) {
        this.setState({
            startDate: startDate,
            previousStartDate: this.state.startDate,
        });
    }

    onEndDayChange(endDate) {
        this.setState({
            endDate: endDate,
            previousEndDate: this.state.endDate,
        });
    }

    navigateStartDate(startDate) {
        this.props.navigateStartDate(startDate);
        this.setState({
            startDate: startDate,
            previousStartDate: this.state.startDate,
        });
    }

    navigateEndDate(endDate) {
        this.props.navigateEndDate(endDate);
        this.setState({
            endDate: endDate,
            previousEndDate: this.state.endDate,
        });
    }

    getCustoms() {
        let r = [];

        this.periodLabels.forEach((item, index) => {
            r.push(
                <li
                    key={index}
                    onClick={() => {
                        this.handleSelectPeriod(item, index);
                    }}
                >
                    {item}
                </li>
            );
        });

        return <ul>{r}</ul>;
    }

    getFromMonth(from) {
        const today = new Date();
        if(today < from) {
            return new Date(getPreviousMonth(getPreviousMonth(from)));
        }
        if(moment(monthStart(today)).diff(
            moment(monthEnd(from)),
            "months"
        ) < 1) {
            return new Date(getPreviousMonth(getPreviousMonth(today)));
        }
        return from;
    }

    render() {
        var from = this.state.startDate;
        var to = this.state.endDate;

        //Permet de parser les dates en fonction du focus
        // var fromValue = this.state.focusFrom === true ? getDateUs(from) : from.toDateString();
        // var toValue = this.state.focusTo === true ? getDateUs(to) : to.toDateString();
        var fromValue = getDateUs(from);
        var toValue = getDateUs(to);

        //N'affiche pas dans le mois courant les dates des mois précédents et suivants si affichés dans la modale
        const daysOfMonth = {
            from: this.state.currentMonth,
            to: new Date(
                monthEnd(getNextMonth(getNextMonth(this.state.currentMonth)))
            ),
        };

        let oneBlock = to - from === 86400000;
        var modifiers = { start: from, end: to, hidden: daysOfMonth, today: "", current: new Date()};
        if(oneBlock === true) {
            modifiers["oneBlock"] = {from: from, to: to}
        }

        return (
            <DatePickerRangeWrapper role="daterangepicker">
                <span className="calendarCnt">{baseIcons.Calendar}</span>
                <div className="InputFromTo">
                    <span className="from">
                        <DayPickerInput
                            ref={this.from}
                            value={fromValue}
                            placeholder="From"
                            overlayComponent={this.CustomOverlay}
                            onDayChange={(date) => {
                                this.showFromMonth("from");
                                if (date !== undefined) {
                                    this.onStartDayChange(date);
                                    this.setState(
                                        {
                                            currentMonth: new Date(
                                                getPreviousMonth(date)
                                            ),
                                        },
                                        () => this.showFromMonth('to')
                                    );
                                }
                            }}
                            format={["YYYY-MM-DD", "YYYY/MM/DD", "YYYY.MM.DD"]}
                            parseDate={parseDate}
                            inputProps={{
                                onClick: () =>
                                    this.setState({ focusFrom: true }),
                                onBlur: () =>
                                    this.setState({ focusFrom: false }),
                            }}
                            onDayPickerShow={() => {
                                this.showFromMonth('from')
                            }}
                            dayPickerProps={{
                                numberOfMonths: 3,
                                modifiers,
                                selectedDays: [from, { from, to: to }],
                                disabledDays: { after: to },
                                onDayClick: () => {
                                    this.to.current.getInput().focus();
                                },
                                showOutsideDays: true,
                                onMonthChange: (month) =>
                                    this.setState({ currentMonth: month }),
                                showWeekNumbers: true,
                                onWeekClick:(week, days) => this.setState({startDate: days[0], endDate: days[6]}),
                                firstDayOfWeek: 1,
                                fixedWeeks: true,
                                ...this.props.dayPickerProps,
                            }}
                        />{" "}
                    </span>
                    {baseIcons.ChevronRight}
                    <span className="To">
                        <DayPickerInput
                            format={["YYYY-MM-DD", "YYYY/MM/DD", "YYYY.MM.DD"]}
                            parseDate={parseDate}
                            value={toValue}
                            ref={this.to}
                            placeholder="To"
                            overlayComponent={this.CustomOverlay}
                            onDayChange={(date) => {
                                this.showFromMonth("to");
                                if (date !== undefined) {
                                    this.onEndDayChange &&
                                        this.onEndDayChange(date);
                                    this.setState(
                                        {
                                            currentMonth: new Date(
                                                getPreviousMonth(date)
                                            ),
                                        },
                                        () => this.showFromMonth('from')
                                    );
                                }
                            }}
                            hideOnDayClick={false}
                            inputProps={{
                                onClick: () => this.setState({ focusTo: true }),
                                onBlur: () => this.setState({ focusTo: false }),
                            }}
                            onDayPickerShow={() =>{
                                this.showFromMonth('to')}
                            }
                            dayPickerProps={{
                                numberOfMonths: 3,
                                showWeekNumbers: true,
                                modifiers,
                                selectedDays: [from, { from, to: to }],
                                disabledDays: { before: from },
                                onDayClick: () => {
                                    this.from.current.getInput().focus();
                                },
                                showOutsideDays: true,
                                onMonthChange: (month) =>
                                    this.setState({ currentMonth: month }),
                                onWeekClick:(week, days) => this.setState({startDate: days[0], endDate: days[6]}),
                                firstDayOfWeek: 1,
                                fixedWeeks: true,
                                ...this.props.dayPickerProps,
                            }}
                        />
                    </span>
                </div>
            </DatePickerRangeWrapper>
        );
    }
}

DateRangePicker.propTypes = {
    from: PropTypes.instanceOf(Date),
    to: PropTypes.instanceOf(Date),
    dayPickerProps: PropTypes.object,
    onCancel: PropTypes.func,
    navigateStartDate: PropTypes.func,
    navigateEndDate: PropTypes.func,
    onChange: PropTypes.func,
    handleCustomPeriodClick: PropTypes.func,
};

DateRangePicker.defaultProps = {
    dayPickerProps: {},
    onCancel: (start, end) => {},
    navigateEndDate: (start) => {},
    navigateStartDate: (end) => {},
    onChange: (start, end) => {},
    handleCustomPeriodClick: (item, index) => {},
};

const ARROW =
    "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSIyMHB4IiBoZWlnaHQ9IjIwcHgiIHZpZXdCb3g9IjAgMCAyMCAyMCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4gICAgPHRpdGxlPkljb24vYmFzZS9hcnJvdyBsZWZ0PC90aXRsZT4gICAgPGcgaWQ9Ikljb24vYmFzZS9hcnJvdy1sZWZ0IiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiPiAgICAgICAgPGcgaWQ9IkludGVyZmFjZS1Fc3NlbnRpYWwtLy1LZXlib2FyZC0vLWtleWJvYXJkLWFycm93LXJpZ2h0IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMC4wMDAwMDAsIDEwLjUwMDAwMCkgcm90YXRlKC0xODAuMDAwMDAwKSB0cmFuc2xhdGUoLTEwLjAwMDAwMCwgLTEwLjUwMDAwMCkgdHJhbnNsYXRlKDMuMDAwMDAwLCA3LjAwMDAwMCkiIHN0cm9rZT0iIzAwMDAwMCI+ICAgICAgICAgICAgPGcgaWQ9Ikdyb3VwIj4gICAgICAgICAgICAgICAgPGcgaWQ9ImtleWJvYXJkLWFycm93LXJpZ2h0Ij4gICAgICAgICAgICAgICAgICAgIDxwb2x5bGluZSBpZD0iU2hhcGUiIHBvaW50cz0iMTAuNzkxNjY2NyAwLjM3MzI1IDEzLjcwODMzMzMgMy4yODk5MTY2NyAxMC43OTE2NjY3IDYuMjA2NTgzMzMiPjwvcG9seWxpbmU+ICAgICAgICAgICAgICAgICAgICA8bGluZSB4MT0iMTMuNzA4MzMzMyIgeTE9IjMuMjg5OTE2NjciIHgyPSIwLjI5MTY2NjY2NyIgeTI9IjMuMjg5OTE2NjciIGlkPSJTaGFwZSI+PC9saW5lPiAgICAgICAgICAgICAgICA8L2c+ICAgICAgICAgICAgPC9nPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+);";

const DatePickerRangeWrapper = styled.span`
    .calendarCnt {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 20px;
        margin-right: 23px;
        svg {
            height: 20px;
            width: 20px;
        }
    }
    .chevronCnt {
        height: 15px;
    }
    svg {
        width: 15px;
        height: 15px;
    }

    /* Portability fixes */

    box-sizing: border-box;
    @import url("https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i");
    @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,500i,700,700i,900,900i");

    /* Container */

    align-items: center;
    position: relative;
    display: flex;
    justify-content: center;
    height: 28px;
    width: 259px;
    padding: 0 15px 0 10px;
    text-decoration: none;
    box-shadow: none;
    font-size: 0.75em;
    border-radius: 5px;
    background: #fff;
    font-weight: 500;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid #CBCBCB;
    outline: none !important;
    font-family: Open Sans;

    /* Library + custom style */

    ${DatePickerStyle}
    .DayPicker {
        margin-left: -20px;
    }
    .DayPicker-Months {
        display: flex;
        flex-wrap: nowrap;
        justify-content: center;
        display: -webkit-inline-box !important;
        height: auto;
        margin-top: 20px;
        margin-left: 20px;
    }

    .DayPicker-Caption {
        font-size: 13px;
        font-weight: 800;
        padding: 0;
        margin: 0;
        margin-left: 21.6px;
        text-align: center !important;
        display: table-caption;
        text-align: left;
        border: 1px solid ${GREY_2};
        border-bottom: none;
        margin-bottom: -5px;
        border-radius: 5px;
        div {
            font-weight: 500;
            font-size: 14px;
            line-height: 19px;
            align-items: center;
            display: flex;
            justify-content: center;
            margin-top: 4px;
        }

        border-bottom: 0;
    }
    .DayPicker-Weekday {
        padding: 5px;
        border-bottom: 1px solid ${GREY_2};
        font-size: 10px;
        abbr {
            font-size: 10px;
        }
        :first-child {
            padding: 5px 0;
            border-bottom: none;
            text-align: right;
            padding-right: 5px;
            :before {
                content: "w";
                color: ${GREY_5};
            }
        }
        :nth-child(2) {
            padding-left: 10px;
            border-left: 1px solid ${GREY_2};
        }
        :last-child {
            padding-right: 10px;
            border-right: 1px solid ${GREY_2};
        }
    }
    .DayPicker-WeekdaysRow {
        border-right: 1px solid ${GREY_2};
    }

    .DayPicker-WeekNumber {
        padding: unset;
        text-align: right;
        width: 21px;
        padding-right: 5px;
        color: ${GREY_5};
        border-right: none;
        box-sizing: border-box;
        :hover{
            color: ${BLACK};
        }
    }

    .DayPicker-WeekNumber,
    .DayPicker-Weekday:first-child:before {
        font-size: 14px;
        font-weight: 600;
        line-height: 19px;
    }
    /* DayPickerInput */

    .DayPickerInput {
        display: inline-block;
        text-align: center;
        width: 77px;

        input {
            height: 19px;
            width: 74px;
            color: #000000;
            font-family: "Open Sans";
            font-size: 14px;
            letter-spacing: 0;
            line-height: 19px;
            text-decoration: none;
            border: none;
            background: #fff;
            user-select: none;
            cursor: pointer;
            padding: 0 !important;
            margin: 0 !important;
            outline: none !important;
        }

        /*calendar month*/

        .DayPicker-Month {
            &:not(:last-child) {
                margin-right: 10px;
            }
            margin-left: 0;
            border-top: 0;
            margin-top: 0;
            border-collapse: separate;
        }

        /*calendar day*/

        .DayPicker-Day {
            padding: unset;
            font-size: 14px;
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
            color: black !important;
            background: none !important;
            :after {
                content: "";
                position: absolute;
                background-color: rgba(3, 54, 255, 0.1);
                z-index: -2;
                left: 0;
                width: 100%;
                height: calc(100% - 5px);
                top: 50%;
                transform: translate(0, -50%);
            }
            &:last-child {
                :after {
                    right: 5px;
                    border-top-right-radius: 50%;
                    border-bottom-right-radius: 50%;
                    width: calc(100% - 5px);
                    height: calc(100% - 5px);
                    top: 50%;
                    transform: translate(0, -50%);
                }
            }
            &:nth-child(2) {
                :after {
                    left: 5px;
                    border-top-left-radius: 50%;
                    border-bottom-left-radius: 50%;
                    width: calc(100% - 5px);
                }
            }
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
            font-weight: 600;
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside).DayPicker-Day--start,
        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside).DayPicker-Day--end {
            background: none;
            color: white !important;
            font-weight: 600;
            position: relative;
            :before {
                content: "";
                position: absolute;
                display: block;
                width: 23px;
                height: 23px;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                border-radius: 50%;
                background: #0036ff;
                z-index: -1;
            }
            :after {
                content: "";
                position: absolute;
                top: 0;
                background-color: rgba(3, 54, 255, 0.1) !important;
                z-index: -2;
                height: 100%;
                width: 50.05% !important;
                height: calc(100% - 5px);
                top: 50%;
                transform: translate(0, -50%);
            }
            :nth-child(2) {
                :before {
                    transform: translate(calc(-50% + 3px), -50%);
                }
            }
            :last-child {
                :before {
                    transform: translate(calc(-50% - 3px), -50%);
                }
            }
        }
        .DayPicker-Day--outside {
            color: #cbcbcb;
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside).DayPicker-Day--start {
            :after {
                left: 50%;
                border-top-left-radius: 50%;
                border-bottom-left-radius: 50%;
            }
            :last-child {
                :after {
                    display: none;
                }
            }
            &.DayPicker-Day--oneBlock:not(:last-child) {
                :after {
                    display: none;          
                }
                :before {
                    width: 26px;
                    left: calc(50% + 2px);
                    border-top-right-radius: 0px;
                    border-bottom-right-radius: 0px;
                }
            }
            &.DayPicker-Day--end {
                :after {
                    display: none;
                }
            }
        }

        .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside).DayPicker-Day--end {
            :after {
                right: 50%;
                border-top-right-radius: 50%;
                border-bottom-right-radius: 50%;
            }
            :nth-child(2) {
                :after {
                    display: none;
                }
            }
            &.DayPicker-Day--oneBlock:not(:nth-child(2)) {
                :after {
                    display: none;
                }
                :before {
                    width: 26px;
                    left: calc(50% - 2px);
                    border-top-left-radius: 0px;
                    border-bottom-left-radius: 0px;
                }
            }
        }

        .DayPicker-Day {
            padding: 5px;
        }
        .DayPicker-Day--disabled{
            color: #CBCBCB;
        }
        .DayPicker-WeekNumber {
            padding-top: 5px;

            padding-bottom: 5px;
        }
        .DayPicker-Week {
            :first-child {
                .DayPicker-WeekNumber {
                    padding-top: 10px;
                }
                .DayPicker-Day {
                    padding-top: 10px;
                }
                .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
                    :before {
                        top: calc(50% + 3px);
                    }
                    :after {
                        height: calc(100% - 10px);
                        top: calc(50% + 3px);
                    }
                }
                .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
                    :before {
                        top: calc(50% + 3px);
                    }
                }
            }
            :last-child {
                .DayPicker-Day {
                    border-bottom: 1px solid ${GREY_2};
                    padding-bottom: 10px;
                    :last-child{
                        border-bottom-right-radius: 3px;
                    }
                    :nth-child(2){
                        border-bottom-left-radius: 3px;
                    }
                }
                .DayPicker-WeekNumber {
                    padding-bottom: 10px;
                    border-bottom-right-radius: 3px;
                }
                .DayPicker-Day--selected:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside) {
                    :before {
                        top: calc(50% - 2px);
                    }
                    :after {
                        height: calc(100% - 10px);
                        top: calc(50% - 2px);
                    }
                }
                .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
                    :before {
                        top: calc(50% - 2px);
                    }
                }
            }
            .DayPicker-Day {
                :nth-child(2) {
                    border-left: 1px solid ${GREY_2};
                    padding-left: 10px;
                }
                :last-child {
                    border-right: 1px solid ${GREY_2};
                    padding-right: 10px;
                }
            }
        }

        .DayPicker-wrapper {
            width: 780px;
            padding-bottom: 0;
        }

        .button-group {
            /*         position: absolute;
        right: 25px;
        bottom: 15px; */
            font-size: 16px;
            display: flex;
            justify-content: flex-end;
            margin: 10px 20px 20px;
        }
    }

    .DayPicker-NavButton--prev {
        font-size: 22px;
        left: 54px;
        top: 25px;
        margin: 0;
        background-image: ${ARROW};
    }

    .DayPicker-NavButton--next {
        font-size: 22px;
        right: 24px;
        top: 25px;
        margin: 0;
        background-image: ${ARROW};
        transform: rotate(180deg);
    }

    .DayPickerInput-OverlayWrapper {
        position: unset;
        display: inherit;
        margin-bottom: --5px;
    }

    .DayPickerInput-Overlay {
        //position: absolute;
        z-index: 1;
        background: white;
        border: 1px solid #CBCBCB;
        box-shadow: none;
        height: auto;
        right: 0;
        left: unset;
        border-radius: 5px;
        ul {
            display: block !important;
            margin: 20px !important;
        }
        li {
            display: block !important;
            letter-spacing: 0;
            line-height: 19px;
            height: 19px;
            margin-bottom: 7px;
            margin-right: 0px;
            padding-left: 0;
            white-space: nowrap;
            width: 75px !important;
        }
    }
    .DayPicker:not(.DayPicker--interactionDisabled)
        .DayPicker-Day:not(.DayPicker-Day--disabled):not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover {
        position: relative;
        background: none;
        :before {
            content: "";
            position: absolute;
            width: 23px;
            height: 23px;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            border-radius: 50%;
            background: ${GREY_1};
            z-index: -1;
            display: block;
        }
        :nth-child(2) {
            :before {
                transform: translate(calc(-50% + 3px), -50%);
            }
        }
        :last-child {
            :before {
                transform: translate(calc(-50% - 3px), -50%);
            }
        }
    }

    /* Custom tyle */

    .InputFromTo {
        display: flex;
        align-items: center;
        justify-content: space-between;
        width: 100%;
        span {
            height: 19px;
        }
        .navigate {
            background: transparent;
            border: none;
            color: #000000;
            font-weight: 900;
            font-size: 13px;
            cursor: pointer;
            &:hover {
                color: #0336ff;
            }
            &.previous {
                padding: 1px 2px 1px 5px;
            }
            &.next {
                padding: 1px 5px 1px 2px;
            }
            &:focus {
                outline: none;
            }
        }

        .button-cancel {
            border: none;
        }

        p {
            margin: 0;
            margin-left: 8px;
            display: inline-block;
            font-size: 12px;
            padding: 0 1.5px;
            font-weight: 500;
            font-family: "Roboto", sans-serif;
        }

        .row {
            font-size: 13px;
            display: flex;
            flex-direction: row;
            ul {
                /*DayPickerInput-Overlay*/
                /*This is for the left predefined selections*/
                padding-left: 0;
                margin: 10px 0 10px 18px;
                li {
                    width: 100%;
                    display: block;
                    cursor: pointer;
                    text-align: left;
                    &:hover {
                        font-weight: bold;
                        color: #00f;
                    }
                }
            }
        }
    }
`;

export default DateRangePicker;
