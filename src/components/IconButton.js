import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Upload, Add, Supp, Download } from "../images/icons/baseIcons";
import * as variables from "../common/variables";

const { GREY_2, WHITE } = variables;

const StyledIconButton = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    height: 28px;
    border-radius: 5px;
    border: 1px solid ${GREY_2};
    width: 28px;
    background-color: ${WHITE};
`;

const IconButton = ({ onClick, className, type, role }) => {
    let icon;
    switch (type) {
        case "add":
            icon = Add;
            break;
        case "delete":
            icon = Supp;
            break;
        case "upload":
            icon = Upload;
            break;
        case "download":
            icon = Download;
            break;
    }

    return (
        <StyledIconButton onClick={onClick} className={className} role={role}>
            {icon}
        </StyledIconButton>
    );
};

IconButton.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    type: PropTypes.oneOf(["add", "delete", "upload", "download"]),
    role: PropTypes.string,
};

IconButton.defaultProps = {};

export default IconButton;
