import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";
import Skeleton from '../components/Skeleton'

const { GREY_6, WHITE } = variables;

const StyledCard = styled.div`
border: 1px solid ${GREY_6};
border-radius: 5px;
background-color: ${WHITE};
padding: 8px 20px;
margin: 14px 20px 30px 0px;
max-width: 202px;
display: inline-block;
margin-right: 20px;
`;

const Card = ({className, children, loading, skeletonWidth, animationDuration, skeletonHeight, ...props}) => {
    return (
        loading ?
        <Skeleton width={skeletonWidth} height={skeletonHeight} animationDuration={animationDuration} />
        :
        <StyledCard className={className} {...props}>
            {children}
        </StyledCard>
    );
};

Card.propTypes = {
    className: PropTypes.string,
    loading: PropTypes.bool,
    skeletonWidth: PropTypes.string,
    skeletonHeight: PropTypes.string
};

Card.defaultProps = {
    className: null,
    loading: false,
};

export default Card;
