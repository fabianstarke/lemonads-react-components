import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import * as variables from "../common/variables";

const { GREY_1 } = variables;

const Wrapper = styled.div`
    background-color: ${({ backgroundColor }) => (backgroundColor ? backgroundColor : GREY_1)};
    width: ${({ width }) => (width ? width : "100%")};
    display: flex;
    height: ${({ height }) => (height ? height : "100%")};

${({ animated }) =>
        animated &&
        css`
        @keyframes placeHolderShimmer {
            0%{
                background-position: -300px 0
            }
            100%{
                background-position: 300px 0
            }
        }
            animation-duration: ${({ animationDuration }) =>(animationDuration ? animationDuration : "2s" )};
            animation-fill-mode: forwards;
            animation-iteration-count: infinite;
            animation-name: placeHolderShimmer;
            animation-timing-function: linear;
            background: linear-gradient(to right, #eeeeee 8%, #f5f5f5 18%, #eeeeee 33%);
        `}
`;

const Skeleton = ({ height, width, className, children, backgroundColor, animated, animationDuration }) => {
    return (
        <Wrapper width={width} height={height} className={className} animated={animated} animationDuration={animationDuration} backgroundColor={backgroundColor}>
            {children}
        </Wrapper>
    );
};

Skeleton.propTypes = {
    height: PropTypes.string,
    width: PropTypes.string,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
    animated: PropTypes.bool,
    animationDuration: PropTypes.string
};

Skeleton.defaultProps = {
    height: "100%",
    width: "100%",
    animated: true
};

export default Skeleton;
