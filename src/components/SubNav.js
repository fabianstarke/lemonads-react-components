import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ResizeObserver as ResizeObserverPolyfill } from '@juggle/resize-observer';

import * as variables from "../common/variables";

const ResizeObserver = window.ResizeObserver || ResizeObserverPolyfill;

const { PRIMARY_BASE, WHITE, BLACK } = variables;

export const SubNav = styled.ul`
    display: flex;
    list-style: none;
    align-items: flex-start;
    position: relative;
    a {
        text-decoration: none;
    }
    .border {
        position: absolute;
        display: block;
        border-bottom: 2px solid ${PRIMARY_BASE};
        left: ${(props) => props.left + "px"};
        transition: all 0.15s ease-in-out;
        width: ${(props) => props.width + "px"};
        bottom: 0;
        left: ${(props) => props.left + "px"};
        visibility: ${(props) => props.useAnimation === true ? 'visible' : 'hidden'};
        z-index: 100;
    }
    li {
        padding: ${(props) => props.animated === false ? '4px 2px 5px 2px' : '4px 0px 5px 0px'};
        line-height: 19px;
        text-align: center;
        margin-right: 18px;
        font-size: 14px;
        color: ${BLACK};
        font-weight: 400;
        user-select: none;
        box-sizing: border-box;
        transition: ${(props) => props.animated === true ? 'all 0.15s ease' : 'all 0s'};

        &.active {
            background: ${WHITE};
            color: ${PRIMARY_BASE};
            padding-bottom: 3px;
            font-weight: ${(props) => props.animated === true ? '600' : '400'};;
            border-bottom: ${(props) => props.borderVisible === true || props.animated === false ? `2px solid ${PRIMARY_BASE}` : 'none'};

        }
        &:hover {
            color: ${PRIMARY_BASE};
            cursor: pointer;
        }
        &:after {
            display: ${(props) => props.animated === false ? 'none' : 'block'};
            content: attr(title);
            font-weight: bold;
            font-size: 14px;
            height: 0px;
            color: transparent;
            overflow: hidden;
            visibility: hidden;

        }
    }
`;

const StyledSubNav = ({
    labels,
    onChangeIndex,
    defaultActiveIndex,
    className,
    role
}) => {
    const [activeIndex, setActiveIndex] = useState(defaultActiveIndex);
    const [width, setWidth] = useState();
    const [leftPos, setLeftPos] = useState();
    const [borderVisible, setBorderVisible] = useState(true);
    const [useAnimation, setUseAnimation] = useState(false);

    const itemsRef = useRef([]);

    const sizeObserver = useRef(
        new ResizeObserver(() => {
            let activeElement = itemsRef.current[defaultActiveIndex];
            if(activeElement) {
                setWidth(activeElement.offsetWidth);
                setLeftPos(activeElement.offsetLeft);
            }
        })
    );

    useEffect(() => {
        let activeElement = itemsRef.current[defaultActiveIndex];
        if(activeElement) {
            sizeObserver.current.observe(activeElement);
        }
    }, []);

    useEffect(() => {
        setBorderVisible(!useAnimation);
    }, [useAnimation]);
    
    let handleNavClick = (indexElem) => {
        if(borderVisible === true || useAnimation === false) {
            sizeObserver.current.unobserve(itemsRef.current[defaultActiveIndex]);
            setUseAnimation(true);
        }

        let activeElement = itemsRef.current[indexElem];
        if(activeElement) {
            setWidth(activeElement.offsetWidth);
            setLeftPos(activeElement.offsetLeft);
        }
        setActiveIndex(indexElem);
        onChangeIndex && onChangeIndex(indexElem);
    };
    return (
        <SubNav animated={true} className={className} width={width} left={leftPos} borderVisible={borderVisible} useAnimation={useAnimation} role={role}>
            {labels.map((title, index) => (
                <li
                    key={index}
                    onClick={() => handleNavClick(index)}
                    className={`${activeIndex === index ? "active" : ""} `}
                    title={title}
                    ref={(el) => (itemsRef.current[index] = el)}
                >
                    {title}
                </li>
            ))}
            <div className="border"></div>
        </SubNav>
    );
};

StyledSubNav.propTypes = {
    labels: PropTypes.array,
    onChangeIndex: PropTypes.func,
    defaultActiveIndex: PropTypes.number,
    className: PropTypes.string,
    role: PropTypes.string
};

StyledSubNav.defaultProps = {
    labels: ["SubNav1", "SubNav2", "SubNav3"],
    onChangeIndex: () => console.log("clicked"),
    defaultActiveIndex: 0,
};
export default StyledSubNav;
