import React from "react";
import ReactTooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";

const Tooltip = ({ children, ...otherProps }) => {
    if(otherProps.overlay !== undefined && otherProps.overlay !== "") {
        return <ReactTooltip mouseLeaveDelay={0} {...otherProps}>{children}</ReactTooltip>;
    } else {
        return <>{children}</>;
    }
    
};

export default Tooltip;
