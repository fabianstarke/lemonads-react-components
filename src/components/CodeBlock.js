import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";

const { WHITE, BLACK } = variables;

const Wrapper = styled.div`
    width: 726px;
    border-radius: 10px;
    background-color: ${BLACK};
    position: relative;
`;

const Script = styled.div`
    color: ${WHITE};
    font-family: 'Courier';
    font-size: 14px;
    letter-spacing: 0.5px;
    line-height: 19px;
    padding: 20px 80px 25px 20px;
    white-space: pre-line;
`;

const StyledButton = styled.div`
    position: absolute;
    right: 20px;
    bottom: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    user-select: none;
    height: 24px;
    width: 49px;
    border-radius: 5px;
    font-size: 12px;
    line-height: 19px;
    border: 1px solid;
    color: ${BLACK};
    background-color: ${WHITE};
    font-family: "Open Sans";
`;


const CodeBlock = ({ script, onClick, ...props }) => {
    return (
        <Wrapper {...props}>
            <Script>{script}</Script>
            <StyledButton onClick={onClick}>Copy</StyledButton>
        </Wrapper>
    );
};

CodeBlock.propTypes = {
    script: PropTypes.any,
    onClick: PropTypes.func,
};

CodeBlock.defaultProps = {};

export default CodeBlock;
