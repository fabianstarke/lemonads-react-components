import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "./Button";
import illustrations from "../images/illustrations";

export class PageHelpBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <StyledPageHelpBoxWrapper role="pagehelpbox">
                <StyledContent>
                    <div className="title">{this.props.title}</div>
                    <div className="desc">{this.props.description}</div>
                    {this.props.buttonText && (
                        <div className="check-btn">
                            <Button
                                id={this.props.buttonId}
                                onClick={this.props.onClick}
                                label={this.props.buttonText}
                            />
                        </div>
                    )}
                </StyledContent>
                <div>
                    {this.props.icon
                        ? this.props.icon
                        : illustrations.RocketBoy}
                </div>
            </StyledPageHelpBoxWrapper>
        );
    }
}

PageHelpBox.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    buttonText: PropTypes.string,
    buttonId: PropTypes.string,
    onClick: PropTypes.func,
    icon: PropTypes.object,
};

PageHelpBox.defaultProps = {
    buttonText: null,
    onClick: () => {},
    icon: null,
};

const StyledPageHelpBoxWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

const StyledContent = styled.div`
    display: flex;
    flex-direction: column;
    margin-right: 100px;
    width: 398px;
    .title {
        color: #000000;
        font-family: "Open Sans";
        font-size: 24px;
        font-weight: 600;
        letter-spacing: 0;
        line-height: 28px;
    }
    .desc {
        color: #000000;
        font-family: "Open Sans";
        font-size: 14px;
        letter-spacing: 0;
        line-height: 19px;
        margin-top: 15px;
    }
    .check-btn {
        margin-top: 30px;
        font-family: "Open Sans";
        font-size: 14px;
        letter-spacing: 0;
        line-height: 19px;
    }
`;

export default PageHelpBox;
