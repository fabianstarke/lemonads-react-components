import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as baseIcons from "../images/icons/baseIcons";
import * as menuIcons from "../images/icons/menuIcons";
import * as variables from "../common/variables";

const { BLACK, GREY_2 } = variables;

const StyledSelectMode = styled.ul`
    display: table;
    padding: 0;
    width: fit-content;
    height: fit-content;
    border: 1px solid ${GREY_2};
    border-radius: 5px;
    overflow: hidden;
    height: 28px;
    box-sizing: border-box;
    cursor: pointer;
    li {
        box-sizing: border-box;
        vertical-align: middle;
        display: table-cell;
        padding: 0 4px;
        svg {
            height: 20px;
            width: 20px;
            box-sizing: border-box;
            vertical-align: middle;
        }
        &.menu-icon {
            svg {
                padding: 3px;
            }
        }

        &.active {
            background-color: ${BLACK};
            border: none;
            svg {
                filter: invert(1);
            }
        }
    }
`;

const SelectMode = ({
    iconList,
    className,
    onSelectItem,
    defaultActiveIndex,
    role
}) => {
    const [itemActive, setItemActive] = useState(defaultActiveIndex);

    let handleSelectClick = (item, index) => {
        setItemActive(index);
        onSelectItem && onSelectItem(item, index);
    };
    return (
        <StyledSelectMode className={className} role={role}>
            {iconList.map((item, index) => {
                return (
                    <li
                        key={index}
                        className={`${
                            item.iconType == "menu" ? "menu-icon" : "base-icon"
                        } ${itemActive == index ? " active" : ""}`}
                        onClick={() => !itemActive == index && handleSelectClick(item, index)}
                    >
                        {item.iconType == "menu"
                            ? menuIcons[item.iconName]
                            : baseIcons[item.iconName]}
                    </li>
                );
            })}{" "}
        </StyledSelectMode>
    );
};

SelectMode.propTypes = {
    iconList: PropTypes.array,
    className: PropTypes.string,
    onSelectItem: PropTypes.func,
    defaultActiveIndex: PropTypes.number,
    role: PropTypes.string
};

SelectMode.defaultProps = {
    iconList: [],
    defaultActiveIndex: 0,
};

export default SelectMode;
