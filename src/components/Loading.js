import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Loading as loadingIcon } from "../images/icons/baseIcons";

const StyledLoading = styled.div`
    flex: 1;
    position: relative;
    &.active {
        min-height: ${(props) => (props.announcement ? "89px" : "180px")};
    }
    .children {
        transition: all 0.6s ease;
        &.active {
            filter: blur(2px);
            opacity: 0.1;
        }
    }
    .overflow {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        display: flex;
        align-items: center;
        justify-content: center;
    }
`;

const Loading = ({ loading, children, announcement }) => {
    return (
        <StyledLoading
            role="loading"
            className={`Loading ${loading && "active"}`}
            announcement={announcement}
        >
            <div className={`children ${loading && "active"}`}>{children}</div>
            {loading && <div className="overflow">{loadingIcon}</div>}
        </StyledLoading>
    );
};

Loading.propTypes = {
    loading: PropTypes.bool,
    children: PropTypes.node,
    announcement: PropTypes.bool,
};

Loading.defaultProps = {
    loading: true,
    announcement: false,
};

export default Loading;
