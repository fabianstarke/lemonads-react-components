import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ChevronRight } from "../images/icons/baseIcons";
import useDimensions from "../common/hooks";

const Wrapper = styled.div`
    position: relative;
    height: 30px;
    margin-bottom: 20px;
    display: none;
    align-items: center;
    overflow-x: auto;
    transition: 0.1s;
    &::-webkit-scrollbar {
        display: none;
    }
    .arrows {
        position: absolute;
        display: flex;
        z-index: 99;
        align-items: center;
        height: 30px;
        width: 20px;
        background-color: #ffffff;

        box-shadow: -5px 0 3px -2px rgba(0, 0, 0, 0.2);

        &.right {
            right: 0;
        }
        &.left {
            left: 0;
            transform: scaleX(-1);
        }
    }
`;

const TagsWrapper = styled.div`
    margin-right: 5px;
    display: flex;
    flex: 0 0 auto;
    .items:not(:last-child) {
        margin-right: 5px;
    }
`;

const TagSlider = ({ children, role }) => {
    const [scroll, setScroll] = useState(0);
    const [ref, { width }] = useDimensions();
    const [ scrollWidth, setScrollWidth] = useState(width)

    const tagsCount = React.Children.toArray(children).length;

    const handlePreviousClick = () =>
        scroll != 0 ? setScroll(scroll + 100) : 0;

    const handleNextClick = () => {
        setScroll(scroll - 100) ;
    };

    const handleMouseScroll = () => {
        document.getElementById("wrapper").addEventListener('mousewheel', function(e){
            e.preventDefault()
            e.wheelDelta < 0 && scrollWidth > width ? setScroll(scroll-100) : e.wheelDelta > 0 && scroll != 0 ? setScroll(scroll+100 ) : null
        })
    }

    useEffect(() => setScrollWidth(ref.current.scrollWidth))

      return (
        <Wrapper id="wrapper" ref={ref} className={"tags-slider"} style={{ display: `${ tagsCount > 0 && "flex" }`}} onWheel={handleMouseScroll} role={role}>
            {scroll < 0 && (
                <div className="arrows left" onClick={handlePreviousClick}>
                    {ChevronRight}
                </div>
            )}
            {children &&
                children.map(
                    (child, i) =>
                        (child.length > 0 || child != false) && (
                            <TagsWrapper
                                className="tags"
                                style={{ transform: `translateX(${scroll}px)` }}
                                key={i}
                            >
                                {child}
                            </TagsWrapper>
                        )
                )}

            {scrollWidth > width && (
                <div className="arrows right" onClick={handleNextClick}>
                    {ChevronRight}
                </div>
            )}
        </Wrapper>
    );
};

TagSlider.propTypes = {
    children: PropTypes.any,
};

export default TagSlider;
