import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as variables from "../common/variables";

const { WHITE, SECONDARY_BASE } = variables;

const StyledBadge = styled.div.attrs({ 'data-testid': 'testid'})`
    position: absolute;
    top: 0;
    left: 0;
    font-size: 12px;
    background-color: ${SECONDARY_BASE};
    padding: 2px 6px;
    color: ${WHITE};
    border-radius: 3px;
    line-height: 14px;
`;

const Badge = ({ label, className }) => {
    return <StyledBadge className={className}>{label}</StyledBadge>;
};

Badge.propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
};

Badge.defaultProps = {};

export default Badge;
