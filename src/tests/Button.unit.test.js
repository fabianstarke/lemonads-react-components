import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import Button from '../components/Button';
import * as variables from "../common/variables";


it("renders and does not crash", () => {
    render(<Button role="some-rol" />);
    const button = screen.getByRole('some-rol');
    expect(button).toBeInTheDocument();
});


it('has the correct props', () => {
    render(<Button role="some-rol" className="veryclass" />);
    const button = screen.getByRole('some-rol');
    expect(button.getAttribute("type")).toEqual('primary'); // Default prop
    expect(button.getAttribute("full")).toBeFalsy(); // Default prop
    expect(button).toHaveClass("veryclass");
});


it('has correct content', () => {
    render(<Button role="some-rol" label="MonTest" />);
    const button = screen.getByRole('some-rol');
    expect(button).toHaveTextContent('MonTest');
});


it('has the correct colors', () => {
    const {
        PRIMARY_BASE,
        SUCCESS,
        WARNING,
        ERROR,
    } = variables;

    render(<Button role="button-success" type="success" label="Test" />);
    render(<Button role="button-warning" type="warning" label="Test" />);
    render(<Button role="button-error" type="error" label="Test" />);
    render(<Button role="button-primary" type="primary" label="Test" />);

    const buttonSuccess = screen.getByRole('button-success');
    const buttonWarning = screen.getByRole('button-warning');
    const buttonError = screen.getByRole('button-error');
    const buttonPrimary = screen.getByRole('button-primary');

    expect(buttonPrimary).toHaveStyleRule('background-color', PRIMARY_BASE)
    expect(buttonSuccess).toHaveStyleRule('background-color', SUCCESS)
    expect(buttonWarning).toHaveStyleRule('background-color', WARNING)
    expect(buttonError).toHaveStyleRule('background-color', ERROR)
});


it('triggers correctly onClick', () => {
    const onClick = jest.fn();
    render(<Button role="some-rol" label="MonTest" onClick={onClick} />);
    const button = screen.getByRole('some-rol');
    fireEvent.click(button)
    expect(onClick).toHaveBeenCalledTimes(1);
});
