import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import SubNav from "../components/SubNav";

it("has the correct props and content", () => {
    render(<SubNav role="subnav" className="subnav-class" />);

    const subnav = screen.getByRole("subnav");
    const activeElement = subnav.querySelector("li:first-child");

    expect(subnav).toHaveClass("subnav-class");
    expect(subnav).toHaveTextContent("SubNav1");
    expect(subnav).toHaveTextContent("SubNav2");
    expect(subnav).toHaveTextContent("SubNav3");
    expect(activeElement).toHaveClass("active");
});

it("handles item click correctly", () => {
    const onChangeIndex = jest.fn();

    render(<SubNav role="subnav" onChangeIndex={onChangeIndex} />);

    const subnav = screen.getByRole("subnav");
    const activeElement = subnav.querySelector("li:first-child");
    const targetElement = subnav.querySelector("li:nth-child(2)");

    expect(activeElement).toHaveClass("active");
    expect(targetElement).not.toHaveClass("active");
    expect(onChangeIndex).toHaveBeenCalledTimes(0);

    fireEvent.click(targetElement);

    expect(activeElement).not.toHaveClass("active");
    expect(targetElement).toHaveClass("active");
    expect(onChangeIndex).toHaveBeenCalledTimes(1);
});
