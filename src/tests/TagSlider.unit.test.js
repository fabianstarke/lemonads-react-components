import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen } from "@testing-library/react";

import TagSlider from "../components/TagSlider";
import Tag from "../../src/components/Tag";

it("has the correct props and content", () => {
    const labelArray = ["Label1", "Label2", "Label3"];

    render(
        <TagSlider
            role="tag-slider"
            children={labelArray.map((label, i) => (
                <Tag key={i} label={label} />
            ))}
        />
    );

    const tagSlider = screen.getByRole("tag-slider");

    expect(tagSlider).toHaveTextContent("Label1");
    expect(tagSlider).toHaveTextContent("Label2");
    expect(tagSlider).toHaveTextContent("Label3");
});
