import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import { render, screen, fireEvent } from "@testing-library/react";

import ReactSelect from "../components/ReactSelect";

it("has the correct props and content", () => {
    render(
        <ReactSelect
            role="react-select"
            wrapperRole="react-select-wrapper"
            wrapperClassName="wrapper-class"
            label="React select label"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            defaultValue={{ label: "first option", value: 0 }}
            classNamePrefix="react-select"
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const firstOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );
    const secondOption = reactSelectWrapper.querySelector(
        ".react-select__option:nth-child(2)"
    );
    const thirdOption = reactSelectWrapper.querySelector(
        ".react-select__option:last-child"
    );

    expect(reactSelectWrapper).toHaveClass("wrapper-class");
    expect(reactSelectWrapper).toHaveTextContent("React select label");
    expect(firstOption).toHaveClass("react-select__option--is-selected");
    expect(secondOption).not.toHaveClass("react-select__option--is-selected");
    expect(thirdOption).not.toHaveClass("react-select__option--is-selected");
});

it("handles array value correctly", () => {
    render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            isMulti
            classNamePrefix="react-select"
            value={[1, 2]}
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const firstOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );
    const secondOption = reactSelectWrapper.querySelector(
        ".react-select__option:nth-child(2)"
    );
    const thirdOption = reactSelectWrapper.querySelector(
        ".react-select__option:last-child"
    );

    expect(firstOption).not.toHaveClass("react-select__option--is-selected");
    expect(secondOption).toHaveClass("react-select__option--is-selected");
    expect(thirdOption).toHaveClass("react-select__option--is-selected");
});

it("handles object value correctly", () => {
    render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            classNamePrefix="react-select"
            value={{ label: "second option", value: 1 }}
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const firstOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );
    const secondOption = reactSelectWrapper.querySelector(
        ".react-select__option:nth-child(2)"
    );
    const thirdOption = reactSelectWrapper.querySelector(
        ".react-select__option:last-child"
    );

    expect(firstOption).not.toHaveClass("react-select__option--is-selected");
    expect(secondOption).toHaveClass("react-select__option--is-selected");
    expect(thirdOption).not.toHaveClass("react-select__option--is-selected");
});

it("handles variable value correctly", () => {
    render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            classNamePrefix="react-select"
            value={2}
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const firstOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );
    const secondOption = reactSelectWrapper.querySelector(
        ".react-select__option:nth-child(2)"
    );
    const thirdOption = reactSelectWrapper.querySelector(
        ".react-select__option:last-child"
    );

    expect(firstOption).not.toHaveClass("react-select__option--is-selected");
    expect(secondOption).not.toHaveClass("react-select__option--is-selected");
    expect(thirdOption).toHaveClass("react-select__option--is-selected");
});

it("handles undefined value correctly", () => {
    const container = render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            classNamePrefix="react-select"
            value={undefined}
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const firstOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );
    const secondOption = reactSelectWrapper.querySelector(
        ".react-select__option:nth-child(2)"
    );
    const thirdOption = reactSelectWrapper.querySelector(
        ".react-select__option:last-child"
    );

    expect(firstOption).not.toHaveClass("react-select__option--is-selected");
    expect(secondOption).not.toHaveClass("react-select__option--is-selected");
    expect(thirdOption).not.toHaveClass("react-select__option--is-selected");
});

it("triggers onChange", () => {
    const onChange = jest.fn();

    render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            isMulti={true}
            classNamePrefix="react-select"
            onChange={onChange}
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const targetOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );

    expect(onChange).toHaveBeenCalledTimes(0);

    fireEvent.click(targetOption);

    expect(onChange).toHaveBeenCalledTimes(1);
});

it("show input value when only one value is selected", () => {
    render(
        <ReactSelect
            wrapperRole="react-select-wrapper"
            options={[
                { label: "first option", value: 0 },
                { label: "second option", value: 1 },
                { label: "third option", value: 2 },
            ]}
            isMulti
            classNamePrefix="react-select"
            placeholder="Select"
            menuIsOpen
        />
    );

    const reactSelectWrapper = screen.getByRole("react-select-wrapper");
    const valueContainer = reactSelectWrapper.querySelector(
        ".react-select__value-container"
    );
    let targetOption = reactSelectWrapper.querySelector(
        ".react-select__option:first-child"
    );

    expect(valueContainer).toHaveTextContent("Select");

    fireEvent.click(targetOption);

    expect(valueContainer).not.toHaveTextContent("Select");
    expect(valueContainer).toHaveTextContent("first option");

    targetOption = screen.getByText("third option");

    fireEvent.click(targetOption);

    expect(valueContainer).not.toHaveTextContent("first option");
    expect(valueContainer).not.toHaveTextContent("third option");
    expect(valueContainer).toHaveTextContent("Select");
});
