import React from "react";
import ReactDOM from "react-dom";
import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen } from "@testing-library/react";
import * as variables from "../common/variables";

const { PRIMARY_BASE } = variables;

import SearchBar from "../components/SearchBar";

describe("SearchBar component tests", () => {

    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<SearchBar />, div);
        render(<SearchBar />);
    }); 

    it('renders correctly props and styling', () => {
        render(<SearchBar role="searchbar" placeholder="Placeholder" />);
        const searchbar = screen.getByRole("searchbar");

        expect(searchbar.getAttribute("placeholder")).toEqual("Placeholder");
        expect(searchbar).toHaveStyleRule("border", "1px solid #EBEBEB");

        render(<SearchBar role="focus" focus />);
        const focus = screen.getByRole("focus");
        expect(focus).toHaveStyleRule("border", `1px solid ${PRIMARY_BASE}`);
    })


});
