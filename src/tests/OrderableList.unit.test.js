import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "regenerator-runtime/runtime";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";

import OrderableList from "../components/OrderableList";

it("has the correct props and content", () => {
    /* render(
        <OrderableList role="orderable-list" order={[0, 1, 2, 3]}>
            {["Ghost", "Unicorn", "Avocado", "Hearth"].map((el, index) => (
                <div key={index}>{el}</div>
            ))}
        </OrderableList>
    );

    const orderableList = screen.getByRole("orderable-list");

    expect(orderableList).toHaveTextContent("Ghost");
    expect(orderableList).toHaveTextContent("Hearth"); */
});

/* it("has the correct order", () => {
    render(
        <OrderableList role="orderable-list" order={[0, 2, 3, 1]}>
            {["Ghost", "Unicorn", "Avocado", "Hearth"].map((el, index) => (
                <div key={index}>{el}</div>
            ))}
        </OrderableList>
    );

    const orderableList = screen.getByRole("orderable-list");
    const FirstElement = orderableList.querySelector("ul li:first-child");
    const SecondElement = orderableList.querySelector("ul li:nth-child(2)");
    const ThirdElement = orderableList.querySelector("ul li:nth-child(3)");
    const LastElement = orderableList.querySelector("ul li:last-child");

    expect(FirstElement).toHaveTextContent("Ghost");
    expect(SecondElement).toHaveTextContent("Avocado");
    expect(ThirdElement).toHaveTextContent("Hearth");
    expect(LastElement).toHaveTextContent("Unicorn");
});

it("changes order and fire onChange when dragged", async () => {
    const onChange = jest.fn();
    const mockdt = { setData: jest.fn() };

    render(
        <OrderableList role="orderable-list" onChange={onChange}>
            {["Ghost", "Unicorn", "Avocado", "Hearth"].map((el, index) => (
                <div key={index}>{el}</div>
            ))}
        </OrderableList>
    );

    const orderableList = screen.getByRole("orderable-list");
    let SecondElement = orderableList.querySelector("ul li:nth-child(2)");
    const SecondElementDiv = SecondElement.querySelector(".handle");
    let ThirdElement = orderableList.querySelector("ul li:nth-child(3)");

    expect(SecondElement).toHaveTextContent("Unicorn");
    expect(ThirdElement).toHaveTextContent("Avocado");

    fireEvent.dragStart(SecondElementDiv, { dataTransfer: mockdt });

    await waitFor(() => expect(SecondElementDiv).toHaveClass("remove"));
    expect(SecondElementDiv).toHaveClass("selected");

    fireEvent.dragEnter(ThirdElement);
    fireEvent.dragOver(ThirdElement);
    fireEvent.dragEnd(SecondElementDiv);

    SecondElement = orderableList.querySelector("ul li:nth-child(2)");
    ThirdElement = orderableList.querySelector("ul li:nth-child(3)");

    await waitFor(() => expect(SecondElementDiv).not.toHaveClass("remove"));
    expect(SecondElementDiv).not.toHaveClass("selected");
    expect(SecondElement).toHaveTextContent("Avocado");
    expect(ThirdElement).toHaveTextContent("Unicorn");
    expect(onChange).toHaveBeenCalledTimes(1);
});

it("doesn't fire onChange if order doesn't change", () => {
    const onChange = jest.fn();
    const mockdt = { setData: jest.fn() };

    render(
        <OrderableList role="orderable-list" onChange={onChange}>
            {["Ghost", "Unicorn", "Avocado", "Hearth"].map((el, index) => (
                <div key={index}>{el}</div>
            ))}
        </OrderableList>
    );

    const orderableList = screen.getByRole("orderable-list");
    let SecondElement = orderableList.querySelector("ul li:nth-child(2)");
    const SecondElementDiv = SecondElement.querySelector(".handle");

    expect(SecondElement).toHaveTextContent("Unicorn");

    fireEvent.dragStart(SecondElementDiv, { dataTransfer: mockdt });
    fireEvent.dragEnter(SecondElement);
    fireEvent.dragOver(SecondElement);
    fireEvent.dragEnd(SecondElementDiv);

    SecondElement = orderableList.querySelector("ul li:nth-child(2)");

    expect(SecondElement).toHaveTextContent("Unicorn");
    expect(onChange).toHaveBeenCalledTimes(0);
}); */

