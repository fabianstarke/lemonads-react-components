import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import LabelButton from "../components/LabelButton";
import * as variables from "../common/variables";

it("has the correct props", () => {
    render(
        <LabelButton role="label-button" className="label-class" label="test" />
    );
    const button = screen.getByRole("label-button");
    expect(button.getAttribute("type")).toEqual("primary");
    expect(button.getAttribute("full")).toBeFalsy();
    expect(button).toHaveClass("label-class");
    expect(button).toHaveTextContent("test");    
});

it("triggers onClick", () => {
    const onClick = jest.fn();

    render(<LabelButton role="label-button" onClick={onClick} />);
    const button = screen.getByRole("label-button");
    fireEvent.click(button);
    expect(onClick).toHaveBeenCalledTimes(1);
});

it("has the correct colors", () => {
    const { PRIMARY_BASE, SUCCESS, WARNING, ERROR } = variables;

    render(<LabelButton role="button-success" type="success" />);
    render(<LabelButton role="button-warning" type="warning" />);
    render(<LabelButton role="button-error" type="error" />);
    render(<LabelButton role="button-primary" type="primary" />);

    const buttonSuccess = screen.getByRole("button-success");
    const buttonWarning = screen.getByRole("button-warning");
    const buttonError = screen.getByRole("button-error");
    const buttonPrimary = screen.getByRole("button-primary");

    expect(buttonPrimary).toHaveStyleRule("background-color", PRIMARY_BASE);
    expect(buttonSuccess).toHaveStyleRule("background-color", SUCCESS);
    expect(buttonWarning).toHaveStyleRule("background-color", WARNING);
    expect(buttonError).toHaveStyleRule("background-color", ERROR);
});

it("has the correct width", () => {
    render(<LabelButton role="full-button" full />);
    render(<LabelButton role="fit-content-button" />);

    const fullButton = screen.getByRole("full-button");
    const fitContentButton = screen.getByRole("fit-content-button");

    expect(fullButton).toHaveStyleRule("width", "100%");
    expect(fitContentButton).toHaveStyleRule("width", "fit-content");
});
