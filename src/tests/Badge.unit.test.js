import React from "react";
import ReactDOM from "react-dom";
import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import renderer from "react-test-renderer";

import { render, screen } from "@testing-library/react";
import * as variables from "../common/variables";

const { WHITE, SECONDARY_BASE } = variables;

import Badge from "../components/Badge";

describe("badge component test", () => {

    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Badge />, div);
        render(<Badge />);
    });

    it("renders props correctly", () => {
        render(<Badge label="TestLabel" className="myTestClassName" />);
        const badge = screen.getByTestId("testid");
        expect(badge).toHaveTextContent("TestLabel");
        expect(badge).toHaveClass("myTestClassName");
    });

    it("renders correct styling", () => {
        const badge = renderer.create(<Badge />).toJSON();
        expect(badge).toHaveStyleRule("background-color", SECONDARY_BASE);
        expect(badge).toHaveStyleRule("color", WHITE);
    });
});
