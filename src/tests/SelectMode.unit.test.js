import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { render, screen, fireEvent } from "@testing-library/react";

import SelectMode from "../components/SelectMode";

it("has the correct props and content", () => {
    render(
        <SelectMode
            role="select-mode"
            className="select-mode-class"
            iconList={[
                { iconType: "menu", iconName: "DashBoard" },
                { iconType: "base", iconName: "BulletList" },
            ]}
        />
    );

    const selectMode = screen.getByRole("select-mode");
    const menuIcon = selectMode.querySelector("li:first-child");
    const baseIcon = selectMode.querySelector("li:nth-child(2)");

    expect(selectMode).toHaveClass("select-mode-class");
    expect(menuIcon).toHaveClass("active");
    expect(menuIcon).toHaveClass("menu-icon");
    expect(baseIcon).not.toHaveClass("active");
    expect(baseIcon).toHaveClass("base-icon");
});

it("handles item click", () => {
    const onSelectItem = jest.fn();
    render(
        <SelectMode
            role="select-mode"
            className="select-mode-class"
            iconList={[
                { iconType: "menu", iconName: "DashBoard" },
                { iconType: "base", iconName: "BulletList" },
            ]}
            onSelectItem={onSelectItem}
        />
    );

    const selectMode = screen.getByRole("select-mode");
    const menuIcon = selectMode.querySelector("li:first-child");
    const baseIcon = selectMode.querySelector("li:nth-child(2)");

    expect(menuIcon).toHaveClass("active");
    expect(baseIcon).not.toHaveClass("active");
    expect(onSelectItem).toHaveBeenCalledTimes(0);

    fireEvent.click(baseIcon);

    expect(menuIcon).not.toHaveClass("active");
    expect(baseIcon).toHaveClass("active");
    expect(onSelectItem).toHaveBeenCalledTimes(1);
});
