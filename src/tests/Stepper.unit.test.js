import React from "react";
import ReactDOM from "react-dom";
import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, fireEvent } from "@testing-library/react";

import Stepper from "../components/Stepper";

describe("Stepper component tests", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Stepper />, div);
        render(<Stepper />);
    });

    it("renders correct content and styling", () => {

        render(<Stepper role="stepper" currentIndex={1} />);

        // steps

        //done step
        const step = document.getElementById("step-0");
        const doneStep = step.querySelector("div");
        expect(doneStep).toHaveTextContent("Step1");
        expect(doneStep).toHaveClass("done");

        //index
        const doneIndex = doneStep.querySelector("span");
        expect(doneIndex).toHaveClass('done');

        //bar
        const doneBar = document.getElementById("bar-0");
        expect(doneBar).toHaveClass("done");

        //current step

        const step1 = document.getElementById("step-1");
        const currentStep = step1.querySelector("div");
        expect(currentStep).toHaveTextContent("Step2");
        expect(currentStep).toHaveClass("current");

        //index
        const currentIndex = currentStep.querySelector("span");
        expect(currentIndex).toHaveClass('current');

        //bar
        const currentBar = document.getElementById("bar-1");
        expect(currentBar).toHaveClass("current");

        //normal step

        const step2 = document.getElementById("step-2");
        const normalStep = step2.querySelector("div");
        expect(normalStep).toHaveTextContent("Step3");
        expect(normalStep).toHaveStyleRule("current");

        //index
        const index = currentStep.querySelector("span");
        expect(index).toHaveClass('current');

        //bar
        const bar = document.getElementById("bar-1");
        expect(bar).toHaveClass("current");

    });

    it("navigates correctly between steps", () => {
        const onChangePeriodIndex = jest.fn();

        render(
            <Stepper role="stepper" onChangePeriodIndex={onChangePeriodIndex} />
        );

        const currentStep = document.getElementById("step-0");
        const activeElement = currentStep.querySelector("div");

        const nextStep = document.getElementById("step-1");
        const targetElement = nextStep.querySelector("div");

        expect(activeElement).toHaveClass("current");
        expect(targetElement).not.toHaveClass("current");
        expect(onChangePeriodIndex).toHaveBeenCalledTimes(0);

        fireEvent.click(targetElement);

/*         expect(activeElement).not.toHaveClass("current");
        expect(activeElement).toHaveClass("done");
        
        expect(targetElement).toHaveClass("current");  */
        expect(onChangePeriodIndex).toHaveBeenCalledTimes(1);
    });
});
