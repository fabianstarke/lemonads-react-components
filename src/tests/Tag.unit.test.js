import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import Tag from "../components/Tag";
import { GREY_1, WHITE } from "../common/variables";

it("has the correct props and content", () => {
    render(<Tag role="tag" label="Test tag" />);

    const tag = screen.getByRole("tag");
    const close = tag.querySelector("svg");

    expect(tag).toHaveTextContent("Test tag");
    expect(tag).toHaveStyleRule("background-color", GREY_1);
    expect(tag).toHaveStyleRule("border", "solid " + GREY_1 + " 1px");
    expect(close).not.toBeInTheDocument();
});

it("shows cross when close is true", () => {
    render(<Tag role="tag" label="Test tag" close />);

    const tag = screen.getByRole("tag");
    const close = tag.querySelector("svg");

    expect(close).toBeInTheDocument();
});

it("fires onClick", () => {
    const onClick = jest.fn();

    render(<Tag role="tag" label="Test tag" onClick={onClick} />);

    const tag = screen.getByRole("tag");

    expect(onClick).toHaveBeenCalledTimes(0);

    fireEvent.click(tag);

    expect(onClick).toHaveBeenCalledTimes(1);
});

it("changes style when selected", () => {
    render(<Tag role="tag" label="Test tag" selected />);

    const tag = screen.getByRole("tag");

    expect(tag).toHaveTextContent("Test tag");
    expect(tag).toHaveStyleRule("background-color", WHITE);
    expect(tag).toHaveStyleRule("border", "solid black 1px");
});
