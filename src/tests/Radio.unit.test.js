import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "jest-canvas-mock";

import { render, screen, fireEvent } from "@testing-library/react";

import { Radio, RadioGroup } from "../components/Radio";

const hastTheCorrectPropsAndContent = (animated) => {
    render(
        <RadioGroup
            className="radio-group-class"
            label="Radio Group Label"
            parentRole="radio-group"
            role="radio-group-wrapper"
            name="radio-group"
            animated={animated}
        >
            <Radio
                value={1}
                role="first-radio"
                wrapperRole="first-radio-wrapper"
                className="first-radio-class"
                label="First Radio Label"
            />
            <Radio
                value={2}
                role="second-radio"
                wrapperRole="second-radio-wrapper"
                className="second-radio-class"
            />
        </RadioGroup>
    );

    const radioGroup = screen.getByRole("radio-group");
    const radioGroupWrapper = screen.getByRole("radio-group-wrapper");

    const firstRadioWrapper = screen.getByRole("first-radio-wrapper");
    const firstRadio = screen.getByRole("first-radio");
    const secondRadio = screen.getByRole("second-radio");

    expect(radioGroupWrapper).toHaveClass("radio-group-class");
    expect(radioGroup).toHaveTextContent("Radio Group Label");

    expect(firstRadio).toHaveClass("first-radio-class");
    expect(secondRadio).toHaveClass("second-radio-class");
    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(firstRadio).toHaveAttribute("value", "1");
    expect(secondRadio).toHaveAttribute("value", "2");

    expect(firstRadioWrapper).toHaveTextContent("First Radio Label");
};

const handlesItemClickCorrectly = (animated) => {
    const onChange = jest.fn();

    render(
        <RadioGroup name="radio-group" onChange={onChange} animated={animated}>
            <Radio value={1} role="first-radio" />
            <Radio value={2} role="second-radio" />
        </RadioGroup>
    );

    const firstRadio = screen.getByRole("first-radio");
    const secondRadio = screen.getByRole("second-radio");

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(onChange).toHaveBeenCalledTimes(0);

    fireEvent.click(firstRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(onChange).toHaveBeenCalledTimes(0);

    fireEvent.click(secondRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toHaveAttribute("aria-checked", "true");
    expect(onChange).toHaveBeenCalledTimes(1);
};

const allowsUnselected = (animated) => {
    const onChange = jest.fn();

    render(
        <RadioGroup
            name="radio-group"
            onChange={onChange}
            allowUnselected
            selected={1}
            animated={animated}
        >
            <Radio value={1} role="first-radio" />
            <Radio value={2} role="second-radio" />
        </RadioGroup>
    );

    const firstRadio = screen.getByRole("first-radio");
    const secondRadio = screen.getByRole("second-radio");

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(onChange).toHaveBeenCalledTimes(0);

    fireEvent.click(firstRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(onChange).toHaveBeenCalledTimes(1);
};

const handlesGroupDisabledProperty = (animated) => {
    render(
        <RadioGroup name="radio-group" disabled animated={animated}>
            <Radio value={1} role="first-radio" />
            <Radio value={2} role="second-radio" />
        </RadioGroup>
    );

    const firstRadio = screen.getByRole("first-radio");
    const secondRadio = screen.getByRole("second-radio");

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");

    fireEvent.click(secondRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
};

const handlesSingleRadiosDisabledProperty = (animated) => {
    render(
        <RadioGroup name="radio-group" animated={animated}>
            <Radio value={1} role="first-radio" />
            <Radio value={2} role="second-radio" disabled={true} />
            <Radio value={3} role="third-radio" />
        </RadioGroup>
    );

    const firstRadio = screen.getByRole("first-radio");
    const secondRadio = screen.getByRole("second-radio");
    const thirdRadio = screen.getByRole("third-radio");

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toBeDisabled();
    expect(thirdRadio).toHaveAttribute("aria-checked", "false");

    fireEvent.click(secondRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "true");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toBeDisabled();
    expect(thirdRadio).toHaveAttribute("aria-checked", "false");

    fireEvent.click(thirdRadio);

    expect(firstRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toHaveAttribute("aria-checked", "false");
    expect(secondRadio).toBeDisabled();
    expect(thirdRadio).toHaveAttribute("aria-checked", "true");
};

it("has the correct props and content without animation", () => {
    hastTheCorrectPropsAndContent(false);
});

it("has the correct props and content with animation", () => {
    hastTheCorrectPropsAndContent(true);
});

it("handles item click correctly without animation", () => {
    handlesItemClickCorrectly(false);
});

it("handles item click correctly with animation", () => {
    handlesItemClickCorrectly(true);
});

it("allows unselected without animation", () => {
    allowsUnselected(false);
});

it("allows unselected with animation", () => {
    allowsUnselected(true);
});

it("handles group disabled property without animation", () => {
    handlesGroupDisabledProperty(false);
});

it("handles group disabled property with animation", () => {
    handlesGroupDisabledProperty(true);
});

it("handles single radio disabled property without animation", () => {
    handlesSingleRadiosDisabledProperty(false);
});

it("handles single radio disabled property with animation", () => {
    handlesSingleRadiosDisabledProperty(true);
});
