import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import Table from "../components/Table";

it("has the correct props and content", () => {
    render(
        <Table
            role="table-wrapper"
            className="table-class"
            id="table1"
            headTitles={[
                { title: "Label 1" },
                { title: "Label 2" },
                { title: "Label 3" },
            ]}
            listItems={[
                {
                    datas: ["Type", "Something", "2345", "Download"],
                },
                {
                    datas: ["AType", "ASomething", "12345", "Download"],
                },
            ]}
        />
    );

    const tableWrapper = screen.getByRole("table-wrapper");
    const loadMore = tableWrapper.querySelector(".load-more");

    expect(tableWrapper).toHaveTextContent("Label 1");
    expect(tableWrapper).toHaveClass("table-class");
    expect(tableWrapper).toHaveAttribute("id", "table1");
    expect(loadMore).not.toBeInTheDocument();
});

it("loads more elements", () => {
    render(
        <Table
            role="table-wrapper"
            headTitles={[
                { title: "Label 1" },
                { title: "Label 2" },
                { title: "Label 3" },
            ]}
            listItems={[
                {
                    datas: ["Type", "Something", "2345", "Download"],
                },
                {
                    datas: ["AType", "ASomething", "12345", "Download"],
                },
            ]}
            clickToLoad
            elementPerPage={1}
        />
    );
    const tableWrapper = screen.getByRole("table-wrapper");
    const loadMore = tableWrapper.querySelector(".load-more");
    const firstItem = tableWrapper.querySelector("table tbody tr:nth-child(1)");
    let secondItem = tableWrapper.querySelector("table tbody tr:nth-child(2)");

    expect(loadMore).toBeInTheDocument();
    expect(firstItem).toBeInTheDocument();
    expect(secondItem).not.toBeInTheDocument();

    fireEvent.click(loadMore);

    secondItem = tableWrapper.querySelector("table tbody tr:nth-child(2)");

    expect(firstItem).toBeInTheDocument();
    expect(secondItem).toBeInTheDocument();
    expect(loadMore).not.toBeInTheDocument();
});

it("triggers onLineClick", () => {
    const onLineClick = jest.fn();

    render(
        <Table
            role="table-wrapper"
            headTitles={[
                { title: "Label 1" },
                { title: "Label 2" },
                { title: "Label 3" },
            ]}
            listItems={[
                {
                    datas: ["Type", "Something", "2345", "Download"],
                },
                {
                    datas: ["AType", "ASomething", "12345", "Download"],
                },
            ]}
            onLineClick={onLineClick}
        />
    );

    const tableWrapper = screen.getByRole("table-wrapper");
    const firstItem = tableWrapper.querySelector("table tbody tr:nth-child(1)");

    expect(onLineClick).toHaveBeenCalledTimes(0);

    fireEvent.click(firstItem);

    expect(onLineClick).toHaveBeenCalledTimes(1);
});

it("is sortable", () => {
    render(
        <Table
            role="table-wrapper"
            headTitles={[
                { title: "Label 1" },
                { title: "Label 2" },
                { title: "Label 3" },
            ]}
            listItems={[
                {
                    datas: ["Btest", "Something", "2345", "Download"],
                },
                {
                    datas: ["Ctest", "ASomething", "12345", "Download"],
                },
                {
                    datas: ["Atest", "ASomething", "12345", "Download"],
                },
            ]}
            sortable
        />
    );

    const tableWrapper = screen.getByRole("table-wrapper");
    const sortLabel = screen.getByText("Label 1");
    const firstItem = tableWrapper.querySelector("table tbody tr:nth-child(1)");
    const lastItem = tableWrapper.querySelector("table tbody tr:last-child");

    expect(firstItem).toHaveTextContent("Btest");
    expect(lastItem).toHaveTextContent("Atest");

    fireEvent.click(sortLabel);

    expect(firstItem).toHaveTextContent("Atest");
    expect(lastItem).toHaveTextContent("Ctest");

    fireEvent.click(sortLabel);

    expect(firstItem).toHaveTextContent("Btest");
    expect(lastItem).toHaveTextContent("Atest");

    fireEvent.click(sortLabel);

    expect(firstItem).toHaveTextContent("Ctest");
    expect(lastItem).toHaveTextContent("Atest");
});
