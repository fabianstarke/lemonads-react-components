import React from 'react';
import renderer from 'react-test-renderer';
import "jest-canvas-mock";
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from "react-router-dom";
import 'jest-styled-components'

import Button from '../components/Button';
import Card from '../components/Card';
import ScrollingList from '../components/ScrollingList';
import DatePicker from '../components/DatePicker';
import DateRangePicker from '../components/DateRangePicker';
import CheckBox from '../components/CheckBox';
import Alert from '../components/Alert';
import IconButton from '../components/IconButton';
import LabelButton from '../components/LabelButton';
import Modal from '../components/Modal';
import Pagination from '../components/Pagination';
import OpenSelect from '../components/OpenSelect';
import Divider from '../components/Divider';
import Loading from '../components/Loading';
import Switch from '../components/Switch';
import OrderableList from '../components/OrderableList';
import Graph from '../components/Graph';
import Input from '../components/Input';
import PageHelpCollapse from '../components/PageHelpCollapse';
import PageHelpBox from '../components/PageHelpBox';
import SubNavRoute from '../components/SubNavRoute';
import SubNav from '../components/SubNav';
import Status from '../components/Status';
import ReactSelect from '../components/ReactSelect';
import SelectMode from '../components/SelectMode';
import { RadioGroup, Radio } from '../components/Radio';
import SwitchCollapse from '../components/SwitchCollapse';
import Table from '../components/Table';
import Tag from '../components/Tag';
import SearchBar from '../components/SearchBar';
import Tooltip from '../components/Tooltip';
import SecondaryTable from '../components/SecondaryTable';
import TagSlider from '../components/TagSlider';  
import Stepper from '../components/Stepper'
import MultiLevelSelect from '../components/MultiLevelSelect';


it('Button snapshot', () => {
    const tree = renderer
        .create(<Button label="My Test" />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});


it('ScrollingList snapshot', () => {
    const tree = renderer
        .create(<ScrollingList role="scrolling-list-role" width="600px">
            {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((el, i) => {
                return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>
                    Hello {el}
                </div>);
            })}
        </ScrollingList>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});


it('Card snapshot', () => {
    const tree = renderer
        .create(<Card skeletonWidth="50px" animationDuration="10s" skeletonHeight="50px" loading>Hello</Card>)
        .toJSON();
    expect(tree).toMatchSnapshot();
});


it('DatePicker snapshot', () => {
    const tree = renderer
        .create(<DatePicker
            dateFormat="YYYY-MM-DD"
            maxDate={new Date("2112-12-12")}
            date={new Date("2021-01-06")}
            dayPickerProps={{
                disabledDays: {
                    after: new Date()
                },
            }}
        />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});


it('DateRangePicker snapshot', () => {
    const tree = renderer
        .create(<DateRangePicker
            from={new Date("2020-01-01")}
            to={new Date("2021-01-01")}
            onChange={() => { }}
            onCancel={() => { }}
            handleCustomPeriodClick={(item, index) => { }}
            dayPickerProps={{
                month: new Date("2019-12-01")
            }}
        />)
        .toJSON();
    expect(tree).toMatchSnapshot();
});


it("CheckBox snapshot", () => {
    const tree = renderer.create(<CheckBox onChange={() => { }} />).toJSON();
    expect(tree).toMatchSnapshot();
});


it("Alert snapshot", () => {
    const tree = renderer.create(<Alert />).toJSON();
    expect(tree).toMatchSnapshot();
});


it("IconButton snapshot", () => {
    const tree = renderer.create(<IconButton type="add" />).toJSON();
    expect(tree).toMatchSnapshot();
});

it("LabelButton snapshot", () => {
    const tree = renderer.create(<LabelButton label="test" />).toJSON();
    expect(tree).toMatchSnapshot();
});

it("Modal snapshot", () => {
    const tree = renderer.create(<Modal header="title modal" defaultToggle />).toJSON();
    expect(tree).toMatchSnapshot();
});

it("Pagination snapshot", () => {
    const tree = renderer.create(<Pagination pages={5} />).toJSON();
    expect(tree).toMatchSnapshot();
});


it('Divider snapshot', () => {
    const tree = renderer.create(<Divider />).toJSON();
    expect(tree).toMatchSnapshot();
});


it('Loading snapshot', () => {
    const tree = renderer.create(<Loading />).toJSON();
    expect(tree).toMatchSnapshot();
});


it('OpenSelect snapshot', () => {
    const tree = renderer.create(<OpenSelect options={[{ label: "label", value: "value" }]} />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Switch snapshot', () => {
    const tree = renderer.create(<Switch />).toJSON();
    expect(tree).toMatchSnapshot();
});
/* 
it("OrderableList snapshot", () => {
    const tree = renderer
        .create(
            <OrderableList role="orderable-list" >
                {["Ghost", "Unicorn", "Avocado", "Hearth"].map((el, index) => (
                    <div key={index}>{el}</div>
                ))}
            </OrderableList>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
}); */

it("Graph snapshot", () => {
    const dataY = [
        { label: 'data 1', labelColor: '', points: [200, 300, 400, 200, 100, 200, 100] },
        { label: 'data 2', labelColor: 'success', points: [300, 200, 400, 200, 500, 600, 100] }
    ];
    const dataX = [1, 2, 3, 4, 5, 6, 7];
    render(<Graph dataY={dataY} dataX={dataX} />);
    const graph = screen.getByRole("graph");
    const canvasAll = graph.querySelectorAll("canvas");
    expect(canvasAll.length).toEqual(1);
    const ctx = canvasAll[0].getContext("2d");
    expect([
        ctx.__getClippingRegion(),
        ctx.__getDrawCalls(),
        ctx.__getEvents(),
        ctx.__getPath()
    ]).toMatchSnapshot()
    // Snapshot of the component itself:
    //     const tree = renderer.create(<Graph dataY={dataY} dataX={dataX} />).toJSON();
    // Fails with:
    // //     TypeError: Cannot read property 'getElementsByTagName' of null

    // //     193 |     updateDatasets() {
    // //     194 |         // Project dataY into Graph.js datasets
    // //   > 195 |         var canvas = this.chartReference.current.getElementsByTagName('canvas')[0];
});

it('Input snapshot', () => {
    const tree = renderer.create(<Input />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('PageHelpBox snapshot', () => {
    const tree = renderer.create(<PageHelpBox title="a" description="b"/>).toJSON();
    expect(tree).toMatchSnapshot();
});

it('PageHelpCollapse snapshot', () => {
    const tree = renderer.create(<PageHelpCollapse />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('SubNavRoute snapshot', () => {
    const tree = renderer.create(<MemoryRouter><SubNavRoute /></MemoryRouter>).toJSON();
    expect(tree).toMatchSnapshot();
});

it('SubNav snapshot', () => {
    const tree = renderer.create(<MemoryRouter><SubNav /></MemoryRouter>).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Status snapshot', () => {
    const tree = renderer.create(<Status />).toJSON();
    expect(tree).toMatchSnapshot();
});

it("ReactSelect snapshot", () => {
    const tree = renderer
        .create(
            <ReactSelect
                options={[
                    { label: "first option", value: 0 },
                    { label: "second option", value: 1 },
                    { label: "third option", value: 2 },
                ]}
                classNamePrefix="react-select"
                menuIsOpen
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("SelectMode snapshot", () => {
    const tree = renderer
        .create(
            <SelectMode
                className="select-mode-class"
                iconList={[
                    { iconType: "menu", iconName: "DashBoard" },
                    { iconType: "base", iconName: "BulletList" },
                ]}
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("Radio snapshot", () => {
    const tree = renderer
        .create(
            <RadioGroup name="radio-group">
                <Radio value={1} role="first-radio" />
                <Radio value={2} role="second-radio" disabled={true} />
                <Radio value={3} role="third-radio" />
            </RadioGroup>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("SwitchCollapse snapshot", () => {
    const tree = renderer
        .create(
            <SwitchCollapse
                className="switch-collapse-class"
                text="Text open"
                textClosed="Text close"
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("Table snapshot", () => {
    const tree = renderer
        .create(
            <Table
                className="table-class"
                id="table1"
                headTitles={[
                    { title: "Label 1" },
                    { title: "Label 2" },
                    { title: "Label 3" },
                ]}
                listItems={[
                    {
                        datas: ["Type", "Something", "2345", "Download"],
                    },
                    {
                        datas: ["AType", "ASomething", "12345", "Download"],
                    },
                ]}
        />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("Tag snapshot", () => {
    const tree = renderer
        .create(
            <Tag role="tag" label="Test tag" />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it('Searchbar snapshot', () => {
    const tree = renderer.create(<SearchBar />).toJSON();
    expect(tree).toMatchSnapshot();
})

it('Tooltip snapshot', () => {
    const tree = renderer.create(<Tooltip placement="top" overlay="Tooltip"><span>Hover me</span></Tooltip>).toJSON();
    expect(tree).toMatchSnapshot();
})

it("SecondaryTable snapshot", () => {
    const tree = renderer
        .create(
            <SecondaryTable
                columns={[
                    {
                        Header: "Column 1",
                        accessor: "col1",
                    },
                    {
                        Header: "Column 2",
                        accessor: "col2",
                    },
                ]}
                data={[
                    {
                        col1: "Id 1",
                        col2: "fdfdf",
                        id: 1,
                    },
                    {
                        col1: "Id 2",
                        col2: "efvd",
                        id: 2,
                    },
                ]}
                defaultSort={[{ id: "col1", desc: true }]}
            />
        )
        .toJSON();
		expect(tree).toMatchSnapshot();
});

it("TagSlider snapshot", () => {
    const labelArray = ["Label1", "Label2", "Label3"];

    const tree = renderer
        .create(
            <TagSlider
                role="tag-slider"
                children={labelArray.map((label, i) => (
                    <Tag key={i} label={label} />
                ))}
            />,
            {
                createNodeMock: () => ({
                    getBoundingClientRect: jest
                        .fn()
                        .mockImplementation(() => ({ width: 1000 })),
                }),
            }
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it("Stepper snapshot", () => {
    const tree = renderer.create(<Stepper />).toJSON();
    expect(tree).toMatchSnapshot();
});

it("MultiLevelSelect snapshot", () => {
    const tree = renderer
        .create(
            <MultiLevelSelect
                role="multi-level-select"
                options={[
                    { label: "first option", value: 0 },
                    {
                        label: "second option",
                        value: 1,
                        children: [
                            {
                                label: "first child",
                                value: 10,
                                children: [
                                    { label: "first grandchild", value: 100 },
                                    { label: "second grandchild", value: 101 },
                                ],
                            },
                            { label: "second child", value: 11 },
                        ],
                    },
                    {
                        label: "third option long name long long long",
                        value: 1,
                        children: [
                            { label: "first child", value: 20 },
                            { label: "second child", value: 21 },
                        ],
                    },
                    { label: "4th option", value: 3 },
                    { label: "5th option", value: 4 },
                    { label: "6th option", value: 5 },
                ]}
                menuIsOpen={true}
                label="label"
                controllerLabel="controller label"
                values={[0]}
            />
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
