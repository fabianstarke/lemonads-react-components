import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import { MemoryRouter } from "react-router-dom";

import { render, screen } from "@testing-library/react";

import SubNavRoute from "../components/SubNavRoute";

it("has the correct props and content", () => {
    render(
        <MemoryRouter>
            <SubNavRoute role="subnavroute" />
        </MemoryRouter>
    );

    const subnav = screen.getByRole("subnavroute");
    const activeElement = subnav.querySelector("a:first-child li");

    expect(subnav).toHaveTextContent("Profile");
    expect(subnav).toHaveTextContent("Business");
    expect(subnav).toHaveTextContent("User");
    expect(activeElement).toHaveClass("active");
});

it("handles location change", () => {
    const { container } = render(
        <MemoryRouter>
            <SubNavRoute role="subnavroute" />
        </MemoryRouter>
    );

    const subnav = screen.getByRole("subnavroute");
    const activeElement = subnav.querySelector("a:first-child li");
    const targetElement = subnav.querySelector("a:nth-child(2) li");

    expect(activeElement).toHaveClass("active");
    expect(targetElement).not.toHaveClass("active");

    render(
        <MemoryRouter>
            <SubNavRoute location="/profile/business" />
        </MemoryRouter>,
        { container }
    );

    expect(activeElement).not.toHaveClass("active");
    expect(targetElement).toHaveClass("active");
});
