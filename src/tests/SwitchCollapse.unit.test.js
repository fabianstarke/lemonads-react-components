import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import SwitchCollapse from "../components/SwitchCollapse";

it("has the correct props and content", () => {
    render(
        <SwitchCollapse className="switch-collapse-class" text="test text" />
    );

    const switchCollapseWrapper = screen.getByRole("switchcollapse");
    const switchCollapse = switchCollapseWrapper.querySelector(
        ".switch-collapse-class"
    );

    expect(switchCollapse).toBeInTheDocument();
    expect(switchCollapse).toHaveTextContent("test text");
});

it("opens and closes without onChange", () => {
    render(
        <SwitchCollapse
            className="switch-collapse-class"
            text="Text open"
            textClosed="Text close"
        />
    );

    const switchCollapseWrapper = screen.getByRole("switchcollapse");
    const switchCollapse = switchCollapseWrapper.querySelector(
        ".switch-collapse-class"
    );

    expect(switchCollapse).not.toHaveTextContent("Text open");
    expect(switchCollapse).toHaveTextContent("Text close");

    fireEvent.click(switchCollapseWrapper);

    expect(switchCollapse).toHaveTextContent("Text open");
    expect(switchCollapse).not.toHaveTextContent("Text close");
});

it("opens and closes with onChange", () => {
    const onChange = jest.fn();

    render(
        <SwitchCollapse
            className="switch-collapse-class"
            text="Text open"
            textClosed="Text close"
            onChange={onChange}
        />
    );

    const switchCollapseWrapper = screen.getByRole("switchcollapse");
    const switchCollapse = switchCollapseWrapper.querySelector(
        ".switch-collapse-class"
    );

    expect(switchCollapse).not.toHaveTextContent("Text open");
    expect(switchCollapse).toHaveTextContent("Text close");
    expect(onChange).toHaveBeenCalledTimes(0);

    fireEvent.click(switchCollapseWrapper);

    expect(switchCollapse).toHaveTextContent("Text open");
    expect(switchCollapse).not.toHaveTextContent("Text close");
    expect(onChange).toHaveBeenCalledTimes(1);
});

it("handles altered result", () => {
    const openSwitch = () => {
        return true;
    };
    const closeSwitch = () => {
        return false;
    };

    const { container } = render(
        <SwitchCollapse
            className="switch-collapse-class"
            text="Text open"
            textClosed="Text close"
            onChange={openSwitch}
            Open
        />
    );

    const switchCollapseWrapper = screen.getByRole("switchcollapse");
    const switchCollapse = switchCollapseWrapper.querySelector(
        ".switch-collapse-class"
    );

    expect(switchCollapse).toHaveTextContent("Text open");
    expect(switchCollapse).not.toHaveTextContent("Text close");

    fireEvent.click(switchCollapseWrapper);

    expect(switchCollapse).toHaveTextContent("Text open");
    expect(switchCollapse).not.toHaveTextContent("Text close");

    render(
        <SwitchCollapse
            text="Text open"
            textClosed="Text close"
            onChange={closeSwitch}
        />,
        { container }
    );

    fireEvent.click(switchCollapseWrapper);

    expect(switchCollapse).not.toHaveTextContent("Text open");
    expect(switchCollapse).toHaveTextContent("Text close");
});
