import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'
import "jest-canvas-mock";

import { render, screen } from '@testing-library/react';

import Graph from '../components/Graph';

const dataY = [
    { label: 'data 1', labelColor: '', points: [200, 300, 400, 200, 100, 200, 100] },
    { label: 'data 2', labelColor: 'success', points: [300, 200, 400, 200, 500, 600, 100] }
];
const dataX = [1, 2, 3, 4, 5, 6, 7];

it('renders without crashing', () => {
    render(<Graph dataX={dataX} dataY={dataY} />);
    const graph = screen.getByRole("graph");
    expect(graph).toBeInTheDocument();
});

it('has a canvas in DOM and has filled the datasets', () => {
    render(<Graph dataX={dataX} dataY={dataY} />);
    const graph = screen.getByRole("graph");
    const canvasAll = graph.querySelectorAll("canvas")
    expect(canvasAll.length).toEqual(1)
    const ctx = canvasAll[0].getContext("2d");
    expect(ctx.createLinearGradient.mock.calls.length).toEqual(dataY.length)
});
