import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen } from '@testing-library/react';

import Card from '../components/Card';

import * as variables from "../common/variables";

it('has the correct props, content and colors', () => {
    const { WHITE, GREY_6 } = variables;
    render(<Card role="card-role" className="veryclass">Hello</Card>);
    const card = screen.getByRole('card-role');
    expect(card).toHaveTextContent('Hello');
    expect(card.getAttribute("loading")).toBeFalsy(); // Default prop
    expect(card).toHaveStyleRule("background-color", WHITE);
    expect(card).toHaveStyleRule("border", "1px solid " +  GREY_6);
    expect(card).toHaveClass("veryclass");
});

