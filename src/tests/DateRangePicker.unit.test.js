import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import DateRangePicker from '../components/DateRangePicker';
import { TODAY, YESTERDAY } from "../common/variables";
import {
    monthStart,
    getDateDayOff,
    getDateMonthOff,
    getCurrentWeekDate,
    getPreviousWeekDate,
    getPreviousMonth,
} from "../common/helpers";

const from = new Date("2021-01-13");
const to = new Date("2021-01-14")


it("renders without crashing", () => {
    render(<DateRangePicker
        from={from}
        to={to}
        dayPickerProps={{
            month: new Date(getPreviousMonth(from))
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');
    expect(datepicker).toBeInTheDocument();
});


it("has inputs in the DOM with correct values", () => {
    render(<DateRangePicker
        from={from}
        to={to}
        dayPickerProps={{
            month: new Date(getPreviousMonth(from))
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');
    const inputs = datepicker.querySelectorAll("input")
    expect(inputs.length).toEqual(2)

    const inputFrom = inputs[0];
    const inputTo = inputs[1];

    expect(inputFrom.getAttribute("value")).toEqual(from.toISOString().split("T")[0])
    expect(inputTo.getAttribute("value")).toEqual(to.toISOString().split("T")[0])
});



it("has functional modal display and cancel", () => {
    const onCancel = jest.fn();

    render(<DateRangePicker
        from={from}
        to={to}
        onCancel={onCancel}
        dayPickerProps={{
            month: new Date(getPreviousMonth(from))
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');

    var modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    expect(modal).not.toBeInTheDocument()

    const [inputFrom, inputTo] = datepicker.querySelectorAll("input")
    fireEvent.click(inputTo)
    modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    expect(modal).toBeInTheDocument()

    const numMonths = modal.querySelectorAll(".DayPicker-Month");
    expect(numMonths.length).toEqual(3)

    const numDays = modal.querySelectorAll("div.DayPicker-Day")
    expect(numDays.length).toBeGreaterThan(27 * 3)

    const cancelButton = modal.querySelector("div.button-cancel")
    expect(cancelButton).toBeInTheDocument()

    fireEvent.click(cancelButton)
    expect(onCancel).toHaveBeenCalledTimes(1)
    expect(modal).not.toBeInTheDocument()
});



it("has functional manual date changement and triggers onChange", () => {

    const onChange = jest.fn();

    render(<DateRangePicker
        from={from}
        to={to}
        onChange={onChange}
        dayPickerProps={{
            month: new Date(to)
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');

    // Test "From" change.

    var [inputFrom, inputTo] = datepicker.querySelectorAll("input")
    fireEvent.click(inputFrom)

    var modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    var month = modal.querySelectorAll(".DayPicker-Month")[0]
    var dayCells = month.querySelectorAll("div.DayPicker-Day")

    const originalFromValue = inputFrom.getAttribute("value");
    const originalToValue = inputTo.getAttribute("value");

    expect(originalFromValue).toEqual(from.toISOString().split("T")[0])
    expect(originalToValue).toEqual(to.toISOString().split("T")[0])

    const destFromDay = parseInt(dayCells[5].textContent);

    // This day should be activable with a single click, but
    // for some reason, it needs to be clicked twice on tests.
    fireEvent.click(dayCells[5])
    fireEvent.click(dayCells[5])

    const newFromValue = inputFrom.getAttribute("value");
    expect(newFromValue).not.toEqual(originalFromValue);
    expect(parseInt(newFromValue.split("-")[2])).toEqual(destFromDay)

    // Test "To" change.

    // This has to be done separately, because refocus does not work alongside tests,
    // so "Apply" needs to be clicked and modal re opened.

    var applyButton = datepicker.querySelectorAll(".button-group > div")[1];
    fireEvent.click(applyButton)

    inputTo = datepicker.querySelectorAll("input")[1]
    fireEvent.click(inputTo)
    modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    month = modal.querySelectorAll(".DayPicker-Month")[0]
    dayCells = month.querySelectorAll("div.DayPicker-Day")

    const destToDay = parseInt(dayCells[6].textContent);
    fireEvent.click(dayCells[6])

    const newToValue = inputTo.getAttribute("value");
    expect(newToValue).not.toEqual(originalToValue)
    expect(parseInt(newToValue.split("-")[2])).toEqual(destToDay)

    applyButton = datepicker.querySelectorAll(".button-group > div")[1];
    fireEvent.click(applyButton)

    expect(onChange).toHaveBeenCalledTimes(2)
});


it("has functional custom period changement and triggers handleCustomPeriodClick", () => {

    const handleCustomPeriodClick = jest.fn();

    render(<DateRangePicker
        from={new Date(YESTERDAY)}
        to={new Date(TODAY)}
        handleCustomPeriodClick={handleCustomPeriodClick}
        dayPickerProps={{
            month: new Date(YESTERDAY)
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');

    const [inputFrom, inputTo] = datepicker.querySelectorAll("input")
    fireEvent.click(inputFrom)

    const modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    const customPeriods = modal.querySelectorAll(".DayPickerInput-Overlay > div > ul > li")
    expect(customPeriods.length).toEqual(8)

    customPeriods.forEach(period => {
        fireEvent.click(period)

        let fromValue = inputFrom.getAttribute("value")
        var toValue = inputTo.getAttribute("value")

        switch (period.textContent) {
            case "Today":
                expect(fromValue).toEqual(TODAY)
                expect(toValue).toEqual(TODAY)
                break;
            case "Yesterday":
                expect(fromValue).toEqual(YESTERDAY)
                expect(toValue).toEqual(YESTERDAY)
                break;
            case "Last 7 days":
                expect(fromValue).toEqual(getDateDayOff(YESTERDAY, 6))
                expect(toValue).toEqual(YESTERDAY)
                break;
            case "This week":
                expect(fromValue).toEqual(getCurrentWeekDate(TODAY).start)
                expect(toValue).toEqual(getCurrentWeekDate(TODAY).end)
                break;
            case "Last week":
                expect(fromValue).toEqual(getPreviousWeekDate(TODAY).start)
                expect(toValue).toEqual(getPreviousWeekDate(TODAY).end)
                break;
            case "Last 30 days":
                expect(fromValue).toEqual(getDateDayOff(TODAY, 29))
                expect(toValue).toEqual(TODAY)
                break;
            case "This month":
                expect(fromValue).toEqual(monthStart(TODAY))
                expect(toValue).toEqual(TODAY)
                break;
            case "Last month":
                expect(fromValue).toEqual(getDateMonthOff(TODAY, 1).start)
                expect(toValue).toEqual(getDateMonthOff(TODAY, 1).end)
                break;
        }
    });
});


it("Has applied styled components styles", () => {
    render(<DateRangePicker
        from={from}
        to={to}
        dayPickerProps={{
            month: new Date(getPreviousMonth(from))
        }}
    />);
    const datepicker = screen.getByRole('daterangepicker');
    expect(datepicker).toHaveStyleRule("width", "77px", {
        modifier: ".DayPickerInput"
    })
});
