import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import SecondaryTable from "../components/SecondaryTable";

it("has the correct props and content", () => {
    render(
        <SecondaryTable
            columns={[
                {
                    Header: "Column 1",
                    accessor: "col1",
                },
                {
                    Header: "Column 2",
                    accessor: "col2",
                },
            ]}
            data={[
                {
                    col1: "Id 1",
                    col2: "fdfdf",
                    id: 1,
                },
                {
                    col1: "Id 2",
                    col2: "efvd",
                    id: 2,
                },
            ]}
        />
    );

    const table = screen.getByRole("table");
    const firstColumnLabel = table.querySelector("thead th:first-child");
    const secondColumnLabel = table.querySelector("thead th:last-child");

    const firstRow = table.querySelector("tbody tr:first-child");
    const secondRow = table.querySelector("tbody tr:last-child");

    expect(firstColumnLabel).toHaveTextContent("Column 1");
    expect(secondColumnLabel).toHaveTextContent("Column 2");
    expect(firstRow).toHaveTextContent("Id 1fdfdf");
    expect(secondRow).toHaveTextContent("Id 2efvd");
});

it("sorts columns", () => {
    render(
        <SecondaryTable
            columns={[
                {
                    Header: "Column 1",
                    accessor: "col1",
                },
                {
                    Header: "Column 2",
                    accessor: "col2",
                },
            ]}
            data={[
                {
                    col1: "Id 1",
                    col2: "fdfdf",
                    id: 1,
                },
                {
                    col1: "Id 2",
                    col2: "efvd",
                    id: 2,
                },
            ]}
            defaultSort={[{ id: "col1", desc: true }]}
        />
    );

    const table = screen.getByRole("table");
    const firstColumnLabel = table.querySelector("thead th:first-child");
    let firstRow = table.querySelector("tbody tr:first-child");
    let secondRow = table.querySelector("tbody tr:last-child");
    const chevrons = firstColumnLabel.querySelector(".chevron");

    expect(firstRow).toHaveTextContent("Id 2efvd");
    expect(secondRow).toHaveTextContent("Id 1fdfdf");
    expect(chevrons).toHaveClass("desc");

    fireEvent.click(firstColumnLabel);

    firstRow = table.querySelector("tbody tr:first-child");
    secondRow = table.querySelector("tbody tr:last-child");

    expect(firstRow).toHaveTextContent("Id 1fdfdf");
    expect(secondRow).toHaveTextContent("Id 2efvd");
    expect(chevrons).toHaveClass("asc");
});

it("renders subrow component", () => {
    const renderRowSubComponent = (row) => {
        return additionalProps.map(
            (el) =>
                el.id === row.original.id && [
                    <td className="country" key={"country" + el.id}>
                        Country:&nbsp;{el.country}
                    </td>,
                    <td key={"empty" + el.id}></td>,
                ]
        );
    };

    const additionalProps = [
        {
            id: 1,
            country: "FR",
        },
    ];

    render(
        <SecondaryTable
            columns={[
                {
                    Header: "Column 1",
                    accessor: "col1",
                },
                {
                    Header: "Column 2",
                    accessor: "col2",
                },
                {
                    Header: () => null,
                    id: "expander",
                    Cell: ({ row }) => (
                        <span {...row.getToggleRowExpandedProps()}>
                            {row.isExpanded ? (
                                <div className="down"></div>
                            ) : (
                                <div className="up"></div>
                            )}
                        </span>
                    ),
                },
            ]}
            data={[
                {
                    col1: "Id 1",
                    col2: "fdfdf",
                    id: 1,
                },
                {
                    col1: "Id 2",
                    col2: "efvd",
                    id: 2,
                },
            ]}
            renderRowSubComponent={renderRowSubComponent}
        />
    );

    const table = screen.getByRole("table");

    const firstRow = table.querySelector("tbody tr:first-child");
    const expandButton = firstRow.querySelector(".up");

    expect(expandButton).toBeInTheDocument();
    expect(table).not.toHaveTextContent("Country: FR");

    fireEvent.click(expandButton);

    expect(expandButton).not.toHaveClass("up");
    expect(expandButton).toHaveClass("down");
    expect(table).toHaveTextContent("Country: FR");
});
