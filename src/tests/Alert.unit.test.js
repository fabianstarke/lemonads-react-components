import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "jest-canvas-mock";

import { render, screen, fireEvent } from "@testing-library/react";

import Alert from "../components/Alert";
import * as variables from "../common/variables";

it("renders correctly without timeout", () => {
    render(<Alert id="alert" message="Test Alert" />);

    const alert = screen.getByRole("alert-fixed");

    expect(alert).toHaveTextContent("Test Alert");
    expect(alert).toHaveAttribute("type", "info");
    expect(alert).toHaveAttribute("id", "alert");
});

it("renders correctly with timeout", () => {
    render(
        <Alert
            id="alert"
            message="Test Alert"
            timeout={5}
            containerClassName="container-class"
        />
    );

    const alert = screen.getByRole("alert-animated");

    expect(alert).toHaveTextContent("Test Alert");
    expect(alert).toHaveAttribute("type", "info");
    expect(alert).toHaveAttribute("id", "alert");
    expect(alert).toHaveClass("container-class");
});

it("triggers correctly remove", () => {
    const remove = jest.fn();
    render(<Alert remove={remove} />);
    const closeButton = screen.getByRole("alert-close");
    fireEvent.click(closeButton);
    expect(remove).toHaveBeenCalledTimes(1);
});

it("has the correct background", () => {
    const { INFO, SUCCESS, WARNING, ERROR } = variables;

    const { container } = render(<Alert type="success" />);

    const alert = screen.getByRole("alert-fixed");
    expect(alert).toHaveStyleRule("background", SUCCESS);

    render(<Alert type="warning" />, { container });
    expect(alert).toHaveStyleRule("background", WARNING);

    render(<Alert type="error" />, { container });
    expect(alert).toHaveStyleRule("background", ERROR);

    render(<Alert type="info" />, { container });
    expect(alert).toHaveStyleRule("background", INFO);
});
