import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen } from "@testing-library/react";

import Status from "../components/Status";
import * as variables from "../common/variables";

it("has the correct props and content", () => {
    render(
        <Status role="status" className="status-class" label="Status label" />
    );

    const status = screen.getByRole("status");
    const statusDot = screen.getByRole("status-dot");

    expect(status).toHaveTextContent("Status label");
    expect(statusDot).toHaveClass("status-class");
});

it("has the color depending on type", () => {
    const { INFO, SUCCESS, WARNING, ERROR } = variables;

    const { container } = render(<Status />);
    const statusDot = screen.getByRole("status-dot");
    expect(statusDot).toHaveStyleRule("background", INFO);

    render(<Status type="success" />, { container });
    expect(statusDot).toHaveStyleRule("background", SUCCESS);

    render(<Status type="warning" />, { container });
    expect(statusDot).toHaveStyleRule("background", WARNING);

    render(<Status type="error" />, { container });
    expect(statusDot).toHaveStyleRule("background", ERROR);

    render(<Status type="info" />, { container });
    expect(statusDot).toHaveStyleRule("background", INFO);
});
