import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import IconButton from "../components/IconButton";

it("has the correct props and content", () => {
    render(
        <IconButton
            role="icon-button"
            className="button-class"
            type="download"
        />
    );
    const button = screen.getByRole("icon-button");
    expect(button).toHaveClass("button-class");
});

it("triggers correctly onClick", () => {
    const onClick = jest.fn();
    render(<IconButton role="icon-button" type="download" onClick={onClick} />);
    const button = screen.getByRole("icon-button");
    fireEvent.click(button);
    expect(onClick).toHaveBeenCalledTimes(1);
});
