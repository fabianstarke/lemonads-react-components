import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen } from '@testing-library/react';

import Loading from '../components/Loading';


it('renders without crashing', () => {
    render(<Loading/>);
    const loading = screen.getByRole('loading');
    expect(loading).toBeInTheDocument();
});


it('does not display the content if not loaded', () => {
    render(<Loading>Caffeine</Loading>);
    const loading = screen.getByRole('loading');
    expect(loading.querySelector(".overflow > svg")).toBeInTheDocument();
});


it('displays the content if loaded', () => {
    render(<Loading loading={false}>Caffeine</Loading>);
    const loading = screen.getByRole('loading');
    expect(loading).toHaveTextContent("Caffeine");
    expect(loading.querySelector(".overflow > svg")).not.toBeInTheDocument();
});


it('has styling applied', () => {
    render(<Loading/>);
    const loading = screen.getByRole('loading');
    expect(loading).toHaveStyleRule("flex", "1")
});
