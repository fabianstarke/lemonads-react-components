import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import Modal from "../components/Modal";

it("has the correct props and content", () => {
    render(
        <Modal
            role="modal"
            header="title modal"
            className="modal-class"
            defaultToggle
        />
    );
    const modal = screen.getByRole("modal");

    expect(modal).toHaveTextContent("title modal");
    expect(modal).toHaveClass("modal-class");
});

it("opens correctly", () => {
    const propagateToggle = jest.fn();

    let activableComponent = <button role="activable-component"></button>;

    render(
        <Modal
            role="modal"
            header="title modal"
            propagateToggle={propagateToggle}
            activableComponent={activableComponent}
        />
    );

    const modal = screen.getByRole("modal");
    const button = screen.getByRole("activable-component");
    let popup = modal.querySelector(".Popup");

    expect(popup).not.toBeInTheDocument();

    fireEvent.click(button);

    popup = modal.querySelector(".Popup");

    expect(propagateToggle).toHaveBeenCalledTimes(1);
    expect(popup).toBeInTheDocument();
});

it("propagates click correctly", () => {
    const propagateToggle = jest.fn();

    render(
        <Modal
            role="modal"
            header="title modal"
            propagateToggle={propagateToggle}
            defaultToggle
        />
    );

    const modal = screen.getByRole("modal");
    const popup = modal.querySelector(".Popup");
    const popupContainer = modal.querySelector(".popup-container");

    expect(popup).toBeInTheDocument();

    fireEvent.click(popupContainer);

    expect(propagateToggle).toHaveBeenCalledTimes(0);
    expect(popup).toBeInTheDocument();

    fireEvent.mouseDown(popup);
    fireEvent.click(popup);

    expect(propagateToggle).toHaveBeenCalledTimes(1);
    expect(popup).not.toBeInTheDocument();
});

it("closes correctly with cross", () => {
    const propagateToggle = jest.fn();

    render(
        <Modal role="modal" propagateToggle={propagateToggle} defaultToggle />
    );

    const modal = screen.getByRole("modal");
    const cross = modal.querySelector(".popup-close");
    const popup = modal.querySelector(".Popup");

    expect(popup).toBeInTheDocument();

    fireEvent.click(cross);

    expect(propagateToggle).toHaveBeenCalledTimes(1);
    expect(popup).not.toBeInTheDocument();
});

it("closes correctly with custom close className", () => {
    const propagateToggle = jest.fn();

    render(
        <Modal
            role="modal"
            propagateToggle={propagateToggle}
            defaultToggle
            customCloseClassName="custom-close"
        >
            <button role="close-btn" className="custom-close"></button>
        </Modal>
    );

    const closeButton = screen.getByRole("close-btn");

    expect(closeButton).toBeInTheDocument();

    fireEvent.click(closeButton);

    expect(propagateToggle).toHaveBeenCalledTimes(1);
    expect(closeButton).not.toBeInTheDocument();
});

it("has the correct size", () => {
    render(<Modal role="modal-small" defaultToggle />);
    render(<Modal role="modal-medium" size="medium" defaultToggle />);

    const smallModal = screen.getByRole("modal-small");
    const mediumModal = screen.getByRole("modal-medium");

    expect(smallModal).toHaveStyleRule("width", "494px", {
        modifier: ".Popup .popup-container .wrapper",
    });
    expect(mediumModal).toHaveStyleRule("width", "726px", {
        modifier: ".Popup .popup-container .wrapper",
    });
});

it("shows the cross correctly", () => {
    render(<Modal role="modal-hidden-cross" defaultToggle hiddenCross />);
    render(<Modal role="modal-default" defaultToggle />);

    const hiddenCrossModal = screen.getByRole("modal-hidden-cross");
    const defaultModal = screen.getByRole("modal-default");

    const hiddenCross = hiddenCrossModal.querySelector(".popup-close");
    const visibleCross = defaultModal.querySelector(".popup-close");

    expect(hiddenCross).not.toBeInTheDocument();
    expect(visibleCross).toBeInTheDocument();
});
