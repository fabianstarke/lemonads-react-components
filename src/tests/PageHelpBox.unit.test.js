import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import PageHelpBox from '../components/PageHelpBox';

import illustrations from "../images/illustrations";

it('renders without crashing', () => {
    render(<PageHelpBox
        title="All Steps completed"
        icon={illustrations.Loading}
        description="Congrats, you completed every step of the onboarding and started recording conversions. It's time to dig into the data!"
        buttonText="Check conversions"
        buttonLink={null}
    />);
    const pageHelpBox = screen.getByRole("pagehelpbox");
    expect(pageHelpBox).toBeInTheDocument();
});

it('has a functional button', () => {
    const onClick = jest.fn();
    render(<PageHelpBox
        title="All Steps completed"
        icon={illustrations.Loading}
        description="Congrats, you completed every step of the onboarding and started recording conversions. It's time to dig into the data!"
        buttonText="Check conversions"
        buttonLink={null}
        onClick={onClick}
    />);
    const pageHelpBox = screen.getByRole("pagehelpbox");
    const btn = pageHelpBox.querySelector(".check-btn > div");
    expect(btn).toBeInTheDocument()
    fireEvent.click(btn)
    expect(onClick).toHaveBeenCalledTimes(1)
    
});

it('has correct composition and content', () => {
    const titleText = "All Steps completed";
    const descriptionText = "Congrats, you completed every step of the onboarding and started recording conversions. It's time to dig into the data!"
    const buttonText = "Check conversions";
    render(<PageHelpBox
        title={titleText}
        icon={illustrations.Loading}
        description={descriptionText}
        buttonText={buttonText}
        buttonLink={null}
    />);
    const pageHelpBox = screen.getByRole("pagehelpbox");

    const btn = pageHelpBox.querySelector(".check-btn > div");
    expect(btn).toBeInTheDocument();
    expect(btn.getAttribute("type")).toEqual("primary");
    expect(btn).toHaveTextContent(buttonText);

    const title = pageHelpBox.querySelector(".title");
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent(titleText)

    const desc = pageHelpBox.querySelector(".desc");
    expect(desc).toBeInTheDocument();
    expect(desc).toHaveTextContent(descriptionText);

    expect(pageHelpBox.querySelector("svg")).toBeInTheDocument();
});

