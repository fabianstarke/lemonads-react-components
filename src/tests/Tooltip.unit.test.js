import React from "react";
import ReactDOM from "react-dom";
import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import Tooltip from "../components/Tooltip";

describe("Tooltip component tests", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Tooltip />, div);
        render(<Tooltip />);
    });

    it("renders correct props and styling", () => {
        render(
            <Tooltip
                className="test-classname"
                placement="bottom"
                overlay="Test Tooltip"
            >
                <span>Pass hover to show tooltip</span>
            </Tooltip>
        );
        const tooltip = screen.getByText("Pass hover to show tooltip");
        expect(tooltip.getAttribute("class")).toEqual("test-classname");

        fireEvent.mouseOver(tooltip);
        
        const overlay = screen.getByRole("tooltip");
        expect(overlay.getAttribute("class")).toEqual("rc-tooltip-inner");
        expect(overlay).toHaveTextContent("Test Tooltip");
    });
});
