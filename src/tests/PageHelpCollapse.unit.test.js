import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import PageHelpCollapse from '../components/PageHelpCollapse';

it('renders without crashing, has a switchcollapse object and correct composition', () => {
    const onChange = jest.fn();
    render(<PageHelpCollapse onChange={onChange} />);

    const pageHelpCollapse = screen.getByRole("switchcollapse");
    expect(pageHelpCollapse).toBeInTheDocument();

    const browserIdeaSvgTitle = pageHelpCollapse.querySelector("svg title");
    expect(browserIdeaSvgTitle).toBeInTheDocument();
    expect(browserIdeaSvgTitle).toHaveTextContent("browser-idea")

    expect(pageHelpCollapse).toHaveTextContent("Need help ?");

    fireEvent.click(pageHelpCollapse)

    expect(pageHelpCollapse).not.toHaveTextContent("Need help ?");
});


