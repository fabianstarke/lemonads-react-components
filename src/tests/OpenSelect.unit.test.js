import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import { OpenSelect } from '../components/OpenSelect';

const testingProps = {
    options: [
        { label: 'first option', value: 0, icon: "test" },
        { label: 'second option', value: 1 },
        { label: 'third option', value: 2 }
    ],
    selected: 2
}

it('renders without crashing', () => {
    render(<OpenSelect {...testingProps} />);
    const select = screen.getByRole('select');
    expect(select).toBeInTheDocument();
});


it('has correct options and correct selected option', () => {
    render(<OpenSelect {...testingProps} />);
    const select = screen.getByRole('select');
    const options = select.querySelectorAll(':scope > div');
    expect(options.length).toEqual(testingProps.options.length)
    expect(options[testingProps.selected]).toHaveClass("selected")
    for (var i = 0; i < options.length; i++) {
        expect(options[i].textContent).toEqual(testingProps.options[i].label)
    }
});


it('changes value corretly', () => {
    var callbackValue = null;

    render(<OpenSelect {...testingProps} onChange={(value) => { callbackValue = value }} />);
    const select = screen.getByRole('select');
    fireEvent.click(select)
    fireEvent.click(select.querySelectorAll(":scope > div")[1]);

    expect(callbackValue).toEqual(testingProps.options[1].value)
});


it('has applied styling', () => {
    render(<OpenSelect {...testingProps} />);
    const select = screen.getByRole('select');
    expect(select).toHaveStyleRule("display", "inline-block");
});

