import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "jest-canvas-mock";
import { render, screen, fireEvent } from "@testing-library/react";

import CheckBox from "../components/CheckBox";

it("has the correct props and content", () => {
    render(
        <CheckBox
            label="TestCheckBox"
            name="checkbox"
            value="value"
            id="checkbox"
            className="class-checkbox"
            onChange={() => {}}
            labelRight="Label Right"
        />
    );
    const checkbox = screen.getByRole("checkbox");
    const inputByLabel = screen.getByLabelText("TestCheckBox");
    const inputByLabelRight = screen.getByLabelText("Label Right");

    expect(checkbox).not.toBeChecked();
    expect(checkbox.getAttribute("checked")).toBeFalsy();
    expect(checkbox).toHaveClass("class-checkbox");
    expect(checkbox).toHaveAttribute("name", "checkbox");
    expect(checkbox).toHaveAttribute("id", "checkbox");
    expect(inputByLabel).toBeInTheDocument();
    expect(inputByLabelRight).toBeInTheDocument();
});

it("triggers correctly on click", () => {
    const onChange = jest.fn();

    render(<CheckBox onChange={onChange} />);

    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).toHaveClass("visible");
    expect(checkbox).not.toHaveClass("hidden");

    fireEvent.click(checkbox);

    expect(checkbox).toHaveClass("hidden");
    expect(checkbox).not.toHaveClass("visible");
    expect(onChange).toHaveBeenCalledTimes(1);
});

it("handles checked prop correctly", () => {
    const { container } = render(
        <CheckBox onChange={() => {}} />
    );

    const checkbox = screen.getByRole("checkbox");

    expect(checkbox).not.toHaveClass("checked");
    expect(checkbox).not.toBeChecked();

    render(<CheckBox checked={true} />, { container });

    expect(checkbox).toBeChecked();
    expect(checkbox).toHaveClass("checked");
});
