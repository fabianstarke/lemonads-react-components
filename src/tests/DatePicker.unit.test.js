import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen, fireEvent } from '@testing-library/react';

import DatePicker from '../components/DatePicker';
import { TODAY } from "../common/variables";

const today = new Date(TODAY)


it("renders without crashing", () => {
    render(<DatePicker dateFormat="YYYY-MM-DD"
        date={today}
    />);
    const datepicker = screen.getByRole('datepicker');
    expect(datepicker).toBeInTheDocument();
});


it("has an input in the DOM with a correct value", () => {
    render(<DatePicker 
        dateFormat="YYYY-MM-DD"
        maxDate={new Date()}
        date={today}
        dayPickerProps={{
            disabledDays: {
                after: new Date()
            },
        }}
    />);
    const datepicker = screen.getByRole('datepicker');
    const input = datepicker.querySelector("input")
    expect(input).toBeInTheDocument();
    expect(input.getAttribute("value")).toEqual(today.toISOString().slice(0, 10))
});


it("Day selection modal appears on click", () => {
    render(<DatePicker 
        dateFormat="YYYY-MM-DD"
        maxDate={new Date()}
        date={today}
        dayPickerProps={{
            disabledDays: {
                after: new Date()
            },
        }}
    />);

    const datepicker = screen.getByRole('datepicker');
    var modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    expect(modal).not.toBeInTheDocument()

    const input = datepicker.querySelector("input")
    fireEvent.click(input)
    modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")
    expect(modal).toBeInTheDocument()
});


it("Date selection changes input value and triggers callbacks", () => {

    const onChange = jest.fn();

    render(<DatePicker dateFormat="YYYY-MM-DD"
        onChange={onChange}
        maxDate={new Date()}
        date={today}
        dayPickerProps={{
            disabledDays: {
                after: new Date()
            },
        }}
    />);

    const datepicker = screen.getByRole('datepicker');
    const input = datepicker.querySelector("input")
    fireEvent.click(input)
    var modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")

    // Click any other day and test the input value
    let dayCells = modal.querySelectorAll("div.DayPicker-Day")

    expect(dayCells.length).toBeGreaterThan(27)

    let someOtherDay = dayCells[0].textContent;
    fireEvent.click(dayCells[0])

    expect(onChange).toHaveBeenCalledTimes(1);

    const inputValue = input.getAttribute("value")
    expect(inputValue).not.toEqual(today.toISOString().slice(0, 10))

    const inputDayValue = parseInt(inputValue.split("-")[2])
    expect(inputDayValue).toEqual(parseInt(someOtherDay))

});



it("Have month display and changes month correctly", () => {

    render(<DatePicker 
        dateFormat="YYYY-MM-DD"
        maxDate={new Date()}
        date={today}
        dayPickerProps={{
            disabledDays: {
                after: new Date()
            },
        }}
    />);

    const datepicker = screen.getByRole('datepicker');
    const input = datepicker.querySelector("input")
    fireEvent.click(input)
    const modal = datepicker.querySelector("div.DayPickerInput-OverlayWrapper")

    const nextMonthBtn = modal.querySelector(".DayPicker-NavButton--next")
    expect(nextMonthBtn).toBeInTheDocument()

    const monthTextDOM = modal.querySelector(".DayPicker-Caption")
    expect(monthTextDOM).toBeInTheDocument()

    const monthText = monthTextDOM.textContent;
    expect(monthText.length).toBeGreaterThan(5)
    
    fireEvent.click(nextMonthBtn)
    const nextMonthText = modal.querySelector(".DayPicker-Caption").textContent;

    expect(monthText).not.toEqual(nextMonthText)
});


it("Has applied styled components styles", () => {
    render(<DatePicker dateFormat="YYYY-MM-DD"
        date={today}
    />);
    const datepicker = screen.getByRole('datepicker');
    expect(datepicker).toHaveStyleRule("width", "110px", {
        modifier: ".DayPickerInput"
    })
});
