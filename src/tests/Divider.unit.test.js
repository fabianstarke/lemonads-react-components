import React from 'react';

import "@testing-library/react"
import '@testing-library/jest-dom/extend-expect';
import 'jest-styled-components'

import { render, screen } from '@testing-library/react';

import Divider from '../components/Divider';

import { GREY_2 } from "../common/variables";

it('renders without crashing', () => {
    render(<Divider />);
    const divider = screen.getByRole("divider");
    expect(divider).toBeInTheDocument();
});


it('has the correct default props and style', () => {
    render(<Divider />);
    const divider = screen.getByRole("divider");
    expect(divider).toHaveClass("divider")
    expect(divider).toHaveStyleRule("margin-top", "30px");
    expect(divider).toHaveStyleRule("margin-bottom", "30px");
    expect(divider).toHaveStyleRule("border-top", "1px solid " + GREY_2);
});


it('has functional props', () => {
    render(<Divider marginTop={"20px"} marginBottom={"40px"} />);
    const divider = screen.getByRole("divider");
    expect(divider).toHaveStyleRule("margin-top", "20px");
    expect(divider).toHaveStyleRule("margin-bottom", "40px");
});


