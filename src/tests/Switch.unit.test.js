import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "jest-canvas-mock";

import { render, screen, fireEvent } from "@testing-library/react";

import Switch from "../components/Switch";

it("has the correct props and content", () => {
    render(
        <Switch role="switch" className="switch-class" label="Test switch" />
    );

    const switchWrapper = screen.getByRole("switch");

    expect(switchWrapper).toHaveTextContent("Test switch");
    expect(switchWrapper).toHaveClass("switch-class");
});

it("triggers onChange", () => {
    const onChange = jest.fn();

    render(<Switch role="switch" onChange={onChange} />);

    const switchWrapper = screen.getByRole("switch");
    const switchComponent = switchWrapper.querySelector("div[type='button']");

    fireEvent.click(switchComponent);

    expect(onChange).toHaveBeenCalledTimes(1);
});
