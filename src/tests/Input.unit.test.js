import React from "react";
import ReactDOM from "react-dom";
import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";
import * as variables from "../common/variables";

const { SUCCESS, ERROR, PRIMARY_BASE } = variables;

import Input from "../components/Input";

describe("Input component tests", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");
        ReactDOM.render(<Input />, div);
        render(<Input />);
    });

    it("renders default props correctly", () => {
        render(<Input role="default-input" />);
        const input = screen.getByRole("default-input");
        expect(input.getAttribute("type")).toEqual("text");
        expect(input.getAttribute("name")).toEqual("input");
        expect(input.getAttribute("required")).toBeFalsy;
        expect(input.getAttribute("disabled")).toBeFalsy;
        expect(input.getAttribute("focus")).toBeFalsy;
        expect(input.getAttribute("full")).toBeFalsy;
        expect(input.getAttribute("borderColor")).toBeNull;
    });

    it("renders additional props correctly", () => {
        render(
            <Input
                role="input"
                label="My Label"
                placeholder="Placeholder"
                mode="success"
                className="className"
                htmlFor="myid"
                ariaLabel="label"
                id="myid"
            />
        );
        
        const label = screen.getByTestId("input-label");
        expect(label).toHaveTextContent("My Label");

        const input = screen.getByRole("input");
        expect(input.getAttribute("placeholder")).toEqual("Placeholder");
        expect(input.getAttribute("mode")).toEqual("success");
        expect(input.getAttribute("id")).toEqual("myid");
        expect(input.getAttribute("disabled")).toBeFalsy;
    });

    it("renders correct styling", () => {
        render(<Input role="defaultInput" />);
        const defaultInput = screen.getByRole("defaultInput");
        expect(defaultInput).toHaveStyleRule("border-color", "#CBCBCB");

        render(<Input role="success" mode="success" />);
        const success = screen.getByRole("success");
        expect(success).toHaveStyleRule("border-color", SUCCESS);

        render(<Input role="error" mode="error" />);
        const error = screen.getByRole("error");
        expect(error).toHaveStyleRule("border-color", ERROR);

        render(<Input role="disabled" disabled />);
        const disabled = screen.getByRole("disabled");
        expect(disabled).toHaveStyleRule("border-color", "#EBEBEB");

        render(<Input role="colored" borderColor="red" />);
        const colored = screen.getByRole("colored");
        expect(colored).toHaveStyleRule("border-color", "red");

        render(<Input role="focus" focus />);
        const focus = screen.getByRole("focus");
        expect(focus).toHaveStyleRule("border", `1px solid ${PRIMARY_BASE}`);
    });

    it('triggers correctly onClick for icon', () => {
        const onClick = jest.fn();
        render(<Input role="input" onIconClick={onClick} />);
        const icon = screen.getByRole('icon-wrapper');
        expect(onClick).toHaveBeenCalledTimes(0);
        fireEvent.click(icon)
        expect(onClick).toHaveBeenCalledTimes(1);
    });
});
