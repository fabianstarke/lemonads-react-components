import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";
import "jest-canvas-mock";

import { render, screen, fireEvent } from "@testing-library/react";

import MultiLevelSelect from "../components/MultiLevelSelect";

it("has the correct props and content", () => {
    render(
        <MultiLevelSelect
            role="multi-level-select"
            options={[
                { label: "first option", value: 0 },
                {
                    label: "second option",
                    value: 1,
                    children: [
                        {
                            label: "first child",
                            value: 10,
                            children: [
                                { label: "first grandchild", value: 100 },
                                { label: "second grandchild", value: 101 },
                            ],
                        },
                        { label: "second child", value: 11 },
                    ],
                },
                {
                    label: "third option long name long long long",
                    value: 1,
                    children: [
                        { label: "first child", value: 20 },
                        { label: "second child", value: 21 },
                    ],
                },
                { label: "4th option", value: 3 },
                { label: "5th option", value: 4 },
                { label: "6th option", value: 5 },
            ]}
            menuIsOpen={true}
            label="label"
            controllerLabel="controller label"
            values={[0]}
        />
    );

    const multiLevelSelect = screen.getByRole("multi-level-select");
    const listWrapper = screen.getByRole("list-wrapper");
    const controller = screen.getByRole("controller");
    const targetCheckbox = listWrapper.querySelector("#checkbox-00");
    const secondCheckbox = listWrapper.querySelector("#checkbox-01");

    expect(multiLevelSelect).toHaveTextContent("label");
    expect(controller).toHaveTextContent("controller label");
    expect(listWrapper).toBeInTheDocument();
    expect(listWrapper).toHaveTextContent("first option");
    expect(listWrapper).not.toHaveTextContent("first child");
    expect(targetCheckbox).toBeChecked();
    expect(secondCheckbox).not.toBeChecked();
});

it("triggers handleCheckboxChange", () => {
    const onChange = jest.fn();

    render(
        <MultiLevelSelect
            role="multi-level-select"
            options={[
                { label: "first option", value: 0 },
                {
                    label: "second option",
                    value: 1,
                    children: [
                        {
                            label: "first child",
                            value: 10,
                            children: [
                                { label: "first grandchild", value: 100 },
                                { label: "second grandchild", value: 101 },
                            ],
                        },
                        { label: "second child", value: 11 },
                    ],
                },
                {
                    label: "third option long name long long long",
                    value: 1,
                    children: [
                        { label: "first child", value: 20 },
                        { label: "second child", value: 21 },
                    ],
                },
                { label: "4th option", value: 3 },
                { label: "5th option", value: 4 },
                { label: "6th option", value: 5 },
            ]}
            handleCheckboxChange={onChange}
            menuIsOpen={true}
        />
    );

    const listWrapper = screen.getByRole("list-wrapper");
    const targetCheckbox = listWrapper.querySelector("#checkbox-00");
    fireEvent.mouseOver(targetCheckbox);

    expect(onChange).toHaveBeenCalledTimes(0);
    fireEvent.click(targetCheckbox);

    expect(onChange).toHaveBeenCalledTimes(1);
});

it("triggers onOpen and onClose", () => {
    const onOpen = jest.fn();
    const onClose = jest.fn();

    render(
        <MultiLevelSelect
            role="multi-level-select"
            options={[
                { label: "first option", value: 0 },
                {
                    label: "second option",
                    value: 1,
                    children: [
                        {
                            label: "first child",
                            value: 10,
                            children: [
                                { label: "first grandchild", value: 100 },
                                { label: "second grandchild", value: 101 },
                            ],
                        },
                        { label: "second child", value: 11 },
                    ],
                },
                {
                    label: "third option long name long long long",
                    value: 1,
                    children: [
                        { label: "first child", value: 20 },
                        { label: "second child", value: 21 },
                    ],
                },
                { label: "4th option", value: 3 },
                { label: "5th option", value: 4 },
                { label: "6th option", value: 5 },
            ]}
            onOpen={onOpen}
            onClose={onClose}
        />
    );

    var controller = screen.getByRole("controller");

    expect(onOpen).toHaveBeenCalledTimes(0);
    expect(onClose).toHaveBeenCalledTimes(0);

    fireEvent.click(controller);

    var listWrapper = screen.getByRole("list-wrapper");

    expect(listWrapper).toBeInTheDocument();
    expect(onOpen).toHaveBeenCalledTimes(1);
    expect(onClose).toHaveBeenCalledTimes(0);

    fireEvent.click(controller);

    expect(listWrapper).not.toBeInTheDocument();
    expect(onOpen).toHaveBeenCalledTimes(1);
    expect(onClose).toHaveBeenCalledTimes(1);
});

it("renders subitems", () => {
    render(
        <MultiLevelSelect
            role="multi-level-select"
            options={[
                { label: "first option", value: 0 },
                {
                    label: "second option",
                    value: 1,
                    children: [
                        {
                            label: "first child",
                            value: 10,
                            children: [
                                { label: "first grandchild", value: 100 },
                                { label: "second grandchild", value: 101 },
                            ],
                        },
                        { label: "second child", value: 11 },
                    ],
                },
                {
                    label: "third option long name long long long",
                    value: 1,
                    children: [
                        { label: "first child", value: 20 },
                        { label: "second child", value: 21 },
                    ],
                },
                { label: "4th option", value: 3 },
                { label: "5th option", value: 4 },
                { label: "6th option", value: 5 },
            ]}
            menuIsOpen={true}
        />
    );

    const listWrapper = screen.getByRole("list-wrapper");
    const targetCheckbox = listWrapper.querySelector("#checkbox-01");
    const secondCheckbox = listWrapper.querySelector("#checkbox-00");

    expect(listWrapper).toBeInTheDocument();
    expect(listWrapper).not.toHaveTextContent("first child");

    fireEvent.mouseOver(targetCheckbox);

    expect(listWrapper).toHaveTextContent("first child");

    fireEvent.mouseOver(secondCheckbox);

    expect(listWrapper).not.toHaveTextContent("first child");
});
