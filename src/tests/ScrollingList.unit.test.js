import React from 'react';
import "@testing-library/react"

import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import 'jest-styled-components'

import ScrollingList from '../components/ScrollingList';

const slides = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(i => "Hello " + i);


it("renders and does not crash", () => {
    const div = document.createElement("div");
    render(<ScrollingList role="scrolling-list-role" width="600px">
        {slides.map((slide, i) => {
            return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>{slide}</div>);
        })}
    </ScrollingList>, div);
    const scrollingList = screen.getByRole('scrolling-list-role');
    expect(scrollingList).toBeInTheDocument();

    const verticalContainer = scrollingList.querySelector("div:first-child");
    expect(verticalContainer).toBeInTheDocument();

    const horizontalContainer = verticalContainer.querySelector("div:first-child");
    expect(horizontalContainer).toBeInTheDocument();
});


it("the correct props", () => {
    const testWidth = "600px";
    const div = document.createElement("div");
    render(<ScrollingList role="scrolling-list-role" width={testWidth}>
        {slides.map((slide, i) => {
            return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>{slide}</div>);
        })}
    </ScrollingList>, div);
    const scrollingList = screen.getByRole('scrolling-list-role');
    const verticalContainer = scrollingList.querySelector("div:first-child");
    const horizontalContainer = verticalContainer.querySelector("div:first-child");
    expect(verticalContainer.getAttribute("width")).toEqual(testWidth);
    expect(horizontalContainer.getAttribute("width")).toEqual(testWidth);
});


it('has correct content', () => {
    const div = document.createElement("div");
    render(<ScrollingList role="scrolling-list-role" width="600px">
        {slides.map((slide, i) => {
            return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>{slide}</div>);
        })}
    </ScrollingList>, div);
    const scrollingList = screen.getByRole('scrolling-list-role');
    slides.forEach(slide => {
        expect(scrollingList).toHaveTextContent(slide);
    })
    expect(scrollingList).not.toHaveTextContent("This content does not exist");
});


it('has correct styling', () => {
    const div = document.createElement("div");
    render(<ScrollingList role="scrolling-list-role" width="600px">
        {slides.map((slide, i) => {
            return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>{slide}</div>);
        })}
    </ScrollingList>, div);
    const scrollingList = screen.getByRole('scrolling-list-role');
    expect(scrollingList).toHaveStyleRule('display', "flex");
    expect(scrollingList).toHaveStyleRule('flex-direction', "column");
});


// Arrows aren't displayed because scroll box dimensions aren't the real ones.
// it('has functional scroll buttons', () => {
//     render(<ScrollingList role="scrolling-list-role" width="600px">
//         {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((el, i) => {
//             return (<div key={i} style={{ background: "white", width: "200px", padding: "100px" }}>
//                 Hello {el}
//             </div>);
//         })}
//     </ScrollingList>
//     );
//     const myScrollingList = screen.getByRole('scrolling-list-role');
//     const leftButton = screen.queryByRole('scroll-left');
//     const rightButton = screen.queryByRole('scroll-right');
//     // const leftButton = myScrollingList.querySelector('img.chevron.left');
//     // const rightButton = myScrollingList.querySelector('img.chevron.right');
//     expect(leftButton).not.toBeInTheDocument()
//     // expect(rightButton).toBeInTheDocument() // Doesn't find it -> to fix
// });
