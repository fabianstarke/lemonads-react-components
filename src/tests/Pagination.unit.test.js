import React from "react";

import "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import "jest-styled-components";

import { render, screen, fireEvent } from "@testing-library/react";

import Pagination from "../components/Pagination";

it("has the correct props and content", () => {
    render(<Pagination role="pagination" className="class" pages={5} />);
    const pagination = screen.getByRole("pagination");
    const pageSizeOptions = pagination.querySelector(".-pageSizeOptions");
    const paginationBar = pagination.querySelector(".paginationBar");

    expect(pagination).toHaveClass("class -pagination");
    expect(pageSizeOptions).toBeInTheDocument();
    expect(paginationBar).toBeInTheDocument();
    expect(pageSizeOptions).toHaveTextContent("rows");
});

it("changes page correctly", () => {
    const onPageChange = jest.fn();

    render(
        <Pagination role="pagination" onPageChange={onPageChange} pages={5} />
    );

    const pagination = screen.getByRole("pagination");
    const paginationBar = pagination.querySelector(".paginationBar");

    const activePage = paginationBar.querySelector(
        ".PaginationUL li:nth-child(2)"
    );
    const targetPage = paginationBar.querySelector(
        ".PaginationUL li:nth-child(4)"
    );

    expect(activePage).toHaveClass("active");
    expect(targetPage).not.toHaveClass("active");

    fireEvent.click(targetPage);

    expect(activePage).not.toHaveClass("active");
    expect(targetPage).toHaveClass("active");
    expect(onPageChange).toHaveBeenCalledTimes(1);
});

it("handles correctly next and previous", () => {
    const onPageChange = jest.fn();

    render(
        <Pagination role="pagination" onPageChange={onPageChange} pages={5} />
    );

    const pagination = screen.getByRole("pagination");
    const paginationBar = pagination.querySelector(".paginationBar");

    const previous = paginationBar.querySelector(
        ".PaginationUL li:nth-child(1)"
    );
    const activePage = paginationBar.querySelector(
        ".PaginationUL li:nth-child(2)"
    );
    const targetPage = paginationBar.querySelector(
        ".PaginationUL li:nth-child(3)"
    );
    const next = paginationBar.querySelector(".PaginationUL li:nth-child(7)");

    expect(activePage).toHaveClass("active");
    expect(targetPage).not.toHaveClass("active");

    fireEvent.click(next);

    expect(activePage).not.toHaveClass("active");
    expect(targetPage).toHaveClass("active");
    expect(onPageChange).toHaveBeenCalledTimes(1);

    fireEvent.click(previous);

    expect(activePage).toHaveClass("active");
    expect(targetPage).not.toHaveClass("active");
    expect(onPageChange).toHaveBeenCalledTimes(2);
});

it("hides paginaton bar if only one page", () => {
    render(<Pagination role="pagination" pages={1} />);

    const pagination = screen.getByRole("pagination");
    const paginationBar = pagination.querySelector(".paginationBar");

    expect(paginationBar).not.toBeInTheDocument();
});

it("handles page size change correctly", () => {
    const onPageSizeChange = jest.fn();
    render(
        <Pagination
            role="pagination"
            onPageSizeChange={onPageSizeChange}
            pages={5}
            pageSize={25}
            selectClassNamePrefix={"pagination-select"}
        />
    );
    const pagination = screen.getByRole("pagination");
    const pageSizeSelect = pagination.querySelector(".-pageSizeOptions");
    const control = pageSizeSelect.querySelector(".pagination-select__control");

    fireEvent.mouseDown(control);

    const targetValue = screen.getByText("50 rows");
    fireEvent.click(targetValue);

    expect(onPageSizeChange).toHaveBeenCalledTimes(1);
});

it("disable previous or next if first or last page", () => {
    const onPageChange = jest.fn();

    render(
        <Pagination role="pagination" onPageChange={onPageChange} pages={5} />
    );

    const pagination = screen.getByRole("pagination");
    const paginationBar = pagination.querySelector(".paginationBar");
    const previous = paginationBar.querySelector(
        ".PaginationUL li:nth-child(1)"
    );
    const lastPage = paginationBar.querySelector(
        ".PaginationUL li:nth-child(6)"
    );
    const next = paginationBar.querySelector(".PaginationUL li:nth-child(7)");

    expect(previous).toHaveClass("disable");
    expect(next).not.toHaveClass("disable");

    fireEvent.click(previous);

    expect(onPageChange).toHaveBeenCalledTimes(0);

    fireEvent.click(lastPage);

    expect(onPageChange).toHaveBeenCalledTimes(1);
    expect(next).toHaveClass("disable");
    expect(previous).not.toHaveClass("disable");

    fireEvent.click(next);

    expect(onPageChange).toHaveBeenCalledTimes(1);
});

it("doesn't show page size select if props is false", () => {
    render(
        <Pagination role="pagination" showPageSizeOptions={false} pages={5} />
    );

    const pagination = screen.getByRole("pagination");
    const pageSizeSelect = pagination.querySelector(".-pageSizeOptions");

    expect(pageSizeSelect).not.toBeInTheDocument();
});
