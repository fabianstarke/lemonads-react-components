  
const path = require('path');

module.exports = function ({ config }) {
  config.module.rules.push({
    // test: /\.stories\.jsx?$/,
    test: /\.js$|jsx/,
    include: path.resolve('./stories'),
    loaders: [require.resolve('@storybook/source-loader')],
    enforce: 'pre',
  });

  config.module.rules.push({
    test: /\.m?js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['@babel/preset-env','@babel/preset-react'],
        }
    }
  });

  return config;
};
