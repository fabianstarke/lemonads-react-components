import React from "react";
import Button from "../../src/components/Button";
import IconButton from "../../src/components/IconButton";
import LabelButton from "../../src/components/LabelButton";
import Input from "../../src/components/Input";

export default {
    title: "Button",
    component: Button,
};

export const All = () => {
    return (
        <div>
            <h1>Button Guideline</h1>
            <h2>Default Buttons</h2>
            <div style={{ display: "flex", justifyContent: "space-around", marginBottom:"20px" }}>
                <Button type="primary" label="Primary" />
                <Button type="primary-disabled" label="Primary disabled" />
                <Button type="secondary" label="Secondary" />
                <Button type="secondary-blue" label="Secondary Blue" />
                <Button type="secondary-disabled" label="Secondary disabled" />
            </div>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Button type="error" label="Error" />
                <Button type="warning" label="Warning" />
                <Button type="success" label="Success" />
            </div>
            <h2>Icon Buttons</h2>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <IconButton type="upload" />
                <IconButton type="download" />
                <IconButton type="add" />
                <IconButton type="delete" />
            </div>
        </div>
    );
};

export const Default = (args) => (
    <Button {...args}/>
);

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    label : "Button",
    type : "primary",
    className : "className",
    full : false,
    width : null,
}

Default.argTypes = {
    type: { control: { type: 'select', options: {
        Primary: "primary",
        PrimaryDisabled: "primary-disabled",
        Secondary: "secondary",
        SecondaryBlue: "secondary-blue",
        SecondaryDisabled: "secondary-disabled",
        Error: "error",
        Warning: "warning",
        Success: "success"
    } } },
    onClick: { action: 'clicked' },
    width: { control: { type: 'text' } },
};

export const Icon = (args) => (
    <IconButton {...args} />
);

Icon.parameters = {
    info: { inline: true }
};

Icon.args = {
    type : "add",
    className : "className",
}

Icon.argTypes = {
    type: { control: { type: 'select', options: { Add: "add", Delete: "delete", Upload: "upload", Download: "download" } } },
    onClick: { action: 'clicked' },
};

export const Label = (args) => (
    <div>
        <LabelButton htmlFor="inputTest" {...args} />
        <br />
        <Input id="inputTest" placeholder="Test Input" />
    </div>
);

Label.parameters = {
    info: { inline: true }
};

Label.args = {
    label : "Button",
    type : "primary",
    className : "className",
    full : false,
}

Label.argTypes = {
    type: { control: { type: 'select', options: {
        Primary: "primary",
        PrimaryDisabled: "primary-disabled",
        Secondary: "secondary",
        SecondaryBlue: "secondary-blue",
        SecondaryDisabled: "secondary-disabled",
        Error: "error",
        Warning: "warning",
        Success: "success"
    } } },
    onClick: { action: 'clicked' },
};
