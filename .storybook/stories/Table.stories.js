import React from "react";
import Table from "../../src/components/Table";
import SecondaryTable from "../../src/components/SecondaryTable";
import { ChevronDown, ChevronUp } from "../../src/images/icons/baseIcons";

export const All = () => {
    return (
        <div>
            <h1>Table Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div style={{ width: "45%" }}>
                    <h2>Default Table</h2>

                    <Table
                        headTitles={[
                            { sort: -1, title: "Label 1" },
                            { sort: 0, title: "Label 2" },
                            { sort: 0, title: "Label 3" },
                            ""
                        ]}
                        listItems={[
                            {
                                datas: ["Type", "Something", "2345", "Download"]
                            },
                            {
                                datas: [
                                    "AType",
                                    "ASomething",
                                    "12345",
                                    "Download"
                                ]
                            }
                        ]}
                    />
                </div>
                <div style={{ width: "45%" }}>
                    <h2>Secondary Table</h2>
                    <SecondaryTable
                        columns={[
                            {
                                Header: "column 1",
                                accessor: "col1",
                                style: {
                                    width: "150px"
                                }
                            },
                            {
                                Header: "Column 2",
                                accessor: "col2"
                            },
                            {
                                Header: "Column 3",
                                accessor: "col3"
                            }
                        ]}
                        data={[
                            {
                                col1: "Hello",
                                col2: "World",
                                col3: "sample"
                            },
                            {
                                col1: "Hello",
                                col2: "World",
                                col3: "sample"
                            }
                        ]}
                    />
                </div>
            </div>
        </div>
    );
};

export default {
    title: "Table",
    component: Table,
};

export const Default = (args) => {
    return (
        <Table {...args} />
    );
};


Default.parameters = {
    info: { inline: true }
};

Default.args = {
    headTitles: [
        { sort: -1, title: "Label 1" },
        { sort: 0, title: "Label 2" },
        { sort: 0, title: "Label 3" },
        ""
    ],
    listItems: [
        { datas: ["Type", "Something", "2345", "Download"] },
        { datas: ["AType", "ASomething", "12345", "Download"] }
    ],
    align: ["left", "left", "left", ""],
    specialMargin: false,
    elementPerPage: 1,
    clickToLoad: true,
    sortable: true,
    className: "className",
}

export const Secondary = (args) => {
    return <SecondaryTable {...args} />;
};

const additionalProps = [
    {
        id: 1,
        country: "FR",
        lang: "fr"
    },
    {
        id: 2,
        country: "GB",
        lang: "gb"
    },
    {
        id: 3,
        country: "DE",
        lang: "de"
    },
    {
        id: 4,
        country: "RO",
        lang: "ro"
    }
];

Secondary.args = {
    columns: [
        {
            Header: "column 1",
            accessor: "col1",
            style: {
                width: "150px"
            }
        },
        {
            Header: "Column 2",
            accessor: "col2",
            style: {
                width: "150px"
            },
            style: {
                width: "150px"
            }
        },
        {
            Header: "Column 3",
            accessor: "col3",
            style: {
                width: "150px"
            }
        },
        {
            Header: () => null,
            id: 'expander', // 'id' is required
            Cell: ({ row }) => (
              <span {...row.getToggleRowExpandedProps()}>
                {row.isExpanded ? ChevronDown : ChevronUp}
              </span>
            )
        }
    ],
    data: [
        {
            col1: "Id 1 FR",
            col2: "fdfdf",
            col3: "fdfd",
            id: 1
        },
        {
            col1: "ID 2 GB",
            col2: "World",
            col3: "sample",
            id: 2
        },
        {
            col1: "Id 3 DE",
            col2: "World",
            col3: "sample",
            id: 3
        },
        {
            col1: "Id 4 RO",
            col2: "World",
            col3: "sample",
            id: 4
        }
    
    ],
    defaultSort: [
        {
            id: 'col1',
            desc: true
        }
    ],
    renderRowSubComponent: (row) => {
        return (
            <>
            {additionalProps.map(el => el.id === row.original.id && <><td>Country:&nbsp;{el.country}</td><td>Language: {el.lang}</td><td /><td/></>) }
          </>
        );
    }
};
