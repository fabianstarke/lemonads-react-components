import React, { useState, useEffect } from "react";
import TagSlider from "../../src/components/TagSlider";
import Tag from "../../src/components/Tag";
import IconButton from "../../src/components/IconButton";

export default {
    title: "TagSlider",
    component: TagSlider,
};

const labelArray = [
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
    "Label",
];

export const Default = () => {
    const [list, setList] = useState(labelArray);

    const addItem = () => {
        const newList = [...list, "Label"]
        setList(newList)
    }

    const removeItem = (index) => {
        const newList = list.filter((item,i) => i != index)
        setList(newList)
    }

    return (
        <div style={{ width: 1276 }}>
            <div style={{ display: "flex", alignItems: "center", marginBottom: "20px" }}>
                <IconButton type="add" onClick={addItem} />
                <div style={{ marginLeft: 20}}>Add Items to see the arrows appear</div>
            </div>
            <TagSlider
                children={list.map((label, i) => (
                    <Tag close key={i} onClick={() => removeItem(i)}label={label} />
                ))}
            />
        </div>
    );
};

Default.parameters = {
    info: { inline: true }
};
