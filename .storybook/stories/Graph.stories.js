import React from "react";
import Graph from "../../src/components/Graph";
import { withKnobs, array, number, text } from "@storybook/addon-knobs";

export default {
    title: "Graph",
    component: Graph,
    decorator: [withKnobs]
};


const dataY = [
    { label: 'data 1', labelColor: '', points: [200, 300, 400, 200, 100, 200] },
    { label: 'data 2', labelColor: 'success', points: [300, 200, 400, 200, 500, 600] }
];

const dataX = [1, 2, 3, 4, 5, 6];


export const All = () => {
    const onChange = (isToggled) => { console.log("Changed to " + isToggled) };
    return (
        <div>
            <h1>Graph Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
            <div style={{width:'400px'}}> {/** By default the graph will adapt to the container size */}
                    <Graph
                        dataY={dataY}
                        dataX={dataX}
                        />
                </div>
                <div style={{width:'400px'}}> {/** But a fixed width can be specified */}
                    <Graph
                        width={350}
                        />
                </div>
            </div>
        </div>
    );
};

export const Default = (args) => {
    let dataY = JSON.stringify(args.dataY);

    const htmlDecode = input => {
        var e = document.createElement('textarea');
        e.innerHTML = input;
        // handle case of empty input
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
      }
      
    return (
        <Graph
            dataY={JSON.parse(htmlDecode(dataY))}
            dataX={args.dataX}
            height={args.height}
        />);
};

Default.args = {
    height: 164,
    dataY: dataY,
    dataX: dataX
}
