import React from "react";
import ScrollBar from "../../src/components/ScrollBar";

export default {
    title: "ScrollBar",
    component: ScrollBar,
};

export const All = () => {
    return (
        <div>
            <h1>ScrollBar Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <ScrollBar>
                    <ul
                        style={{
                            height: "80px",
                            width: "50px",
                            whiteSpace: "nowrap"
                        }}
                    >
                        <li>Lorem ipsum</li>
                        <li>dolor sit</li>
                        <li>amet, consectetur</li>
                        <li>adipiscing elit,</li>
                        <li>sed do</li>
                        <li>eiusmod tempor</li>
                        <li>incididunt ut</li>
                    </ul>
                </ScrollBar>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <div style={{ display: "flex", justifyContent: "space-around" }}>
            <ScrollBar {...args}>
                <ul
                    style={{
                        height: "80px",
                        width: "50px",
                        whiteSpace: "nowrap"
                    }}
                >
                    <li>Lorem ipsum</li>
                    <li>dolor sit</li>
                    <li>amet, consectetur</li>
                    <li>adipiscing elit,</li>
                    <li>sed do</li>
                    <li>eiusmod tempor</li>
                    <li>incididunt ut</li>
                </ul>
            </ScrollBar>
        </div>
    );
};

export const Skeleton = () => {
    return (
            <ScrollBar loading skeletonHeight="80px" skeletonWidth="50px" animationDuration="10s" />
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    id: "myid",
    className: "className",
}
