import React from "react";
import Alert from "../../src/components/Alert";

export default {
    title: "Alert",
    component: Alert,
};

export const All = () => {
    return (
        <div>
            <h1>Alert Guideline</h1>
            <div>
                <Alert
                    message="Info Alert"
                    type="info"
                    remove={console.log("Removed")}
                />
                <Alert message="Success Alert" type="success" />
                <Alert message="Warning Alert" type="warning" timeout={4} />
                <Alert message="Error Alert" type="error" />
            </div>
        </div>
    );
};

export const Default = (args) => (
    <Alert {...args} />
);

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    message: "Default Message",
    type: "info",
    timeout: 4,
    showClose: true,
};

Default.argTypes = {
    type: { control: { type: 'radio', options: {
        Info: "info",
        Success: "success",
        Error: "error",
        Warrning: "warning"
    } } },
    remove: { action: 'remove alert' },
};
