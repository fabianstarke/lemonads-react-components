import React from "react";
import Close from "../../src/animations/Close";
import Download from "../../src/animations/Download";
import Upload from "../../src/animations/Upload";
import ArrowRight from "../../src/animations/ArrowRight";
import Christmas from "../../src/animations/Christmas";
import {
    ErrorAnim,
    ValidAnim,
    InfoAnim,
    WarningAnim
} from "../../src/animations/Notifications";

export default {
    title: "Animations",
    component: All
};

export const All = () => {
    return (
        <div>
            <h1>Animations Guideline</h1>
            <div className="icon-container">
                <div className="icon">
                    <h3>Close</h3>
                    <Close />
                </div>
                <div className="icon">
                    <h3>Download</h3>
                    <Download />
                </div>
                <div className="icon">
                    <h3>Upload</h3>
                    <Upload />
                </div>
                <div className="icon">
                    <h3>Arrow Right</h3>
                    <ArrowRight />
                </div>
            </div>
        </div>
    );
};

export const Notifications = () => {
    return (
        <div>
            <h1>Notifications</h1>
            <div style={{ position: "relative" }}>
                <h3>Error</h3>
                <ErrorAnim />
            </div>
            <div style={{ position: "relative" }}>
                <h3>Success</h3>
                <ValidAnim />
            </div>
            <div style={{ position: "relative" }}>
                <h3>Info</h3>
                <InfoAnim />
            </div>
            <div style={{ position: "relative" }}>
                <h3>Warning</h3>
                <WarningAnim />
            </div>
        </div>
    );
};
export const Special = () => {
    return (
        <div>
            <h1>Christmas animation</h1>
            <div className="icon-container">
                <div className="icon">
                    <Christmas />
                </div>
            </div>
        </div>
    );
};
