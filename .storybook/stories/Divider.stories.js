import React from "react";
import Divider from "../../src/components/Divider";

export default {
    title: "Divider",
    component: Divider,
};

export const All = () => {
    return (
        <div>
            <h1>Divider Guideline</h1>
            <div style={{ width: "300px" }}>
                <p>Divider :</p>
                <Divider />
            </div>
        </div>
    );
};

export const Default = (args) => (
    <Divider {...args} />
);

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    marginTop: "30px",
    marginBottom: "30px",
}
