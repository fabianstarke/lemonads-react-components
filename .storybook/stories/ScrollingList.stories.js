import React from "react";
import ScrollingList from "../../src/components/ScrollingList";

export default {
    title: "ScrollingList",
    component: ScrollingList,
};
1
export const All = () => {
    return (
        <div style={{height:"500px"}}>
            <ScrollingList width="600px">
                {
                    [0,1,2,3,4,5,6,7,8].map((el,i)=>{
                    return (<div key={i}style={{background:"white",width:"200px",padding:"100px"}}>
                            Hello{" "+ el}
                        </div>)
                    })
                }
            </ScrollingList>
        </div>
    );
};

export const Default = () => {
    return (
        <ScrollingList width="600px">
            {
                [0,1,2,3,4,5,6,7,8].map((el,i)=>{
                return (<div key={i}style={{background:"white",width:"200px",padding:"100px"}}>
                        Hello {" "+ el}
                    </div>)
                })
            }
        </ScrollingList>
    );
};


export const Skeleton = () => {
    return (
        <ScrollingList width="100%" loading skeletonHeight="253px">
            {
                [0,1,2,3,4,5,6,7,8].map((el,i)=>{
                return (<div key={i}style={{background:"white",width:"200px",padding:"100px"}}>
                        Hello {" "+ el}
                    </div>)
                })
            }
        </ScrollingList>
    );
};

Default.parameters = {
    info: { inline: true}
};
  