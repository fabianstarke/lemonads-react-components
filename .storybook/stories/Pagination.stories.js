import React from "react";
import Pagination from "../../src/components/Pagination";
import { withKnobs, boolean, select, text } from "@storybook/addon-knobs";

export default {
    title: "Pagination",
    component: Pagination,
    decorator: [withKnobs]
};

export const All = () => {
    const onChange = (isToggled) => { console.log("Changed to " + isToggled) };
    return (
        <div>
            <h1>Pagination Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Pagination
                    minRows={0}
                    defaultPageSize={5}
                    pageSizeOptions={[5, 25, 50, 100, 200, 500]}
                    pages={15}
                    page={1} />
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <Pagination {...args} />
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    minRows: 0,
    defaultPageSize: 5,
    pageSizeOptions: [5, 25, 50, 100, 200, 500],
    pages: 15,
    page: 1,
}
