import React from 'react';
import { Radio, RadioGroup } from '../../src/components/Radio'


export default {
  title: 'Radio',
  component: Radio,
};


export const All = () => {
  return (
    <div>
      <h1>Radio Guideline</h1>
      <div style={{ display: "flex", justifyContent: "space-around", width: "100%"}}>
        <RadioGroup label="Some label" name="myoptions" onChange={(e)=>console.log("Custom Group",e.target.value)} allowUnselected={true}>
            <Radio value="1" label="First"/>
            <Radio value="2" label="Second"/>
        </RadioGroup>
        <RadioGroup label="Disabled" disabled={true} selected="1" name="myoptions2" onChange={(e)=>console.log("Custom Group",e.target.value)} allowUnselected={true}>
            <Radio value="1" label="First"/>
            <Radio value="2" label="Second"/>
        </RadioGroup>
        <RadioGroup label="Only one radio disabled" selected="2" name="myoptions3" onChange={(e)=>console.log("Custom Group",e.target.value)} allowUnselected={true}>
            <Radio value="1" label="First disabled" disabled={true} />
            <Radio value="2" label="Second enabled"/>
        </RadioGroup>
      </div>
    </div>
  )
}

export const Default = (args) =>
    <RadioGroup name="myoptions" {...args}>
        <Radio value="1" label="First"/>
        <Radio value="2" label="Second"/>
    </RadioGroup>;


Default.parameters = {
    info: { inline: true}
};

Default.args = {
    label: "My Label",
    animated: false,
    allowUnselected: false,
    disabled: false,
}
