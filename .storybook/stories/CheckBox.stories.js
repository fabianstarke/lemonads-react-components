import React, { useEffect, useState } from 'react';
import CheckBox from "../../src/components/CheckBox";

export default {
  title: 'CheckBox',
  component: CheckBox,
};


export const All = () => {
  return (
    <div>
      <h1>CheckBox Guideline</h1>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <CheckBox label="Checkbox" checked />
        <CheckBox label="Checkbox empty" />
        <CheckBox labelRight="Label on the Right" checked/>
      </div>
    </div>

  )
}

export const Default = (args) => {
  const [check, setChecked] = useState(false);
    
  const handleChange = () => {
      setChecked(!check)
  }

  useEffect(() => console.log(check))
  return (
    <CheckBox
    {...args}
    checked={check}
    onChange={handleChange}
  />

  )
}

Default.parameters = {
    info: { inline: true}
};

Default.args = {
    label : "Label",
    labelRight : "Label Right",
    id : "my-id",
}

Default.argTypes = {
    onClick: { action: 'checked' },
};

export const LabelRight = (args) =>
  <CheckBox {...args} />;

LabelRight.parameters = {
    info: { inline: true}
};

LabelRight.args = {
    labelRight : "Label Right",
    id : "my-id",
}

LabelRight.argTypes = {
    onClick: { action: 'checked' },
};
