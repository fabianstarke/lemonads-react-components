import React from 'react';
import SelectedIndicator from "../../src/components/SelectedIndicator";

export default {
  title: 'SelectedIndicator',
  component: SelectedIndicator,
};


export const All = () => {
  return (
    <div>
      <h1>SelectedIndicator Guideline</h1>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <SelectedIndicator label="Selected Indicator" selected />
        <SelectedIndicator label="Selected Indicator empty" />
        <SelectedIndicator label="Selected Indicator readOnly" readOnly/>
      </div>
    </div>

  )
}

export const Default = (args) =>
  <SelectedIndicator {...args} />;

Default.parameters = {
    info: { inline: true}
};

Default.args = {
    label: "Label",
    selected: false,
    readOnly: false,
}
