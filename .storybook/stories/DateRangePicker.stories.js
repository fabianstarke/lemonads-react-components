import React from "react";
import DateRangePicker from "../../src/components/DateRangePicker";
import {TODAY, YESTERDAY} from "../../src/common/variables";
import {getPreviousMonth} from "../../src/common/helpers";

export default {
    title: "DateRangePicker",
    component: DateRangePicker,
};

const from = new Date(YESTERDAY);
const to = new Date(TODAY)

export const All = () => {
    return (
        <div>
            <h1>DateRangePicker Guideline</h1>
            <div style={{ float:'right' }}>
                <DateRangePicker 
                    from = {from}
                    to = {to}
                    onChange = {(startDate, endDate)=>{console.log("Modified dates: ", startDate, endDate)}}
                    onCancel = {console.log("Canceled")}
                    handleCustomPeriodClick={((item, index) => console.log("Clicked custom period", item))}
                    dayPickerProps = {{
                        month: new Date(getPreviousMonth(from))
                    }}
                />
            </div>
        </div>
    );
};

export const Default = () => {
    return (
        <DateRangePicker 
            from = {from}
            to = {to}
            onChange = {(startDate, endDate)=>{console.log("Modified dates: ", startDate, endDate)}}
            onCancel = {console.log("Canceled")}
            handleCustomPeriodClick={((item, index) => console.log("Clicked custom period", item))}
            dayPickerProps = {{
                month: new Date(getPreviousMonth(from))
            }}
        />
    );
};

Default.parameters = {
    info: { inline: true}
};
  