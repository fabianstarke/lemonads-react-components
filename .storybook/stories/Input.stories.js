import React from "react";
import Input from "../../src/components/Input";
import SearchBar from "../../src/components/SearchBar";
import InputNumber from "../../src/components/InputNumber";
import { CopyLink } from "../../src/images/icons/baseIcons";

export default {
    title: "Input",
    component: Input,
};

export const All = () => {
    return (
        <div>
            <h1>Input Guideline</h1>
            <h2>Default Inputs</h2>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Input label="Default Input" placeholder="Default" />
                <Input label="Focused Input" focus placeholder="Focused" />
                <Input
                    label="Success Input"
                    mode="success"
                    placeholder="Success"
                />
                <Input label="Error Input" mode="error" placeholder="Error" />
                <Input label="Required Input" placeholder="Required" required />
                <Input label="Disabled Input" placeholder="Disabled" disabled />
                <Input label="Colored border" placeholder="pink" borderColor="pink"/>

            </div>
            <h2>Search input</h2>
            <SearchBar placeholder="Search Input" />
            <h2>With Icon</h2>
            <Input label="With Icon" placeholder="With Icon" icon={CopyLink} iconTooltip="Copy Link" />


        </div>
    );
};

export const SearchInput = (args) => (
    <SearchBar {...args} />
);

SearchInput.parameters = {
    info: { inline: true }
};

SearchInput.args = {
    placeholder: "Search input",
    focus: false,
    onChange: (e) => console.log(e.target.value),
}

export const Default = (args) => (
    <Input {...args} />
);

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    id: "",
    label: "Label",
    placeholder: "input",
    mode: "",
    required: false,
    disabled: false,
    focus: false,
    onChange: (e) => console.log(e.target.value),
    full: false,
    tooltip: "",
    htmlFor: null,
};
Default.argTypes = {
    mode: { control: { type: 'radio', options: ["", "success", "error"] } }
};

export const Number = (args) => (
    <div>
    <InputNumber {...args} />
    </div>
);

Number.parameters = {
    info: { inline: true }
};

Number.args = {
    id: "",
    label: "My Label",
    placeholder: "0",
    required: false,
    disabled: false,
    focus: false,
    onChange: (e) => console.log(e.target.value, typeof e.target.value),
    full: false,
    min: 0,
    max: 100,
    step:10,
};
