import React from "react";
import SelectMode from "../../src/components/SelectMode";

export default {
    title: "SelectMode",
    component: SelectMode,
};

export const All = () => {
    return (
        <div>
            <h1>SelectMode Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <SelectMode
                    iconList={[
                        { iconType: "menu", iconName: "DashBoard" },
                        { iconType: "base", iconName: "BulletList" }
                    ]}
                    onSelectItem={() => console.log("clicked")}
                />
            </div>
        </div>
    );
};

export const Default = (args) => (
    <SelectMode {...args} />
);

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    className: "className",
    defaultActiveIndex: 0,
    iconList: [
        { iconType: "menu", iconName: "DashBoard" },
        { iconType: "base", iconName: "BulletList" }
    ],
}

Default.argTypes = {
    onSelectItem: { action: 'on-clicked' },
};
