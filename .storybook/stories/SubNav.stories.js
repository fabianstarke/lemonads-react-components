import React from "react";
import SubNav from "../../src/components/SubNav";
import SubNavRoute from "../../src/components/SubNavRoute";
import { MemoryRouter } from "react-router-dom";

export default {
    title: "SubNav",
    component: SubNav,
};

export const All = () => {
    return (
        <div>
            <h1>SubNav Guideline</h1>
            <div
                style={{
                    display: "flex",
                    justifyContent: "space-around",
                    width: "100%"
                }}
            >
                <div>
                    Default <SubNav />
                </div>

                <div>
                    With react-router
                    <MemoryRouter>
                        <SubNavRoute />
                    </MemoryRouter>
                </div>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return <SubNav {...args} />;
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    labels: ["subnav1", "subnav2", "subnav3"],
    className: "className",
    defaultActiveIndex: 1,
};

export const Route = (args) => {
    return (
        <MemoryRouter>
            <SubNavRoute {...args} />
        </MemoryRouter>
    );
};

Route.parameters = {
    info: { inline: true }
};

Route.args = {
    labels: [
        { title: "Profile", pathname: "/profile" },
        { title: "Business", pathname: "/profile/business" },
        { title: "User", pathname: "/profile/users" }
    ],
    location: "/profile/business",
    animated: true,
    className: "className",
};
