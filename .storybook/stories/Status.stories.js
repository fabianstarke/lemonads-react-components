import React from "react";
import Status from "../../src/components/Status";

export default {
    title: "Status",
    component: Status,
};

export const All = () => {
    return (
        <div>
            <h1>Status Guideline</h1>
            <div
                style={{
                    display: "flex",
                    justifyContent: "space-around",
                    position: "relative"
                }}
            >
                <Status label="Success" type="success" />
                <Status label="Error" type="error" />
                <Status label="Warning" type="warning" />
                <Status label="Info" type="info" />
            </div>
        </div>
    );
};

export const Default = (args) =>
  <Status {...args} />;


Default.parameters = {
    info: { inline: true }
};

Default.args = {
    label: "My Label",
}

Default.argTypes = {
    type: { control: { type: 'radio', options: {
        Info: "info",
        Success: "success",
        Error: "error",
        Warrning: "warning"
    } } },
};
