import React from "react";
import CodeBlock from "../../src/components/CodeBlock";

export default {
    title: "Code Block",
    component: CodeBlock,
};

const MyScript = `<script type="text/javascript">
window.lemonads.conversion({
    "email_hash" : "",
    "email" : "",
    "phone_hash" : "",
    "phone" : "",
    "category" : ""
});
</script>`


export const All = () => {
    return (
        <CodeBlock
            script={MyScript} 
        />
    );
};

export const Default = () => {
    return (
        <div style={{ position: "relative" }}>
            <CodeBlock onClick={() => alert('click')} script={MyScript} />
        </div>
    );
};

Default.parameters = {
    info: { inline: true },
};

Default.args = {
    label: "Code Block",
    onClick: { action: 'click' },

};
