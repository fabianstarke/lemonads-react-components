import React from "react";
import "../index.css";

export default {
    title: "Text Styles",
    component: TextStyles
};

export const TextStyles = () => {
    return (
        <div>
            <p className="sections">HEADINGS</p>
            <div className="m-b-40 container">
                <div className="p-20">
                    <p className="hero">HERO - ExtraBold - 36px - 42lh</p>
                    <h1>H1 - SemiBold - 24px - 28lh</h1>
                    <h2>H2 - SemiBold - 20px - 26lh</h2>
                    <h3>H3 - SemiBold - 16px - 20lh</h3>
                </div>
                <div className="background-primary ">
                    <p className="hero light">HERO - ExtraBold - 36px - 42lh</p>
                    <h1 className="light">H1 - SemiBold - 24px - 28lh</h1>
                    <h2 className="light">H2 - SemiBold - 20px - 26lh</h2>
                    <h3 className="light">H3 - SemiBold - 16px - 20lh</h3>
                </div>
            </div>
            <p className="sections">PARAGRAPH</p>
            <div className="m-b-40">
                <div className="p-20 container">
                    <div>
                        <p className="bold">Paragraphe - Bold - 14px - 19lh</p>
                        <p>Paragraphe - Regular - 14px - 19lh</p>
                        <p className="italic">
                            Paragraphe - italic - 14px - 19lh
                        </p>
                        <p className="small">
                            {" "}
                            Paragraphe - Regular - 12px - 19lh
                        </p>
                    </div>
                    <div>
                        <p className="note">Note - Regular - 12px - 14lh</p>
                        <p className="note note-grey">
                            Note - Regular - 12px - 14lh
                        </p>
                    </div>
                    <div>
                        <p>Texte simple - 14px - 19lh</p>
                        <p className="description">Descriion - 10 - 14lh</p>
                    </div>
                </div>
                <div className="p-20 container background-primary">
                    <div>
                        <p className="bold light">
                            Paragraphe - Bold - 14 - 19lh
                        </p>
                        <p className="light">
                            Paragraphe - Regular - 14 - 19lh
                        </p>
                        <p className="italic light">
                            Paragraphe - italic - 14 - 19lh
                        </p>
                        <p className="small light">
                            {" "}
                            Paragraphe - Regular - 12px - 19lh
                        </p>
                    </div>
                    <div>
                        <p className="note light">
                            Note - Regular - 12px - 14lh
                        </p>
                        <p className="note note-grey light">
                            Note - Regular - 12px - 14lh
                        </p>
                    </div>
                    <div>
                        <p className="light">Texte simple - 14px - 19lh</p>
                        <p className="descripxion light">
                            Descripxion - 10px - 14lh
                        </p>
                    </div>
                </div>
            </div>
            <p className="sections">LINK</p>
            <div className="m-b-40 container">
                <div className="p-20 container-column">
                    <a>Link - Regular - 14px - 22lh</a>
                    <a className="small">Link - Regular - 12px - 22lh</a>
                </div>
                <div className="p-20 container-column background-primary">
                    <a className="light">Link - Regular - 14px - 22lh</a>
                    <a className="small light">Link - Regular - 12px - 22lh</a>
                </div>
            </div>
            <p className="sections">ACTION</p>
            <div className="m-b-40 container">
                <div className="p-20">
                    <p className="action">Action - Regular - 14px - 19lh</p>
                </div>
                <div className="p-20 background-primary">
                    <p className="action light">
                        Action - Regular - 14px - 19lh
                    </p>{" "}
                </div>
            </div>
            <p className="sections">LABEL</p>
            <div className="m-b-40">
                <div className="p-20 m-l-40">
                    <p className="label">Label - Regular - 14px - 19lh</p>
                </div>
            </div>
            <p className="sections">COMPONENT</p>
            <div className="m-b-40 container">
                <div className="p-20">
                    <p className="button-secondary">Button/Secondary - regular - 14px - 8lh</p>
                    <p className="button-tertiary">Button/Tertiary - regular - 14px - 8lh</p>
                    <p className="number">Number - Regular - 24px - 33lh</p>
                </div>
                <div className="p-20">
                    <p className="name-offer">Name offer - Bold - 12px - 17lh</p>
                </div>
                <div className="p-20">
                    <p className="input">Input - Regular - 12px - 17lh</p>
                </div>
            </div>
            <div className="m-b-40 container background-primary">
                <div className="p-20">
                    <p className="button-secondary light">Button/Secondary - regular - 14px - 8lh</p>
                    <p className="button-tertiary light">Button/Tertiary - regular - 14px - 8lh</p>
                    <p className="number light">Number - Regular - 24px - 33lh</p>
                </div>
                <div className="p-20">
                    <p className="name-offer light">Name offer - Bold - 12px - 17lh</p>
                </div>
                <div className="p-20">
                    <p className="input light">Input - Regular - 12px - 17lh</p>
                </div>
            </div>
            <p className="sections">NOTE</p>
            <div className="m-b-40">
                <div className="p-20 m-l-40">
                    <p className="note note-third">Note devs - Regular - 14px - 19lh</p>
                </div>
            </div>
        </div>
    );
};
