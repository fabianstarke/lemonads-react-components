import React from "react";
import Switch from "../../src/components/Switch";

export default {
    title: "Switch",
    component: Switch,
};

export const All = () => {
    const onChange = (isToggled)=>{console.log("Changed to "+isToggled)};
    return (
        <div>
            <h1>Switch Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Switch On onChange={onChange}/>
                <Switch Off onChange={onChange}/>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <Switch {...args} />
    );
};

Default.parameters = {
    info: { inline: true}
};

Default.args = {
    On: true,
    Off: false,
    label: "Switch label",
    className: "className",
}
