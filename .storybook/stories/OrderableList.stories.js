import React from "react";
import OrderableList from "../../src/components/OrderableList";

export default {
    title: "OrderableList",
    component: OrderableList,
};

export const All = () => {
    return (
        <div>
            <h1>OrderableList Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <OrderableList order={[0, 2, 3, 1]} onChange={m => console.log("Changed order: " + m)}>
                    {["👻 Ghost", "🦄 Unicorn", "🥑 Avocado", "❤️ Hearth"].map((el,i) => (
                        <div key={i}style={{padding:'5px', border:'1px solid black', margin:'5px'}}>{el}</div>
                    ))}
                </OrderableList>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <OrderableList order={args.order} onChange={m => console.log("Changed order: " + m)}>
            {["👻 Ghost", "🦄 Unicorn", "🥑 Avocado", "❤️ Hearth"].map((el,i) => (
                <div key={i} style={{padding:'5px', border:'1px solid black', margin:'5px'}}>{el}</div>
            ))}
        </OrderableList>
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    order: [0, 2, 3, 1],
};
