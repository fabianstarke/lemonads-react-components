import React from "react";
import Card from "../../src/components/Card";

export default {
    title: "Card",
    component: Card,
};

export const All = () => {
    return (
        <div>
            <h1>Card Guideline</h1>
            <div style={{ width: "300px" }}>
                {[0,1,2,3,4,5].map((x,i)=><Card key={i}>Hello</Card>)}
            </div>
        </div>
    );
};

export const Default = () => {
    return <Card>Hello</Card>;
};

export const Skeleton = () => {
    return <Card skeletonWidth="50px" animationDuration="10s"skeletonHeight="50px"loading>Hello</Card>;
};

Default.parameters = {
    info: { inline: true }
};
