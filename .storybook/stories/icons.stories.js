import React from "react";
import baseIcons from "../../src/images/icons/baseIcons";
import menuIcons from "../../src/images/icons/menuIcons";
import socialIcons from "../../src/images/icons/socialIcons";
import whiteIcons from "../../src/images/icons/whiteIcons";
import illustrations from "../../src/images/illustrations";

export default {
    title: "Icons & Illustrations",
    component: All
};

export const All = () => {
    return (
        <>
            <BaseIcons />
            <MenuIcons />
            <SocialIcons />
            <WhiteIcons />
            <Illustrations />
        </>
    );
};

export const BaseIcons = () => {
    return (
        <>
            <h1>Base Icons</h1>
            <div className="icon-container">
                {Object.entries(baseIcons).map(icon => (
                    <div className="icon" key={icon[0]}>
                        <p>{icon[0]}</p>
                        <p>{icon[1]}</p>
                    </div>
                ))}
            </div>
        </>
    );
};

export const MenuIcons = () => {
    return (
        <>
            <h1>Menu Icons</h1>
            <div className="icon-container">
                {Object.entries(menuIcons).map(icon => (
                    <div className="icon" key={icon[0]}>
                        <p>{icon[0]}</p>
                        <p>{icon[1]}</p>
                    </div>
                ))}
            </div>
        </>
    );
};

export const SocialIcons = () => {
    return (
        <>
            <h1>Social Icons</h1>
            <div className="icon-container">
                {Object.entries(socialIcons).map(icon => (
                    <div className="icon" key={icon[0]}>
                        <p>
                            {icon[0]}
                        </p>
                        <p>{icon[1]}</p>
                    </div>
                ))}
            </div>
        </>
    );
};

export const WhiteIcons = () => {
    return (
        <>
            <h1>White Icons</h1>
            <div className="icon-container dark-background">
                {Object.entries(whiteIcons).map(icon => (
                    <div className="icon light" key={icon[0]}>
                        <p>
                            {icon[0]}
                        </p>
                        <p>{icon[1]}</p>
                    </div>
                ))}
            </div>
        </>
    );
};

export const Illustrations = () => {
    return (
        <>
            <h1>Illustrations</h1>
            <div className="icon-container">
                {Object.entries(illustrations).map(illustration => (
                    <div className="icon" key={illustration[0]}>
                        <p>
                            {illustration[0]}
                        </p>
                        <p>{illustration[1]}</p>
                    </div>
                ))}
            </div>
        </>
    );
};
