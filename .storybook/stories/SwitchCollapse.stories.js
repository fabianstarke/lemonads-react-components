import React from "react";
import SwitchCollapse from "../../src/components/SwitchCollapse";
import baseIcons from "../../src/images/icons/baseIcons";

export default {
    title: "SwitchCollapse",
    component: SwitchCollapse,
};

export const All = () => {
    return (
        <div>
            <h1>SwitchCollapse Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <SwitchCollapse Closed text="Some Text" icon={baseIcons.MessageBubble} textOpen="Other Text" />
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <SwitchCollapse {...args} />
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    Closed: true,
    Open: false,
    text: "SwitchCollapse label",
}
