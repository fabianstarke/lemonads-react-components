import React from "react";
import Badge from "../../src/components/Badge";

export default {
    title: "Badge",
    component: Badge,
};

export const All = () => {
    return (
        <div>
            <h1>Badge Guideline</h1>
            <div
                style={{
                    display: "flex",
                    justifyContent: "space-around",
                    position: "relative"
                }}
            >
                <Badge label="Badge" />
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <div style={{ position: "relative" }}>
            <Badge {...args} />
        </div>
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    label: "Badge",
    className: "className",
}
