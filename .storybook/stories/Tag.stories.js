import React from "react";
import Tag from "../../src/components/Tag";

export default {
    title: "Tag",
    component: Tag,
};

export const All = () => {
    return (
        <div>
            <h1>Tag Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <div>
                    <h2>Tag Off</h2>
                    <Tag label="Tag" />
                </div>
                <div>
                    <h2>Tag On</h2>
                    <Tag label="Tag" selected />
                </div>
                <div>
                    <h2>Tag On Supp</h2>
                    <Tag label="Tag" close onClick={() => console.log('click')} selected/>
                </div>
                <div>
                    <h2>Tag Supp</h2>
                    <Tag label="Tag" close onClick={() => console.log('click')}/>
                </div>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <Tag {...args} />
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    label: "Tags",
    selected: false,
    close: false,
}
