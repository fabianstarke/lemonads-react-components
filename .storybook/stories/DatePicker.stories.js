import React from "react";
import DatePicker from "../../src/components/DatePicker";
import {TODAY} from "../../src/common/variables";

export default {
    title: "DatePicker",
    component: DatePicker,
};

const today = new Date(TODAY)

export const All = () => {
    return (
        <div>
            <h1>DatePicker Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <DatePicker dateFormat="YYYY-MM-DD"
                    onChange={(date) => {
                        console.log(date)
                    }}
                    maxDate={new Date()}
                    date={today}
                    dayPickerProps={{
                        disabledDays: {
                            after: new Date()
                        },
                    }}
                />
            </div>
        </div>
    );
};

export const Default = () => {
    return (
        <DatePicker dateFormat="YYYY-MM-DD"
            onChange={(date) => {
                console.log(date)
            }}
            date={today}
        />
    );
};

Default.parameters = {
    info: { inline: true}
};
  