import React from 'react';
import Select from '../../src/components/Select';
import ReactSelect from '../../src/components/ReactSelect';
import OpenSelect from '../../src/components/OpenSelect';
import { Calendar } from "../../src/images/icons/baseIcons";
import MultiLevelSelect from "../../src/components/MultiLevelSelect";
import { MemoryRouter } from "react-router-dom";


export default {
    title: 'Select',
    component: Select,
};


const options = [
    { name: "first option", value: "first option" },
    { name: "second option", value: "second option" },
    { name: "third option", value: "third option" },
    { name: "fourth option", value: "fourth option" },
]

const defaultOption = { name: "default", value: "value" }


export const All = () => {
    return (
        <div>
            <h1>Select Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Select label="Default Select" options={options} defaultOption={defaultOption} />
                <Select label="Secondary Select" options={options} secondary />
            </div>
            <div style={{ display: "flex", justifyContent: "space-around", marginTop: '20px' }}>
                <Select label="Default with Icon Select" options={options} defaultOption={defaultOption} icon={Calendar} />
                <Select label="Default Select Disabled" disabled={true} options={options} defaultOption={defaultOption} />
            </div>
        </div>

    )
}

export const Default = (args) => {
    return <Select {...args} />;
}

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    options: options,
    label: "Default Label",
    secondary: false,
    defaultOption: defaultOption,
    full: true,
    width: "",
}

Default.argTypes = {
    onChange: { action: 'on-change' },
};

export const Open = (args) => {
    return <OpenSelect {...args} />
}

Open.args = {
    options: [
        { label: 'first option', value: 0, icon: "test" }, 
        { label: 'second option', value: 1 }, 
        { label: 'third option', value: 2 }, 
        { label: '4th option', value: 3 }, 
        { label: '5th option', value: 4 }, 
        { label: '6th option', value: 5 }
    ],
    selected: 4,
}

export const ReactComponent = (args) => {
    return <MemoryRouter><ReactSelect {...args} /></MemoryRouter>;
}

ReactComponent.args = {
    options: [
        { label: 'first option', value: 0, icon: "test" },
        { label: 'second option', value: 1, isFocused: true },
        { label: 'third option', value: 2 },
        { label: '4th option', value: 3 },
        { label: '5th option', value: 4 },
        { label: '6th option', value: 5 }
    ],
    label: "Default Label",
    full: true,
    width: null,
    responsiveMenu: true,
    customMenuListLabel: "Create a promotional space",
    customMenuListLink: "/promotional-spaces"
}

ReactComponent.argTypes = {
    onChange: { action: 'on-change' },
};

export const MultiLevelSelectComponent = (args) => {
    return <MultiLevelSelect {...args} />;
}

MultiLevelSelectComponent.args = {
    options: [
        { label: "first option", value: 0 },
        {
            label: "second option",
            value: 1,
            children: [
                {
                    label: "first child",
                    value: 10,
                    children: [
                        { label: "first grandchild", value: 100 },
                        { label: "second grandchild", value: 101 },
                    ],
                },
                { label: "second child", value: 11 },
            ],
        },
        {
            label: "third option long name long long long",
            value: 1,
            children: [
                { label: "first child", value: 20 },
                { label: "second child", value: 21 },
            ],
        },
        { label: "4th option", value: 3 },
        { label: "5th option", value: 4 },
        { label: "6th option", value: 5 },
    ],
    onOpen: () => {
        console.log("open");
    },
    label: "Multi level select",
    values: [3, 101],
    handleCheckboxChange: () => {
        console.log("change");
    },
};
