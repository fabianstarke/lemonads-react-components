import React from "react";
import PageHelpBox from "../../src/components/PageHelpBox";
import illustrations from "../../src/images/illustrations";

export default {
    title: "PageHelpBox",
    component: PageHelpBox,
};

export const All = () => {
    const onChange = (isToggled)=>{console.log("Changed to "+isToggled)};
    return (
        <div>
            <h1>PageHelpBox Guideline</h1>
            <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}>
                <PageHelpBox  title="All Steps completed" icon={illustrations.Loading}description="Congrats, you completed every step of the onboarding and started recording conversions. It's time to dig into the data!" buttonText="Check conversions" buttonLink={null}/>
                <PageHelpBox title="Want a coffee?" description="First, turn on the coffee machine. Then, press the button." buttonText="Yes, I'm the button" buttonLink={null}/>

            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <PageHelpBox {...args} />
    );
};

Default.parameters = {
    info: { inline: true}
};

Default.args = {
    title: "Let's get started ?",
    description: "First, pick one or more offers you would like to promote. After sending some traffic through your tracking links, more data will show up",
    buttonText: "Browse our offers now!",
}
  