import React from "react";
import MultiGraph from "../../src/components/MultiGraph";

export default {
    title: "MultiGraph",
    component: MultiGraph,
};


const data = {
    graph1:{
        title: "Graph 1",
        dataY:[
            { label: 'G1 data 1', labelColor: '', points: [400, 300, 400, 200, 100, 200] },
            { label: ' G1 data 2', labelColor: 'success', points: [300, 200, 400, 200, 500, 600] }
        ],
        dataX:[1, 2, 3, 4, 5, 6]
    },

    graph2:{
        title: "Graph 2",
        dataY:[
            { label: 'G2 data 1', labelColor: '', points: [200, 300, 400, 200, 100, 200] },
            { label: 'G2 data 2', labelColor: 'success', points: [300, 200, 400, 200, 500, 600] }
        ],
        dataX:[1, 2, 3, 4, 5, 6]
    }
}


export const All = () => {
    const onChange = (isToggled) => { console.log("Changed to " + isToggled) };
    return (
        <div>
            <h1>MultiGraph Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around"}}>
                <div> {/**wrapped in div just so the MultiGraph doesn't fit the height of the flex. */}
                    <MultiGraph data={data} graphWidth={300}/> {/**This will make the graph 300. The MultiGraph result will be more width */}
                </div>
                <MultiGraph width={500}/> {/** This will make the MultiGraph width 500, the Graph result will be less  */} 
            </div>
            <div style={{marginTop:'30px'}}>
                <MultiGraph/> {/**This will fit the container width: 100% */}
            </div>
        </div>
    );
};

export const Default = (args) => {
    let knobsData = JSON.stringify(args.data);

    const htmlDecode = input => {
        var e = document.createElement('textarea');
        e.innerHTML = input;
        // handle case of empty input
        return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
      }
      
    return (
        <MultiGraph
            data={knobsData ? JSON.parse(htmlDecode(knobsData)) : {}}
            graphWidth={600}
        />);
};

export const Skeleton = () => {
    return (
        <div>
            <h1>MultiGraph Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around"}}>
                <div> {/**wrapped in div just so the MultiGraph doesn't fit the height of the flex. */}
                    <MultiGraph loading skeletonWidth="340px" skeletonHeight="247px" data={data} graphWidth={300}/> {/**This will make the graph 300. The MultiGraph result will be more width */}
                </div>
                <MultiGraph loading skeletonWidth="500px" skeletonHeight="326px" width={500}/> {/** This will make the MultiGraph width 500, the Graph result will be less  */} 
            </div>
        </div>
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    data: data,
};
