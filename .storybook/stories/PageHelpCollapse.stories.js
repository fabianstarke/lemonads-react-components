import React from "react";
import PageHelpCollapse from "../../src/components/PageHelpCollapse";

export default {
    title: "PageHelpCollapse",
    component: PageHelpCollapse,
};

export const All = () => {
    const onChange = (isToggled)=>{console.log("Changed to "+isToggled)};
    return (
        <div>
            <h1>PageHelpCollapse Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <PageHelpCollapse onChange={onChange}/>
            </div>
        </div>
    );
};

export const Default = () => {

    return (
        <PageHelpCollapse />
    );
};

Default.parameters = {
    info: { inline: true}
};
  