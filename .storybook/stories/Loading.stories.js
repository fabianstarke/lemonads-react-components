import React from "react";
import Loading from "../../src/components/Loading";

export default {
    title: "Loading",
    component: Loading,
};

export const All = () => {
    return (
        <div>
            <h1>Loading Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Loading>
                    <p>Loading component</p>
                </Loading>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <Loading {...args}>
            <p>Loading component</p>
        </Loading>
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    loading: true,
    announcement: false,
};
