import React from "react";
import Tooltip from "../../src/components/Tooltip";

export default {
    title: "Tooltip",
    component: Tooltip,
};

export const Default = (args) => {
    return (
        <div style={{ textAlign: "center" }}>
            <Tooltip {...args} >
                <span>Pass hover to show tooltip</span>
            </Tooltip>
            <Tooltip {...args} >
                <span style={{ paddingLeft: '15px' }}>Pass hover to show tooltip</span>
            </Tooltip>
        </div>
    );
};

Default.parameters = {
    info: { inline: true }
};

Default.args = {
    placement: "top",
    overlay: "My Tooltip",
}

Default.argTypes = {
    placement: { control: { type: 'radio', options: ["top", "right", "bottom", "left"] } },
};
