import React, { useState } from "react";
import Stepper from "../../src/components/Stepper";
import { withKnobs } from "@storybook/addon-knobs";

export default {
    title: "Stepper",
    component: Stepper,
    decorators: [withKnobs]
};

export const Default = () => {
    const [next, setNext] = useState(0);

    const handleSubmit = (e, i) => setNext(i);

    return (
        <div>
            <h1>Stepper Guideline</h1>
            <div
                style={{
                    position: "relative",
                    width: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    margin: "80px auto"
                }}
            >
                <Stepper
                    small
                    currentIndex={next}
                    navItems={[
                        "General Settings",
                        "Payouts & Conversion Flow",
                        "Traffic Restrictions",
                        "Descriptions",
                        "Screenshots",
                        "Creatives",
                        "Confirmation"
                    ]}
                    onChangePeriodIndex={(e, i) => handleSubmit(e, i)}
                />
            </div>
        </div>
    );
};

Default.parameters = {
    info: { inline: true }
};
