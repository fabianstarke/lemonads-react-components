import React from "react";
import Modal from "../../src/components/Modal";

export default {
    title: "Modal",
    component: Modal,
};

export const All = () => {
    return (
        <div>
            <h1>Modal Guideline</h1>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
                <Modal header="This is a title"activableComponent={<button>Open modal</button>}>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.
                    </p>
                </Modal>
            </div>
        </div>
    );
};

export const Default = (args) => {
    return (
        <Modal
            {...args}
            activableComponent={<button>Open modal</button>}
        >
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <div className={args.customCloseClassName}>Close Modal</div>
        </Modal>
    );
};

// disabling addon-info as it throws an error when using React.forwardRef
Default.parameters = {
    info: { disable: true },
};

Default.args = {
    size: "small",
    className: "className",
    customCloseClassName: "customCloseClassName",
    header: "Modal title",
    defaultToggle: false,
    hiddenCross: false,
};

Default.argTypes = {
    size: { control: { type: 'radio', options: ["small", "medium"] } }
};
