module.exports = function (api) {
    api.cache(true);
  
    return {
      presets: [
        '@babel/env',
        '@babel/react',
      ],
      overrides: [
        {
          presets: ['@babel/env'],
          plugins: [
            '@babel/plugin-proposal-object-rest-spread'
          ],
        }
      ]
    };
  };
