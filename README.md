Lemonads React Components
=========================

## Installation

Run `yarn install` or `npm install` at the root of the project, then run `yarn install` or `npm install` in the .storybook folder.

## Run the storybook

In the .storybook folder, run `yarn run storybook` or `npm run storybook`

## Adding a new component to the storybook

If a new component was added and you want to see it in the storybook, then previously add a demo in `./storybook/stories` and then run `yarn run build-storybook` in `./storybook`

## Publish new version

To make available a new component to other react app, you should publish it to the internal npm registry.

First merge only validated commits from the master to the prod branch.

**/!\ For all the following steps, you have to make sure you are on the prod branch, otherwise nonvalidated changes will appear in production.**

Then, [bump the package version](https://docs.npmjs.com/cli/version) in the package.json following [Semantic Versioning](https://semver.org/). You should have commit all your modifications.

```console
npm version (patch, minor or major)
```

>if you haven't done it before, you have to create an user on the npm registry
>
>npm adduser --registry http://192.168.200.31:4873

Launch the publishing

```console
npm publish --registry http://192.168.200.31:4873
```

## Use a component

- To get the last updates on the repository :

install the dependency directly from the master branch

```console
yarn add git+http://gitlab.flex-multimedia.tech/FMM/react-components.git#master
```
or
```console
npm install git+http://gitlab.flex-multimedia.tech/FMM/react-components.git#master
```

- To get the last published version :

install the package
```console
yarn add @lemonads/react-components
```
or
```console
npm install @lemonads/react-components
```

in both cases import the component from the package @lemonads/react-components
```javascript
import React, {Component} from "react";
import { Alert } from "@lemonads/react-components";

class Example extends Component {
    render() {
        return (
            <div>
                <Alert message="Info Alert" type="info" />
            </div>
        )
    }
}
```

## Run tests

In order to run all test together you can run the command 

```
jest --watch
```

this will continuously re-run on every change.
You can add flags to specify of you want to run all tests, only failed tests etc.

In order to only run a specific file you can run the command 

```
jest -- TestFileName.js

```
## Updating Snapshots

if a snapshot test failed due to an intentional change, we need to update the snapshot file

```
jest --updateSnapshot

```

It is also possible to update only a specific snapshot adding a flag to the above command

```
--testNamePattern
```

Finally it is also possible to update snapshots interactively in watch mode with the `i` flag



